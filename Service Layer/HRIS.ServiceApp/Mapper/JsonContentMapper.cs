﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Channels;
using System.Web;

namespace HRIS.ServiceApp.Mapper
{
    public class JsonContentMapper : WebContentTypeMapper
    {
        /// <summary>
        /// Method to override message format for content type in Json services
        /// </summary>
        /// <param name="contentType">content type</param>
        /// <returns>returns json format</returns>
        public override WebContentFormat GetMessageFormatForContentType(string contentType)
        {
            return WebContentFormat.Json;
        }
    }
}