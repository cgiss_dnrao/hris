﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HRIS.Ioc;
using HRIS.Aspects.Factory;

namespace HRIS.ServiceApp
{
    public class ServiceBootstrapper
    {
        public static IIocManager IocEngine { get; private set; }
        public static void Initialize()
        {
            IocEngine = new IocManager(DiscoveryStrategy.SearchBaseDirectory);
            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();
        }
    }
}