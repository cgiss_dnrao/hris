﻿using HRIS.Business.Contract;
using HRIS.Data.Request;
using HRIS.Data.UserData;
using HRIS.Data.MasterData;
using HRIS.ServiceApp.Core;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.WCF;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Collections.Generic;
using System;

namespace HRIS.ServiceApp
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(ConcurrencyMode=ConcurrencyMode.Multiple)]
    [ExceptionShielding("ServiceExceptionPolicy")]
    [Export(typeof(IHRISService))]
    public partial class HRISService : ServiceBase, IHRISService
    {
        [Dependency]
        public IUser UserManager { get; set; }

        [Dependency]

        public IEmailManager EmailManager { get; set; }

        [Dependency]
        public IMasterDataManager MasterDataManager { get; set; }

        public UserMasterResponseData GetUserData(string userName)
        {
            return UserManager.GetUserData(userName);
        }

        public IList<HRISMasterData> GetHRISMasterDataList(bool isEXECOMData)
        {
            return MasterDataManager.GetHRISMasterDataList(isEXECOMData);
        }

        public EmployeeMasterData GetHRISMasterDataListWisePage(EmployeeRequest employeeRequest)
        {
            return MasterDataManager.GetHRISMasterDataListPageWise(employeeRequest);
        }

        public IList<CostCenterMasterData> GetCostCenterMasterDataList(string costCenter)
        {
            return MasterDataManager.GetCostCenterMasterDataList(costCenter);
        }

        public IList<ProfitCenterMasterData> GetProfitCenterMasterDataList(string profitCenter)
        {
            return MasterDataManager.GetProfitCenterMasterDataList(profitCenter);
        }

      

        public string SaveEmployeeDataList(IList<HRISMasterData> model, bool changeStatus = false)
        {
            return MasterDataManager.SaveHRISMasterDataList(model, changeStatus);
        }

        public string SaveProfitCenterDataList(ProfitCenterMasterData model)
        {
            return MasterDataManager.SaveProfitCenterDataList(model);
        }

        public string SaveCostCenterDataList(CostCenterMasterData model)
        {
            return MasterDataManager.SaveCostCenterDataList(model);
        }
        public string SaveLeaderShipRoleDataList(LeaderShipRoleMasterData model)
        {
            return MasterDataManager.SaveLeaderShipRoleDataList(model);
        }

        public IList<LeaderShipRoleMasterData> GetLeaderShipRoleMasterDataList(string leaderShipRole)
        {
            return MasterDataManager.GetLeaderShipRoleMasterDataList(leaderShipRole);
        }

        public IList<TransferLeaderShipData> GetTransferLeaderShipDataList()
        {
            return MasterDataManager.GetTransferLeaderShipDataList();
        }

        public string SaveSiteSubSiteMapping(List<SiteSubSiteMappingData> model)
        {
            return MasterDataManager.SaveSiteSubSiteMapping(model);
        }

        public IList<SiteMasterData> GetSiteDataList(string SiteCode, string SectorCode)
        {
            return MasterDataManager.GetSiteDataList(SiteCode, SectorCode);
        }
        //Krish
        public IList<LocationDesignationMappingData> GetLocationDesignationMappingDataList()
        {
            return MasterDataManager.GetLocationDesignationMappingDataList();
        }
        public IList<LocationDesignationMappingData> GetUniqueLocationAndDesignationDataList()
        {
            return MasterDataManager.GetUniqueLocationAndDesignationDataList();
        }
        public string SaveLocationDesignationData(string location, string designation)
        {
            return MasterDataManager.SaveLocationDesignationData(location,designation);
        }
        public IList<SectorDepartmentLeadershipMappingData> GetSectorDepartmentLeadershipMapping()
        {
            return MasterDataManager.GetSectorDepartmentLeadershipMapping();
        }
        public string SaveSectorDepartmentLeadershipMapping(SectorDepartmentLeadershipMappingData model)
        {
            return MasterDataManager.SaveSectorDepartmentLeadershipMapping(model);
        }
        public IList<CustomSelectListItem> GetSectorMasterList()
        {
            return MasterDataManager.GetSectorMasterList();
        }
        public IList<CustomSelectListItem> GetDepartmentMasterList()
        {
            return MasterDataManager.GetDepartmentMasterList();
        }
        public string SaveConsultantUploaderHistory(string status, string ecode, string userid, string batchnumb, string type)
        {
            return MasterDataManager.SaveConsultantUploaderHistory(status,ecode,userid, batchnumb,type);
        }
        public IList<ConsultantUploaderBatchHeaderData> GetConsultantUploaderBatchHeader()
        {
            return MasterDataManager.GetConsultantUploaderBatchHeader();
        }
        public string UploadConsultantData(string xmlstring, string template, int userid, string historyBatchno)
        {
            return MasterDataManager.UploadConsultantData( xmlstring,  template,  userid, historyBatchno);
        }
        public IList<ConsultantUploaderByBatchData> GetConsultantUploaderByBatchData(string template)
        {
            return MasterDataManager.GetConsultantUploaderByBatchData(template);
        }
        public IList<ConsultantUploaderBatchDetailsData> GetConsultantUploaderByBatchDetailsData(string template, string batchNo)
        {
            return MasterDataManager.GetConsultantUploaderByBatchDetailsData(template,batchNo);
        }
        public IList<ConsultantUploaderData> GetConsultantUploaderData(string template)
        {
            return MasterDataManager.GetConsultantUploaderData(template);
        }
        public string GetUserNameByEncryptedString(string encUsername, string key)
        {
            return UserManager.GetUserNameByEncryptedString(encUsername,key);
        }
        public HRISMasterData GetHRISMasterDataByCode(string empCode)
        {
            return MasterDataManager.GetHRISMasterDataByCode(empCode);
        }
        public IList<DiceUploaderData> GetDiceUploaderData()
        {
            return MasterDataManager.GetDiceUploaderData();
        }
        public string UploadDiceUploaderData(string xmlstring, int userid)
        {
            return MasterDataManager.UploadDiceUploaderData(xmlstring, userid);
        }
        public IList<LensUploaderData> GetLensUploaderData()
        {
            return MasterDataManager.GetLensUploaderData();
        }
        public string UploadLensUploaderData(string xmlstring, int userid)
        {
            return MasterDataManager.UploadLensUploaderData(xmlstring, userid);
        }
        public string SaveLensData(LensUploaderData lens)
        {
            return MasterDataManager.SaveLensData(lens);
        }
    }
}