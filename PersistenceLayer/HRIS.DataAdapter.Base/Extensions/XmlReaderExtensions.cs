﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.XPath;

namespace HRIS.DataAdapter.Base.Extensions
{
    public static class XmlReaderExtensions
    {
        public static IXPathNavigable GetXmlDocument(this XmlReader reader)
        {
            if(reader != null)
            {
                XPathDocument xp = new XPathDocument(reader);
                XPathNavigator xn = xp.CreateNavigator();
                XmlDocument document = new XmlDocument();
                XmlNode root = document.CreateElement("root");
                root.InnerXml = xn.OuterXml;
                document.AppendChild(root);
                return document as IXPathNavigable;
            }
            return null;
        }
    }
}
