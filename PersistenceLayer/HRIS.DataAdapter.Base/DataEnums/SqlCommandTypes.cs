﻿namespace HRIS.DataAdapter.Base.DataEnums
{
    public enum SqlCommandTypes
    {
        SqlString,
        StoredProc,
        DirectTable
    }
}
