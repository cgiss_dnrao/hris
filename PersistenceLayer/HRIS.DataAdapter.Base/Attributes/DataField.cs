﻿using System;

namespace HRIS.DataAdapter.Base.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public class DataField : Attribute
    {
        public string TableField { get; set; }
        public string SourceType { get; set; }
        public string SourceDataFormatter { get; set; }
        public string TargetDataFormatter { get; set; }
    }
}
