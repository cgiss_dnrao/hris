﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRIS.DataAdapter.Base.Contracts;

namespace HRIS.DataAdapter.Base
{
    public class EFTransaction : ITransaction
    {
        private Dictionary<Type, DbContext> Contexts { get; set; }

        public EFTransaction()
        {
            this.Contexts = new Dictionary<Type, DbContext>();
        }

        public DbContext GetContext<TDbContext>(TDbContext entityContext, string nameOrConnectionString)
            where TDbContext : DbContext, new()
        {
            DbContext context = null;
            //Type dbContextType = typeof(TDbContext);
            if (entityContext == null)
            {
                entityContext = Activator.CreateInstance<TDbContext>();
            }
            Type dbContextType = entityContext.GetType();
            if (entityContext != null)
            {
                context = entityContext;
            }
            if (!Contexts.ContainsKey(dbContextType))
            {
                if (string.IsNullOrEmpty(nameOrConnectionString))
                {
                    context = (TDbContext)Activator.CreateInstance(typeof(TDbContext));
                }
                else
                {
                    context = (TDbContext)Activator.CreateInstance(typeof(TDbContext), nameOrConnectionString);
                }
                Contexts.Add(dbContextType, context);
            }
            else
            {
                context = Contexts[dbContextType];
            }
            return context;
        }

        public object Commit()
        {
            int commitedItems = 0;
            bool success = false;
            List<DbContextTransaction> contextTransactions = new List<DbContextTransaction>();
            ExecutionStrategyDbConfiguration.SuspendExecutionStrategy(true);
            foreach (var context in this.Contexts.Values)
            {
                contextTransactions.Add(context.Database.BeginTransaction());
                try
                {
                    commitedItems += context.SaveChanges();
                    success = true;
                }
                catch (DbEntityValidationException ex)
                {
                    success = false;
                    List<string> errors = new List<string>();
                    foreach (var error in ex.EntityValidationErrors)
                    {
                        var entityName = "(UNKNOWN)";

                        if (error.Entry != null &&
                            error.Entry.Entity != null)
                        {
                            entityName = error.Entry.Entity.GetType().Name;
                        }
                        errors.Add(string.Format("Validation Error on {0}", entityName));
                        foreach (var propertyError in error.ValidationErrors)
                        {
                            errors.Add(string.Format("  {0} - {1}", propertyError.PropertyName, propertyError.ErrorMessage));
                        }
                    }

                    string errorMessage = string.Join(Environment.NewLine, errors);

                    throw new InvalidOperationException(errorMessage, ex);
                }
            }
            if (success)
            {
                contextTransactions.ForEach(contextTransaction => contextTransaction.Commit());
            }
            else
            {
                contextTransactions.ForEach(contextTransaction => contextTransaction.Rollback());
            }
            contextTransactions.ForEach(contextTransaction => contextTransaction.Dispose());
            ExecutionStrategyDbConfiguration.SuspendExecutionStrategy(false);
            return commitedItems;
        }

        public object Cancel()
        {
            Dispose();
            // this implementation doesn't return anything of significance.
            //Writing new method for same job
            //Cleanup();
            return null;
        }

        //protected virtual void Cleanup()
        //{
        //    // free managed resources
        //    foreach (var context in this.Contexts.Values)
        //    {
        //        context.Dispose();
        //    }
        //}

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                foreach (var context in this.Contexts.Values)
                {
                    context.Dispose();
                }
            }

        }
    }
}
