﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using HRIS.DataAdapter.Base.Contracts;

namespace HRIS.DataAdapter.Base
{
    public abstract class RepositoryTransactionManagerBase : IRepositoryTransactionManager
    {
        public abstract ITransaction BeginChanges();

        public bool Perform(Action<ITransaction> action, Action<ITransaction> localFinalAction = null, ITransaction externalTransaction = null)
        {
            return Perform(localTransaction => { action(localTransaction); return true; }, localFinalAction, externalTransaction);
        }

        public bool Perform(Func<ITransaction, bool> action, Action<ITransaction> localFinalAction = null, ITransaction externalTransaction = null)
        {
            Debug.Assert(action != null, "Attempted to invoke RepositoryTransactionManager.Perform without an action");

            bool success = false;
            bool localTransaction = false;

            if (externalTransaction == null)
            {
                localTransaction = true;
                externalTransaction = this.BeginChanges();
            }

            try
            {
                success = action(externalTransaction);
                if (success && localTransaction && localFinalAction != null)
                {
                    localFinalAction(externalTransaction);
                }

            }
            finally
            {
                if (localTransaction)
                {
                    externalTransaction.Dispose();
                }
            }
            return success;
        }

        public bool Perform(Action action, bool isTransactionScope)
        {
            Debug.Assert(action != null, "Attempted to invoke RepositoryTransactionManager.Perform without an action");

            bool success = false;
            if (isTransactionScope)
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    action.Invoke();
                    scope.Complete();
                    success = true;
                }
            }
            else
            {
                action.Invoke();
                success = true;
            }
            return success;
        }

        public bool PerformWithNoLock(Action action)
        {
            Debug.Assert(action != null, "Attempted to invoke RepositoryTransactionManager.PerformWithNoLock without an action");

            bool success = false;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = IsolationLevel.ReadUncommitted }))
            {
                action.Invoke();
                scope.Complete();
                success = true;
            }
            return success;
        }
    }
}
