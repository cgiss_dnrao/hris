﻿using HRIS.DataAdapter.Base.DataEnums;
using HRIS.DataAdapter.Base.Extensions;
using HRIS.DataAdapter.Base.Model;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Xml;

namespace HRIS.DataAdapter.Base.NoEF
{
    [Export(typeof(ISqlDataRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class SqlDataRepository : SqlBaseManager, ISqlDataRepository
    {
        #region Execute Non Query Commands

        /// <summary>
        /// Method to execute the non query command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        public NonQueryCommandOutput ExecuteNonQuery(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters)
        {
            NonQueryCommandOutput output = new NonQueryCommandOutput();
            try
            {
                using (DbCommand command = GetDbCommand(commandName, commandType, parameters))
                {
                    output.EffectedRows = DataAccess.ExecuteNonQuery(command);
                    output.OutputValues = GetOutputParameterValues(command, parameters);
                    output.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the non query command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        public NonQueryCommandOutput ExecuteNonQuery(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired)
        {
            NonQueryCommandOutput output = new NonQueryCommandOutput();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            DbCommand command = GetDbCommand(commandName, commandType, parameters);
                            output.EffectedRows = DataAccess.ExecuteNonQuery(command, transaction);
                            output.OutputValues = GetOutputParameterValues(command, parameters);
                            transaction.Commit();
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }

                    }
                }
                else
                {
                    output = ExecuteNonQuery(commandName, commandType, parameters);
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the non query command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        public NonQueryCommandOutput ExecuteNonQuery(string storedProcedureName, bool isTransactionRequired, params object[] parameters)
        {
            NonQueryCommandOutput output = new NonQueryCommandOutput();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            output.EffectedRows = DataAccess.ExecuteNonQuery(transaction, storedProcedureName, parameters);
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    output.EffectedRows = DataAccess.ExecuteNonQuery(storedProcedureName, parameters);
                    output.IsSuccess = true;
                }

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the sql non query using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        public NonQueryCommandOutput ExecuteNonQuery(string commandText, SqlCommandTypes commandType, bool isTransactionRequired)
        {
            NonQueryCommandOutput output = new NonQueryCommandOutput();
            try
            {
                CommandType type = CommandType.Text;
                if (commandType == SqlCommandTypes.StoredProc)
                {
                    type = CommandType.StoredProcedure;
                }
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            output.EffectedRows = DataAccess.ExecuteNonQuery(transaction, type, commandText);
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    output.EffectedRows = DataAccess.ExecuteNonQuery(type, commandText);
                    output.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        #endregion

        #region Execute Scalar Commands

        /// <summary>
        /// Method to execute the scalar command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        public ScalarCommandOutput ExecuteScalar(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters)
        {
            ScalarCommandOutput output = new ScalarCommandOutput();
            try
            {
                using (DbCommand command = GetDbCommand(commandName, commandType, parameters))
                {
                    output.Value = DataAccess.ExecuteScalar(command);
                    output.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the scalar command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        public ScalarCommandOutput ExecuteScalar(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired)
        {
            ScalarCommandOutput output = new ScalarCommandOutput();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            DbCommand command = GetDbCommand(commandName, commandType, parameters);
                            output.Value = DataAccess.ExecuteScalar(command, transaction);
                            transaction.Commit();
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }

                    }
                }
                else
                {
                    output = ExecuteScalar(commandName, commandType, parameters);
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the scalar command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        public ScalarCommandOutput ExecuteScalar(string storedProcedureName, bool isTransactionRequired, params object[] parameters)
        {
            ScalarCommandOutput output = new ScalarCommandOutput();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            output.Value = DataAccess.ExecuteScalar(transaction, storedProcedureName, parameters);
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    output.Value = DataAccess.ExecuteScalar(storedProcedureName, parameters);
                    output.IsSuccess = true;
                }

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the scalar query using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        public ScalarCommandOutput ExecuteScalar(string commandText, SqlCommandTypes commandType, bool isTransactionRequired)
        {
            ScalarCommandOutput output = new ScalarCommandOutput();
            try
            {
                CommandType type = CommandType.Text;
                if (commandType == SqlCommandTypes.StoredProc)
                {
                    type = CommandType.StoredProcedure;
                }
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            output.Value = DataAccess.ExecuteScalar(transaction, type, commandText);
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    output.Value = DataAccess.ExecuteScalar(type, commandText);
                    output.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        #endregion

        #region Execute Reader Commands

        /// <summary>
        /// Method to execute the reader command using name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        public DataReaderOutput<T> ExecuteReader<T>(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters) where T : class,new()
        {
            DataReaderOutput<T> output = new DataReaderOutput<T>();
            try
            {
                DbCommand command = GetDbCommand(commandName, commandType, parameters);

                using (var reader = DataAccess.ExecuteReader(command))
                {
                    output.Data = reader.AutoMap<T>();
                    output.IsSuccess = true;
                }

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;
        }

        /// <summary>
        /// Method to execute the reader command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        public DataReaderOutput<T> ExecuteReader<T>(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired) where T : class,new()
        {
            DataReaderOutput<T> output = new DataReaderOutput<T>();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            DbCommand command = GetDbCommand(commandName, commandType, parameters);
                            using (var reader = DataAccess.ExecuteReader(command, transaction))
                            {
                                output.Data = reader.AutoMap<T>();
                            }
                            transaction.Commit();
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }

                    }
                }
                else
                {
                    output = ExecuteReader<T>(commandName, commandType, parameters);
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the reader command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        public DataReaderOutput<T> ExecuteReader<T>(string storedProcedureName, bool isTransactionRequired, params object[] parameters) where T : class,new()
        {
            DataReaderOutput<T> output = new DataReaderOutput<T>();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            using (var reader = DataAccess.ExecuteReader(transaction, storedProcedureName, parameters))
                            {
                                output.Data = reader.AutoMap<T>();
                                output.IsSuccess = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    using (var reader = DataAccess.ExecuteReader(storedProcedureName, parameters))
                    {
                        output.Data = reader.AutoMap<T>();
                        output.IsSuccess = true;
                    }
                }

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the data reader using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        public DataReaderOutput<T> ExecuteReader<T>(string commandText, SqlCommandTypes commandType, bool isTransactionRequired) where T : class,new()
        {
            DataReaderOutput<T> output = new DataReaderOutput<T>();
            try
            {
                CommandType type = CommandType.Text;
                if (commandType == SqlCommandTypes.StoredProc)
                {
                    type = CommandType.StoredProcedure;
                }
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            using (var reader = DataAccess.ExecuteReader(transaction, type, commandText))
                            {
                                output.Data = reader.AutoMap<T>();

                                output.IsSuccess = true;
                            }
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    using (var reader = DataAccess.ExecuteReader(type, commandText))
                    {
                        output.Data = reader.AutoMap<T>();
                        output.IsSuccess = true;
                    }
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        #endregion

        #region Execute Dataset Commands

        /// <summary>
        /// Method to load the data set with specified table name and command text
        /// </summary>
        /// <param name="commandText">command text</param>
        /// <param name="commandType">Sql Command Type</param>
        /// <param name="dataSet">dataset</param>
        /// <param name="tableName">table name</param>
        /// <param name="parameters">sql parameters</param>
        /// <param name="isTransactionRequired">transaction required</param>
        public void LoadDataSet(string commandText, SqlCommandTypes commandType, DataSet dataSet, string tableName, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired)
        {
            LoadDataSet(commandText, commandType, dataSet, new string[] { tableName }, parameters, isTransactionRequired);
        }

        /// <summary>
        /// Method to load the data set with specified table name and command text
        /// </summary>
        /// <param name="commandText">command text</param>
        /// <param name="commandType">Sql Command Type</param>
        /// <param name="dataSet">dataset</param>
        /// <param name="tableNames">table name</param>
        /// <param name="parameters">sql parameters</param>
        /// <param name="isTransactionRequired">transaction required</param>
        public void LoadDataSet(string commandText, SqlCommandTypes commandType, DataSet dataSet, string[] tableNames, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired)
        {
            DbCommand command = GetDbCommand(commandText, commandType, parameters);
            using (DbConnection connection = DataAccess.CreateConnection())
            {
                connection.Open();
                if (isTransactionRequired)
                {
                    DbTransaction transaction = connection.BeginTransaction();
                    DataAccess.LoadDataSet(command, dataSet, tableNames, transaction);
                }
                else
                {
                    DataAccess.LoadDataSet(command, dataSet, tableNames);
                }
            }
        }

        /// <summary>
        /// Method to execute the Dataset command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the command output</returns>
        public DataSetCommandOutput ExecuteDataSet(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters)
        {
            DataSetCommandOutput output = new DataSetCommandOutput();
            try
            {
                using (DbCommand command = GetDbCommand(commandName, commandType, parameters))
                {
                    output.Data = DataAccess.ExecuteDataSet(command);
                    output.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the dataset command using command name and sql object parameters
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns the command output</returns>
        public DataSetCommandOutput ExecuteDataSet(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters, bool isTransactionRequired)
        {
            DataSetCommandOutput output = new DataSetCommandOutput();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            DbCommand command = GetDbCommand(commandName, commandType, parameters);
                            output.Data = DataAccess.ExecuteDataSet(command, transaction);
                            transaction.Commit();
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }

                    }
                }
                else
                {
                    output = ExecuteDataSet(commandName, commandType, parameters);
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the dataset command using stored procedure name and sql object parameters
        /// </summary>
        /// <param name="storedProcedureName">stored procedure name</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <param name="parameters">sql object parametr array</param>
        /// <returns>returns command output</returns>
        public DataSetCommandOutput ExecuteDataSet(string storedProcedureName, bool isTransactionRequired, params object[] parameters)
        {
            DataSetCommandOutput output = new DataSetCommandOutput();
            try
            {
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            output.Data = DataAccess.ExecuteDataSet(transaction, storedProcedureName, parameters);
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    output.Data = DataAccess.ExecuteDataSet(storedProcedureName, parameters);
                    output.IsSuccess = true;
                }

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        /// <summary>
        /// Method to execute the dataset query using command type and command text 
        /// </summary>
        /// <param name="commandText">command text to be executed</param>
        /// <param name="commandType">command type i.e. stored procedure or sql string</param>
        /// <param name="isTransactionRequired">is command executions will be part of database transaction</param>
        /// <returns>returns command output</returns>
        public DataSetCommandOutput ExecuteDataSet(string commandText, SqlCommandTypes commandType, bool isTransactionRequired)
        {
            DataSetCommandOutput output = new DataSetCommandOutput();
            try
            {
                CommandType type = CommandType.Text;
                if (commandType == SqlCommandTypes.StoredProc)
                {
                    type = CommandType.StoredProcedure;
                }
                if (isTransactionRequired)
                {
                    using (DbConnection connection = DataAccess.CreateConnection())
                    {
                        connection.Open();
                        DbTransaction transaction = connection.BeginTransaction();
                        try
                        {
                            output.Data = DataAccess.ExecuteDataSet(transaction, type, commandText);
                            output.IsSuccess = true;
                        }
                        catch (Exception ex)
                        {
                            transaction.Rollback();
                            HandleDataException(ex);
                            throw;
                        }
                    }
                }
                else
                {
                    output.Data = DataAccess.ExecuteDataSet(type, commandText);
                    output.IsSuccess = true;
                }
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;

        }

        #endregion

        #region Stored Procedure Accessor Methods

        /// <summary>
        /// Method to execute the stored procedure and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// </summary>
        /// <param name="procedureName">stored procedure name</param>
        /// <param name="parameters">sql object parameter list</param>
        /// <returns>returns the sproc accessor output</returns>
        public SprocAccessorOutput<T> ExecuteSprocAccessor<T>(string procedureName, params object[] parameters) where T : class,new()
        {
            SprocAccessorOutput<T> output = new SprocAccessorOutput<T>();
            try
            {
                if (parameters == null)
                {
                    output.Data = DataAccess.ExecuteSprocAccessor<T>(procedureName);
                }
                else
                {
                    output.Data = DataAccess.ExecuteSprocAccessor<T>(procedureName, parameters);
                }

                output.IsSuccess = true;

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;
        }

        /// <summary>
        /// Method to execute the sql string and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// </summary>
        /// <param name="sqlString">sql string</param>
        /// <returns>returns the sproc accessor output</returns>
        public SprocAccessorOutput<T> ExecuteSqlStringAccessor<T>(string sqlString) where T : class,new()
        {
            SprocAccessorOutput<T> output = new SprocAccessorOutput<T>();
            try
            {
                output.Data = DataAccess.ExecuteSqlStringAccessor<T>(sqlString);
                output.IsSuccess = true;

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;
        }

        /// <summary>
        /// Method to execute the stored procedures and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// If mappings is provided then for mapping columns rule will be applicable
        /// </summary>
        /// <param name="procedureName">sql string</param>
        ///  <param name="mappings"> mappings</param>
        ///  <param name="parameters">sql object parameter list</param>
        /// <returns>returns the sproc accessor output</returns>
        public SprocAccessorOutput<T> ExecuteSprocAccessorWithMapping<T>(string procedureName, IRowMapper<T> mappings, params object[] parameters) where T : class, new()
        {
            SprocAccessorOutput<T> output = new SprocAccessorOutput<T>();
            try
            {
                if (parameters == null)
                {
                    output.Data = DataAccess.ExecuteSprocAccessor<T>(procedureName, mappings);
                }
                else
                {
                    output.Data = DataAccess.ExecuteSprocAccessor<T>(procedureName, mappings, parameters);
                }
                output.IsSuccess = true;

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;
        }

        /// <summary>
        /// Method to execute the sql string and returns the result as IEnumerable T. 
        /// The conversion from data record to T will be done for each property based on matching property name to column name
        /// If mappings is provided then for mapping columns rule will be applicable
        /// </summary>
        /// <param name="sqlString">sql string</param>
        /// <param name="mappings">mappings</param>
        /// <returns>returns the sproc accessor output</returns>
        public SprocAccessorOutput<T> ExecuteSqlStringAccessorWithMapping<T>(string sqlString, IRowMapper<T> mappings) where T : class, new()
        {
            SprocAccessorOutput<T> output = new SprocAccessorOutput<T>();
            try
            {
                output.Data = DataAccess.ExecuteSqlStringAccessor<T>(sqlString, mappings);
                output.IsSuccess = true;

            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            return output;
        }

        #endregion

        #region Xml Reader Commands

        /// <summary>
        /// Method to execute xnl reader on basis of xml query
        /// </summary>
        /// <param name="commandText">xml sql query</param>
        /// <param name="commandType">command type</param>
        /// <param name="isTransactionRequired">is transaction required</param>
        /// <param name="parameters">sql parameters</param>
        /// <returns>returns xml reader output</returns>
        public XmlReaderOutput ExecuteXmlReader(string commandText, SqlCommandTypes commandType, bool isTransactionRequired, IEnumerable<SqlCommandParameter> parameters)
        {
            XmlReaderOutput output = new XmlReaderOutput();
            DbConnection connection = null;
            connection = DataAccess.CreateConnection();
            DbCommand command = null;
            XmlReader xmlReader = null;
            connection.Open();
            try
            {

                command = GetDbCommand(commandText, commandType, parameters);
                if (isTransactionRequired)
                {
                    DbTransaction transaction = connection.BeginTransaction();
                    xmlReader = SqlDataAccess.ExecuteXmlReader(command, transaction);
                    output.Document = xmlReader.GetXmlDocument();
                }
                else
                {
                    xmlReader = SqlDataAccess.ExecuteXmlReader(command);
                    output.Document = xmlReader.GetXmlDocument();
                }
                output.IsSuccess = true;
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                }
                if (xmlReader != null)
                {
                    xmlReader.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
            }
            return output;

        }

        /// <summary>
        /// Method to execute xnl reader on basis of xml query
        /// </summary>
        /// <param name="commandText">xml sql query</param>
        /// <param name="commandType">command type</param>
        /// <param name="isTransactionRequired">is transaction required</param>
        /// <returns>returns xml reader output</returns>
        public XmlReaderOutput ExecuteXmlReader(string commandText, SqlCommandTypes commandType, bool isTransactionRequired)
        {
            XmlReaderOutput output = new XmlReaderOutput();
            DbConnection connection = null;
            DbCommand command = null;
            connection = DataAccess.CreateConnection();
            connection.Open();
            try
            {
                command = GetDbCommand(commandText, commandType, null);
                if (isTransactionRequired)
                {
                    DbTransaction transaction = connection.BeginTransaction();
                    using (var xmlReader = SqlDataAccess.ExecuteXmlReader(command, transaction))
                    {
                        output.Document = xmlReader.GetXmlDocument();
                    }
                }
                else
                {
                    using (var xmlReader = SqlDataAccess.ExecuteXmlReader(command))
                    {
                        output.Document = xmlReader.GetXmlDocument();
                    }
                }
                output.IsSuccess = true;
            }
            catch (Exception ex)
            {
                HandleDataException(ex);
                throw;
            }
            finally
            {
                if (connection != null)
                {
                    connection.Dispose();
                }
                if (command != null)
                {
                    command.Dispose();
                }
            }
            return output;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Method to handle the sql exceptions raised during the data factory execution
        /// </summary>
        /// <param name="exception">exception need to handle</param>
        /// <returns>returns command output</returns>
        private NonQueryCommandOutput HandleDataException(Exception exception)
        {
            NonQueryCommandOutput output = new NonQueryCommandOutput();
            output.IsSuccess = false;
            output.ErrorMessage = new InvalidOperationException(exception.Message, exception.InnerException);
            return output;
        }

        /// <summary>
        /// Method to get the sql output parameter values from command after the database execution
        /// </summary>
        /// <param name="command">database command</param>
        /// <param name="parameters">Output Sql parameters</param>
        /// <returns>returs dictionary of values</returns>
        private Dictionary<string, object> GetOutputParameterValues(DbCommand command, IEnumerable<SqlCommandParameter> parameters)
        {
            Dictionary<string, object> outputs = new Dictionary<string, object>();
            IEnumerable<SqlCommandParameter> outputparameters = parameters.Where(k => k.Direction == ParameterDirection.Output || k.Direction == ParameterDirection.InputOutput).ToList();
            foreach (var parameter in outputparameters)
            {
                outputs.Add(parameter.Name, DataAccess.GetParameterValue(command, parameter.Name));
            }
            return outputs;
        }

        /// <summary>
        /// Method to get the sql command with sql parameters attached in it
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="commandType">command type</param>
        /// <returns>returns database command</returns>
        private DbCommand GetDbCommand(string commandName, SqlCommandTypes commandType, IEnumerable<SqlCommandParameter> parameters)
        {
            DbCommand command = null;
            if (commandType == SqlCommandTypes.StoredProc)
            {
                command = DataAccess.GetStoredProcCommand(commandName);
            }
            else
            {
                command = DataAccess.GetSqlStringCommand(commandName);
            }
            if (parameters != null && parameters.Count() > 0)
            {
                AddCommandParameters(command, parameters);
            }
            return command;
        }

        /// <summary>
        /// Method to add sql parameters  into the database command
        /// </summary>
        /// <param name="command">Sql Database command</param>
        /// <param name="parameters">collection of parameters</param>
        private void AddCommandParameters(DbCommand command, IEnumerable<SqlCommandParameter> parameters)
        {
            foreach (var parameter in parameters)
            {
                switch (parameter.Direction)
                {
                    case ParameterDirection.Input:
                        DataAccess.AddInParameter(command, parameter.Name, parameter.DataType, parameter.Value);
                        break;
                    case ParameterDirection.Output:
                        DataAccess.AddOutParameter(command, parameter.Name, parameter.DataType, parameter.Size);
                        break;
                    default:

                        break;
                }

            }
        }

        #endregion
    }
}
