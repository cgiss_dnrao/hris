﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Data.Sql;

namespace HRIS.DataAdapter.Base.NoEF
{
    public abstract class SqlBaseManager : ISqlBase
    {
        private bool isDataSourceActive;

        public Database DataAccess { get; private set; }
        public SqlDatabase SqlDataAccess { get; private set; }

        public SqlBaseManager()
        {
            InitializeDatasource();
        }

        public void InitializeDatabase(string name)
        {
            DataAccess = DatabaseFactory.CreateDatabase(name);
            isDataSourceActive = true;
        }

        public bool IsDatabaseConnectionActive()
        {
            return isDataSourceActive;
        }

        #region Private Methods

        private void InitializeDatasource()
        {
            if (DataAccess == null)
            {
                DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
                DataAccess = DatabaseFactory.CreateDatabase();
                SqlDataAccess = DatabaseFactory.CreateDatabase() as SqlDatabase;
                isDataSourceActive = true;
            }
        }

        #endregion

    }
}
