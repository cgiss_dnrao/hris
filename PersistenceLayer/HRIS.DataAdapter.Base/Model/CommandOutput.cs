﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.DataAdapter.Base.Model
{
    public class CommandOutput
    {
        /// <summary>
        /// If there are no errors and query executed successfully
        /// </summary>
        public bool IsSuccess { get; set; }
        /// <summary>
        /// If any exception occurred in database
        /// </summary>
        public Exception ErrorMessage { get; set; }
    }
}
