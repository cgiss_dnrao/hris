﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.DataAdapter.Base.Model
{
    public class DataReaderOutput<T> : CommandOutput
        where T: class, new ()
    {
        public IEnumerable<T> Data { get; set; }
    }
}
