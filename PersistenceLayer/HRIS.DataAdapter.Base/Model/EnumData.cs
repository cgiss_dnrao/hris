﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.DataAdapter.Base.Model
{
    [DataContract]
    public class EnumData
    {
        [DataMember]
        public string Text { get; set; }
        [DataMember]
        public int Value { get; set; }
    }
}
