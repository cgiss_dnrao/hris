﻿using System.Collections.Generic;
using System.Data;

namespace HRIS.DataAdapter.Base.Contracts
{
    public interface IExecutionObject
    {
        /// <summary>
        /// Execute the database stored procedures and inline queries
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns></returns>
        IEnumerable<TData> ExecuteDbCommand<TData>(string commandName, params object[] parameters);

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="ensureTransaction">require transaction</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        bool ExecuteNonQueryCommand(string commandName, bool ensureTransaction = false, params object[] parameters);

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns boolean status</returns>
        bool ExecuteNonQueryCommand(string commandName, params object[] parameters);

        /// <summary>
        /// Method to execute the scalar command values
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns scalar value</returns>
        T ExecuteScalarCommand<T>(string commandName, params object[] parameters);

        /// <summary>
        /// Method to retuns collection using inline sql query
        /// </summary>
        /// <typeparam name="TData">Generic type</typeparam>
        /// <param name="query">sql query</param>
        /// <returns>returns entity collection</returns>
        IEnumerable<TData> ExecuteSqlInlineQuery<TData>(string query);

        /// <summary>
        /// Execute the sql command to get the data table 
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="tableToFill">table to fill</param>
        /// <param name="parameters">command parameters</param>
        /// <returns>returns data table</returns>
        DataTable ExecuteDataTableCommand(string commandName, DataTable tableToFill, params object[] parameters);

        /// <summary>
        /// Execute the non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="commandName">command name</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns rows affected</returns>
        int ExecuteNonQuery(string commandName, params object[] parameters);

        /// <summary>
        /// Execute the sql inline non query command such as insert, update & delete queries/objects in database
        /// </summary>
        /// <param name="query">query</param>
        /// <param name="parameters">parameters</param>
        /// <returns>returns rows affected</returns>
        int ExecuteSqlInlineNonQuery(string query, params object[] parameters);
    }
}
