﻿using System.Collections.Generic;

namespace HRIS.DataAdapter.Base.Contracts
{
    public interface ICompositeModel
    {
        Dictionary<string, dynamic> CompositeKeys { get; }
    }
}
