﻿namespace HRIS.DataAdapter.Base.Contracts
{
    public interface ITransactable
    {
        IRepositoryTransactionManager RepositoryTransactionManager { get; set; }
    }
}
