﻿namespace HRIS.DataAdapter.Base.Contracts
{
    public interface IKeyedModel<TKey>
    {
        TKey Key { get; }
    }
}
