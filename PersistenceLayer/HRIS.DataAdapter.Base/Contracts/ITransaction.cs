﻿using System;

namespace HRIS.DataAdapter.Base.Contracts
{
    public interface ITransaction : IDisposable
    {
        object Commit();
        object Cancel();
    }
}
