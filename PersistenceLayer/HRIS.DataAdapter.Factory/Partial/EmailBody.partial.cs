﻿using HRIS.DataAdapter.Base.Contracts;

namespace HRIS.DataAdapter.Factory.Transaction
{
    public partial class EmailBody : IKeyedModel<int>
    {
        public int Key
        {
            get { return this.ID; }
        }
    }
}
