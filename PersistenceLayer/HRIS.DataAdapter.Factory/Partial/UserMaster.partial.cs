﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRIS.DataAdapter.Base.Contracts;

namespace HRIS.DataAdapter.Factory.Transaction
{
    public partial class UserMaster : IKeyedModel<int>
    {
        public int Key
        {
            get { return UserId; }
        }
    }
}
