﻿using HRIS.DataAdapter.Base.Contracts;

namespace HRIS.DataAdapter.Factory.Transaction
{
    public partial class UserRoleMaster : IKeyedModel<int>
    {
        public int Key
        {
            get { return this.UserRoleId; }
        }
    }
}
