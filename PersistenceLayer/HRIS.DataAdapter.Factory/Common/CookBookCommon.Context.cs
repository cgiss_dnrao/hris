﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRIS.DataAdapter.Factory.Common
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class CookBookCommonEntities : DbContext
    {
        public CookBookCommonEntities()
            : base("name=CookBookCommonEntities")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Action> Actions { get; set; }
        public virtual DbSet<Allergen> Allergens { get; set; }
        public virtual DbSet<AplCategoryMaster> AplCategoryMasters { get; set; }
        public virtual DbSet<APLMaster> APLMasters { get; set; }
        public virtual DbSet<AplTypeMaster> AplTypeMasters { get; set; }
        public virtual DbSet<ApplicationSector> ApplicationSectors { get; set; }
        public virtual DbSet<Area> Areas { get; set; }
        public virtual DbSet<AuditorType> AuditorTypes { get; set; }
        public virtual DbSet<Cafe> Cafes { get; set; }
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CategoryLog> CategoryLogs { get; set; }
        public virtual DbSet<ConceptType1> ConceptType1 { get; set; }
        public virtual DbSet<ConceptType2> ConceptType2 { get; set; }
        public virtual DbSet<ConceptType3> ConceptType3 { get; set; }
        public virtual DbSet<ConceptType4> ConceptType4 { get; set; }
        public virtual DbSet<ConceptType5> ConceptType5 { get; set; }
        public virtual DbSet<DefectCategory> DefectCategories { get; set; }
        public virtual DbSet<DefectFoundIn> DefectFoundIns { get; set; }
        public virtual DbSet<DefectReportedBy> DefectReportedBies { get; set; }
        public virtual DbSet<DefectSeverity> DefectSeverities { get; set; }
        public virtual DbSet<DefectStage> DefectStages { get; set; }
        public virtual DbSet<DefectSubType> DefectSubTypes { get; set; }
        public virtual DbSet<DefectType> DefectTypes { get; set; }
        public virtual DbSet<DietCategory> DietCategories { get; set; }
        public virtual DbSet<EmailBody> EmailBodies { get; set; }
        public virtual DbSet<EmailNotification> EmailNotifications { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<ItemType1> ItemType1 { get; set; }
        public virtual DbSet<ItemType2> ItemType2 { get; set; }
        public virtual DbSet<MerchandiseMaster> MerchandiseMasters { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<Operation> Operations { get; set; }
        public virtual DbSet<RCACategoryMaster> RCACategoryMasters { get; set; }
        public virtual DbSet<RCACodeMaster> RCACodeMasters { get; set; }
        public virtual DbSet<SectorMaster> SectorMasters { get; set; }
        public virtual DbSet<Serveware> Servewares { get; set; }
        public virtual DbSet<ServingTemperature> ServingTemperatures { get; set; }
        public virtual DbSet<SiteMaster> SiteMasters { get; set; }
        public virtual DbSet<SiteProfile1> SiteProfile1 { get; set; }
        public virtual DbSet<SiteProfile2> SiteProfile2 { get; set; }
        public virtual DbSet<SiteProfile3> SiteProfile3 { get; set; }
        public virtual DbSet<SmtpServerSetting> SmtpServerSettings { get; set; }
        public virtual DbSet<SqlErrorLog> SqlErrorLogs { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<SuppliedBy> SuppliedBies { get; set; }
        public virtual DbSet<Tbl_ExceptionLoggingToDataBase> Tbl_ExceptionLoggingToDataBase { get; set; }
        public virtual DbSet<UserMaster> UserMasters { get; set; }
        public virtual DbSet<UserRoleMaster> UserRoleMasters { get; set; }
        public virtual DbSet<VendorsAction> VendorsActions { get; set; }
        public virtual DbSet<VisualCategory> VisualCategories { get; set; }
        public virtual DbSet<SiteMaster_Backup> SiteMaster_Backup { get; set; }
        public virtual DbSet<VendorMaster> VendorMasters { get; set; }
        public virtual DbSet<VendorMaster_Backup> VendorMaster_Backup { get; set; }
        public virtual DbSet<UserSectorMapping> UserSectorMappings { get; set; }
        public virtual DbSet<Reason> Reasons { get; set; }
        public virtual DbSet<Color> Colors { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Segment> Segments { get; set; }
        public virtual DbSet<SiteCategory> SiteCategories { get; set; }
        public virtual DbSet<RegionMaster> RegionMasters { get; set; }
        public virtual DbSet<RoleBasedAccessMatrix> RoleBasedAccessMatrices { get; set; }
    
        [DbFunction("CookBookCommonEntities", "fnSplit")]
        public virtual IQueryable<fnSplit_Result> fnSplit(string commadelimitedString)
        {
            var commadelimitedStringParameter = commadelimitedString != null ?
                new ObjectParameter("CommadelimitedString", commadelimitedString) :
                new ObjectParameter("CommadelimitedString", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fnSplit_Result>("[CookBookCommonEntities].[fnSplit](@CommadelimitedString)", commadelimitedStringParameter);
        }
    
        public virtual int ExceptionLoggingToDataBase(string exceptionMsg, string exceptionType, string exceptionSource)
        {
            var exceptionMsgParameter = exceptionMsg != null ?
                new ObjectParameter("ExceptionMsg", exceptionMsg) :
                new ObjectParameter("ExceptionMsg", typeof(string));
    
            var exceptionTypeParameter = exceptionType != null ?
                new ObjectParameter("ExceptionType", exceptionType) :
                new ObjectParameter("ExceptionType", typeof(string));
    
            var exceptionSourceParameter = exceptionSource != null ?
                new ObjectParameter("ExceptionSource", exceptionSource) :
                new ObjectParameter("ExceptionSource", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ExceptionLoggingToDataBase", exceptionMsgParameter, exceptionTypeParameter, exceptionSourceParameter);
        }
    
        public virtual ObjectResult<GetAPLMasterData_Result> GetAPLMasterData(string merchandiseCode)
        {
            var merchandiseCodeParameter = merchandiseCode != null ?
                new ObjectParameter("MerchandiseCode", merchandiseCode) :
                new ObjectParameter("MerchandiseCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAPLMasterData_Result>("GetAPLMasterData", merchandiseCodeParameter);
        }
    
        public virtual ObjectResult<GetAplMasterDataInformation_Result> GetAplMasterDataInformation()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAplMasterDataInformation_Result>("GetAplMasterDataInformation");
        }
    
        public virtual ObjectResult<GetCafeListBySiteCode_Result> GetCafeListBySiteCode(string siteCode)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCafeListBySiteCode_Result>("GetCafeListBySiteCode", siteCodeParameter);
        }
    
        public virtual ObjectResult<GetSiteMasterData_Result> GetSiteMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetSiteMasterData_Result>("GetSiteMasterData");
        }
    
        public virtual ObjectResult<GetVendorMasterData_Result> GetVendorMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetVendorMasterData_Result>("GetVendorMasterData");
        }
    
        public virtual ObjectResult<procGetCPUList_Result> procGetCPUList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetCPUList_Result>("procGetCPUList");
        }
    
        public virtual ObjectResult<procGetDKList_Result> procGetDKList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetDKList_Result>("procGetDKList");
        }
    
        public virtual int sp_alterdiagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_alterdiagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_creatediagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_creatediagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_dropdiagram(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_dropdiagram", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagramdefinition_Result> sp_helpdiagramdefinition(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagramdefinition_Result>("sp_helpdiagramdefinition", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagrams_Result> sp_helpdiagrams(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagrams_Result>("sp_helpdiagrams", diagramnameParameter, owner_idParameter);
        }
    
        public virtual int sp_renamediagram(string diagramname, Nullable<int> owner_id, string new_diagramname)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var new_diagramnameParameter = new_diagramname != null ?
                new ObjectParameter("new_diagramname", new_diagramname) :
                new ObjectParameter("new_diagramname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_renamediagram", diagramnameParameter, owner_idParameter, new_diagramnameParameter);
        }
    
        public virtual int sp_upgraddiagrams()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_upgraddiagrams");
        }
    
        public virtual ObjectResult<Nullable<int>> SpAddCategory(string categoryName, Nullable<int> logID)
        {
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("categoryName", categoryName) :
                new ObjectParameter("categoryName", typeof(string));
    
            var logIDParameter = logID.HasValue ?
                new ObjectParameter("LogID", logID) :
                new ObjectParameter("LogID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SpAddCategory", categoryNameParameter, logIDParameter);
        }
    
        public virtual int SpClearLogs()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpClearLogs");
        }
    
        public virtual ObjectResult<Nullable<int>> SpInsertCategoryLog(Nullable<int> categoryID, Nullable<int> logID)
        {
            var categoryIDParameter = categoryID.HasValue ?
                new ObjectParameter("categoryID", categoryID) :
                new ObjectParameter("categoryID", typeof(int));
    
            var logIDParameter = logID.HasValue ?
                new ObjectParameter("logID", logID) :
                new ObjectParameter("logID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SpInsertCategoryLog", categoryIDParameter, logIDParameter);
        }
    
        public virtual int SpWriteLog(Nullable<int> eventID, Nullable<int> priority, string severity, string title, Nullable<System.DateTime> timestamp, string machineName, string appDomainName, string processID, string processName, string threadName, string win32ThreadId, string message, string formattedMessage, ObjectParameter logId)
        {
            var eventIDParameter = eventID.HasValue ?
                new ObjectParameter("EventID", eventID) :
                new ObjectParameter("EventID", typeof(int));
    
            var priorityParameter = priority.HasValue ?
                new ObjectParameter("Priority", priority) :
                new ObjectParameter("Priority", typeof(int));
    
            var severityParameter = severity != null ?
                new ObjectParameter("Severity", severity) :
                new ObjectParameter("Severity", typeof(string));
    
            var titleParameter = title != null ?
                new ObjectParameter("Title", title) :
                new ObjectParameter("Title", typeof(string));
    
            var timestampParameter = timestamp.HasValue ?
                new ObjectParameter("Timestamp", timestamp) :
                new ObjectParameter("Timestamp", typeof(System.DateTime));
    
            var machineNameParameter = machineName != null ?
                new ObjectParameter("MachineName", machineName) :
                new ObjectParameter("MachineName", typeof(string));
    
            var appDomainNameParameter = appDomainName != null ?
                new ObjectParameter("AppDomainName", appDomainName) :
                new ObjectParameter("AppDomainName", typeof(string));
    
            var processIDParameter = processID != null ?
                new ObjectParameter("ProcessID", processID) :
                new ObjectParameter("ProcessID", typeof(string));
    
            var processNameParameter = processName != null ?
                new ObjectParameter("ProcessName", processName) :
                new ObjectParameter("ProcessName", typeof(string));
    
            var threadNameParameter = threadName != null ?
                new ObjectParameter("ThreadName", threadName) :
                new ObjectParameter("ThreadName", typeof(string));
    
            var win32ThreadIdParameter = win32ThreadId != null ?
                new ObjectParameter("Win32ThreadId", win32ThreadId) :
                new ObjectParameter("Win32ThreadId", typeof(string));
    
            var messageParameter = message != null ?
                new ObjectParameter("Message", message) :
                new ObjectParameter("Message", typeof(string));
    
            var formattedMessageParameter = formattedMessage != null ?
                new ObjectParameter("FormattedMessage", formattedMessage) :
                new ObjectParameter("FormattedMessage", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpWriteLog", eventIDParameter, priorityParameter, severityParameter, titleParameter, timestampParameter, machineNameParameter, appDomainNameParameter, processIDParameter, processNameParameter, threadNameParameter, win32ThreadIdParameter, messageParameter, formattedMessageParameter, logId);
        }
    
        public virtual ObjectResult<usp_GetAplCategoryMasterList_Result> usp_GetAplCategoryMasterList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<usp_GetAplCategoryMasterList_Result>("usp_GetAplCategoryMasterList");
        }
    
        public virtual ObjectResult<string> usp_GetMerchandiseList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("usp_GetMerchandiseList");
        }
    
        public virtual ObjectResult<Nullable<int>> USP_InsertSqlErrorLog(Nullable<int> processLogId, string sp_name, Nullable<int> error_number, Nullable<int> errorLine, string errormessage, Nullable<int> errorSeverity, Nullable<int> errorState)
        {
            var processLogIdParameter = processLogId.HasValue ?
                new ObjectParameter("ProcessLogId", processLogId) :
                new ObjectParameter("ProcessLogId", typeof(int));
    
            var sp_nameParameter = sp_name != null ?
                new ObjectParameter("sp_name", sp_name) :
                new ObjectParameter("sp_name", typeof(string));
    
            var error_numberParameter = error_number.HasValue ?
                new ObjectParameter("error_number", error_number) :
                new ObjectParameter("error_number", typeof(int));
    
            var errorLineParameter = errorLine.HasValue ?
                new ObjectParameter("errorLine", errorLine) :
                new ObjectParameter("errorLine", typeof(int));
    
            var errormessageParameter = errormessage != null ?
                new ObjectParameter("errormessage", errormessage) :
                new ObjectParameter("errormessage", typeof(string));
    
            var errorSeverityParameter = errorSeverity.HasValue ?
                new ObjectParameter("errorSeverity", errorSeverity) :
                new ObjectParameter("errorSeverity", typeof(int));
    
            var errorStateParameter = errorState.HasValue ?
                new ObjectParameter("errorState", errorState) :
                new ObjectParameter("errorState", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("USP_InsertSqlErrorLog", processLogIdParameter, sp_nameParameter, error_numberParameter, errorLineParameter, errormessageParameter, errorSeverityParameter, errorStateParameter);
        }
    
        public virtual int usp_UpdateAPLMaster(string aPLMaster)
        {
            var aPLMasterParameter = aPLMaster != null ?
                new ObjectParameter("APLMaster", aPLMaster) :
                new ObjectParameter("APLMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateAPLMaster", aPLMasterParameter);
        }
    
        public virtual int usp_UpdateSiteMasterData(string siteMaster)
        {
            var siteMasterParameter = siteMaster != null ?
                new ObjectParameter("SiteMaster", siteMaster) :
                new ObjectParameter("SiteMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateSiteMasterData", siteMasterParameter);
        }
    
        public virtual int usp_UpdateVendorMaster(string vendorMaster)
        {
            var vendorMasterParameter = vendorMaster != null ?
                new ObjectParameter("VendorMaster", vendorMaster) :
                new ObjectParameter("VendorMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateVendorMaster", vendorMasterParameter);
        }
    
        public virtual ObjectResult<procGetUserDataByUsername_Result> procGetUserDataByUsername(string username)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("Username", username) :
                new ObjectParameter("Username", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetUserDataByUsername_Result>("procGetUserDataByUsername", usernameParameter);
        }
    
        public virtual ObjectResult<procGetSiteList_Result> procGetSiteList(string siteCode, string sectorNumber)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            var sectorNumberParameter = sectorNumber != null ?
                new ObjectParameter("SectorNumber", sectorNumber) :
                new ObjectParameter("SectorNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetSiteList_Result>("procGetSiteList", siteCodeParameter, sectorNumberParameter);
        }
    
        public virtual ObjectResult<procGetCafeList_Result> procGetCafeList(string siteCode, string sectorNumber)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            var sectorNumberParameter = sectorNumber != null ?
                new ObjectParameter("SectorNumber", sectorNumber) :
                new ObjectParameter("SectorNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetCafeList_Result>("procGetCafeList", siteCodeParameter, sectorNumberParameter);
        }
    
        public virtual ObjectResult<procGetSectorDataByUserID_Result> procGetSectorDataByUserID(Nullable<int> userID)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetSectorDataByUserID_Result>("procGetSectorDataByUserID", userIDParameter);
        }
    
        public virtual int procGetSiteWiseDishList(string siteCode)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procGetSiteWiseDishList", siteCodeParameter);
        }
    
        public virtual ObjectResult<procGetUserModulePermission_Result> procGetUserModulePermission(Nullable<int> userID, string moduleCode)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("userID", userID) :
                new ObjectParameter("userID", typeof(int));
    
            var moduleCodeParameter = moduleCode != null ?
                new ObjectParameter("moduleCode", moduleCode) :
                new ObjectParameter("moduleCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetUserModulePermission_Result>("procGetUserModulePermission", userIDParameter, moduleCodeParameter);
        }
    
        public virtual ObjectResult<procGetUserModules_Result> procGetUserModules(Nullable<int> userID)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("userID", userID) :
                new ObjectParameter("userID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetUserModules_Result>("procGetUserModules", userIDParameter);
        }
    }
}
