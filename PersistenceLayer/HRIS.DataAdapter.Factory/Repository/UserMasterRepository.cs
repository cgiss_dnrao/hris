﻿using HRIS.DataAdapter.Base;
using HRIS.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using HRIS.DataAdapter.Factory.Contract.Transaction;
using HRIS.DataAdapter.Factory.Transaction;
using System.Security.Cryptography.X509Certificates;
using System.Data.Entity.Migrations;
using System;
using HRIS.DataAdapter.Factory.Contract.Common;

namespace HRIS.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IUserMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserMasterRepository : EFSynchronousRepository<int, UserMaster, HRCoreEntities1>,
        IUserMasterRepository
    {
        protected override DbSet<UserMaster> GetDbSet(HRCoreEntities1 entityContext)
        {
            return entityContext.UserMasters;
        }

        protected override UserMaster FindImpl(int key, DbSet<UserMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.UserId == key);
        }

        protected override UserMaster FindImplWithExpand(int key, DbSet<UserMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.UserId == key);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public procGetUserDataByUsername_Result GetUserDetailsByUsername(string username)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetUserDataByUsername(username).FirstOrDefault();
                }
            }
        }

    }
}
