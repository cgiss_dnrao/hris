﻿using HRIS.DataAdapter.Base;
using HRIS.DataAdapter.Factory.Contract;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using HRIS.DataAdapter.Factory.Contract.Common;
using HRIS.DataAdapter.Factory.Transaction;
using HRIS.DataAdapter.Factory.Contract.Transaction;

namespace HRIS.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IUserRoleMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserRoleMasterRepository : EFSynchronousRepository<int, UserRoleMaster, HRCoreEntities1>,
        IUserRoleMasterRepository
    {
        protected override DbSet<UserRoleMaster> GetDbSet(HRCoreEntities1 entityContext)
        {
            return entityContext.UserRoleMasters;
        }

        protected override UserRoleMaster FindImpl(int key, DbSet<UserRoleMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.UserRoleId == key);
        }

        protected override UserRoleMaster FindImplWithExpand(int key, DbSet<UserRoleMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.UserRoleId == key);
        }
    }
}
