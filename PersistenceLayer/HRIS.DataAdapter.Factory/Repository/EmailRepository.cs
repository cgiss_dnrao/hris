﻿using HRIS.Aspects.Constants;
using HRIS.Aspects.Factory;
using HRIS.DataAdapter.Base;
using HRIS.DataAdapter.Factory.Contract;
using HRIS.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using HRIS.Data.MasterData;
using HRIS.DataAdapter.Factory.Transaction;

namespace HRIS.DataAdapter.Factory.Repository.Common
{
    [Export(typeof(IEmailRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EmailRepository : EFSynchronousRepository<long, EmailNotification, HRCoreEntities1>,
       IEmailRepository
    {
        protected override DbSet<EmailNotification> GetDbSet(HRCoreEntities1 entityContext)
        {
            return entityContext.EmailNotifications;
        }

        protected override EmailNotification FindImpl(long key, DbSet<EmailNotification> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.EmailNotificationID == key);
        }

        protected override EmailNotification FindImplWithExpand(long key, DbSet<EmailNotification> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.EmailNotificationID == key);
        }

        public void SendPendingEmail()
        {
            bool isMailSent = false;
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    context.Configuration.LazyLoadingEnabled = false;

                    IEnumerable<EmailNotification> pendingEmails = context.EmailNotifications.Where(x => x.IsDelivered == false).ToList();


                    SmtpServerSetting smtpSetting = context.SmtpServerSettings.FirstOrDefault();

                    if (smtpSetting != null)
                    {
                        if (pendingEmails != null)
                        {
                            foreach (var email in pendingEmails)
                            {
                                isMailSent = false;
                                isMailSent = SendEmail(email, smtpSetting);

                                if (isMailSent) //Mail sent successfully
                                {
                                    var data = context.EmailNotifications.Where(m => m.EmailNotificationID == email.EmailNotificationID).FirstOrDefault();
                                    data.IsDelivered = true;
                                    context.SaveChanges();
                                }
                                else //Mail sent fail
                                {
                                }
                            }
                        }
                    }
                }


            }

        }

        private bool SendEmail(EmailNotification emailNotification, SmtpServerSetting smtpSetting)
        {
            bool isSuccess = false;
            string fileName = string.Empty;
            try
            {
                //Sets the from address for this e-mail message. 
                string fromEmail = string.Empty;
                string fromName = string.Empty;

                fromEmail = smtpSetting.FromEmail;
                fromName = smtpSetting.FromName;

                //Setup email header
                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress(fromEmail, fromName);

                mailMessage.To.Add(new MailAddress(emailNotification.ToEmail));

                // sets the message subject.
                mailMessage.Subject = emailNotification.Subject;

                if (emailNotification.CcEmail != "" && emailNotification.CcEmail != null)
                {
                    if (!string.IsNullOrEmpty(emailNotification.CcEmail))
                    {
                        string[] CCId = emailNotification.CcEmail.Split(',');

                        foreach (string CCEmail in CCId)
                        {
                            mailMessage.CC.Add(new MailAddress(CCEmail)); //Adding Multiple CC email Id
                        }
                    }
                    else
                    {
                        mailMessage.CC.Add(new MailAddress(emailNotification.CcEmail));
                    }
                }
                // sets the message body. 
                mailMessage.Body = emailNotification.EmailBody;

                    // sets a value indicating whether the mail message body is in Html. 
                    mailMessage.BodyEncoding = System.Text.Encoding.UTF8;
                    mailMessage.IsBodyHtml = true;


                    //Attach file
                    if (!string.IsNullOrEmpty(emailNotification.Attachment))
                    {
                        mailMessage.Attachments.Add(new Attachment(emailNotification.Attachment));
                    }

                    // SmtpClient Class Allows applications to send e-mail by using the Simple Mail Transfer Protocol (SMTP).
                    SmtpClient smtpClient = new SmtpClient();

                    //Specifies how email messages are delivered. Here Email is sent through the network to an SMTP server.
                    smtpClient.Credentials = new System.Net.NetworkCredential(smtpSetting.UserName, smtpSetting.Password);
                    smtpClient.Port = Convert.ToInt32(smtpSetting.PortNumber);
                    smtpClient.Host = smtpSetting.ServerName;
                    smtpClient.EnableSsl = smtpSetting.IsSsl.Value;

                    LogTraceFactory.WriteLogWithCategory("Smtp settings configured and mail is composed.", LogTraceCategoryNames.Tracing);
                    //Let's send it
                    smtpClient.Send(mailMessage);
                    isSuccess = true;
                    LogTraceFactory.WriteLogWithCategory("Mail is sent to the respective addresses.", LogTraceCategoryNames.Tracing);
                    // Do cleanup
                    mailMessage.Dispose();
                    smtpClient = null;
                    //if (!string.IsNullOrEmpty(fileName))
                    //    File.Delete(fileName);
                }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory("Exception occured while sending mail:" + ex.Message, LogTraceCategoryNames.Tracing);
                isSuccess = false;
                //    failedMessage = ex.Message;
                //if (!string.IsNullOrEmpty(fileName))
                //    File.Delete(fileName);
            }
            return isSuccess;
        }

        public EmailBody GetEmailDetails(string type)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.EmailBodies.Where(m => m.ModuleType == type).FirstOrDefault();
                }
            }
        }

        public SmtpServerSetting GetSmtpServerSettings()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.SmtpServerSettings.FirstOrDefault();
                }
            }
        }

    }
}
