﻿using HRIS.DataAdapter.Base;
using HRIS.DataAdapter.Factory.Contract;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Data.Entity;
using System.Linq;
using HRIS.Data.MasterData;
using HRIS.DataAdapter.Factory.Contract.Transaction;
using HRIS.DataAdapter.Factory.Transaction;
using System;
using System.Data.Entity.Migrations;
using HRIS.Data.Request;
using System.Data.Entity.Core.Objects;

namespace HRIS.DataAdapter.Factory.Repository.Transaction
{
    [Export(typeof(IHRISMasterRepository))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class HRISMasterRepositoy : EFSynchronousRepository<int, HRISMaster, HRCoreEntities1>,
        IHRISMasterRepository
    {
        protected override DbSet<HRISMaster> GetDbSet(HRCoreEntities1 entityContext)
        {
            return entityContext.HRISMasters;
        }

        protected override HRISMaster FindImpl(int key, DbSet<HRISMaster> dbSet)
        {
            return dbSet.FirstOrDefault(row => row.ID == key);
        }

        protected override HRISMaster FindImplWithExpand(int key, DbSet<HRISMaster> dbSet,
           string includeQueryPath)
        {
            return dbSet.Include(includeQueryPath).FirstOrDefault(row => row.ID == key);
        }

        public IList<procGetHRISMasterList_Result> GetHRISMasterDataList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetHRISMasterList().ToList();
                }
            }
        }
        public HRISMaster GetHRISMasterDataByCode(string empCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.HRISMasters.FirstOrDefault(a=>a.EmployeeCode==empCode);
                }
            }
        }

        public IList<procGetHRISMasterListPageWise_Result> GetHRISMasterDataListPageWise(EmployeeRequest employeeRequest, out int totalCount)
        {
            System.Data.Entity.Core.Objects.ObjectParameter myOutputParamInt = new System.Data.Entity.Core.Objects.ObjectParameter("totalrow", typeof(Int32));
            if (employeeRequest.SearchKeyword == null)
            {
                employeeRequest.SearchKeyword = "";
            }
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    var result = context.procGetHRISMasterListPageWise(employeeRequest.PageNumber, employeeRequest.PageSize, employeeRequest.SearchKeyword, employeeRequest.IsItiliteSearch, myOutputParamInt).ToList();
                    totalCount = (int)myOutputParamInt.Value;
                    return result;
                }
            }
        }

        public string SaveHRISMasterDataList(IList<HRISMaster> model, bool changeStatus = false)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    foreach (HRISMaster m in model)
                    {
                        var empDetails = context.HRISMasters.FirstOrDefault(e => e.EmployeeCode == m.EmployeeCode);

                        if (empDetails != null && !string.IsNullOrWhiteSpace(empDetails.EmployeeCode))
                        {
                            if (!changeStatus)
                            {
                                empDetails.ProfitCenter = m.ProfitCenter;
                                empDetails.CostCenter = m.CostCenter;
                                empDetails.LeadershipRoleCode = m.LeadershipRoleCode;
                                empDetails.ModifiedOn = DateTime.Now;
                                empDetails.ModifiedBy = m.ModifiedBy;
                                empDetails.SiteCode = m.SiteCode;
                                empDetails.SubSiteName = m.SubSiteName;
                            }
                            else
                            {
                                empDetails.IsActive = m.IsActive;
                            }
                        }
                        context.HRISMasters.AddOrUpdate(empDetails);
                    }
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<CostCenterMaster> GetCostCenterMasterDataList(string costCenter)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.CostCenterMasters.ToList();
                }
            }
        }

        public IList<ProfitCenterMaster> GetProfitCenterMasterDataList(string profitCenter)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.ProfitCenterMasters.ToList();
                }
            }
        }

        public string SaveProfitCenterDataList(ProfitCenterMaster model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    context.ProfitCenterMasters.AddOrUpdate(model);
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public string SaveCostCenterDataList(CostCenterMaster model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    context.CostCenterMasters.AddOrUpdate(model);
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<LeaderShipRoleMaster> GetLeaderShipRoleMasterDataList(string LeaderShipRoleMaster)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.LeaderShipRoleMasters.ToList();
                }
            }
        }

        public string SaveLeaderShipRoleDataList(LeaderShipRoleMaster model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    if (model.ID == 0)
                    {
                        var code = context.LeaderShipRoleMasters.OrderByDescending(z => z.LeadershipRoleCode).FirstOrDefault()?.LeadershipRoleCode;
                        if (code is null)
                            code = "EC-001";
                        else
                        {
                            var lcode = code.Substring(0, 2);
                            var rcode = code.Substring(3, 3);
                            code = lcode + "-" + (Convert.ToInt32(rcode) + 1).ToString().PadLeft(3, '0');
                        }
                        model.LeadershipRoleCode = code;
                    }
                    context.LeaderShipRoleMasters.AddOrUpdate(model);
                    context.SaveChanges();
                    return "true";
                }
            }
        }

        public IList<procGetTransferLeaderShipData_Result> GetTransferLeadershipRoleList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetTransferLeaderShipData().ToList();
                }
            }
        }

        public IList<procGetEXECOMUserDataList_Result> GetEXECOMUserDataList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetEXECOMUserDataList().ToList();
                }
            }
        }

        public IList<procGetSiteList_Result> GetSiteDataList(string SiteCode, string SectorCode)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetSiteList(SiteCode, SectorCode).ToList();
                }
            }
        }

        public string SaveSiteSubSiteMapping(List<SiteSubSiteMappingData> siteSubSiteMappingData)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    if (siteSubSiteMappingData != null && siteSubSiteMappingData.Count() > 0)
                    {
                        context.procDeActivateSiteSubSiteMapping(siteSubSiteMappingData[0].SiteCode, siteSubSiteMappingData[0].CreatedBy);
                        foreach (var model in siteSubSiteMappingData)
                        {
                            context.procSaveSiteSubSiteMapping(model.SiteCode, model.SubSiteName, model.CreatedBy, model.IsActive);
                        }
                    }
                    return "true";
                }
            }
        }
        //Krish 18-08-2022
        public IList<LocationDesignationMapping> GetLocationDesignationMappingDataList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.LocationDesignationMappings.ToList();
                }
            }
        }
        public IList<procGetUniqueLocationAndDesignation_Result> GetUniqueLocationAndDesignationDataList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetUniqueLocationAndDesignation().ToList();
                }
            }
        }


        public string SaveLocationDesignationData(string location, string designation)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    context.procUpdateLeaveEncashment(location, designation);
                    return "true";
                }
            }
        }
        //public string SaveLocationDesignationData(string location, string designation)
        //{
        //    using (var transaction = new EFTransaction())
        //    {
        //        using (var context = GetContext(transaction))
        //        {
        //            List<LocationDesignationMapping> getLDM = context.LocationDesignationMappings.Where(a => a.Location == location).ToList();
        //            var getMasters = context.HRISMasters.Where(a => a.Location == location ).ToList();
        //            var locationCode = getMasters[0].LocationCode;
        //            foreach (var master in getMasters)
        //            {
        //                master.MonthlyLeaveEncashment = false;
        //                if (master.IsActive.Value)
        //                {
        //                    master.LeaveEncashmentUpdatedOn = DateTime.Now;
        //                }
        //                else
        //                {
        //                    master.LeaveEncashmentUpdatedOn = null;
        //                }

        //                context.SaveChanges();
        //            }

                   
        //            if (getLDM.Count() > 0)
        //            {
        //                context.LocationDesignationMappings.Where(a => a.Location == location).ToList().ForEach(a => context.LocationDesignationMappings.Remove(a));
        //                context.SaveChanges();
        //            }

        //            if (designation.Contains(","))
        //            {
        //                var deisgList = designation.Split(',');
        //                foreach (var item in deisgList)
        //                {
        //                    if (!string.IsNullOrWhiteSpace(item))
        //                    {
        //                        var model = new LocationDesignationMapping();
        //                        model.Location = location;
        //                        model.LocationCode = locationCode;
        //                        model.Designation = item;
        //                        model.CreatedBy = 1;
        //                        model.CreatedOn = DateTime.Now;
        //                        context.LocationDesignationMappings.Add(model);
        //                        context.SaveChanges();

        //                        //Match with HRIS Master data
        //                        var getMaster = context.HRISMasters.Where(a => a.Location == location && a.Designation == item).ToList();
        //                        foreach (var master in getMaster)
        //                        {

        //                            master.MonthlyLeaveEncashment = true;
        //                            if (master.IsActive.Value)
        //                            {
        //                                master.LeaveEncashmentUpdatedOn = DateTime.Now;
        //                            }
        //                            else
        //                            {
        //                                master.LeaveEncashmentUpdatedOn = null;
        //                            }
        //                            context.SaveChanges();

        //                        }
        //                    }
        //                }
        //            }
        //            else
        //            {
        //                var model = new LocationDesignationMapping();
        //                model.Location = location;
        //                model.LocationCode = locationCode;
        //                model.Designation = designation;
        //                model.CreatedBy = 1;
        //                model.CreatedOn = DateTime.Now;

        //                context.LocationDesignationMappings.Add(model);
        //                context.SaveChanges();

        //                //Match with HRIS Master data
        //                var getMaster = context.HRISMasters.Where(a => a.Location == location && a.Designation == designation).ToList();
        //                foreach (var master in getMaster)
        //                {

        //                    master.MonthlyLeaveEncashment = true;
        //                    if (master.IsActive.Value)
        //                    {
        //                        master.LeaveEncashmentUpdatedOn = DateTime.Now;
        //                    }
        //                    else
        //                    {
        //                        master.LeaveEncashmentUpdatedOn = null;
        //                    }
        //                    context.SaveChanges();

        //                }
        //            }

        //            return "true";
        //        }
        //    }
        //}

        public IList<procGetSectorDepartmentLeadershipMapping_Result> GetSectorDepartmentLeadershipMapping()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetSectorDepartmentLeadershipMapping().ToList();
                }
            }
        }
        public string SaveSectorDepartmentLeadershipMapping(SectorDepartmentLeadershipMappingData model)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    var result = context.procSaveSectorDepartmentLeadershipMapping(model.ID, model.DepartmentCode, model.SectorCode, model.LeadershipRoleCode, model.IsActive, model.CreatedBy).FirstOrDefault().ToString();

                    return result;
                }
            }
        }

        public IList<CustomSelectListItem> GetSectorMasterList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.HRSectorMasters.Where(a => a.IsActive == true).Select(a => new CustomSelectListItem { Code = a.SectorCode, Name = a.Name }).ToList();
                }
            }
        }
        public IList<CustomSelectListItem> GetDepartmentMasterList()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.Departments.Where(a => a.IsActive == true).Select(a => new CustomSelectListItem { Code = a.DepartmentCode, Name = a.Name }).ToList();
                }
            }
        }

        //Save History
        public string SaveConsultantUploaderHistory(string status, string ecode, string userid,string batchnumb,string type)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    
                    //var result = context.batc(model.ID, model.DepartmentCode, model.SectorCode, model.LeadershipRoleCode, model.IsActive, model.CreatedBy).FirstOrDefault().ToString();
                    context.ConsultantUploaderBatchHeaders.Add(new ConsultantUploaderBatchHeader
                    {
                        BatchNumber = batchnumb,
                        BatchStatus = status== "Successfully read"?"Success":"Failed",
                        EmployeeCode = ecode,
                        Notes=status,
                        //ReadStatus = "True",
                        UploadedBy = userid,
                        UploadedOn = DateTime.Now,
                         UploaderType=type
                    });
                    context.SaveChanges();
                    //return result;
                }
                return "Saved";
            }
        }
        public IList<ConsultantUploaderBatchHeader> GetConsultantUploaderBatchHeader()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.ConsultantUploaderBatchHeaders.ToList();
                }
            }
        }
        public IList<procGetConsultantUploaderByBATCH_Result> GetConsultantUploaderByBatch(string template)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetConsultantUploaderByBATCH(template).ToList();
                }
            }
        }
        public IList<procGetConsultantUploaderBATCHDetails_Result> GetConsultantUploaderByBatchDetails(string template,string batchNo)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetConsultantUploaderBATCHDetails(template,batchNo).ToList();
                }
            }
        }
        public IList<procGetConsultantUploader_Result> GetConsultantUploaderData(string template)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetConsultantUploader(template).ToList();
                }
            }
        }
        public string UploadConsultantData(string xmlstring,string template,int userid, string historyBatchno)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {                   
                    ObjectParameter myOutputParamString = new ObjectParameter("result", typeof(string));
                    var result = context.procConsultantUploader(xmlstring,template,userid, historyBatchno, myOutputParamString);

                    return myOutputParamString.Value.ToString();
                    //return "Success";
                }
            }
        }

        public IList<procGetDiceUploaderGrid_Result> GetDiceUploaderData()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetDiceUploaderGrid().ToList();
                }
            }
        }
        public string UploadDiceUploaderData(string xmlstring, int userid)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    ObjectParameter myOutputParamString = new ObjectParameter("result", typeof(string));
                    var result = context.procDiceUploader(xmlstring,  userid, "", myOutputParamString);

                    return myOutputParamString.Value.ToString();
                    //return "Success";
                }
            }
        }

        public IList<procGetLensUploader_Result> GetLensUploaderData()
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    return context.procGetLensUploader().ToList();
                }
            }
        }
        public string UploadLensUploaderData(string xmlstring, int userid)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    ObjectParameter myOutputParamString = new ObjectParameter("result", typeof(string));
                    var result = context.procLensUploader(xmlstring, userid, "", myOutputParamString);

                    return myOutputParamString.Value.ToString();
                    //return "Success";
                }
            }
        }
        public string SaveLensData(string empcode,bool isactive, int modifiedby, DateTime modifieddate)
        {
            using (var transaction = new EFTransaction())
            {
                using (var context = GetContext(transaction))
                {
                    var getemployee = context.HRISMasters.FirstOrDefault(a => a.EmployeeCode == empcode);
                    getemployee.IsLensUser = isactive;
                    getemployee.ModifiedBy = modifiedby;
                    getemployee.ModifiedOn = modifieddate;
                    context.SaveChanges();
                    //return "Success";
                }
            }
            return "Success";
        }
    }
}
