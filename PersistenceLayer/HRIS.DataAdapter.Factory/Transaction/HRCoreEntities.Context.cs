﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace HRIS.DataAdapter.Factory.Transaction
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class HRCoreEntities1 : DbContext
    {
        public HRCoreEntities1()
            : base("name=HRCoreEntities1")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<Category> Categories { get; set; }
        public virtual DbSet<CategoryLog> CategoryLogs { get; set; }
        public virtual DbSet<CostCenterMaster> CostCenterMasters { get; set; }
        public virtual DbSet<EmailBody> EmailBodies { get; set; }
        public virtual DbSet<EmailNotification> EmailNotifications { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<ProfitCenterMaster> ProfitCenterMasters { get; set; }
        public virtual DbSet<SmtpServerSetting> SmtpServerSettings { get; set; }
        public virtual DbSet<SqlErrorLog> SqlErrorLogs { get; set; }
        public virtual DbSet<Tbl_ExceptionLoggingToDataBase> Tbl_ExceptionLoggingToDataBase { get; set; }
        public virtual DbSet<UserMaster> UserMasters { get; set; }
        public virtual DbSet<UserRoleMaster> UserRoleMasters { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<LeaderShipRoleMaster> LeaderShipRoleMasters { get; set; }
        public virtual DbSet<DepartmentLeaderShipRoleMapping> DepartmentLeaderShipRoleMappings { get; set; }
        public virtual DbSet<HRISMaster> HRISMasters { get; set; }
        public virtual DbSet<LocationDesignationMapping> LocationDesignationMappings { get; set; }
        public virtual DbSet<SectorDepartmentMapping> SectorDepartmentMappings { get; set; }
        public virtual DbSet<HRSectorMaster> HRSectorMasters { get; set; }
        public virtual DbSet<ConsultantUploaderBatchHeader> ConsultantUploaderBatchHeaders { get; set; }
    
        [DbFunction("HRCoreEntities1", "fnSplit")]
        public virtual IQueryable<fnSplit_Result> fnSplit(string commadelimitedString)
        {
            var commadelimitedStringParameter = commadelimitedString != null ?
                new ObjectParameter("CommadelimitedString", commadelimitedString) :
                new ObjectParameter("CommadelimitedString", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.CreateQuery<fnSplit_Result>("[HRCoreEntities1].[fnSplit](@CommadelimitedString)", commadelimitedStringParameter);
        }
    
        public virtual int ExceptionLoggingToDataBase(string exceptionMsg, string exceptionType, string exceptionSource)
        {
            var exceptionMsgParameter = exceptionMsg != null ?
                new ObjectParameter("ExceptionMsg", exceptionMsg) :
                new ObjectParameter("ExceptionMsg", typeof(string));
    
            var exceptionTypeParameter = exceptionType != null ?
                new ObjectParameter("ExceptionType", exceptionType) :
                new ObjectParameter("ExceptionType", typeof(string));
    
            var exceptionSourceParameter = exceptionSource != null ?
                new ObjectParameter("ExceptionSource", exceptionSource) :
                new ObjectParameter("ExceptionSource", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ExceptionLoggingToDataBase", exceptionMsgParameter, exceptionTypeParameter, exceptionSourceParameter);
        }
    
        public virtual int GetAPLMasterData(string merchandiseCode)
        {
            var merchandiseCodeParameter = merchandiseCode != null ?
                new ObjectParameter("MerchandiseCode", merchandiseCode) :
                new ObjectParameter("MerchandiseCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetAPLMasterData", merchandiseCodeParameter);
        }
    
        public virtual int GetAplMasterDataInformation()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetAplMasterDataInformation");
        }
    
        public virtual int GetCafeListBySiteCode(string siteCode)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetCafeListBySiteCode", siteCodeParameter);
        }
    
        public virtual int GetCafeMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetCafeMasterData");
        }
    
        public virtual ObjectResult<GetHRISMasterData_Result> GetHRISMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetHRISMasterData_Result>("GetHRISMasterData");
        }
    
        public virtual int GetSiteMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetSiteMasterData");
        }
    
        public virtual int GetVendorMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("GetVendorMasterData");
        }
    
        public virtual int procGetCafeList(string siteCode, string sectorNumber)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            var sectorNumberParameter = sectorNumber != null ?
                new ObjectParameter("SectorNumber", sectorNumber) :
                new ObjectParameter("SectorNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procGetCafeList", siteCodeParameter, sectorNumberParameter);
        }
    
        public virtual int procGetCPUList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procGetCPUList");
        }
    
        public virtual int procGetDKList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procGetDKList");
        }
    
        public virtual int procGetSectorDataByUserID(Nullable<int> userID)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procGetSectorDataByUserID", userIDParameter);
        }
    
        public virtual int procGetSiteWiseDishList(string siteCode)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procGetSiteWiseDishList", siteCodeParameter);
        }
    
        public virtual ObjectResult<Nullable<int>> SpAddCategory(string categoryName, Nullable<int> logID)
        {
            var categoryNameParameter = categoryName != null ?
                new ObjectParameter("categoryName", categoryName) :
                new ObjectParameter("categoryName", typeof(string));
    
            var logIDParameter = logID.HasValue ?
                new ObjectParameter("LogID", logID) :
                new ObjectParameter("LogID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SpAddCategory", categoryNameParameter, logIDParameter);
        }
    
        public virtual int SpClearLogs()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpClearLogs");
        }
    
        public virtual ObjectResult<Nullable<int>> SpInsertCategoryLog(Nullable<int> categoryID, Nullable<int> logID)
        {
            var categoryIDParameter = categoryID.HasValue ?
                new ObjectParameter("categoryID", categoryID) :
                new ObjectParameter("categoryID", typeof(int));
    
            var logIDParameter = logID.HasValue ?
                new ObjectParameter("logID", logID) :
                new ObjectParameter("logID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("SpInsertCategoryLog", categoryIDParameter, logIDParameter);
        }
    
        public virtual int SpWriteLog(Nullable<int> eventID, Nullable<int> priority, string severity, string title, Nullable<System.DateTime> timestamp, string machineName, string appDomainName, string processID, string processName, string threadName, string win32ThreadId, string message, string formattedMessage, ObjectParameter logId)
        {
            var eventIDParameter = eventID.HasValue ?
                new ObjectParameter("EventID", eventID) :
                new ObjectParameter("EventID", typeof(int));
    
            var priorityParameter = priority.HasValue ?
                new ObjectParameter("Priority", priority) :
                new ObjectParameter("Priority", typeof(int));
    
            var severityParameter = severity != null ?
                new ObjectParameter("Severity", severity) :
                new ObjectParameter("Severity", typeof(string));
    
            var titleParameter = title != null ?
                new ObjectParameter("Title", title) :
                new ObjectParameter("Title", typeof(string));
    
            var timestampParameter = timestamp.HasValue ?
                new ObjectParameter("Timestamp", timestamp) :
                new ObjectParameter("Timestamp", typeof(System.DateTime));
    
            var machineNameParameter = machineName != null ?
                new ObjectParameter("MachineName", machineName) :
                new ObjectParameter("MachineName", typeof(string));
    
            var appDomainNameParameter = appDomainName != null ?
                new ObjectParameter("AppDomainName", appDomainName) :
                new ObjectParameter("AppDomainName", typeof(string));
    
            var processIDParameter = processID != null ?
                new ObjectParameter("ProcessID", processID) :
                new ObjectParameter("ProcessID", typeof(string));
    
            var processNameParameter = processName != null ?
                new ObjectParameter("ProcessName", processName) :
                new ObjectParameter("ProcessName", typeof(string));
    
            var threadNameParameter = threadName != null ?
                new ObjectParameter("ThreadName", threadName) :
                new ObjectParameter("ThreadName", typeof(string));
    
            var win32ThreadIdParameter = win32ThreadId != null ?
                new ObjectParameter("Win32ThreadId", win32ThreadId) :
                new ObjectParameter("Win32ThreadId", typeof(string));
    
            var messageParameter = message != null ?
                new ObjectParameter("Message", message) :
                new ObjectParameter("Message", typeof(string));
    
            var formattedMessageParameter = formattedMessage != null ?
                new ObjectParameter("FormattedMessage", formattedMessage) :
                new ObjectParameter("FormattedMessage", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SpWriteLog", eventIDParameter, priorityParameter, severityParameter, titleParameter, timestampParameter, machineNameParameter, appDomainNameParameter, processIDParameter, processNameParameter, threadNameParameter, win32ThreadIdParameter, messageParameter, formattedMessageParameter, logId);
        }
    
        public virtual int usp_GetAplCategoryMasterList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_GetAplCategoryMasterList");
        }
    
        public virtual int usp_GetMerchandiseList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_GetMerchandiseList");
        }
    
        public virtual ObjectResult<Nullable<int>> USP_InsertSqlErrorLog(Nullable<int> processLogId, string sp_name, Nullable<int> error_number, Nullable<int> errorLine, string errormessage, Nullable<int> errorSeverity, Nullable<int> errorState)
        {
            var processLogIdParameter = processLogId.HasValue ?
                new ObjectParameter("ProcessLogId", processLogId) :
                new ObjectParameter("ProcessLogId", typeof(int));
    
            var sp_nameParameter = sp_name != null ?
                new ObjectParameter("sp_name", sp_name) :
                new ObjectParameter("sp_name", typeof(string));
    
            var error_numberParameter = error_number.HasValue ?
                new ObjectParameter("error_number", error_number) :
                new ObjectParameter("error_number", typeof(int));
    
            var errorLineParameter = errorLine.HasValue ?
                new ObjectParameter("errorLine", errorLine) :
                new ObjectParameter("errorLine", typeof(int));
    
            var errormessageParameter = errormessage != null ?
                new ObjectParameter("errormessage", errormessage) :
                new ObjectParameter("errormessage", typeof(string));
    
            var errorSeverityParameter = errorSeverity.HasValue ?
                new ObjectParameter("errorSeverity", errorSeverity) :
                new ObjectParameter("errorSeverity", typeof(int));
    
            var errorStateParameter = errorState.HasValue ?
                new ObjectParameter("errorState", errorState) :
                new ObjectParameter("errorState", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<int>>("USP_InsertSqlErrorLog", processLogIdParameter, sp_nameParameter, error_numberParameter, errorLineParameter, errormessageParameter, errorSeverityParameter, errorStateParameter);
        }
    
        public virtual int usp_UpdateAPLMaster(string aPLMaster)
        {
            var aPLMasterParameter = aPLMaster != null ?
                new ObjectParameter("APLMaster", aPLMaster) :
                new ObjectParameter("APLMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateAPLMaster", aPLMasterParameter);
        }
    
        public virtual int usp_UpdateCafeMasterData(string cafeMaster)
        {
            var cafeMasterParameter = cafeMaster != null ?
                new ObjectParameter("CafeMaster", cafeMaster) :
                new ObjectParameter("CafeMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateCafeMasterData", cafeMasterParameter);
        }
    
        public virtual int usp_UpdateHRISMasterData(string hRISMaster)
        {
            var hRISMasterParameter = hRISMaster != null ?
                new ObjectParameter("HRISMaster", hRISMaster) :
                new ObjectParameter("HRISMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateHRISMasterData", hRISMasterParameter);
        }
    
        public virtual int usp_UpdateSiteMasterData(string siteMaster)
        {
            var siteMasterParameter = siteMaster != null ?
                new ObjectParameter("SiteMaster", siteMaster) :
                new ObjectParameter("SiteMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateSiteMasterData", siteMasterParameter);
        }
    
        public virtual int usp_UpdateVendorMaster(string vendorMaster)
        {
            var vendorMasterParameter = vendorMaster != null ?
                new ObjectParameter("VendorMaster", vendorMaster) :
                new ObjectParameter("VendorMaster", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateVendorMaster", vendorMasterParameter);
        }
    
        public virtual ObjectResult<procGetUserDataByUsername_Result> procGetUserDataByUsername(string username)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("Username", username) :
                new ObjectParameter("Username", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetUserDataByUsername_Result>("procGetUserDataByUsername", usernameParameter);
        }
    
        public virtual ObjectResult<procGetTransferLeaderShipData_Result> procGetTransferLeaderShipData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetTransferLeaderShipData_Result>("procGetTransferLeaderShipData");
        }
    
        public virtual int usp_UpdateITLiteMasterData()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("usp_UpdateITLiteMasterData");
        }
    
        public virtual ObjectResult<procGetEXECOMUserDataList_Result> procGetEXECOMUserDataList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetEXECOMUserDataList_Result>("procGetEXECOMUserDataList");
        }
    
        public virtual int procSaveSiteSubSiteMapping(string siteCode, string subSiteName, Nullable<int> createdBy, Nullable<bool> isActive)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            var subSiteNameParameter = subSiteName != null ?
                new ObjectParameter("SubSiteName", subSiteName) :
                new ObjectParameter("SubSiteName", typeof(string));
    
            var createdByParameter = createdBy.HasValue ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(int));
    
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procSaveSiteSubSiteMapping", siteCodeParameter, subSiteNameParameter, createdByParameter, isActiveParameter);
        }
    
        public virtual ObjectResult<procGetSiteList_Result> procGetSiteList(string siteCode, string sectorNumber)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            var sectorNumberParameter = sectorNumber != null ?
                new ObjectParameter("SectorNumber", sectorNumber) :
                new ObjectParameter("SectorNumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetSiteList_Result>("procGetSiteList", siteCodeParameter, sectorNumberParameter);
        }
    
        public virtual int procDeActivateSiteSubSiteMapping(string siteCode, Nullable<int> modifiedBy)
        {
            var siteCodeParameter = siteCode != null ?
                new ObjectParameter("SiteCode", siteCode) :
                new ObjectParameter("SiteCode", typeof(string));
    
            var modifiedByParameter = modifiedBy.HasValue ?
                new ObjectParameter("ModifiedBy", modifiedBy) :
                new ObjectParameter("ModifiedBy", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procDeActivateSiteSubSiteMapping", siteCodeParameter, modifiedByParameter);
        }
    
        public virtual ObjectResult<procGetHRISMasterList_Result> procGetHRISMasterList()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetHRISMasterList_Result>("procGetHRISMasterList");
        }
    
        public virtual ObjectResult<procGetUniqueLocationAndDesignation_Result> procGetUniqueLocationAndDesignation()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetUniqueLocationAndDesignation_Result>("procGetUniqueLocationAndDesignation");
        }
    
        public virtual ObjectResult<procGetSectorDepartmentLeadershipMapping_Result> procGetSectorDepartmentLeadershipMapping()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetSectorDepartmentLeadershipMapping_Result>("procGetSectorDepartmentLeadershipMapping");
        }
    
        public virtual ObjectResult<string> procSaveSectorDepartmentLeadershipMapping(Nullable<int> iD, string departmentCode, string sectorCode, string leaderShipRoleCode, Nullable<bool> isActive, Nullable<int> createdBy)
        {
            var iDParameter = iD.HasValue ?
                new ObjectParameter("ID", iD) :
                new ObjectParameter("ID", typeof(int));
    
            var departmentCodeParameter = departmentCode != null ?
                new ObjectParameter("DepartmentCode", departmentCode) :
                new ObjectParameter("DepartmentCode", typeof(string));
    
            var sectorCodeParameter = sectorCode != null ?
                new ObjectParameter("SectorCode", sectorCode) :
                new ObjectParameter("SectorCode", typeof(string));
    
            var leaderShipRoleCodeParameter = leaderShipRoleCode != null ?
                new ObjectParameter("LeaderShipRoleCode", leaderShipRoleCode) :
                new ObjectParameter("LeaderShipRoleCode", typeof(string));
    
            var isActiveParameter = isActive.HasValue ?
                new ObjectParameter("IsActive", isActive) :
                new ObjectParameter("IsActive", typeof(bool));
    
            var createdByParameter = createdBy.HasValue ?
                new ObjectParameter("CreatedBy", createdBy) :
                new ObjectParameter("CreatedBy", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("procSaveSectorDepartmentLeadershipMapping", iDParameter, departmentCodeParameter, sectorCodeParameter, leaderShipRoleCodeParameter, isActiveParameter, createdByParameter);
        }
    
        public virtual ObjectResult<procGetConsultantUploaderByBATCH_Result> procGetConsultantUploaderByBATCH(string template)
        {
            var templateParameter = template != null ?
                new ObjectParameter("template", template) :
                new ObjectParameter("template", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetConsultantUploaderByBATCH_Result>("procGetConsultantUploaderByBATCH", templateParameter);
        }
    
        public virtual int procConsultantUploader(string xml, string template, Nullable<int> userid, string batchnumber, ObjectParameter result)
        {
            var xmlParameter = xml != null ?
                new ObjectParameter("xml", xml) :
                new ObjectParameter("xml", typeof(string));
    
            var templateParameter = template != null ?
                new ObjectParameter("template", template) :
                new ObjectParameter("template", typeof(string));
    
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            var batchnumberParameter = batchnumber != null ?
                new ObjectParameter("batchnumber", batchnumber) :
                new ObjectParameter("batchnumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procConsultantUploader", xmlParameter, templateParameter, useridParameter, batchnumberParameter, result);
        }
    
        public virtual ObjectResult<procGetConsultantUploader_Result> procGetConsultantUploader(string template)
        {
            var templateParameter = template != null ?
                new ObjectParameter("template", template) :
                new ObjectParameter("template", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetConsultantUploader_Result>("procGetConsultantUploader", templateParameter);
        }
    
        public virtual ObjectResult<procGetHRISMasterListPageWise_Result> procGetHRISMasterListPageWise(Nullable<int> pageNumber, Nullable<int> pageSize, string keyword, Nullable<bool> isItiliteSearch, ObjectParameter totalrow)
        {
            var pageNumberParameter = pageNumber.HasValue ?
                new ObjectParameter("PageNumber", pageNumber) :
                new ObjectParameter("PageNumber", typeof(int));
    
            var pageSizeParameter = pageSize.HasValue ?
                new ObjectParameter("PageSize", pageSize) :
                new ObjectParameter("PageSize", typeof(int));
    
            var keywordParameter = keyword != null ?
                new ObjectParameter("Keyword", keyword) :
                new ObjectParameter("Keyword", typeof(string));
    
            var isItiliteSearchParameter = isItiliteSearch.HasValue ?
                new ObjectParameter("IsItiliteSearch", isItiliteSearch) :
                new ObjectParameter("IsItiliteSearch", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetHRISMasterListPageWise_Result>("procGetHRISMasterListPageWise", pageNumberParameter, pageSizeParameter, keywordParameter, isItiliteSearchParameter, totalrow);
        }
    
        public virtual int procLensUploader(string xml, Nullable<int> userid, string batchnumber, ObjectParameter result)
        {
            var xmlParameter = xml != null ?
                new ObjectParameter("xml", xml) :
                new ObjectParameter("xml", typeof(string));
    
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            var batchnumberParameter = batchnumber != null ?
                new ObjectParameter("batchnumber", batchnumber) :
                new ObjectParameter("batchnumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procLensUploader", xmlParameter, useridParameter, batchnumberParameter, result);
        }
    
        public virtual ObjectResult<procGetLensUploader_Result> procGetLensUploader()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetLensUploader_Result>("procGetLensUploader");
        }
    
        public virtual int procDiceUploader(string xml, Nullable<int> userid, string batchnumber, ObjectParameter result)
        {
            var xmlParameter = xml != null ?
                new ObjectParameter("xml", xml) :
                new ObjectParameter("xml", typeof(string));
    
            var useridParameter = userid.HasValue ?
                new ObjectParameter("userid", userid) :
                new ObjectParameter("userid", typeof(int));
    
            var batchnumberParameter = batchnumber != null ?
                new ObjectParameter("batchnumber", batchnumber) :
                new ObjectParameter("batchnumber", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procDiceUploader", xmlParameter, useridParameter, batchnumberParameter, result);
        }
    
        public virtual ObjectResult<procGetConsultantUploaderBATCHDetails_Result> procGetConsultantUploaderBATCHDetails(string template, string batchNo)
        {
            var templateParameter = template != null ?
                new ObjectParameter("template", template) :
                new ObjectParameter("template", typeof(string));
    
            var batchNoParameter = batchNo != null ?
                new ObjectParameter("batchNo", batchNo) :
                new ObjectParameter("batchNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetConsultantUploaderBATCHDetails_Result>("procGetConsultantUploaderBATCHDetails", templateParameter, batchNoParameter);
        }
    
        public virtual int procUpdateLeaveEncashment(string location, string designation)
        {
            var locationParameter = location != null ?
                new ObjectParameter("Location", location) :
                new ObjectParameter("Location", typeof(string));
    
            var designationParameter = designation != null ?
                new ObjectParameter("Designation", designation) :
                new ObjectParameter("Designation", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("procUpdateLeaveEncashment", locationParameter, designationParameter);
        }
    
        public virtual ObjectResult<procGetDiceUploaderGrid_Result> procGetDiceUploaderGrid()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<procGetDiceUploaderGrid_Result>("procGetDiceUploaderGrid");
        }
    }
}
