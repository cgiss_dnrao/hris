﻿using HRIS.Data.MasterData;
using HRIS.DataAdapter.Base.Contracts;
using HRIS.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.DataAdapter.Factory.Contract
{
    public interface IEmailRepository : ICrudRepository<long, EmailNotification>
    {

        EmailBody GetEmailDetails(string type);

        SmtpServerSetting GetSmtpServerSettings();
    }
}
