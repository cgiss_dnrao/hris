﻿using HRIS.Data.MasterData;
using HRIS.Data.Request;
using HRIS.DataAdapter.Base.Contracts;
using HRIS.DataAdapter.Factory.Transaction;
using System;
using System.Collections.Generic;

namespace HRIS.DataAdapter.Factory.Contract.Transaction
{
    public interface IHRISMasterRepository : ICrudRepository<int, HRISMaster>
    {
        IList<procGetHRISMasterList_Result> GetHRISMasterDataList();
        IList<procGetHRISMasterListPageWise_Result> GetHRISMasterDataListPageWise(EmployeeRequest employeeRequest, out int totalCount);
        IList<procGetEXECOMUserDataList_Result> GetEXECOMUserDataList();
        IList<procGetTransferLeaderShipData_Result> GetTransferLeadershipRoleList();
        IList<CostCenterMaster> GetCostCenterMasterDataList(string CostCenter);
        IList<ProfitCenterMaster> GetProfitCenterMasterDataList(string ProfitCenter);
        IList<LeaderShipRoleMaster> GetLeaderShipRoleMasterDataList(string LeaderShipRoleMaster);
        IList<procGetSiteList_Result> GetSiteDataList(string SiteCode, string SectorCode);

        //Krish
        IList<LocationDesignationMapping> GetLocationDesignationMappingDataList();
        IList<procGetUniqueLocationAndDesignation_Result> GetUniqueLocationAndDesignationDataList();
        string SaveLocationDesignationData(string location, string designation);

        IList<procGetSectorDepartmentLeadershipMapping_Result> GetSectorDepartmentLeadershipMapping();
        string SaveSectorDepartmentLeadershipMapping(SectorDepartmentLeadershipMappingData model);


        string SaveHRISMasterDataList(IList<HRISMaster> model, bool changeStatus = false);
        string SaveProfitCenterDataList(ProfitCenterMaster model);
        string SaveCostCenterDataList(CostCenterMaster model);
        string SaveLeaderShipRoleDataList(LeaderShipRoleMaster model);
        string SaveSiteSubSiteMapping(List<SiteSubSiteMappingData> model);
        IList<CustomSelectListItem> GetSectorMasterList();
        IList<CustomSelectListItem> GetDepartmentMasterList();
        string SaveConsultantUploaderHistory(string status, string ecode, string userid, string batchnumb, string type);
        IList<ConsultantUploaderBatchHeader> GetConsultantUploaderBatchHeader();
        string UploadConsultantData(string xmlstring, string template, int userid, string historyBatchno);
        IList<procGetConsultantUploader_Result> GetConsultantUploaderData(string template);
        IList<procGetConsultantUploaderBATCHDetails_Result> GetConsultantUploaderByBatchDetails(string template, string batchNo);
        IList<procGetConsultantUploaderByBATCH_Result> GetConsultantUploaderByBatch(string template);
        HRISMaster GetHRISMasterDataByCode(string empCode);
        IList<procGetDiceUploaderGrid_Result> GetDiceUploaderData();
        string UploadDiceUploaderData(string xmlstring, int userid);
        IList<procGetLensUploader_Result> GetLensUploaderData();
        string UploadLensUploaderData(string xmlstring, int userid);
        string SaveLensData(string empcode, bool isactive, int modifiedby, DateTime modifieddate);
    }
}
