﻿using HRIS.DataAdapter.Base.Contracts;
using HRIS.DataAdapter.Factory.Transaction;
using System.Collections.Generic;

namespace HRIS.DataAdapter.Factory.Contract.Common
{
    public interface IUserMasterRepository : ICrudRepository<int, UserMaster>
    {
        procGetUserDataByUsername_Result GetUserDetailsByUsername(string username);
    }
}
