﻿using HRIS.DataAdapter.Base.Contracts;
using HRIS.DataAdapter.Factory.Transaction;

namespace HRIS.DataAdapter.Factory.Contract.Transaction
{
    public interface IUserRoleMasterRepository : ICrudRepository<int, UserRoleMaster>
    {
    }
}
