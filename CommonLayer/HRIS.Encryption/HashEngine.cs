﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Encryption
{
    [Export(typeof(IHashing))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class HashEngine : IHashing
    {
        private readonly string saltKey;

        public HashEngine()
        {
            if (ConfigurationManager.AppSettings["HashingSaltVector"] == null)
            {
                throw new ArgumentException("Null value found, argument value not specified. Class is not able to find AppSettings key 'EncryptionSaltVector' in config file of application. Provide key value in config file.");
            }
            saltKey = ConfigurationManager.AppSettings["EncryptionSaltVector"];
        }

        /// <summary>
        /// Method to compute hashing of plain text as per the Hashing algorithm name provided
        /// </summary>
        /// <param name="plainText">plain text</param>
        /// <param name="algorithm">algorithm name</param>
        /// <returns>returns hashed text</returns>
        public string ComputeHash(string plainText, HashEngineAlgorithm algorithm)
        {
            string hashValue = string.Empty;
            byte[] saltBytes = Encoding.ASCII.GetBytes(saltKey);
            
            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.ASCII.GetBytes(plainText);

            // Allocate array, which will hold plain text and salt.
            byte[] plainTextWithSaltBytes =
                    new byte[plainTextBytes.Length + saltBytes.Length];

            // Copy plain text bytes into resulting array.
            for (int i = 0; i < plainTextBytes.Length; i++)
            {
                plainTextWithSaltBytes[i] = plainTextBytes[i];
            }
            // Append salt bytes to the resulting array.
            for (int i = 0; i < saltBytes.Length; i++)
            {
                plainTextWithSaltBytes[plainTextBytes.Length + i] = saltBytes[i];
            }

            // Because we support multiple hashing algorithms, we must define
            // hash object as a common (abstract) base class. We will specify the
            // actual hashing algorithm class later during object creation.
            HashAlgorithm hash = null;
            try
            {
                // Initialize appropriate hashing algorithm class.
                switch (algorithm)
                {
                    case HashEngineAlgorithm.SHA1:
                        hash = new SHA1Managed();
                        break;

                    case HashEngineAlgorithm.SHA256:
                        hash = new SHA256Managed();
                        break;

                    case HashEngineAlgorithm.SHA384:
                        hash = new SHA384Managed();
                        break;

                    case HashEngineAlgorithm.SHA512:
                        hash = new SHA512Managed();
                        break;

                    case HashEngineAlgorithm.MD5:
                        hash = new MD5CryptoServiceProvider();
                        break;
                }

                // Compute hash value of our plain text with appended salt.
                byte[] hashBytes = hash.ComputeHash(plainTextWithSaltBytes);

                // Create array which will hold hash and original salt bytes.
                byte[] hashWithSaltBytes = new byte[hashBytes.Length +
                                                    saltBytes.Length];

                // Copy hash bytes into resulting array.
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    hashWithSaltBytes[i] = hashBytes[i];
                }

                // Append salt bytes to the result.
                for (int i = 0; i < saltBytes.Length; i++)
                {
                    hashWithSaltBytes[hashBytes.Length + i] = saltBytes[i];
                }

                // Convert result into a base64-encoded string.
                hashValue = Convert.ToBase64String(hashWithSaltBytes);
            }
            finally
            {
                if(hash != null)
                {
                    hash.Dispose();
                    hash = null;
                }
            }

            // Return the result.
            return hashValue;
        }

        /// <summary>
        /// Compares a hash of the specified plain text value to a given hash
        /// value. Plain text is hashed with the same salt value as the original
        /// hash.
        /// </summary>
        /// <param name="plainText">
        /// Plain text to be verified against the specified hash. The function
        /// does not check whether this parameter is null.
        /// </param>
        /// <param name="algorithm">
        /// Name of the hash algorithm. Allowed values are: "MD5", "SHA1", 
        /// "SHA256", "SHA384", and "SHA512" (if any other value is specified,
        /// MD5 hashing algorithm will be used). This value is case-insensitive.
        /// </param>
        /// <param name="hashValue">
        /// Base64-encoded hash value produced by ComputeHash function. This value
        /// includes the original salt appended to it.
        /// </param>
        /// <returns>
        /// If computed hash mathes the specified hash the function the return
        /// value is true; otherwise, the function returns false.
        /// </returns>
        public bool VerifyHash(string plainText, HashEngineAlgorithm algorithm, string hashValue)
        {
            // Convert base64-encoded hash value into a byte array.
            byte[] hashWithSaltBytes = Convert.FromBase64String(hashValue);
            // We must know size of hash (without salt).
            int hashSizeInBits = 0;
            int hashSizeInBytes = 0;

            switch (algorithm)
            {
                case HashEngineAlgorithm.SHA1:
                    hashSizeInBits = 160;
                    break;

                case HashEngineAlgorithm.SHA256:
                    hashSizeInBits = 256;
                    break;

                case HashEngineAlgorithm.SHA384:
                    hashSizeInBits = 384;
                    break;

                case HashEngineAlgorithm.SHA512:
                    hashSizeInBits = 512;
                    break;

                case HashEngineAlgorithm.MD5:
                    hashSizeInBits = 128;
                    break;
            }

            // Convert size of hash from bits to bytes.
            hashSizeInBytes = hashSizeInBits / 8;

            // Make sure that the specified hash value is long enough.
            if (hashWithSaltBytes.Length < hashSizeInBytes)
            {
                return false;
            }

            // Allocate array to hold original salt bytes retrieved from hash.
            byte[] saltBytes = new byte[hashWithSaltBytes.Length -
                                        hashSizeInBytes];

            // Copy salt from the end of the hash to the new array.
            for (int i = 0; i < saltBytes.Length; i++)
            {
                saltBytes[i] = hashWithSaltBytes[hashSizeInBytes + i];
            }

            // Compute a new hash string.
            string expectedHashString =
                        ComputeHash(plainText, algorithm);

            // If the computed hash matches the specified hash,
            // the plain text value must be correct.
            return (hashValue == expectedHashString);
        }
            
    }
}
