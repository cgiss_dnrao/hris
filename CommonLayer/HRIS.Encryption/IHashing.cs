﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Encryption
{
    public interface IHashing
    {
        /// <summary>
        /// Method to compute hashing of plain text as per the Hashing algorithm name provided
        /// </summary>
        /// <param name="plainText">plain text</param>
        /// <param name="algorithm">algorithm name</param>
        /// <returns>returns hashed text</returns>
        string ComputeHash(string plainText, HashEngineAlgorithm algorithm);

        /// <summary>
        /// Method to verify hash value against a plain text using hashing algorithm specified
        /// </summary>
        /// <param name="plainText">plain text</param>
        /// <param name="algorithm">algorithm</param>
        /// <param name="hashValue">hash value</param>
        /// <returns>returns boolean status</returns>
        bool VerifyHash(string plainText, HashEngineAlgorithm algorithm, string hashValue);
    }
}
