﻿using HRIS.Encryption.Interfaces;
using System;
using System.ComponentModel.Composition;
using System.Security.Cryptography;
using System.Text;

namespace HRIS.Encryption.EncryptImpl
{
    [Export(typeof(ISha256CryptoEngine))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class Sha256CryptoEngine : ISha256CryptoEngine
    {
        #region Public Methods

        /// <summary>
        /// this overloaded method will encrypt the Data based on encryptionKey
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public string EncryptData(string plainText, String key = "")
        {
            // Convert plain text into a byte array.
            byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

            HashAlgorithm hash;

            // Initialize appropriate hashing algorithm class.
            hash = new SHA256Managed();

            // Compute hash value of our plain text with appended salt.
            byte[] hashBytes = hash.ComputeHash(plainTextBytes);

            // Convert result into a base64-encoded string.
            string hashValue = Convert.ToBase64String(hashBytes);

            // Return the result.
            return hashValue;
        }

        #endregion
    }
}
