﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Encryption.Providers
{
    internal class DESCryptoProvider : SymmetricEncryptTransformer
    {

        internal override ICryptoTransform GetCryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (DES des = new DESCryptoServiceProvider())
            {

                des.Mode = CipherMode.CBC;
                // See if a key was provided
                if (null == bytesKey)
                {
                    encKey = des.Key;
                }
                else
                {
                    des.Key = bytesKey;
                    encKey = des.Key;
                }

                // See if the client provided an initialization vector
                if (null == vector)
                { // Have the algorithm create one
                    vector = des.IV;
                }
                else
                { //No, give it to the algorithm
                    des.IV = vector;
                }
                cryptoTransform = des.CreateEncryptor();
            }
            return cryptoTransform;
        }

        internal override ICryptoTransform GetDecryptoServiceProvider(byte[] bytesKey, byte[] initVector)
        {
            ICryptoTransform cryptoTransform = null;
            using (DES des = new DESCryptoServiceProvider())
            {
                des.Mode = CipherMode.CBC;
                des.Key = bytesKey;
                des.IV = initVector;

                cryptoTransform = des.CreateEncryptor();
            }
            return cryptoTransform;
        }
    }
}
