﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Encryption.Interfaces
{
    public interface ICompassEncryption
    {
        /// <summary>
        /// this overloaded method will encrypt the Data based on encryptionKey
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        string EncryptData(string plainText, String key = "");

        /// <summary>
        /// this overloaded method will decrypt the data based on decryptionKey
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        string DecryptData(string cipherText, String key = "");
    }
}
