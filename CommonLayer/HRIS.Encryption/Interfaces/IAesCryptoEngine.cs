﻿using System;

namespace HRIS.Encryption.Interfaces
{
    public interface IAesCryptoEngine
    {
        /// <summary>
        /// this overloaded method will encrypt the Data based on encryptionKey
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        string EncryptData(string plainText, String key = "");

        /// <summary>
        /// this overloaded method will decrypt the data based on decryptionKey
        /// </summary>
        /// <param name="cipherText"></param>
        /// <returns></returns>
        string DecryptData(string cipherText, String key = "");
    }
}
