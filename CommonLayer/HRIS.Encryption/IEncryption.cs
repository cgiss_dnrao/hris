﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Encryption
{
    public interface IEncryption
    {
        /// <summary>
        /// Method to encrypt the plain text by using the algorithm option passed as a parameter
        /// </summary>
        /// <param name="plainText">plain text to encrypt</param>
        /// <param name="algorithm">encryption algorithm</param>
        /// <returns>returns encrypted text</returns>
        string Encrypt(string plainText, SymmetricEncryptionAlgorithm algorithm);

        /// <summary>
        /// Method to encrypt the plain text by using the algorithm option passed as a parameter
        /// </summary>
        /// <param name="encryptedText">ciphered text to decrypt</param>
        /// <param name="algorithm">encryption algorithm</param>
        /// <returns>returns plain text</returns>
        string Decrypt(string encryptedText, SymmetricEncryptionAlgorithm algorithm);

        /// <summary>
        /// Method to encrypt the file 
        /// </summary>
        /// <param name="inputFileName">input file physical path</param>
        /// <param name="outputFileName">output file physical path</param>
        /// <param name="fileKey">encryption key, if not then call method GenerateKey</param>
        void EncryptFile(string inputFileName, string outputFileName, string fileKey);

        /// <summary>
        /// Method to decrypt the file
        /// </summary>
        /// <param name="inputFileName">input file physical path</param>
        /// <param name="outputFileName">output file physical path</param>
        /// <param name="fileKey">stored encryption key</param>
        void DecryptFile(string inputFileName, string outputFileName, string fileKey);

        /// <summary>
        /// This method generates the key required for the file encryption, store this key as safe place to decrypt the file, 
        /// if there is any mismatch in key characters at the time of decryption then file can be gibberish
        /// </summary>
        /// <returns>returns key</returns>
        string GenerateFileEncryptionKey();
    }
}
