﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using HRIS.Encryption.Providers;

namespace HRIS.Encryption
{
    [Export(typeof(IEncryption))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EncryptionEngine : IEncryption, IDisposable
    {
        private const string DES_INIT_VECTOR = "init vec";
        private const string RIJNDL_INIT_VECTOR = "init vec is big.";
        private const string TRIPLEDES_INIT_VECTOR = "init vec is big.";
        private readonly string saltKey;
        
        private byte[] iv = null;
        private byte[] cipherText = null;
        private byte[] key = null;
        private SymmetricEncryptTransformer encryptor = null;

        //  Call this function to remove the key from memory after use for security.
        [System.Runtime.InteropServices.DllImport("KERNEL32.DLL", EntryPoint = "RtlZeroMemory")]
        public static extern bool ZeroMemory(ref string destination, int length);

        public EncryptionEngine()
        {
            if (ConfigurationManager.AppSettings["EncryptionSaltVector"] == null)
            {
                throw new ArgumentException("Null value found, argument value not specified. Class is not able to find AppSettings key 'EncryptionSaltVector' in config file of application. Provide key value in config file.");
            }
            saltKey = ConfigurationManager.AppSettings["EncryptionSaltVector"];
        }

        //public EncryptionEngine(SymmetricEncryptionAlgorithm algorithm)
        //{
        //    if (ConfigurationManager.AppSettings["EncryptionSaltVector"] == null)
        //    {
        //        throw new ArgumentException("Null value found, argument value not specified. Class is not able to find AppSettings key 'EncryptionSaltVector' in config file of application. Provide key value in config file.");
        //    }
        //    saltKey = ConfigurationManager.AppSettings["EncryptionSaltVector"];
        //}

        /// <summary>
        /// Method to encrypt the plain text by using the algorithm option passed as a parameter
        /// </summary>
        /// <param name="plainText">plain text to encrypt</param>
        /// <param name="algorithm">encryption algorithm</param>
        /// <returns>returns encrypted text</returns>
        public string Encrypt(string plainText, SymmetricEncryptionAlgorithm algorithm)
        {
            string encryptedText = string.Empty;
            byte[] plainTextBytes = Encoding.ASCII.GetBytes(plainText);
            key = Encoding.ASCII.GetBytes(saltKey);
            GetCryptoProvider(algorithm);
            encryptor.IV = iv;
            cipherText = encryptor.Encrypt(plainTextBytes, key, iv);
            encryptedText = Convert.ToBase64String(cipherText);
            return encryptedText;
        }

        /// <summary>
        /// Method to encrypt the plain text by using the algorithm option passed as a parameter
        /// </summary>
        /// <param name="encryptedText">ciphered text to decrypt</param>
        /// <param name="algorithm">encryption algorithm</param>
        /// <returns>returns plain text</returns>
        public string Decrypt(string encryptedText, SymmetricEncryptionAlgorithm algorithm)
        {
            string plainText = string.Empty;
            byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);
            key = Encoding.ASCII.GetBytes(saltKey);
            GetCryptoProvider(algorithm);
            encryptor.IV = iv;
            cipherText = encryptor.Decrypt(encryptedTextBytes, key, iv);
            plainText = Encoding.ASCII.GetString(cipherText);
            return plainText;
        }

        /// <summary>
        /// Method to encrypt the file 
        /// </summary>
        /// <param name="inputFileName">input file physical path</param>
        /// <param name="outputFileName">output file physical path</param>
        /// <param name="fileKey">encryption key, if not then call method GenerateFileEncryptionKey</param>
        public void EncryptFile(string inputFileName, string outputFileName, string fileKey)
        {
            FileStream streamInput = null;
            FileStream streamEncrypted = null;
            DESCryptoServiceProvider cryptoProvider = null;
            try
            {
                streamInput = new FileStream(inputFileName, FileMode.Open, FileAccess.Read);


                streamEncrypted = new FileStream(outputFileName, FileMode.Create, FileAccess.Write);


                cryptoProvider = new DESCryptoServiceProvider();
                cryptoProvider.Padding = PaddingMode.None;
                cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes(fileKey);
                cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes(fileKey);

                ICryptoTransform desencrypt = cryptoProvider.CreateEncryptor();
                CryptoStream cryptostream = new CryptoStream(streamEncrypted,
                                    desencrypt,
                                    CryptoStreamMode.Write);

                byte[] byteArrayInput = new byte[streamInput.Length - 1];
                streamInput.Read(byteArrayInput, 0, byteArrayInput.Length);
                cryptostream.Write(byteArrayInput, 0, byteArrayInput.Length);

                streamEncrypted.Flush();
                streamInput.Flush();
            }
            finally
            {
                if (streamEncrypted != null)
                {
                    streamEncrypted.Dispose();
                    streamEncrypted = null;
                }
                if (streamInput != null)
                {
                    streamInput.Dispose();
                    streamInput = null;
                }
                if (cryptoProvider != null)
                {
                    cryptoProvider.Dispose();
                    cryptoProvider = null;
                }
            }

        }

        /// <summary>
        /// Method to decrypt the file
        /// </summary>
        /// <param name="inputFileName">input file physical path</param>
        /// <param name="outputFileName">output file physical path</param>
        /// <param name="fileKey">stored encryption key</param>
        public void DecryptFile(string inputFileName, string outputFileName, string fileKey)
        {
            FileStream streamRead = null;
            StreamWriter streamDecrypted = null;
            DESCryptoServiceProvider cryptoProvider = null;
            try
            {
                cryptoProvider = new DESCryptoServiceProvider();
                cryptoProvider.Padding = PaddingMode.None;
                //A 64 bit key and IV is required for this provider.
                //Set secret key For DES algorithm.
                cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes(fileKey);
                //Set initialization vector.
                cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes(fileKey);

                //Create a file stream to read the encrypted file back.
                streamRead = new FileStream(inputFileName, FileMode.Open, FileAccess.Read);

                //Create a DES decryptor from the DES instance.
                ICryptoTransform desdecrypt = cryptoProvider.CreateDecryptor();
                //Create crypto stream set to read and do a 
                //DES decryption transform on incoming bytes.
                CryptoStream cryptostreamDecr = new CryptoStream(streamRead,desdecrypt,CryptoStreamMode.Read);
                //Print the contents of the decrypted file.
                streamDecrypted = new StreamWriter(outputFileName);
                streamDecrypted.Write(new StreamReader(cryptostreamDecr).ReadToEnd());
                streamDecrypted.Flush();
                streamRead.Flush();
 
            }
            finally
            {
                if (streamDecrypted != null)
                {
                    streamDecrypted.Dispose();
                    streamDecrypted = null;
                }
                if (streamRead != null)
                {
                    streamRead.Dispose();
                    streamRead = null;
                }
                if (cryptoProvider != null)
                {
                    cryptoProvider.Dispose();
                    cryptoProvider = null;
                }
            }
        }

        /// <summary>
        /// This method generates the key required for the file encryption, store this key as safe place to decrypt the file, 
        /// if there is any mismatch in key characters at the time of decryption then file can be gibberish
        /// </summary>
        /// <returns>returns key</returns>
        public string GenerateFileEncryptionKey()
        {
            string encryptionKey = string.Empty;
            // Create an instance of Symetric Algorithm. Key and IV is generated automatically.
            using (DESCryptoServiceProvider desCrypto = (DESCryptoServiceProvider)DESCryptoServiceProvider.Create())
            {
                // Use the Automatically generated key for Encryption. 
                encryptionKey = ASCIIEncoding.ASCII.GetString(desCrypto.Key);
            }
            return encryptionKey;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (encryptor != null)
                {
                    encryptor.Dispose();
                }
            }
        }

        #region Private Methods



        /// <summary>
        /// Method to get encryption/decryption crypto provider
        /// </summary>
        /// <param name="algorithm"></param>
        private void GetCryptoProvider(SymmetricEncryptionAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case SymmetricEncryptionAlgorithm.DES:
                    if (DES_INIT_VECTOR.Length > 8)
                    {
                        throw new NotSupportedException("Des encryption only works with an 8 byte key. Provide key 'EncryptionSaltVector' value in AppSettings which is of length 8");
                    }
                    encryptor = new DESCryptoProvider();
                    iv = Encoding.ASCII.GetBytes(DES_INIT_VECTOR);

                    break;
                case SymmetricEncryptionAlgorithm.RC2:
                    if (DES_INIT_VECTOR.Length > 8)
                    {
                        throw new NotSupportedException("Des encryption only works with an 8 byte key. Provide key 'EncryptionSaltVector' value in AppSettings which is of length 8");
                    }
                    encryptor = new RC2CryptoProvider();
                    iv = Encoding.ASCII.GetBytes(DES_INIT_VECTOR);

                    break;
                case SymmetricEncryptionAlgorithm.Rijndael:
                    if (RIJNDL_INIT_VECTOR.Length != 16)
                    {
                        throw new NotSupportedException("Rijndael encryption only works with an 16 byte key. Provide key 'EncryptionSaltVector' value in AppSettings which is of length 16");
                    }
                    encryptor = new RijndaelCryptoProvider();
                    iv = Encoding.ASCII.GetBytes(RIJNDL_INIT_VECTOR);

                    break;
                case SymmetricEncryptionAlgorithm.TripleDES:
                    if (TRIPLEDES_INIT_VECTOR.Length == 16 || TRIPLEDES_INIT_VECTOR.Length == 24)
                    {
                        encryptor = new TripleDesCryptoProvider();
                        iv = Encoding.ASCII.GetBytes(DES_INIT_VECTOR);
                    }
                    else
                    {
                        throw new NotSupportedException("TripleDES encryption only works with an 16 or 24 byte key. Provide key 'EncryptionSaltVector' value in AppSettings which is of length 16 or 24");
                    }

                    break;
            }
        }

        #endregion
    }
}
