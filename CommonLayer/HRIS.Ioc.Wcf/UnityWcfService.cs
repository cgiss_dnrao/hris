﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;

namespace HRIS.Ioc.Wcf
{
    public abstract class UnityWcfService
    {
        public UnityWcfService()
        {
        }

        private IUnityContainer container;
        [Dependency]
        public IUnityContainer Container
        {
            get
            {
                if (this.container == null)
                {
                    var host = OperationContext.Current.Host as UnityServiceHost;
                    if (host != null)
                    {
                        this.container = host.Container;
                    }
                }
                return this.container;
            }
            set
            {
                this.container = value;
            }
        }
    }
}
