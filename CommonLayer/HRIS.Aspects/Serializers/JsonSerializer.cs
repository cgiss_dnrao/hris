﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Aspects.Serializers
{
    public static class JsonSerializer
    {
        public static string Serialize<T>(this T model)
        {
            MemoryStream stream = null;
            StreamReader reader = null;
            string jsonString = null;
            //try
            //{
                using (stream = new MemoryStream())
                {
                    DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                    ser.WriteObject(stream, model);
                    stream.Position = 0;
                    reader = new StreamReader(stream);
                    jsonString = reader.ReadToEndAsync().Result;
                }
            //}
            //finally
            //{
            //    //if (stream != null)
            //    //{
            //    //    stream.Dispose();
            //    //}
            //    //if (reader != null)
            //    //{
            //    //    reader.Dispose();
            //    //}
            //}
            return jsonString;
        }

        public static T DeSerialize<T>(this string jsonValue)
        {
            using (MemoryStream stream = new MemoryStream(GetBytes(jsonValue)))
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(T));
                stream.Position = 0;
                T model = (T)ser.ReadObject(stream);
                return model;

            }
        }

        private static byte[] GetBytes(string value)
        {
            byte[] bytes = new byte[value.Length * sizeof(char)];
            System.Buffer.BlockCopy(value.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        
    }
}
