﻿using System;
using System.Runtime.Remoting.Messaging;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using HRIS.Aspects.Constants;

namespace HRIS.Aspects.Factory
{
    public class LogTraceFactory
    {
        private static LogWriter defaultLogWriter;
        private delegate void WriteLogWithCategoryAsync(string logMessage, LogTraceCategoryNames category);
        private static object sync = new object();
        private static string queuePath = string.Empty;

        public static void InitializeLoggingService()
        {
            try
            {
                DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
                LogWriterFactory factory = new LogWriterFactory();
                //get the immediate application log writer and trace manager instance from 
                defaultLogWriter = factory.Create();
                Logger.SetLogWriter(defaultLogWriter);
                IsLoggerActive = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool IsLoggerActive { get; private set; }

        /// <summary>
        /// Method to log messages with log category mentioned
        /// </summary>
        /// <param name="message">message to log</param>
        /// <param name="category">log category</param>
        public static void LogAsync(string message, LogTraceCategoryNames category)
        {
            WriteLogWithCategoryAsync logDefinition = new WriteLogWithCategoryAsync(WriteLogWithCategory);
            AsyncCallback logDefinitionWithCategoryCompletedCallBack = new AsyncCallback(LogDefinitionWithCategoryCompletedCallBack);
            lock (sync)
            {
                logDefinition.BeginInvoke(message, category, logDefinitionWithCategoryCompletedCallBack, null);
            }
        }

        /// <summary>
        /// Method to write log information on the basis of category and message provided
        /// </summary>
        /// <param name="logMessage">log message</param>
        /// <param name="category">log category</param>
        public static void WriteLogWithCategory(string logMessage, LogTraceCategoryNames category)
        {
            if(!IsLoggerActive)
            {
                InitializeLoggingService();
            }
            if (defaultLogWriter != null)
            {
                defaultLogWriter.Write(logMessage, category.ToString());
            }

        }

        /// <summary>
        /// Method to handle the log with category completed call back method
        /// </summary>
        /// <param name="result">result obtained</param>
        private static void LogDefinitionWithCategoryCompletedCallBack(IAsyncResult result)
        {
            //Added this line of code to resolve unused variable issue of SONAR
            result = null;  
        }
    }
}
