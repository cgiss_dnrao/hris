﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Aspects.Factory
{
    public class DatabaseThrownException : Exception
    {
        private int number;
        //public DatabaseThrownException()
        //{

        //}

        internal DatabaseThrownException(string message, int errorNumber)
            : base(message)
        {
            this.number = errorNumber;
        }

        //internal DatabaseThrownException(string message, Exception inner)
        //    : base(message, inner)
        //{

        //}

        public int ErrorNumber
        {
            get
            {
                return number;
            }
            //private set
            //{
            //    number = value;
            //}
        }
    }
}
