﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.Unity.InterceptionExtension;
using HRIS.Aspects.Constants;
using HRIS.Aspects.Factory;
using HRIS.Aspects.Utils;

namespace HRIS.Aspects.Behaviors
{
    public class ExceptionBehavior : IInterceptionBehavior
    {
        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return (Type.EmptyTypes);
        }

        public IMethodReturn Invoke(IMethodInvocation input, GetNextInterceptionBehaviorDelegate getNext)
        {
            IMethodReturn methodReturn = null;
            if (getNext() != null && input != null)
            {
                bool isLogging = XmlTextSerializer.GetAppSettings(ConfigKeys.MethodLevelLoggingEnabled) == "1";
                string methodName = input.MethodBase.Name;
                if (isLogging)
                {
                    LogTraceFactory.WriteLogWithCategory(string.Format("Method {0} called at time {1}", methodName, DateTime.Now), LogTraceCategoryNames.General);
                    LogTraceFactory.WriteLogWithCategory(GetMethodInputs(input), LogTraceCategoryNames.General);
                }

                methodReturn = getNext().Invoke(input, getNext);
                if (methodReturn.Exception != null && !methodReturn.Exception.StackTrace.Contains("Microsoft.Practices.EnterpriseLibrary.ExceptionHandling"))
                {
                    ExceptionFactory.AppExceptionManager.HandleException(methodReturn.Exception, ExceptionPolicyNames.AssistingAdministrators.ToString());
                }

                if (isLogging)
                {
                    LogTraceFactory.WriteLogWithCategory(string.Format("Method {0} executed successfully at time {1}", methodName, DateTime.Now), LogTraceCategoryNames.DiskFiles);
                }
            }
            return methodReturn;
        }

        public bool WillExecute
        {
            get { return true; }
        }

        private static string GetMethodInputs(IMethodInvocation input)
        {
            var arguments = string.Empty;
            try
            {
                for (int i = 0; i < input.Arguments.Count; ++i)
                {
                    var paramType = input.Arguments.GetParameterInfo(i).ParameterType.Name.ToLower();
                    // in that case object definition need to start with "Obj*"
                    if (!paramType.StartsWith("obj"))
                    {
                        arguments += input.Arguments.GetParameterInfo(i).Name + " - " + input.Arguments[i] + " | ";
                    }
                    else
                    {
                        foreach (var value in input.Arguments)
                        {
                            PropertyInfo[] properties = value.GetType().GetProperties();

                            arguments += properties.Aggregate(arguments, (current, pi) => current + (pi.Name + " - " + pi.GetValue(value, null) + " | "));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.DiskFiles);
                throw;
            }
            return arguments;
        }

    }
}
