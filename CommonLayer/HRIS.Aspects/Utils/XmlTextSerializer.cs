﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRIS.Aspects.Constants;
using HRIS.Aspects.Factory;

namespace HRIS.Aspects.Utils
{
    public class XmlTextSerializer
    {
        /// <summary>
        /// Method to get the Application Configuration Settings value using key name of settings
        /// </summary>
        /// <param name="key">string Key</param>
        /// <returns>returns application settings value</returns>
        public static string GetAppSettings(string key)
        {
            string value = string.Empty;
            try
            {
                value = ConfigurationManager.AppSettings[key];
            }
            catch (Exception ex)
            {
                value = string.Empty;
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.General);
                throw;

            }
            return value;
        }

        /// <summary>
        /// Method to get the Application Configuration Settings value using key name of settings
        /// </summary>
        /// <param name="key">config key name enum</param>
        /// <returns>returns application settings value</returns>
        public static string GetAppSettings(ConfigKeys key)
        {
            string value = string.Empty;
            try
            {
                value = ConfigurationManager.AppSettings[key.ToString()];
            }
            catch (Exception ex)
            {
                value = string.Empty;
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.General);
                throw;
            }
            return value;
        }
    }
}
