﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Aspects.Constants
{
    public enum ApiTypes
    {
        None = 0,
        APL = 1,
        Site = 2,
        Vendor = 3,
    }
}
