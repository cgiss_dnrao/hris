﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Aspects.Security
{
    [Export(typeof(IEncryption))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EncryptionManager : IEncryption
    {

    }
}
