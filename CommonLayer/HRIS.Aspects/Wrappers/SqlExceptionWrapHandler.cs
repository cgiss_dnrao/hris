﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Configuration;
using HRIS.Aspects.Factory;

namespace HRIS.Aspects.Wrappers
{
    /// <summary>
    /// Represents the configuration data for a <see cref="WrapHandler"/>.
    /// </summary>	
    //[Assembler(typeof(SqlExceptionWrapHandlerAssembler))]
    [ConfigurationElementType(typeof(CustomHandlerData))]
    public class SqlExceptionWrapHandler : IExceptionHandler
    {
        private const string ERRORCODESPROPERTY = "errorCodes";
        private const string EXCEPTIONMESSAGEPROPERTY = "defaultExceptionMessage";
        private const string WRAPEXCEPTIONTYPEPROPERTY = "defaultWrapExceptionType";
        /// <summary>
        /// Represents the error code returned from stored procedure when entity could not be found.
        /// </summary>
        private const int SQL_ERROR_CODE_ENTITY_NOT_FOUND = 242;

        /// <summary>
        /// Represents the error code returned from stored procedure when entity to be updated has time mismatch.
        /// </summary>
        private const int SQL_ERROR_CODE_TIME_MISMATCH = 50002;


        /// <summary>
        /// Represents the error code returned from stored procedure when a persistence exception occurs (ex.
        /// billing flag is invalid, child records exist which prevent a delete, etc.).
        /// </summary>
        private const int SQL_ERROR_CODE_PERSISTENCE_ERROR = 50003;

        public SqlExceptionWrapHandler(NameValueCollection configValues)
        {
            configValues = null;

        }

        public Exception HandleException(Exception exception, Guid handlingInstanceId)
        {
            SqlException sqlError = exception as SqlException;

            if (sqlError != null)
            {
                int number = sqlError.Number;
                if (number == 2601)
                {
                    // Do something.
                }
                else
                {
                    //throw;
                    //exception = new Exception(string.Format("Database Error: {0},Message: {1}", number,exception.Message));
                    exception = new DatabaseThrownException(exception.Message, number) { };
                }
            }
            else
            {
                DbEntityValidationException entityError = exception as DbEntityValidationException;
                if (entityError != null)
                {
                    var errorMessages = entityError.EntityValidationErrors
                                    .SelectMany(x => x.ValidationErrors)
                                    .Select(x => x.ErrorMessage);

                    // Join the list to a single string.
                    var fullErrorMessage = string.Join("; ", errorMessages);

                    // Combine the original exception message with the new one.
                    var exceptionMessage = string.Concat(entityError.Message, " The validation errors are: ", fullErrorMessage);

                    // Throw a new DbEntityValidationException with the improved exception message.
                    throw new DbEntityValidationException(exceptionMessage, entityError.EntityValidationErrors);
                }
                else
                {
                    throw new ExceptionHandlingException("The SqlExceptionWrapHandler can only operate on SqlException types.");
                }
            }
            return exception;
        }
    }
}
