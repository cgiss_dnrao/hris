﻿using Microsoft.Practices.Unity;

namespace HRIS.Ioc
{
    public interface IIocManager
    {
        T Resolve<T>();
        void BuildUp<T>(T existingObject);
        IUnityContainer Container { get; set; }
    }
}
