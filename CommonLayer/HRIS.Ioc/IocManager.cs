﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using HRIS.Aspects.Behaviors;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using Microsoft.Practices.ServiceLocation;

namespace HRIS.Ioc
{
    //[Export(typeof(IIocManager))]

    public class IocManager : IIocManager
    {
        public IUnityContainer Container { get; set; }
        private DiscoveryStrategy discoverStrategy;
        private List<Tuple<Type, object>> deferredBuilds;
        private string assemblySearchWildCharacter = "HRIS";

        public IocManager()
            : this(DiscoveryStrategy.SearchBaseDirectory)
        {
            this.Container = new UnityContainer();
            this.Container.RegisterInstance<IIocManager>(this);
            DiscoverAssemblies();
            ServiceLocator.SetLocatorProvider(() => new IocServiceLocator(Container));
        }

        public IocManager(DiscoveryStrategy defaultDiscoveryStrategy = DiscoveryStrategy.SearchBaseDirectory, string namespaceWildCharacters = null)
        {
            if (namespaceWildCharacters != null)
            {
                this.assemblySearchWildCharacter = namespaceWildCharacters;
            }
            this.discoverStrategy = defaultDiscoveryStrategy;
            this.Container = new UnityContainer();
            this.Container.RegisterInstance<IIocManager>(this);
            DiscoverAssemblies();
            ServiceLocator.SetLocatorProvider(() => new IocServiceLocator(Container));
        }

        public T Resolve<T>()
        {
            return this.Container.Resolve<T>();
        }

        public void BuildUp<T>(T existingObject)
        {
            if (existingObject != null)
            {
                this.Container.BuildUp(existingObject);
            }
        }

        /// <summary>
        /// This method will be used to register the object instances in container which can be overriden in deriving class
        /// </summary>
        /// <param name="type">type of object need to register</param>
        /// <param name="contractType">contract type</param>
        /// <param name="contractName">contract name</param>
        /// <param name="manager">lige time manager i.e. transoient instance, singleton instance</param>
        /// <returns>returns container</returns>
        protected virtual IUnityContainer RegisterContract(Type type, Type contractType, string contractName, LifetimeManager manager)
        {
            // For Interception refer to below code
            //return Container.RegisterType(contractType, type, contractName, manager, new InjectionMember[]
            //                        {
            //                            new Interceptor<InterfaceInterceptor>(),
            //                            new InterceptionBehavior<PolicyInjectionBehavior>()
            //                        });
            return Container.RegisterType(contractType, type, contractName, manager);

        }

        private void DiscoverAssemblies()
        {
            deferredBuilds = new List<Tuple<Type, object>>();
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            if (entryAssembly != null)
            {
                DiscoverExports(Assembly.GetEntryAssembly());
            }

            if (discoverStrategy == DiscoveryStrategy.SearchBaseDirectory)
            {
                ProcessBaseDirectory();
            }
            else if (discoverStrategy == DiscoveryStrategy.Loaded)
            {
                ProcessAssemblies(AppDomain.CurrentDomain.GetAssemblies()
                    .Where(assembly => assembly.FullName.Contains(assemblySearchWildCharacter) &&
                                      !assembly.FullName.Contains("DynamicProxies")));
            }
            foreach (var build in deferredBuilds)
            {
                this.Container.BuildUp(build.Item1, build.Item2);
            }
        }

        /// <summary>
        /// Method to collect assemblies for the current app domain and to filter as per defined wild card value
        /// </summary>
        private void ProcessBaseDirectory()
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(assembly => assembly.FullName.Contains(assemblySearchWildCharacter) &&
                                  !assembly.FullName.Contains("DynamicProxies"))
                .ToDictionary<Assembly, string>(assembly => assembly.Location);

            string assemblyFolder = AppDomain.CurrentDomain.BaseDirectory;
            var assemblyFiles = Directory.EnumerateFiles(assemblyFolder, "*" + assemblySearchWildCharacter + "*.dll", SearchOption.AllDirectories);
            foreach (var fileName in assemblyFiles)
            {
                Assembly assembly;
                if (loadedAssemblies.ContainsKey(fileName))
                {
                    assembly = loadedAssemblies[fileName];
                }
                else
                {
                    var assemblyName = AssemblyName.GetAssemblyName(fileName);
                    assembly = AppDomain.CurrentDomain.Load(assemblyName);
                }

                DiscoverExports(assembly);
            }
        }

        private void ProcessAssemblies(IEnumerable<Assembly> assemblies)
        {
            foreach (var assembly in assemblies)
            {
                DiscoverExports(assembly);
            }
        }

        private void DiscoverExports(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                var exports = GetCustomAttribute<ExportAttribute>(type);
                if (exports != null)
                {
                    foreach (var export in exports)
                    {
                        ProcessExport(type, export);
                    }
                    //Container.Configure<Interception>()
                    //       .AddPolicy("MemberNameTestPolicy")
                    //       .AddMatchingRule(new MemberNameMatchingRule(new List<string>() { "IsActiveBuild", "SaveBuildData" }))
                    //       .AddCallHandler(new CustomPolicyInjectionBehavior());

                    // Add custom policy
                }
            }
        }

        private void ProcessExport(Type type, ExportAttribute export)
        {
            Type contractType = type;
            if (export != null)
            {
                string contractName = export.ContractName;
                if (export.ContractType != null)
                {
                    contractType = export.ContractType;
                }

                var scopeAttributes = GetCustomAttribute<PartCreationPolicyAttribute>(type);
                if (scopeAttributes != null)
                {
                    var scope = scopeAttributes.FirstOrDefault();
                    Container.AddNewExtension<Interception>();
                    if (scope.CreationPolicy == CreationPolicy.Shared)
                    {

                        using (ContainerControlledLifetimeManager singletonManager = new ContainerControlledLifetimeManager())
                        {
                            object sharedObject = null;
                            try
                            {
                                sharedObject = Activator.CreateInstance(type);
                            }
                            catch (MissingMethodException ex)
                            {
                                throw new ArgumentException(string.Format("Unable to construct type {0} and reason for non-construct is :{1}", type.FullName, ex.Message));
                            }

                            Container.RegisterInstance(contractType, contractName, sharedObject, singletonManager);
                            this.deferredBuilds.Add(new Tuple<Type, object>(contractType, sharedObject));
                        }
                    }
                    else
                    {
                        TransientLifetimeManager manager = new TransientLifetimeManager();
                        // Add Interceptors here if required
                        RegisterContract(type, contractType, contractName, manager);
                    }
                }
                else
                {
                    Container.RegisterType(contractType, type, contractName);
                }
            }
        }

        /// <summary>
        /// This method explicitly only finds the first matching attribute.
        /// We currently don't need to return multiple instances of the same attribute
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        private IEnumerable<TAttribute> GetCustomAttribute<TAttribute>(Type type)
            where TAttribute : Attribute
        {
            IEnumerable<TAttribute> results = null;
            var attributes = type.GetCustomAttributes(typeof(TAttribute), true);
            if (attributes != null &&
                attributes.Length > 0)
            {
                results = attributes.Cast<TAttribute>();
            }
            return results;
        }
    }
}
