﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Mvc;
using HRIS.Ioc;

namespace HRIS.Ioc.Mvc
{
    public class MvcBootstrapper
    {
        public static void Initialize(IUnityContainer container = null)
        {
            if (container == null)
            {
                container = new IocManager().Container;
            }

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(container));

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}
