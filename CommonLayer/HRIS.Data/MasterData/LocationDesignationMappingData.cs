﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public partial class LocationDesignationMappingData
    {
        public long ID { get; set; }
        public string Location { get; set; }
        public string LocationCode { get; set; }
        public string Designation { get; set; }
    }
}
