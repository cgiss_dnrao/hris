﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class ConsultantUploaderData
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string OfficeEmailAddress { get; set; }
        public string ProfitCenter { get; set; }
        public string CostCenter { get; set; }
        public string ConsultantOrGroup { get; set; }
        public string Division { get; set; }
        public string L1Manager { get; set; }
        public string L2Manager { get; set; }
        public string ExecomName { get; set; }
        public string TEPortalUserYesNo { get; set; }
    }
}
