﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class ConsultantUploaderByBatchData
    {
        public string BatchNumber { get; set; }
        public string BatchStatus { get; set; }
        public Nullable<int> TotalRecords { get; set; }
        public Nullable<int> FailedRecords { get; set; }
        public Nullable<System.DateTime> UploadedOn { get; set; }
        public string UploadedBy { get; set; }
    }
}
