﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class LensUploaderData
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string OfficeEmailAddress { get; set; }
        public string MobileNumber { get; set; }
        public bool IsLensUser { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public string IsActiveYesNo { get; set; }

        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}
