﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class ConsultantUploaderBatchDetailsData
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string ConsultantOrGroup { get; set; }
        public string IsTEPortalUser { get; set; }
        public string BatchStatus { get; set; }
        public string Action { get; set; }
        public string Notes { get; set; }
    }
}
