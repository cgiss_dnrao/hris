﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class CustomSelectListItem
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
