﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class DiceUploaderData
    {
        public string UserType { get; set; }
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string Email { get; set; }
        public string MobileNumber { get; set; }
        public string SiteName { get; set; }
        public string Moderate { get; set; }
        public string CostCenter { get; set; }
        public string ProfitCenter { get; set; }
        public string Region { get; set; }
        public string Org { get; set; }
        public string Entity { get; set; }
        public string BAName { get; set; }
        public string BAEmail { get; set; }
        public string BAContactNo { get; set; }
        public string L1Code { get; set; }
        public string L2Code { get; set; }
        public string L3Code { get; set; }
        public string L4Code { get; set; }
        public string L5Code { get; set; }
        public string UPINumberICICI { get; set; }
        public string PDCCardNo { get; set; }
        public string IsActive { get; set; }
    }
}
