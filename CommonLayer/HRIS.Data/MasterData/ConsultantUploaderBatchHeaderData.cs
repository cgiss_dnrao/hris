﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public class ConsultantUploaderBatchHeaderData
    {
        public int ID { get; set; }
        public string BatchNumber { get; set; }
        public string BatchStatus { get; set; }
        public string EmployeeCode { get; set; }
        public string ReadStatus { get; set; }
        public string UploadedBy { get; set; }
        public Nullable<System.DateTime> UploadedOn { get; set; }
    }
}
