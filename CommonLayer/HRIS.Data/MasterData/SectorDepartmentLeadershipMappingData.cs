﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.MasterData
{
    public partial class SectorDepartmentLeadershipMappingData
    {
        public string LeaderShipRoleName { get; set; }
        public string DepartmentName { get; set; }
        public string SectorName { get; set; }
        public string DepartmentCode { get; set; }
        public string SectorCode { get; set; }
        public string LeadershipRoleCode { get; set; }
        public int ID { get; set; }

        public bool IsActive { get; set; }
        public int CreatedBy { get; set; }
    }
}
