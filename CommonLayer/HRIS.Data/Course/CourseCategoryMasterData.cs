﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HSEQ.Data.Course
{
    [DataContract]
    public class CourseCategoryMasterData
    {
        [DataMember]
        public int coursecategoryid { get; set; }
        [DataMember]
        public string coursecategorycode { get; set; }
        [DataMember]
        public string coursecategorydesc { get; set; }
        [DataMember]
        public bool IsActive { get; set; }
    }
}
