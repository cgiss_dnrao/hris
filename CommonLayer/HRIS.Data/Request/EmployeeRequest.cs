﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.Request
{
    public class EmployeeRequest
    {
        public int PageNumber { get; set; }

        public int PageSize { get; set; }

        public string SearchKeyword { get; set; }
        public bool IsItiliteSearch { get; set; }

    }
}
