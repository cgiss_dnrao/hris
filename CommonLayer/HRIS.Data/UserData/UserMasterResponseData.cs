﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.UserData
{
    public class UserMasterResponseData
    {
        public int UserId { get; set; }

        public string EmailId { get; set; }

        public string UserName { get; set; }

        public int UserRoleId { get; set; }

        public string UserRoleName { get; set; }

        public int StateRegionId { get; set; }
        public int SiteId { get; set; }
        public string SiteCode { get; set; }
        public string SiteName { get; set; }
        public string SiteLocation { get; set; }
        public string DisplayName { get; set; }
        public string SectorName { get; set; }
    }
}
