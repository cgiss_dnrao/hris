﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.UserData
{
    public class UserMasterData
    {
        public int UserId { get; set; }
        public string EmailId { get; set; }
        public string UserName { get; set; }
        public int StateRegionId { get; set; }
        public int UserRoleId { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
    }
}
