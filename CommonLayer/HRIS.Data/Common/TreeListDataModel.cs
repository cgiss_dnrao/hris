﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.Common
{
    public class TreeListDataModel
    {
        public long? ParentId { get; set; }
        public long? parentId { get; set; }
        public long Id { get; set; }
        public string DisplayName { get; set; }
        public bool IsSelected { get; set; }
        public bool IsSite { get; set; }
        public bool IsState { get; set; }
        public bool IsRegion { get; set; }
        public string SiteCode { get; set; }
        public string StateCode { get; set; }
        public string RegionCode { get; set; }
        public bool DisplayCheck { get; set; } = true;
    }
}
