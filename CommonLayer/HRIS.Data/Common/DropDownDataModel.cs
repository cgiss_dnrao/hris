﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.Common
{
    public class DropDownDataModel : IComparable<DropDownDataModel>
    {
        public string Name { get; set; }
        public string Value { get; set; }

        public int CompareTo(DropDownDataModel other)
        {
            return this.Name.CompareTo(other.Name);
        }
    }
}
