﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Data.Common
{
    [DataContract(Name = "{0}Response")]
    public class JsonCollectionResponse<T> : ResponseBase
    {
        #region [Private Member]


        private List<T> result;
        

        #endregion

        #region Properties

        /// <summary>
        /// Response result
        /// </summary>
        [DataMember]
        public List<T> Results
        {
            get { return result; }
            set { result = value; }
        }

        [DataMember]
        public int TotalRecordCount { get; set; }


        #endregion
    }
}
