﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Compilation;
using System.Web.Mvc;
using HRIS.DryIoc.Web;
using DryIoc;

namespace HRIS.DryIoc
{
    public static class DryIocMvc
    {
        /// <summary>Creates new container from original one with <see cref="HttpContextScopeContext"/>.
        /// Then registers MVC controllers in container, 
        /// sets <see cref="DryIocFilterAttributeFilterProvider"/> as filter provider,
        /// and at last sets container as <see cref="DependencyResolver"/>.</summary>
        /// <param name="container">Original container.</param>
        /// <param name="controllerAssemblies">(optional) By default uses <see cref="BuildManager.GetReferencedAssemblies"/>.</param>
        /// <param name="scopeContext">(optional) Specific scope context to use, by default MVC uses <see cref="HttpContextScopeContext"/> 
        /// (if container does not have its own context specified).</param>
        /// <returns>New container with applied Web context.</returns>
        public static IContainer WithMvc(this IContainer container,
            IEnumerable<Assembly> controllerAssemblies = null, IScopeContext scopeContext = null)
        {
            container.ThrowIfNull();
            if (container.ScopeContext == null)
            {
                container = container.With(scopeContext: scopeContext ?? GetHttpContext());
            }

            container.RegisterMvcControllers(controllerAssemblies);

            container.SetFilterAttributeFilterProvider(FilterProviders.Providers);

            DependencyResolver.SetResolver(new DryIocDependencyResolver(container));

            return container;
        }

        /// <summary>Returns all application specific referenced assemblies (except from GAC and Dynamic).</summary>
        /// <returns>The assemblies.</returns>
        public static IEnumerable<Assembly> GetReferencedAssemblies()
        {
            return BuildManager.GetReferencedAssemblies().OfType<Assembly>()
                .Where(a => !a.IsDynamic && !a.GlobalAssemblyCache).ToList(); // filter out non-app specific assemblies
        }

        /// <summary>Registers controllers types in container with InWebRequest reuse.</summary>
        /// <param name="registrator">Registrator to controllers</param>
        /// <param name="controllerAssemblies">(optional) Uses <see cref="BuildManager.GetReferencedAssemblies"/> by default.</param>
        public static void RegisterMvcControllers(this IRegistrator registrator, IEnumerable<Assembly> controllerAssemblies = null)
        {
            controllerAssemblies = controllerAssemblies ?? GetReferencedAssemblies();
            //var types = (from k in controllerAssemblies
            //             from h in k.GetTypes()
            //             where h.IsAssignableTo(typeof(IController))
            //             select h).ToList();
            var types = controllerAssemblies.SelectMany(s => s.GetTypes()).Where(p => typeof(IController).IsAssignableFrom(p)).ToList();
            //container.RegisterMany(controllerAssemblies, type => type.IsAssignableTo(typeof(IController)),
            //    Reuse.InWebRequest, FactoryMethod.ConstructorWithResolvableArguments);
            foreach (var type in types)
            {
                registrator.Register(type, Reuse.InWebRequest, FactoryMethod.ConstructorWithResolvableArguments);
            }
            //container.RegisterMany(types,Reuse.InWebRequest,made:Made.Of(FactoryMethod.ConstructorWithResolvableArguments));
        }

        /// <summary>Replaces default Filter Providers with instance of <see cref="DryIocFilterAttributeFilterProvider"/>,
        /// add in addition registers aggregated filter to container..</summary>
        /// <param name="container">Container to register to.</param>
        /// <param name="filterProviders">Original filter providers.</param>
        public static void SetFilterAttributeFilterProvider(this IContainer container, Collection<IFilterProvider> filterProviders = null)
        {
            filterProviders = filterProviders ?? FilterProviders.Providers;
            var filterProvidersSnapshot = filterProviders.OfType<FilterAttributeFilterProvider>().ToArray();
            foreach (var provider in filterProvidersSnapshot)
            {
                filterProviders.Remove(provider);
            }

            var filterProvider = new DryIocFilterAttributeFilterProvider(container);
            filterProviders.Add(filterProvider);

            container.RegisterInstance<IFilterProvider>(filterProvider);
        }

        private static HttpContextScopeContext GetHttpContext()
        {
            return new HttpContextScopeContext();
        }

    }

    /// <summary>Resolver delegating to DryIoc container.</summary>
    public class DryIocDependencyResolver : IDependencyResolver
    {
        /// <summary>Creates resolver from DryIoc resolver.</summary>
        /// <param name="resolver">DryIoc resolver (container interface).</param>
        public DryIocDependencyResolver(IResolver resolver)
        {
            dryResolver = resolver;
        }

        /// <summary> Resolves singly registered services that support arbitrary object creation. </summary>
        /// <returns> The requested service or object. </returns>
        /// <param name="serviceType">The type of the requested service or object.</param>
        public object GetService(Type serviceType)
        {
            try
            {
                return dryResolver.Resolve(serviceType, IfUnresolved.Throw);
            }
            catch(ContainerException ex)
            {
                Debug.Write(ex.Message);
            }
            return null;
        }

        /// <summary> Resolves multiply registered services. </summary>
        /// <returns> The requested services. </returns>
        /// <param name="serviceType">The type of the requested services.</param>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return dryResolver.ResolveMany<object>(serviceType);
        }

        private readonly IResolver dryResolver;
    }

    /// <summary>Defines an filter provider for filter attributes. Uses DryIoc container to inject filter properties.</summary>
    [ComVisible(false)]
    public class DryIocFilterAttributeFilterProvider : FilterAttributeFilterProvider
    {
        /// <summary>Creates filter provider.</summary> <param name="container"></param>
        public DryIocFilterAttributeFilterProvider(IContainer container)
        {
            iocContainer = container;
        }

        /// <summary> Aggregates the filters from all of the filter providers into one collection. </summary>
        /// <returns> The collection filters from all of the filter providers. </returns>
        /// <param name="controllerContext">The controller context.</param><param name="actionDescriptor">The action descriptor.</param>
        public override IEnumerable<Filter> GetFilters(ControllerContext controllerContext, ActionDescriptor actionDescriptor)
        {
            var filters = base.GetFilters(controllerContext, actionDescriptor).ToArray();
            for (var i = 0; i < filters.Length; i++)
            {
                iocContainer.InjectPropertiesAndFields(filters[i].Instance);
            }
            return filters;
        }

        private readonly IContainer iocContainer;
    }
}

