﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using HRIS.DryIoc.WebApi;
using DryIoc;
using Microsoft.Practices.ServiceLocation;

namespace HRIS.DryIoc
{
    public class DryIocManager : IDryIocManager, IDisposable
    {
        public IContainer Container { get; set; }
        private DiscoveryStrategy discoverStrategy;
        private HostType hostType;
        private string assemblySearchWildCharacter = "HRIS";
        private HttpConfiguration config = null;
        public DryIocManager()
            : this(DiscoveryStrategy.SearchBaseDirectory, HostType.None)
        {

        }

        public DryIocManager(DiscoveryStrategy defaultDiscoveryStrategy = DiscoveryStrategy.SearchBaseDirectory, HostType defaultHostType = HostType.None, string namespaceWildCharacters = null)
        {
            if (namespaceWildCharacters != null)
            {
                assemblySearchWildCharacter = namespaceWildCharacters;
            }

            BuildContainer(defaultDiscoveryStrategy, defaultHostType);
        }

        public DryIocManager(HttpConfiguration configuration, DiscoveryStrategy defaultDiscoveryStrategy = DiscoveryStrategy.SearchBaseDirectory, HostType defaultHostType = HostType.None, string namespaceWildCharacters = null)
        {
            config = configuration;
            if (namespaceWildCharacters != null)
            {
                assemblySearchWildCharacter = namespaceWildCharacters;
            }

            BuildContainer(defaultDiscoveryStrategy, defaultHostType);
        }

        public object Resolve(Type type, string keyName)
        {
            return Container.Resolve(type, serviceKey: keyName);
        }

        public T Resolve<T>()
        {
            return Container.Resolve<T>();
        }

        public T Resolve<T>(string keyName)
        {
            return Container.Resolve<T>(serviceKey: keyName);
        }

        public void ExplicitRegister<T>(string keyName)
        {
            Container.Register<T>(serviceKey: keyName);
        }

        /// <summary>
        /// This method will be used to register the object instances in container which can be overriden in deriving class
        /// </summary>
        /// <param name="type">type of object need to register</param>
        /// <param name="contractType">contract type</param>
        /// <param name="contractName">contract name</param>
        protected virtual void RegisterContract(Type type, Type contractType, string contractName)
        {
            Container.Register(contractType, type, Reuse.Transient, setup: Setup.With(allowDisposableTransient: true), serviceKey: contractName);

        }

        private void DiscoverAssemblies()
        {
            Assembly entryAssembly = Assembly.GetEntryAssembly();

            if (entryAssembly != null)
            {
                DiscoverExports(Assembly.GetEntryAssembly());
            }

            if (discoverStrategy == DiscoveryStrategy.SearchBaseDirectory)
            {
                ProcessBaseDirectory();
            }
            else if (discoverStrategy == DiscoveryStrategy.Loaded)
            {
                ProcessAssemblies(AppDomain.CurrentDomain.GetAssemblies()
                    .Where(assembly => assembly.FullName.Contains(assemblySearchWildCharacter) &&
                                      !assembly.FullName.Contains("DynamicProxies")));
            }

        }

        private void BuildContainer(DiscoveryStrategy defaultDiscoveryStrategy, HostType defaultHostType)
        {
            discoverStrategy = defaultDiscoveryStrategy;
            hostType = defaultHostType;
            Container = GetContainer().With(rules => rules.With(propertiesAndFields: PropertiesAndFields.Auto));
            Container.RegisterInstance<IDryIocManager>(this, Reuse.Singleton);
            DiscoverAssemblies();
            SetupHost();
            ServiceLocator.SetLocatorProvider(() => new DryIocServiceLocator(Container));
        }

        private Container GetContainer()
        {
            return new Container();
        }

        private void SetupHost()
        {
            if (config == null)
            {
                config = new HttpConfiguration();
            }
            if (Container != null)
            {
                switch (hostType)
                {
                    case HostType.Webforms:
                        break;
                    case HostType.Mvc:
                        Container = Container.WithMvc();
                        break;
                    case HostType.Assembly:
                        break;
                    case HostType.Wcf:
                        break;
                    case HostType.WebApi:
                        Container = Container.WithWebApi(config);
                        break;
                    case HostType.WindowService:
                        break;
                    default:
                        break;
                }
            }
        }


        /// <summary>
        /// Method to collect assemblies for the current app domain and to filter as per defined wild card value
        /// </summary>
        private void ProcessBaseDirectory()
        {
            var loadedAssemblies = AppDomain.CurrentDomain.GetAssemblies()
                .Where(assembly => assembly.FullName.Contains(assemblySearchWildCharacter) &&
                                  !assembly.FullName.Contains("DynamicProxies"))
                .ToDictionary<Assembly, string>(assembly => assembly.Location);

            string assemblyFolder = AppDomain.CurrentDomain.BaseDirectory;
            var assemblyFiles = Directory.EnumerateFiles(assemblyFolder, "*" + assemblySearchWildCharacter + "*.dll", SearchOption.AllDirectories);
            foreach (var fileName in assemblyFiles)
            {
                Assembly assembly;
                if (loadedAssemblies.ContainsKey(fileName))
                {
                    assembly = loadedAssemblies[fileName];
                }
                else
                {
                    var assemblyName = AssemblyName.GetAssemblyName(fileName);
                    assembly = AppDomain.CurrentDomain.Load(assemblyName);
                }

                DiscoverExports(assembly);
            }
        }

        private void ProcessAssemblies(IEnumerable<Assembly> assemblies)
        {
            foreach (var assembly in assemblies)
            {
                DiscoverExports(assembly);
            }
        }

        private void DiscoverExports(Assembly assembly)
        {
            foreach (var type in assembly.GetTypes())
            {
                var exports = GetCustomAttribute<ExportAttribute>(type);
                if (exports != null)
                {
                    foreach (var export in exports)
                    {
                        ProcessExport(type, export);
                    }
                }
            }
        }

        private void ProcessExport(Type type, ExportAttribute export)
        {
            Type contractType = type;
            if (export != null)
            {
                string contractName = export.ContractName;
                if (export.ContractType != null)
                {
                    contractType = export.ContractType;
                }

                var scopeAttributes = GetCustomAttribute<PartCreationPolicyAttribute>(type);
                if (scopeAttributes != null)
                {
                    var scope = scopeAttributes.FirstOrDefault();
                    if (scope.CreationPolicy == CreationPolicy.Shared)
                    {


                        object sharedObject = null;
                        try
                        {
                            sharedObject = Activator.CreateInstance(type);
                        }
                        catch (MissingMethodException ex)
                        {
                            throw new ArgumentException(string.Format("Unable to construct type {0} and reason for non-construct is :{1}", type.FullName, ex.Message));
                        }

                        Container.RegisterInstance(contractType, sharedObject, Reuse.Singleton);
                    }
                    else
                    {
                        // Add Interceptors here if required
                        RegisterContract(type, contractType, contractName);
                    }
                }
                else
                {
                    RegisterContract(type, contractType, contractName);
                }
            }
        }

        /// <summary>
        /// This method explicitly only finds the first matching attribute.
        /// We currently don't need to return multiple instances of the same attribute
        /// </summary>
        /// <typeparam name="TAttribute"></typeparam>
        /// <param name="type"></param>
        /// <returns></returns>
        private IEnumerable<TAttribute> GetCustomAttribute<TAttribute>(Type type)
            where TAttribute : Attribute
        {
            IEnumerable<TAttribute> results = null;
            var attributes = type.GetCustomAttributes(typeof(TAttribute), true);
            if (attributes != null &&
                attributes.Length > 0)
            {
                results = attributes.Cast<TAttribute>();
            }
            return results;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(disposing)
            {
                if(config != null)
                {
                    config.Dispose();
                    config = null;
                }
                if(Container != null)
                {
                    Container.Dispose();
                    Container = null;
                }
            }
        }

        ~DryIocManager()
        {
            Dispose(false);
        }
    }
}
