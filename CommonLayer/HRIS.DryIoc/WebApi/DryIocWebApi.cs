﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Dispatcher;
using System.Web.Http.Filters;
using DryIoc;

namespace HRIS.DryIoc.WebApi
{
    public static class DryIocWebApi
    {
        /// <summary>Configures container to work with ASP.NET WepAPI by: 
        /// setting container scope context to <see cref="AsyncExecutionFlowScopeContext"/> (if scope context is not set already),
        /// registering HTTP controllers, setting filter provider and dependency resolver.</summary>
        /// <param name="container">Original container.</param> <param name="config">Http configuration.</param>
        /// <param name="controllerAssemblies">(optional) Assemblies to look for controllers, default is ExecutingAssembly.</param>
        /// <param name="scopeContext">(optional) Specific scope context to use, by default method sets
        /// <see cref="AsyncExecutionFlowScopeContext"/>, only if container does not have context specified already.</param>
        /// <param name="throwIfUnresolved">(optional) Instructs DryIoc to throw exception
        /// for unresolved type instead of fallback to default Resolver.</param>
        /// <returns>New container.</returns>
        public static IContainer WithWebApi(this IContainer container, HttpConfiguration config,
            IEnumerable<Assembly> controllerAssemblies = null, IScopeContext scopeContext = null,
            Func<Type, bool> throwIfUnresolved = null)
        {
            container.ThrowIfNull();
            AsyncExecutionFlowScopeContext asyncContext = null;
            try
            {
                asyncContext = new AsyncExecutionFlowScopeContext();
                if (container.ScopeContext == null)
                {
                    container = container.With(scopeContext: scopeContext ?? asyncContext);
                }

                container.RegisterWebApiControllers(config, controllerAssemblies);

                container.SetFilterProvider(config.Services);

                InsertRegisterRequestMessageHandler(config);

                config.DependencyResolver = new DryIocWebApiDependencyResolver(container, throwIfUnresolved);

                return container;
            }
            catch (Exception ex)
            {
                if (asyncContext != null)
                {
                    asyncContext.Dispose();
                    asyncContext = null;
                }
                Debug.Write(ex.Message);
                throw;
            }
        }

        /// <summary>Registers controllers found in provided assemblies with <see cref="Reuse.InWebRequest"/>.</summary>
        /// <param name="registrator">Registrator.</param>
        /// <param name="config">Http configuration.</param>
        /// <param name="assemblies">Assemblies to look for controllers.</param>
        public static void RegisterWebApiControllers(this IRegistrator registrator, HttpConfiguration config,
            IEnumerable<Assembly> assemblies = null)
        {
            var assembliesResolver = assemblies == null
                 ? config.Services.GetAssembliesResolver()
                 : new GivenAssembliesResolver(assemblies.ToArrayOrSelf());

            var controllerTypeResolver = config.Services.GetHttpControllerTypeResolver();
            var controllerTypes = controllerTypeResolver.GetControllerTypes(assembliesResolver);
            foreach (var type in controllerTypes)
            {
                registrator.Register(type, Reuse.InWebRequest, FactoryMethod.ConstructorWithResolvableArguments);
            }
            //container.RegisterMany(controllerTypes, Reuse.InWebRequest);
        }

        /// <summary>Helps to find if type is controller type.</summary>
        /// <param name="type">Type to check.</param>
        /// <returns>True if controller type</returns>
        public static bool IsController(this Type type)
        {
            return ControllerResolver.Default.IsController(type);
        }

        private sealed class ControllerResolver : DefaultHttpControllerTypeResolver
        {
            public static readonly ControllerResolver Default = new ControllerResolver();

            public bool IsController(Type type)
            {
                return IsControllerTypePredicate(type);
            }
        }

        private sealed class GivenAssembliesResolver : IAssembliesResolver
        {
            private readonly ICollection<Assembly> contextAssemblies;
            public GivenAssembliesResolver(ICollection<Assembly> assemblies) { contextAssemblies = assemblies; }
            public ICollection<Assembly> GetAssemblies() { return contextAssemblies; }
        }

        /// <summary>Replaces all filter providers in services with <see cref="DryIocFilterProvider"/>, and registers it in container.</summary>
        /// <param name="container">DryIoc container.</param> <param name="services">Services</param>
        public static void SetFilterProvider(this IContainer container, ServicesContainer services)
        {
            var providers = services.GetFilterProviders();
            services.RemoveAll(typeof(IFilterProvider), _ => true);
            var filterProvider = new DryIocWebApiFilterProvider(container, providers);
            services.Add(typeof(IFilterProvider), filterProvider);
            container.RegisterInstance<IFilterProvider>(filterProvider);
        }

        /// <summary>Inserts DryIoc delegating request handler into message handlers.</summary>
        /// <param name="config">Current configuration.</param>
        public static void InsertRegisterRequestMessageHandler(HttpConfiguration config)
        {
            RegisterRequestMessageHandler requestHandler = null;
            try
            {
                requestHandler = new RegisterRequestMessageHandler();
                var handlers = config.ThrowIfNull().MessageHandlers;
                if (!handlers.Any(h => h is RegisterRequestMessageHandler))
                {
                    handlers.Insert(0, requestHandler);
                }
            }
            catch (Exception ex)
            {
                if (requestHandler != null)
                {
                    requestHandler.Dispose();
                    requestHandler = null;
                }
                Debug.Write(ex.Message);
                throw;
            }
        }
    }
}
