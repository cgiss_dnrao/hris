﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Dependencies;
using DryIoc;

namespace HRIS.DryIoc.WebApi
{
    /// <summary>Dependency scope adapter to scoped DryIoc container (created by <see cref="IContainer.OpenScope"/>).</summary>
    public sealed class DryIocWebApiDependencyScope : IDependencyScope
    {
        /// <summary>Wrapped DryIoc container.</summary>
        public readonly IContainer ScopedContainer;

        private readonly Func<Type, bool> throwExceptionIfUnresolved;

        /// <summary>Adapts input container.</summary>
        /// <param name="scopedContainer">Container returned by OpenScope method.</param>
        /// <param name="throwIfUnresolved">(optional) Instructs DryIoc to throw exception
        /// for unresolved type instead of fallback to default Resolver.</param>
        public DryIocWebApiDependencyScope(IContainer scopedContainer, Func<Type, bool> throwIfUnresolved = null)
        {
            ScopedContainer = scopedContainer;
            throwExceptionIfUnresolved = throwIfUnresolved;
        }

        /// <summary>Disposed underlying scoped container.</summary>
        public void Dispose()
        {
            if (ScopedContainer != null)
            {
                ScopedContainer.Dispose();
            }
        }

        /// <summary>Retrieves a service from the scope or returns null if not resolved.</summary>
        /// <returns>The retrieved service.</returns> <param name="serviceType">The service to be retrieved.</param>
        public object GetService(Type serviceType)
        {
            var ifUnresolvedReturnDefault = throwExceptionIfUnresolved == null || !throwExceptionIfUnresolved(serviceType);
            return ScopedContainer.Resolve(serviceType, ifUnresolvedReturnDefault);
        }

        /// <summary>Retrieves a collection of services from the scope or empty collection.</summary>
        /// <returns>The retrieved collection of services.</returns>
        /// <param name="serviceType">The collection of services to be retrieved.</param>
        public IEnumerable<object> GetServices(Type serviceType)
        {
            return ScopedContainer.ResolveMany<object>(serviceType);
        }
    }
}
