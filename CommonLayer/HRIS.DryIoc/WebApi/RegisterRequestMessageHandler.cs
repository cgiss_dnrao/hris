﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DryIoc;

namespace HRIS.DryIoc.WebApi
{
    /// <summary>Registers current <see cref="HttpRequestMessage"/> into dependency scope.</summary>
    internal sealed class RegisterRequestMessageHandler : DelegatingHandler
    {
        /// <summary>Registers request into dependency scope and sends proceed the pipeline.</summary> 
        /// <param name="request">The HTTP request message to send to the server.</param>
        /// <param name="cancellationToken">A cancellation token to cancel operation.</param>
        /// <returns>The task object representing the asynchronous operation.</returns>
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            RegisterInDependencyScope(request);
            return base.SendAsync(request, cancellationToken);
        }

        /// <summary>Registers request into current dependency scope.</summary>
        /// <param name="request">Request to register.</param>
        public void RegisterInDependencyScope(HttpRequestMessage request)
        {
            var dependencyScope = request.ThrowIfNull()
                .GetDependencyScope().ThrowIfNotOf(typeof(DryIocWebApiDependencyScope), Error.RequestMessageDoesnotReferenceDryiocDependencyScope);

            var container = ((DryIocWebApiDependencyScope)dependencyScope).ScopedContainer;
            container.RegisterInstance(request, Reuse.InWebRequest, IfAlreadyRegistered.Replace);
        }
    }

}
