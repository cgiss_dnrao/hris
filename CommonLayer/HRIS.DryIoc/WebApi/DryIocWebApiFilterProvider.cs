﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using DryIoc;

namespace HRIS.DryIoc.WebApi
{
    /// <summary>Aggregated filter provider.</summary>
    public sealed class DryIocWebApiFilterProvider: IFilterProvider
    {
        /// <summary>Creates filter provider.</summary>
        /// <param name="container"></param> <param name="providers"></param>
        public DryIocWebApiFilterProvider(IContainer container, IEnumerable<IFilterProvider> providers)
        {
            iocContainer = container;
            filterProviders = providers;
        }

        /// <summary> Returns an enumeration of filters. </summary>
        /// <returns> An enumeration of filters. </returns>
        /// <param name="configuration">The HTTP configuration.</param><param name="actionDescriptor">The action descriptor.</param>
        public IEnumerable<FilterInfo> GetFilters(HttpConfiguration configuration, HttpActionDescriptor actionDescriptor)
        {
            var filters = filterProviders.SelectMany(p => p.GetFilters(configuration, actionDescriptor)).ToArray();
            for (var i = 0; i < filters.Length; i++)
            {
                iocContainer.InjectPropertiesAndFields(filters[i].Instance);
            }
            return filters;
        }

        private readonly IContainer iocContainer;
        private readonly IEnumerable<IFilterProvider> filterProviders;
    }
}
