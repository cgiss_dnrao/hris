﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using DryIoc;

namespace HRIS.DryIoc.Web
{
    /// <summary>Hooks up <see cref="Container.OpenScope"/> on request beginning and scope dispose on request end.</summary>
    public sealed class DryIocHttpModule : IHttpModule
    {
        /// <summary>Initializes a module and prepares it to handle requests. </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpApplication"/> that provides access to the methods, properties, and events common to all application objects within an ASP.NET application </param>
        void IHttpModule.Init(HttpApplication context)
        {
            var scopeName = Reuse.WebRequestScopeName;
            HttpContext httpContext = null;
            HttpContextScopeContext scopeContext = null;
            try
            {
                context.BeginRequest += (sender, _) =>
                {

                    httpContext = (sender as HttpApplication).ThrowIfNull().Context;
                    scopeContext = new HttpContextScopeContext(() => httpContext.Items);

                    // If current scope does not have WebRequestScopeName then create new scope with this name, 
                    // otherwise - use current.
                    scopeContext.SetCurrent(current =>
                        current != null && scopeName.Equals(current.Name) ? current : new Scope(current, scopeName));
                };

                context.EndRequest += (sender, _) =>
                {
                    httpContext = (sender as HttpApplication).ThrowIfNull().Context;
                    scopeContext = new HttpContextScopeContext(() => httpContext.Items);

                    var currentScope = scopeContext.GetCurrentOrDefault();
                    if (currentScope != null && scopeName.Equals(currentScope.Name))
                    {
                        currentScope.Dispose();
                    }
                };
            }
            finally
            {
                if(scopeContext != null)
                {
                    scopeContext.Dispose();
                }
            }
        }

        /// <summary>Disposes of the resources (other than memory) used by the module  that implements <see cref="IHttpModule"/>.</summary>
        void IHttpModule.Dispose() 
        { 
            //No objects to dispose
        }
    }
}
