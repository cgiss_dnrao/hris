﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;

namespace HRIS.DryIoc.Web
{
    /// <summary>Registers <see cref="DryIocHttpModule"/>.</summary>
    public static class DryIocHttpModuleInitializer
    {
        /// <summary>Registers once the type of <see cref="DryIocHttpModule"/>.</summary>
        public static void Initialize()
        {
            if (Interlocked.CompareExchange(ref initialized, 1, 0) == 0)
            {
                DynamicModuleUtility.RegisterModule(typeof(DryIocHttpModule));
            }
        }

        private static int initialized;
    }
}
