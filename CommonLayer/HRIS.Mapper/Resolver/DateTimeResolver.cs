﻿using HSEQ.Data.User;
using HSEQ.DataAdapter.Factory;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HSEQ.Mapper.Resolver
{
    public class DateTimeResolver : 
        AutoMapper.IValueResolver<UserMaster, UserData, string>
    {
        public string Resolve(UserMaster source, 
            UserData destination, string member, 
            ResolutionContext context)
        {
            //return source.CreatedDate.ToShortDateString();
            return "Miller"+" "+source.CreatedDate.ToShortDateString() + " " + source.FirstName + source.LastName;
        }
    }
}
