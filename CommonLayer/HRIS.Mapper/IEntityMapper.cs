﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Mapper
{
    public interface IEntityMapper
    {
        /// <summary>
        /// Overload method to get the destination object from source object
        /// </summary>
        /// <typeparam name="T">Type of Destination Object</typeparam>
        /// <param name="source">Source Object instance</param>
        /// <returns>returns the detination object instance</returns>
        T Map<T>(object source);

        /// <summary>
        /// Overload method to map the source and destination object
        /// </summary>
        /// <typeparam name="TSource">Source Object Type</typeparam>
        /// <typeparam name="TTarget">Destination Object Type</typeparam>
        /// <param name="source">Source object instance</param>
        /// <param name="target">Destination Object Instance</param>
        /// <returns>returns the destination object instance</returns>
        TTarget Map<TSource, TTarget>(TSource source, TTarget target);

        ///// <summary>
        ///// Method to create register types for auto mapper mapping
        ///// </summary>
        ///// <typeparam name="TSourceObject">source generic type</typeparam>
        ///// <typeparam name="TTargetObject">target object generic type</typeparam>
        //AutoMapper.IMappingExpression<TSourceObject, TTargetObject> CreateMap<TSourceObject, TTargetObject>();

        /// <summary>
        /// Method to determine whetner mapping library is ready for function or not
        /// </summary>
        /// <returns>returns boolean status</returns>
        bool IsActivated();

        AutoMapper.MapperConfiguration GetConfigurations();
    }
}
