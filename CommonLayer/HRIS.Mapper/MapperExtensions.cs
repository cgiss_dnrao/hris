﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Mapper
{
    public static class MapperExtensions
    {
        /// <summary>
        /// Extension method to project from a queryable using the provided mapping engine
        /// </summary>
        /// <remarks>Projections are only calculated once and cached</remarks>
        /// <typeparam name="TDestination">Destination type</typeparam>
        /// <param name="source">Queryable source</param>
        /// <param name="membersToExpand">Explicit members to expand</param>
        /// <returns>Expression to project into</returns>
        public static IQueryable<TDestination> ProjectTo<TDestination>(this IQueryable source)
        {
            var mapper = ServiceLocator.Current.GetInstance<IEntityMapper>();
            return source.ProjectTo<TDestination>(mapper.GetConfigurations());
        }
    }
}
