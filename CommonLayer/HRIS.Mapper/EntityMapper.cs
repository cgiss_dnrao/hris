﻿using HRIS.Data.UserData;
using HRIS.Data.MasterData;
using System.ComponentModel.Composition;
using HRIS.DataAdapter.Factory.Transaction;
using System;

namespace HRIS.Mapper
{
    [Export(typeof(IEntityMapper))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class EntityMapper : IEntityMapper
    {
        private AutoMapper.IMapper mapper = null;
        private AutoMapper.MapperConfiguration config = null;

        private bool isMapperActivated;

        public EntityMapper()
        {
            InitializeMapper();
        }

        /// <summary>
        /// Method to initialize the mapping library. Here all object mappings need to be defined for first time so that later 
        /// in down layers we can call Map API to get the mapped output object
        /// </summary>
        private void InitializeMapper()
        {
            config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<UserMaster, UserMasterData>().ReverseMap();
                cfg.CreateMap<HRISMaster, HRISMasterData>()
                 //.ForMember(dest => dest.DateOfBirthAsString, source => source.MapFrom(m => !string.IsNullOrWhiteSpace( m.DateOfBirth)? String.Format("{0:dd-MMM-yyyy}", m.DateOfBirth):""))
                 .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Update" ? "Changed" : m.Action ));
                cfg.CreateMap<HRISMasterData, HRISMaster>()
               .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Changed"? "Update" : m.Action));
                cfg.CreateMap<procGetHRISMasterList_Result, HRISMasterData>()
               .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Update" ? "Changed" : m.Action));
                cfg.CreateMap<HRISMasterData, procGetHRISMasterList_Result>()
                .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Changed" ? "Update" : m.Action));

                cfg.CreateMap<procGetHRISMasterListPageWise_Result, HRISMasterData>()
               .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Update" ? "Changed" : m.Action));
                cfg.CreateMap<HRISMasterData, procGetHRISMasterListPageWise_Result>()
                .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Changed" ? "Update" : m.Action));


                cfg.CreateMap<procGetEXECOMUserDataList_Result, HRISMasterData>()
                .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Update" ? "Changed" : m.Action));
                cfg.CreateMap<HRISMasterData, procGetEXECOMUserDataList_Result>()
                .ForMember(dest => dest.Action, source => source.MapFrom(m => m.Action == "Changed"? "Update" : m.Action));

                cfg.CreateMap<CostCenterMaster, CostCenterMasterData>().ReverseMap();
                cfg.CreateMap<ProfitCenterMaster, ProfitCenterMasterData>().ReverseMap();
                cfg.CreateMap<LeaderShipRoleMaster, LeaderShipRoleMasterData>().ReverseMap();
                cfg.CreateMap<procGetTransferLeaderShipData_Result,TransferLeaderShipData >().ReverseMap();
                cfg.CreateMap<procGetSiteList_Result, SiteMasterData >().ReverseMap();

                //Krish
                cfg.CreateMap<procGetUniqueLocationAndDesignation_Result, LocationDesignationMappingData>().ReverseMap();
                cfg.CreateMap<LocationDesignationMapping, LocationDesignationMappingData>().ReverseMap();
                cfg.CreateMap<procGetSectorDepartmentLeadershipMapping_Result, SectorDepartmentLeadershipMappingData>().ReverseMap();
                cfg.CreateMap<ConsultantUploaderBatchHeader, ConsultantUploaderBatchHeaderData>().ReverseMap();

                cfg.CreateMap<procGetConsultantUploader_Result, ConsultantUploaderData>().ReverseMap();
                cfg.CreateMap<procGetConsultantUploaderBATCHDetails_Result, ConsultantUploaderBatchDetailsData>().ReverseMap();
                cfg.CreateMap<procGetConsultantUploaderByBATCH_Result, ConsultantUploaderByBatchData>().ReverseMap();

                cfg.CreateMap<procGetDiceUploaderGrid_Result, DiceUploaderData>().ReverseMap();
                cfg.CreateMap<procGetLensUploader_Result, LensUploaderData>().ReverseMap();
            });
            mapper = config.CreateMapper();
            isMapperActivated = true;
        }

        public AutoMapper.MapperConfiguration GetConfigurations()
        {
            return config;
        }

        /// <summary>
        /// Overload method to get the destination object from source object
        /// </summary>
        /// <typeparam name="T">Type of Destination Object</typeparam>
        /// <param name="source">Source Object instance</param>
        /// <returns>returns the detination object instance</returns>
        public T Map<T>(object source)
        {
            return mapper.Map<T>(source);
        }

        /// <summary>
        /// Overload method to map the source and destination object
        /// </summary>
        /// <typeparam name="TSource">Source Object Type</typeparam>
        /// <typeparam name="TTarget">Destination Object Type</typeparam>
        /// <param name="source">Source object instance</param>
        /// <param name="target">Destination Object Instance</param>
        /// <returns>returns the destination object instance</returns>
        public TTarget Map<TSource, TTarget>(TSource source, TTarget target)
        {
            return mapper.Map<TSource, TTarget>(source, target);

        }

        /// <summary>
        /// Method to determine whetner mapping library is ready for function or not
        /// </summary>
        /// <returns>returns boolean status</returns>
        public bool IsActivated()
        {
            return isMapperActivated;
        }

       
    }
}
