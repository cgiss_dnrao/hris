﻿using HRIS.Aspects.Constants;
using HRIS.Aspects.Factory;
using HRIS.BackendScheduler.Jobs.Contracts;
using HRIS.BackendScheduler.Jobs.Implementation;
using Quartz;

namespace HRIS.BackendScheduler.Jobs.Jobs
{
    public class CompassEmailServiceJob : IJob
    {
        #region Private Members
        private ICompassEmailService cookBookServiceInstance = null;
        #endregion

        #region Properties
        /// <summary>
        /// Property to get claim process service business manager instance
        /// </summary>
        private ICompassEmailService CookBookServiceInstance
        {
            get
            {
                if (cookBookServiceInstance == null)
                {
                    cookBookServiceInstance = new CompassEmailServiceManager();
                }
                return cookBookServiceInstance;
            }
        }
        #endregion 

        #region Public Methods
        /// <summary>
        /// Method to override Quartz Job - Execute method 
        /// </summary>
        /// <param name="context">Job Execution Context</param>
        public void Execute(IJobExecutionContext context)
        {
            LogTraceFactory.WriteLogWithCategory("Job Execution begins", LogTraceCategoryNames.Tracing);
            CookBookServiceInstance.SendExceptionEmail();
            LogTraceFactory.WriteLogWithCategory("Job Execution Ends", LogTraceCategoryNames.Tracing);
        }
        #endregion
    }
}