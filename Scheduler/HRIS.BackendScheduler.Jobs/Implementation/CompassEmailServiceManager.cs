﻿using System;
using HRIS.BackendScheduler.Jobs.Contracts;
using System.Configuration;
using Microsoft.Practices.Unity;
using System.ComponentModel.Composition;
using System.ComponentModel;
using HRIS.Aspects.Factory;
using HRIS.Aspects.Constants;
using HRIS.BackendScheduler.Jobs.Utilities;

namespace HRIS.BackendScheduler.Jobs.Implementation
{
    [Export(typeof(ICompassEmailService))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class CompassEmailServiceManager : ICompassEmailService
    {

        #region Public Methods
        /// <summary>
        /// Send Exception Email
        /// </summary>
        public void SendExceptionEmail()
        {
            try
            {                
                LogTraceFactory.WriteLogWithCategory("Calling method to Send Pending Emails", LogTraceCategoryNames.Tracing);
                ServiceHelper.GetCookBookService().SendExceptionEmail();
                LogTraceFactory.WriteLogWithCategory("Calling method to Send Pending Emails Completed",LogTraceCategoryNames.Tracing);
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.Message, LogTraceCategoryNames.Important);
            }

        }

        #endregion
    }
}
