﻿using HRIS.Aspects.Factory;
using HRIS.BackendScheduler.Jobs.CookBookService;
using System;
using System.Configuration;
using System.ServiceModel;

namespace HRIS.BackendScheduler.Jobs.Utilities
{
    public static class ServiceHelper
    {
        #region Service Methods

        /// <summary>
        /// Method to Get Admin service with endpoint and binding
        /// </summary>
        /// <returns></returns>
        public static ICookBookService GetCookBookService()
        {
            Uri serviceUri;
            CookBookServiceClient client = null;
            try
            {
                string serviceEndPoint = string.Format("{0}/{1}", ConfigurationManager.AppSettings[Constants.COOKBOOKSERVICE_ENDPOINTKEY], Constants.COOKBOOKSERVICE_ENDPOINT);

                serviceUri = new Uri(serviceEndPoint);
                EndpointAddress endpointAddress = new EndpointAddress(serviceUri);
                BasicHttpBinding binding = CreateBinding();
                client = new CookBookServiceClient(binding, endpointAddress);
                return client;
            }
            catch (Exception ex)
            {
                LogTraceFactory.WriteLogWithCategory(ex.ToString(), Aspects.Constants.LogTraceCategoryNames.Important);
                throw;
            }
            finally
            {
                serviceUri = null;
                //if (client != null)
                //{
                //    client.Close();
                //}
            }
        }


        /// <summary>
        /// Method to Create service binding
        /// </summary>
        /// <returns>binding</returns>
        public static BasicHttpBinding CreateBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            try
            {
                binding = new BasicHttpBinding();
                binding.MaxReceivedMessageSize = 2147483647;
                binding.MaxBufferSize = 2147483647;
                binding.MaxBufferPoolSize = 2147483647;

                binding.ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas()
                {
                    MaxArrayLength = int.MaxValue,
                    MaxDepth = int.MaxValue,
                    MaxStringContentLength = int.MaxValue,
                    MaxBytesPerRead = int.MaxValue,
                    MaxNameTableCharCount = int.MaxValue
                };

                binding.Security.Mode = BasicHttpSecurityMode.None;
                binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.None;
                binding.UseDefaultWebProxy = true;
                binding.OpenTimeout = TimeSpan.FromMinutes(10);
                binding.CloseTimeout = TimeSpan.FromMinutes(10);
                binding.ReceiveTimeout = TimeSpan.FromMinutes(10);
                binding.SendTimeout = TimeSpan.FromMinutes(10);
            }
            catch
            {
                //Do something
            }
            return binding;
        }

        #endregion
    }
}