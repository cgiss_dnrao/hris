﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HRIS.Mapper;
using HRIS.DataAdapter.Base.Contracts;

namespace HRIS.Business.Base.Contract
{
    public interface IManager
    {
        IRepositoryTransactionManager TransactionManager { get; set; }

        IEntityMapper MapEngine { get; set; }
    }
}
