﻿using HRIS.Business.Base.Contract;
using HRIS.DataAdapter.Base.Contracts;
using HRIS.Mapper;
using Microsoft.Practices.Unity;

namespace HRIS.Business.Base.Impl
{
    public class ImplBase : IManager
    {
        [Dependency]
        public IRepositoryTransactionManager TransactionManager { get; set; }

        [Dependency]
        public IEntityMapper MapEngine { get; set; }
    }
}
