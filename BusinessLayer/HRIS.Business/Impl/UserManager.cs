﻿using HRIS.Aspects.Constants;
using HRIS.Encryption.Interfaces;
using HRIS.Aspects.Factory;
using HRIS.Aspects.Utils;
using HRIS.Business.Base.Impl;
using HRIS.Business.Contract;
using HRIS.Data.Common;
using HRIS.Data.UserData;
using HRIS.DataAdapter.Factory;
using HRIS.DataAdapter.Factory.Contract;
using HRIS.Mapper;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using Microsoft.SharePoint.Client;
using HRIS.DataAdapter.Factory.Contract.Common;
using HRIS.DataAdapter.Factory.Transaction;
using HRIS.DataAdapter.Base.DataEnums;
using HRIS.Data.MasterData;

namespace HRIS.Business.Impl
{
    [Export(typeof(IUser))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class UserManager : ImplBase, IUser
    {
        [Dependency]
        public IUserMasterRepository UserMasterRepository { get; set; }      
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IAesCryptoEngine AesCryptoEngine { get; set; }

        #region NewCode

        public UserMasterResponseData GetUserData(string userName)
        {
            //LogTraceFactory.WriteLogWithCategory("Looged in at + ", LogTraceCategoryNames.General);
            UserMasterResponseData result = null;

            string encryptedUserName = AesCryptoEngine.EncryptData(userName.ToLower());

            procGetUserDataByUsername_Result procGetUserDataByUsername_Result = UserMasterRepository.GetUserDetailsByUsername(encryptedUserName);
            if (procGetUserDataByUsername_Result != null)
            {
                result = new UserMasterResponseData();
                result.UserId = procGetUserDataByUsername_Result.UserId;
                result.EmailId = procGetUserDataByUsername_Result.EmailId;
                result.UserName = procGetUserDataByUsername_Result.UserName;
                result.UserRoleId = procGetUserDataByUsername_Result.UserRoleId;
                result.UserRoleName = procGetUserDataByUsername_Result.UserRoleName;
                result.DisplayName = procGetUserDataByUsername_Result.DisplayName;
                GenericData.UserID = procGetUserDataByUsername_Result.UserId;
            }
            return result;
        }
        public string GetUserNameByEncryptedString(string encUsername,string key)
        {
            return AesCryptoEngine.DecryptData(encUsername,key);
        }

        public string AddUpdateUserDetails(UserMasterData userData)
        {
            string result = string.Empty;
            string national = EnumUtils<GeographicLocations>.GetDescription(GeographicLocations.National).ToString();
            try
            {
                if (userData != null)
                {
                    UserMaster data = new UserMaster();
                    var pre = UserMasterRepository.Find(e => e.UserName == userData.UserName).FirstOrDefault();

                    if (pre != null)
                    {
                        data = pre;
                    }

                    data.UserName = userData.UserName;
                    data.IsActive = userData.IsActive;
                    data.EmailId = userData.EmailId;
                    if (pre == null)
                    {
                        data.CreatedDate = DateTime.Now;
                        data.CreatedBy = "Super Admin";
                    }
                    else
                    {
                        data.ModifiedDate = DateTime.Now;
                    }

                    UserMasterRepository.Save(data);
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string DeleteUserData(string userName)
        {
            string result = string.Empty;
            try
            {
                UserMaster data = new UserMaster();
                data = UserMasterRepository.Find(e => e.UserName == userName).FirstOrDefault();
                if (data != null)
                {
                    UserMasterRepository.Delete(data);
                }
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }
        #endregion
    }
}
