﻿using HRIS.Business.Contract;
using HRIS.Data.Common;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using HRIS.Mapper;

using HRIS.Data.MasterData;
using System.Linq;

using HRIS.DataAdapter.Factory.Transaction;
using HRIS.DataAdapter.Factory.Contract.Transaction;
using System;
using System.Text.RegularExpressions;
using HRIS.Encryption.Interfaces;
using System.Globalization;
using HRIS.Data.Request;
using HRIS.Business.Base.Impl;

namespace HRIS.Business.Impl
{
    [Export(typeof(IMasterDataManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class MasterDataManager : ImplBase, IMasterDataManager
    {
        [Dependency]
        public IHRISMasterRepository HRISRepository { get; set; }
        [Dependency]
        public IEntityMapper EntityMapper { get; set; }
        [Dependency]
        public IAesCryptoEngine AesCryptoEngine { get; set; }

        public IList<HRISMasterData> GetHRISMasterDataList(bool isEXECOMData)
        {
            List<HRISMasterData> result = new List<HRISMasterData>();

            if (isEXECOMData)
            {
                var entityList = HRISRepository.GetEXECOMUserDataList();
                EntityMapper.Map<IList<procGetEXECOMUserDataList_Result>, IList<HRISMasterData>>(entityList, result);
            }
            else
            {
                var entityList = HRISRepository.GetHRISMasterDataList();
                EntityMapper.Map<IList<procGetHRISMasterList_Result>, IList<HRISMasterData>>(entityList, result);
            }

            if (result != null && result.Count > 0)
            {
                result.ForEach(m =>
                {
                    if (!string.IsNullOrWhiteSpace(m.EmployeeName))
                        m.EmployeeName = AesCryptoEngine.DecryptData(m.EmployeeName);
                    if (!string.IsNullOrWhiteSpace(m.DateOfBirth))
                    {
                        m.DateOfBirth = AesCryptoEngine.DecryptData(m.DateOfBirth);
                        try
                        {
                            if (m.DateOfBirth.Contains('T'))
                            {

                                var dateString = m.DateOfBirth.Split('T')[0].Split('-');
                                m.DateOfBirthAsString = Convert.ToDateTime(dateString[0] + "/" + dateString[1] + "/" + dateString[2]).ToString("dd-MMM-yyyy");

                            }
                            else
                            {
                                var dateString = m.DateOfBirth.Split(' ')[0].Split('/');
                                m.DateOfBirthAsString = Convert.ToDateTime(dateString[2] + "/" + dateString[0] + "/" + dateString[1]).ToString("dd-MMM-yyyy");
                            }
                        }
                        catch { }
                    }
                    if (!string.IsNullOrWhiteSpace(m.FirstName))
                        m.FirstName = AesCryptoEngine.DecryptData(m.FirstName);
                    if (!string.IsNullOrWhiteSpace(m.LastName))
                        m.LastName = AesCryptoEngine.DecryptData(m.LastName);
                    //if (!string.IsNullOrWhiteSpace(m.MobileNumber))
                    //    m.MobileNumber = AesCryptoEngine.DecryptData(m.MobileNumber);

                });
            }
            return result;
        }

        public EmployeeMasterData GetHRISMasterDataListPageWise(EmployeeRequest employeeRequest)
        {
            EmployeeMasterData result = new EmployeeMasterData();
            List<HRISMasterData> hRISMasterDatas = new List<HRISMasterData>();
            int totalCount = 0;
            var entityList = HRISRepository.GetHRISMasterDataListPageWise(employeeRequest, out totalCount);
            EntityMapper.Map<IList<procGetHRISMasterListPageWise_Result>, IList<HRISMasterData>>(entityList, hRISMasterDatas);

            if (hRISMasterDatas != null && hRISMasterDatas.Count > 0)
            {
                //hRISMasterDatas.ForEach(m =>
                //{
                //    if (!string.IsNullOrWhiteSpace(m.EmployeeName))
                //        m.EmployeeName = AesCryptoEngine.DecryptData(m.EmployeeName);
                //    if (!string.IsNullOrWhiteSpace(m.DateOfBirth))
                //    {
                //        m.DateOfBirth = AesCryptoEngine.DecryptData(m.DateOfBirth);
                //        if (m.DateOfBirth.Contains('T'))
                //        {
                //            var dateString = m.DateOfBirth.Split('T')[0].Split('-');
                //            m.DateOfBirthAsString = Convert.ToDateTime(dateString[0] + "/" + dateString[1] + "/" + dateString[2]).ToString("dd-MMM-yyyy");
                //        }
                //        else
                //        {
                //            if (m.DateOfBirth.Contains('-'))
                //            {
                //                var dateString = m.DateOfBirth.Split('-');
                //                m.DateOfBirthAsString = Convert.ToDateTime(dateString[2] + "/" + dateString[0] + "/" + dateString[1]).ToString("dd-MMM-yyyy");
                //            }
                //            else
                //            {
                //                var dateString = m.DateOfBirth.Split(' ')[0].Split('/');
                //                m.DateOfBirthAsString = Convert.ToDateTime(dateString[2] + "/" + dateString[0] + "/" + dateString[1]).ToString("dd-MMM-yyyy");
                //            }
                //        }
                //    }
                //    if (!string.IsNullOrWhiteSpace(m.FirstName))
                //        m.FirstName = AesCryptoEngine.DecryptData(m.FirstName);
                //    if (!string.IsNullOrWhiteSpace(m.LastName))
                //        m.LastName = AesCryptoEngine.DecryptData(m.LastName);
                //    //if (!string.IsNullOrWhiteSpace(m.MobileNumber))
                //    //    m.MobileNumber = AesCryptoEngine.DecryptData(m.MobileNumber);
                //    //Krish
                //    //m.LastModifiedOn = m.ModifiedOn.HasValue ? m.ModifiedOn : m.CreatedOn;
                //    //m.LastModifiedOnFormatted = m.LastModifiedOn.Value.ToString("dd-MMM-yyyy");
                //    //m.IsActiveFormatted = m.IsActive == true ? "Active" : "Inactive";
                //});
                //result.HRISMasterDataList = hRISMasterDatas.Where(a=>a.IsActiveFormatted.Contains(employeeRequest.SearchKeyword) || a.LastModifiedOnFormatted.Contains(employeeRequest.SearchKeyword)).ToList();
                result.HRISMasterDataList = hRISMasterDatas;
                result.TotalCount = totalCount;
            }
            return result;
        }

        public string SaveHRISMasterDataList(IList<HRISMasterData> model, bool changeStatus = false)
        {
            List<HRISMaster> entity = new List<HRISMaster>();
            if (model != null && model.Count > 0)
            {
                model.ToList().ForEach(m =>
                {
                    //if (!string.IsNullOrWhiteSpace(m.EmployeeName))
                    //    m.EmployeeName = AesCryptoEngine.EncryptData(m.EmployeeName);
                    if (!string.IsNullOrWhiteSpace(m.FirstName))
                        m.FirstName = AesCryptoEngine.EncryptData(m.FirstName);
                    if (!string.IsNullOrWhiteSpace(m.LastName))
                        m.LastName = AesCryptoEngine.EncryptData(m.LastName);
                    if (!string.IsNullOrWhiteSpace(m.DateOfBirth))
                        m.DateOfBirth = AesCryptoEngine.EncryptData(m.DateOfBirth);

                });
            }
            EntityMapper.Map<IList<HRISMasterData>, IList<HRISMaster>>(model, entity);

            string x = HRISRepository.SaveHRISMasterDataList(entity, changeStatus);
            return x;
        }

        public IList<UserPermissionData> GetUserPermissionsByModuleCode(int userID, string moduleCode)
        {
            throw new NotImplementedException();
        }

        public IList<ModuleData> GetModulesByUserID(int userID)
        {
            throw new NotImplementedException();
        }

        public IList<CostCenterMasterData> GetCostCenterMasterDataList(string CostCenter)
        {
            List<CostCenterMasterData> result = new List<CostCenterMasterData>();
            var entityList = HRISRepository.GetCostCenterMasterDataList(CostCenter).ToList();
            EntityMapper.Map<IList<CostCenterMaster>, IList<CostCenterMasterData>>(entityList, result);
            return result;
        }

        public IList<ProfitCenterMasterData> GetProfitCenterMasterDataList(string ProfitCenter)
        {
            List<ProfitCenterMasterData> result = new List<ProfitCenterMasterData>();
            var entityList = HRISRepository.GetProfitCenterMasterDataList(ProfitCenter).ToList();
            EntityMapper.Map<IList<ProfitCenterMaster>, IList<ProfitCenterMasterData>>(entityList, result);
            return result;
        }

        public string SaveProfitCenterDataList(ProfitCenterMasterData model)
        {
            ProfitCenterMaster entity = new ProfitCenterMaster();
            EntityMapper.Map<ProfitCenterMasterData, ProfitCenterMaster>(model, entity);
            string x = HRISRepository.SaveProfitCenterDataList(entity);
            return x;
        }

        public string SaveCostCenterDataList(CostCenterMasterData model)
        {
            CostCenterMaster entity = new CostCenterMaster();
            EntityMapper.Map<CostCenterMasterData, CostCenterMaster>(model, entity);
            string x = HRISRepository.SaveCostCenterDataList(entity);
            return x;
        }

        public IList<LeaderShipRoleMasterData> GetLeaderShipRoleMasterDataList(string LeaderShipRoleMaster)
        {
            List<LeaderShipRoleMasterData> result = new List<LeaderShipRoleMasterData>();
            var entityList = HRISRepository.GetLeaderShipRoleMasterDataList(LeaderShipRoleMaster).ToList();
            EntityMapper.Map<IList<LeaderShipRoleMaster>, IList<LeaderShipRoleMasterData>>(entityList, result);
            return result;
        }

        public string SaveLeaderShipRoleDataList(LeaderShipRoleMasterData model)
        {
            LeaderShipRoleMaster entity = new LeaderShipRoleMaster();
            EntityMapper.Map<LeaderShipRoleMasterData, LeaderShipRoleMaster>(model, entity);
            string x = HRISRepository.SaveLeaderShipRoleDataList(entity);
            return x;
        }

        public IList<TransferLeaderShipData> GetTransferLeaderShipDataList()
        {
            List<TransferLeaderShipData> result = new List<TransferLeaderShipData>();
            var entityList = HRISRepository.GetTransferLeadershipRoleList().ToList();
            EntityMapper.Map<IList<procGetTransferLeaderShipData_Result>, IList<TransferLeaderShipData>>(entityList, result);
            return result;
        }

        public IList<SiteMasterData> GetSiteDataList(string SiteCode, string SectorCode)
        {
            List<SiteMasterData> result = new List<SiteMasterData>();
            var entityList = HRISRepository.GetSiteDataList(SiteCode, SectorCode).ToList();
            EntityMapper.Map<IList<procGetSiteList_Result>, IList<SiteMasterData>>(entityList, result);
            return result;
        }

        public string SaveSiteSubSiteMapping(List<SiteSubSiteMappingData> model)
        {
            string x = HRISRepository.SaveSiteSubSiteMapping(model);
            return x;
        }
        //Krish 18-08-2022
        public IList<LocationDesignationMappingData> GetLocationDesignationMappingDataList()
        {
            List<LocationDesignationMappingData> result = new List<LocationDesignationMappingData>();
            var entityList = HRISRepository.GetLocationDesignationMappingDataList().ToList();
            EntityMapper.Map<IList<LocationDesignationMapping>, IList<LocationDesignationMappingData>>(entityList, result);
            return result;
        }

        public IList<LocationDesignationMappingData> GetUniqueLocationAndDesignationDataList()
        {
            List<LocationDesignationMappingData> result = new List<LocationDesignationMappingData>();
            var entityList = HRISRepository.GetUniqueLocationAndDesignationDataList().ToList();
            EntityMapper.Map<IList<procGetUniqueLocationAndDesignation_Result>, IList<LocationDesignationMappingData>>(entityList, result);
            return result;
        }
        public string SaveLocationDesignationData(string location, string designation)
        {
            string x = HRISRepository.SaveLocationDesignationData(location, designation);
            return x;
        }

        public IList<SectorDepartmentLeadershipMappingData> GetSectorDepartmentLeadershipMapping()
        {
            List<SectorDepartmentLeadershipMappingData> result = new List<SectorDepartmentLeadershipMappingData>();
            var entityList = HRISRepository.GetSectorDepartmentLeadershipMapping().ToList();
            EntityMapper.Map<IList<procGetSectorDepartmentLeadershipMapping_Result>, IList<SectorDepartmentLeadershipMappingData>>(entityList, result);
            return result;
        }

        public string SaveSectorDepartmentLeadershipMapping(SectorDepartmentLeadershipMappingData model)
        {
            string x = HRISRepository.SaveSectorDepartmentLeadershipMapping(model);
            return x;
        }
        public IList<CustomSelectListItem> GetSectorMasterList()
        {
            return HRISRepository.GetSectorMasterList().ToList();
        }
        public IList<CustomSelectListItem> GetDepartmentMasterList()
        {
            return HRISRepository.GetDepartmentMasterList().ToList();
        }
        public string SaveConsultantUploaderHistory(string status, string ecode, string userid, string batchnumb, string type)
        {
            string x = HRISRepository.SaveConsultantUploaderHistory(status,ecode,userid, batchnumb,type);
            return x;
        }
        public IList<ConsultantUploaderBatchHeaderData> GetConsultantUploaderBatchHeader()
        {
            List<ConsultantUploaderBatchHeaderData> result = new List<ConsultantUploaderBatchHeaderData>();
            var entityList = HRISRepository.GetConsultantUploaderBatchHeader().ToList();
            EntityMapper.Map<IList<ConsultantUploaderBatchHeader>, IList<ConsultantUploaderBatchHeaderData>>(entityList, result);
            return result;
        }
        public string UploadConsultantData(string xmlstring, string template, int userid, string historyBatchno)
        {
            string x = HRISRepository.UploadConsultantData(xmlstring,template,userid,historyBatchno);
            return x;
        }
        public IList<ConsultantUploaderByBatchData> GetConsultantUploaderByBatchData(string template)
        {
            List<ConsultantUploaderByBatchData> result = new List<ConsultantUploaderByBatchData>();
            var entityList = HRISRepository.GetConsultantUploaderByBatch(template).ToList();
            EntityMapper.Map<IList<procGetConsultantUploaderByBATCH_Result>, IList<ConsultantUploaderByBatchData>>(entityList, result);
            return result;
        }
        public IList<ConsultantUploaderBatchDetailsData> GetConsultantUploaderByBatchDetailsData(string template, string batchNo)
        {
            List<ConsultantUploaderBatchDetailsData> result = new List<ConsultantUploaderBatchDetailsData>();
            var entityList = HRISRepository.GetConsultantUploaderByBatchDetails(template, batchNo).ToList();
            EntityMapper.Map<IList<procGetConsultantUploaderBATCHDetails_Result>, IList<ConsultantUploaderBatchDetailsData>>(entityList, result);
            return result;
        }
        public IList<ConsultantUploaderData> GetConsultantUploaderData(string template)
        {
            List<ConsultantUploaderData> result = new List<ConsultantUploaderData>();
            var entityList = HRISRepository.GetConsultantUploaderData(template).ToList();
            EntityMapper.Map<IList<procGetConsultantUploader_Result>, IList<ConsultantUploaderData>>(entityList, result);
           
            return result;
        }
        public HRISMasterData GetHRISMasterDataByCode(string empCode)
        {
            HRISMasterData result = new HRISMasterData();
            var entityList= HRISRepository.GetHRISMasterDataByCode(empCode);
            EntityMapper.Map<HRISMaster, HRISMasterData>(entityList, result);
            return result;
        }
        public IList<DiceUploaderData> GetDiceUploaderData()
        {
            List<DiceUploaderData> result = new List<DiceUploaderData>();
            var entityList = HRISRepository.GetDiceUploaderData().ToList();
            EntityMapper.Map<IList<procGetDiceUploaderGrid_Result>, IList<DiceUploaderData>>(entityList, result);

            return result;
        }
        public string UploadDiceUploaderData(string xmlstring, int userid)
        {
            string x = HRISRepository.UploadDiceUploaderData(xmlstring,  userid);
            return x;
        }
        public IList<LensUploaderData> GetLensUploaderData()
        {
            List<LensUploaderData> result = new List<LensUploaderData>();
            var entityList = HRISRepository.GetLensUploaderData().ToList();
            EntityMapper.Map<IList<procGetLensUploader_Result>, IList<LensUploaderData>>(entityList, result);

            return result;
        }
        public string UploadLensUploaderData(string xmlstring, int userid)
        {
            string x = HRISRepository.UploadLensUploaderData(xmlstring, userid);
            return x;
        }
        public string SaveLensData(LensUploaderData model)
        {
            try
            {
                string x = HRISRepository.SaveLensData(model.EmployeeCode, Convert.ToBoolean(model.IsLensUser), Convert.ToInt32(model.ModifiedBy), model.ModifiedDate);
                return x;
            }
            catch (Exception ex)
            { }
            return "Failed";
        }
    }
}
