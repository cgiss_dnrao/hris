﻿using HRIS.Business.Base.Impl;
using HRIS.Business.Contract;
using HRIS.Data.Request;
using HRIS.DataAdapter.Factory;
using HRIS.DataAdapter.Factory.Contract;
using Microsoft.Practices.Unity;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.Composition;
using System;
using HRIS.DataAdapter.Factory.Contract.Common;
using HRIS.Data.MasterData;

namespace HRIS.Business.Impl
{
    [Export(typeof(IEmailManager))]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class EmailManager : ImplBase, IEmailManager
    {
        [Dependency]
        public IEmailRepository EmailRepository { get; set; }

        [Dependency]
        public IUserMasterRepository UserMasterRepository { get; set; }

        public void SendExceptionEmail()
        {
           // EmailRepository.SendPendingEmail();
        }

        public bool SaveReminderEmail(string webSiteUrl, SendEmailFilter request)
        {
            return true;
        }
    }
}
