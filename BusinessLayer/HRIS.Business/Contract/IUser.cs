﻿using HRIS.Business.Base.Contract;
using HRIS.Data.Common;
using HRIS.Data.MasterData;
using HRIS.Data.UserData;
using System.Collections.Generic;

namespace HRIS.Business.Contract
{
    public interface IUser: IManager
    {
        UserMasterResponseData GetUserData(string userName);

        string AddUpdateUserDetails(UserMasterData userData);

        string DeleteUserData(string userName);
        string GetUserNameByEncryptedString(string encUsername, string key);
    }
}
