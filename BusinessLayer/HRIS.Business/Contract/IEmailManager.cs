﻿using HRIS.Business.Base.Contract;
using HRIS.Data.MasterData;
using HRIS.Data.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HRIS.Business.Contract
{
    public interface IEmailManager : IManager
    {
        void SendExceptionEmail();

        bool SaveReminderEmail(string webSiteUrl, SendEmailFilter request);
    }
}
