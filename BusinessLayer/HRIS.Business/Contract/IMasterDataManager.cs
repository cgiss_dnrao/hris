﻿using HRIS.Data.Common;
using System.Collections.Generic;
using HRIS.Data.MasterData;
using HRIS.DataAdapter.Factory.Transaction;
using HRIS.Data.Request;

namespace HRIS.Business.Contract
{
    public interface IMasterDataManager
    {
        IList<HRISMasterData> GetHRISMasterDataList(bool isEXECOMData);
        EmployeeMasterData GetHRISMasterDataListPageWise(EmployeeRequest employeeRequest);
        IList<UserPermissionData> GetUserPermissionsByModuleCode(int userID, string moduleCode);
        IList<ModuleData>  GetModulesByUserID(int userID);
        IList<CostCenterMasterData> GetCostCenterMasterDataList(string CostCenter);
        IList<ProfitCenterMasterData> GetProfitCenterMasterDataList(string ProfitCenter);
        IList<LeaderShipRoleMasterData> GetLeaderShipRoleMasterDataList(string LeaderShipRoleMaster);
        IList<TransferLeaderShipData> GetTransferLeaderShipDataList();
        IList<SiteMasterData> GetSiteDataList(string SiteCode, string SectorCode);
        string SaveHRISMasterDataList(IList<HRISMasterData> model, bool changeStatus = false);
        string SaveProfitCenterDataList(ProfitCenterMasterData model);
        string SaveCostCenterDataList(CostCenterMasterData model);
        string SaveLeaderShipRoleDataList(LeaderShipRoleMasterData model);
        string SaveSiteSubSiteMapping(List<SiteSubSiteMappingData> model);
        //Krish
        IList<LocationDesignationMappingData> GetLocationDesignationMappingDataList();
        IList<LocationDesignationMappingData> GetUniqueLocationAndDesignationDataList();
        string SaveLocationDesignationData(string location, string designation);
        IList<SectorDepartmentLeadershipMappingData> GetSectorDepartmentLeadershipMapping();
        string SaveSectorDepartmentLeadershipMapping(SectorDepartmentLeadershipMappingData model);
        IList<CustomSelectListItem> GetSectorMasterList();
        IList<CustomSelectListItem> GetDepartmentMasterList();
        string SaveConsultantUploaderHistory(string status, string ecode, string userid, string batchnumb, string type);
        IList<ConsultantUploaderBatchHeaderData> GetConsultantUploaderBatchHeader();
        string UploadConsultantData(string xmlstring, string template, int userid, string historyBatchno);
        IList<ConsultantUploaderData> GetConsultantUploaderData(string template);
        IList<ConsultantUploaderBatchDetailsData> GetConsultantUploaderByBatchDetailsData(string template, string batchNo);
        IList<ConsultantUploaderByBatchData> GetConsultantUploaderByBatchData(string template);
        HRISMasterData GetHRISMasterDataByCode(string empCode);
        IList<DiceUploaderData> GetDiceUploaderData();
        string UploadDiceUploaderData(string xmlstring, int userid);
        IList<LensUploaderData> GetLensUploaderData();
        string UploadLensUploaderData(string xmlstring, int userid);
        string SaveLensData(LensUploaderData model);
    }
}
