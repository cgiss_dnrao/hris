﻿using HRIS.Data.UserData;
using HRIS.MvcApp.Core;
using HRIS.MvcApp.HRISService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using System.IdentityModel.Claims;

namespace HRIS.MvcApp.Controllers
{
    public class BaseController : Controller
    {
        IHRISService ServiceClient = ServiceHelper.GetHRISService();
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (Session["UserLoginDetails"] == null || (Session["UserLoginDetails"] != null && ((UserMasterResponseData)Session["UserLoginDetails"]).UserId <= 0))
            {
                if (!Request.IsAuthenticated)
                {
                    HttpContext.GetOwinContext().Authentication.Challenge(
                        new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["redirectUri"].ToString() },
                        OpenIdConnectAuthenticationDefaults.AuthenticationType);
                }
                else
                {
                    //string UserName = User.Identity.Name;
                    var UserInstance = User.Identity as System.Security.Claims.ClaimsIdentity;
                    string UserName = UserInstance.FindFirst("preferred_username").Value;

                    if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                    {
                        UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                    }
                    var res = ServiceClient.GetUserData(UserName);
                    Session["UserLoginDetails"] = res;
                    if (res == null && (res != null && res.UserId <= 0))
                        filterContext.Result = new RedirectResult("~/Home/NotAuthenticated");
                }
            }
        }
    }
}