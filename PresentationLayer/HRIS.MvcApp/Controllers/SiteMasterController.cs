﻿using HRIS.Data.MasterData;
using HRIS.Data.UserData;
using HRIS.MvcApp.Core;
using HRIS.MvcApp.Filters;
using HRIS.MvcApp.HRISService;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HRIS.MvcApp.Controllers
{
    public class SiteMasterController : BaseController
    {
        private IHRISService ServiceClient = ServiceHelper.GetHRISService();

        [CustAuthFilter]
        public ActionResult Index()
        {
            return View();
        }

        [CustAuthFilter]
        public ActionResult GetSiteDataList()
        {
            var siteList = ServiceClient.GetSiteDataList("","");
            var jsonResult = Json(siteList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        
        [CustAuthFilter]
        public ActionResult SaveSiteSubSiteMapping(List<SiteSubSiteMappingData> model)
        {
            var result = ServiceClient.SaveSiteSubSiteMapping(model.ToArray());
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}