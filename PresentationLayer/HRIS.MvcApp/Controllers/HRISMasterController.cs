﻿using ClosedXML.Excel;
using HRIS.Aspects.Factory;
using HRIS.Data;
using HRIS.Data.Request;
using HRIS.Data.UserData;
using HRIS.MvcApp.Core;
using HRIS.MvcApp.Filters;
using HRIS.MvcApp.HRISService;
using HRIS.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
//using System.Web.Script.Serialization;
using HRIS.Data.MasterData;
using HRIS.Encryption.EncryptImpl;
using System.Reflection;
using Newtonsoft.Json;
using System.Globalization;
using System.Runtime.CompilerServices;
using HRIS.Encryption.Interfaces;

namespace HRIS.MvcApp.Controllers
{
    public class HRISMasterController : BaseController
    {
        private IHRISService ServiceClient = ServiceHelper.GetHRISService();

        [CustAuthFilter]
        public ActionResult Index()
        {
            return View();
        }

        [CustAuthFilter]
        public ActionResult GetLoginUserDetails()
        {
            //string UserName = User.Identity.Name;
            //string uname = "";
            //if (UserName.IndexOf("\\") > 0)
            //{
            //    uname = UserName.Split('\\')[1];
            //}
            //else
            //{
            //    uname = UserName;
            //}
            //if (string.IsNullOrEmpty(uname) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
            //{
            //    uname = ConfigurationManager.AppSettings[Constants.TEST_USER];
            //}
            var res = Session["UserLoginDetails"];


            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ProfitCenter()
        {
            return View();
        }

        [CustAuthFilter]
        public ActionResult SaveProfitCenterData(ProfitCenterMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveProfitCenterDataList(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult CostCenter()
        {
            return View();
        }


        [CustAuthFilter]
        public ActionResult SaveCostCenterData(CostCenterMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveCostCenterDataList(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult ITILiteMaster()
        {
            return View();
        }

        [CustAuthFilter]
        public ActionResult SaveLeaderShipRoleData(LeaderShipRoleMasterData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];

            if (model.ID == 0)
            {
                model.CreatedBy = userData.UserId;
                model.CreatedOn = DateTime.Now;
            }
            else
            {
                model.ModifiedBy = userData.UserId;
                model.ModifiedOn = DateTime.Now;
            }
            var result = ServiceClient.SaveLeaderShipRoleDataList(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public ActionResult LeaderShipRole()
        {
            return View();
        }
        [CustAuthFilter]
        public ActionResult LocationDesignation()
        {
            return View();
        }
        [CustAuthFilter]
        public ActionResult GetLocationDesignationMappingDataList()
        {
            // UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var leaderShipRoleList = ServiceClient.GetLocationDesignationMappingDataList();
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetUniqueLocationAndDesignationData()
        {
            // UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var leaderShipRoleList = ServiceClient.GetUniqueLocationAndDesignationDataList();
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult SaveLocationDesignationData(string location, string designation)
        {

            var result = ServiceClient.SaveLocationDesignationData(location, designation);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ConsultantDataUploader()
        {
            return View();
        }
        [CustAuthFilter]
        public ActionResult DiceUploader()
        {
            return View();
        }

        [CustAuthFilter]
        public ActionResult LensUploader()
        {
            return View();
        }
        //[HttpGet]

        [CustAuthFilter]
        public ActionResult DownloadExcTemplateCDU(string template)
        {
            //var mogList = ServiceClient.GetNOTMappedMOGDataList(GetRequestData());
            DataTable dt = new DataTable();
            string name = "", filename = "";
            //if (template == "Group")
            //{
            //    dt.Columns.Add("EmployeeCode");
            //    dt.Columns.Add("EmployeeName");
            //    dt.Columns.Add("GroupYesNo");
            //    name = "GroupUploader";
            //    filename = "GroupUploader";
            //}
            if (template == "Cost Center")
            {
                dt.Columns.Add("EmployeeCode");
                dt.Columns.Add("CostCenterCode");
                dt.Columns.Add("ProfitCenterCode");
                dt.Columns.Add("CostCenterRemarks");
                dt.Columns.Add("ProfitCenterRemarks");
                name = "CostCenterUploader";
                filename = "CostCenterUploader";
            }
            else if (template == "Map4")
            {
                dt.Columns.Add("EmployeeCode");
                dt.Columns.Add("EmployeeName");
                dt.Columns.Add("Division");
                dt.Columns.Add("TEPortalUserYesNo");
                name = "Map4Uploader";
                filename = "Map4Uploader";
            }
            else if (template == "Consultant")
            {
                dt.Columns.Add("EmployeeCode");
                dt.Columns.Add("EmployeeName");
                dt.Columns.Add("FirstName");
                dt.Columns.Add("LastName");
                dt.Columns.Add("Gender");
                dt.Columns.Add("DateOfBirth");
                dt.Columns.Add("Department");
                dt.Columns.Add("HRISSector");
                dt.Columns.Add("Grade");
                dt.Columns.Add("Designation");
                dt.Columns.Add("Company");
                dt.Columns.Add("ProfitCenter");
                dt.Columns.Add("CostCenter");
                dt.Columns.Add("OfficeEmailAddress");
                dt.Columns.Add("Location");
                dt.Columns.Add("State");
                dt.Columns.Add("MobileNumber");
                dt.Columns.Add("L1ManagerCode");
                dt.Columns.Add("L1Manager");
                dt.Columns.Add("L2ManagerCode");
                dt.Columns.Add("L2Manager");
                dt.Columns.Add("TEPortalUserYesNo");
                dt.Columns.Add("GroupEmployeeYesNo");
                dt.Columns.Add("ConstultantEmployeeYesNo");
                dt.Columns.Add("ExeComEmpCode");
                dt.Columns.Add("ExeComEmpName");
                dt.Columns.Add("CFOEmpCode");
                dt.Columns.Add("CFOEmpName");
                name = "ConsultantUploader";
                filename = "ConsultantUploader";
            }
            //dt.Columns.Add("Alias");
            //dt.Columns.Add("ArticleNumber");
            //foreach (MOGNotMapped p in mogList)
            //{
            //    dt.Rows.Add(p.MOGCode, p.Name, p.Alias, p.ArticalNumber);
            //}
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName = name + ".xlsx";
                wb.Worksheets.Add(dt);

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename + ".xlsx");
                }
            }
        }


        [CustAuthFilter]
        [HttpPost]
        public string UploadExcelsheet()
        {
            string Res = string.Empty;
            DataTable dt = new DataTable();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            int username = userData.UserId;

            if (Request.Files.Count > 0)
            {
                var template = Request.Params["template"];
                var file = Request.Files[0];
                string filePath = string.Empty;
                if (Request.Files != null)
                {
                    string extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".xlsx" };
                    string contentType = string.Empty;

                    if (!allowedExtensions.Contains(extension))
                    {
                        return "Invalid file type. Only xlsx files are allowed.";
                    }

                    if (extension == ".xlsx")
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    }
                   
                    var allowedMimeTypes = new[] { "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" };

                    if (!allowedMimeTypes.Contains(contentType))
                    {
                        return "Invalid file type. Only JPEG and PNG files are allowed.";
                    }

                    string path = Path.Combine(Server.MapPath("~/Content/Upload/"), Path.GetFileName(file.FileName));
                    //Saving the file
                    file.SaveAs(path);
                    bool validation = true;
                    AesCryptoEngine enc = new AesCryptoEngine();
                   
                    var generator = new Random();
                    string bno = "Batch-" + generator.Next(0, 999999).ToString("D6");
                    using (XLWorkbook workbook = new XLWorkbook(path))
                    {
                        IXLWorksheet worksheet = workbook.Worksheet(1);
                        bool FirstRow = true;

                        //Range for reading the cells based on the last cell used.
                        string readRange = "1:1";
                        List<string> header = new List<string>();

                        foreach (IXLRow row in worksheet.RowsUsed())
                        {

                            if (FirstRow)
                            {

                                readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Columns.Add(cell.Value.ToString());
                                    header.Add(cell.Value.ToString());
                                }
                                FirstRow = false;
                            }
                            else
                            {

                                dt.Rows.Add();
                                int cellIndex = 0;
                                string ecode = string.Empty;
                                string errorMessage = string.Empty;
                                //bool rowSuccess = true;
                                foreach (IXLCell cell in row.Cells(readRange))
                                {

                                    //var colSuccess = true;
                                    var value = cell.Value.ToString();
                                    if (cellIndex == 0)
                                        ecode = value;
                                    var chktemplate = template == "Cost Center" && (cellIndex == 3 || cellIndex == 4);
                                    if (!string.IsNullOrEmpty(value) || chktemplate)
                                    {
                                        if (template == "Consultant" && cellIndex == 5)
                                        {
                                            try
                                            {
                                                DateTime datevalue = Convert.ToDateTime(cell.Value.ToString());
                                                dt.Rows[dt.Rows.Count - 1][cellIndex] = enc.EncryptData(datevalue.ToString(), ConfigurationManager.AppSettings["key"]);
                                            }
                                            catch (Exception ex)
                                            {
                                                //date value not valid
                                                //Insert history
                                                errorMessage += "Date is not valid - " + header[cellIndex] + "field <br>";
                                                validation = false;
                                            }
                                        }
                                        else if (template == "Consultant" && cellIndex == 2)
                                        {
                                            dt.Rows[dt.Rows.Count - 1][cellIndex] = enc.EncryptData(cell.Value.ToString(), ConfigurationManager.AppSettings["key"]);
                                        }
                                        else if (template == "Consultant" && cellIndex == 3)
                                        {
                                            dt.Rows[dt.Rows.Count - 1][cellIndex] = enc.EncryptData(cell.Value.ToString(), ConfigurationManager.AppSettings["key"]);
                                        }
                                        else
                                        {
                                            if (cellIndex == 22)
                                            {
                                                if (cell.Value.ToString() == "Yes")
                                                {
                                                    var getconsul = ServiceClient.GetHRISMasterDataByCode(ecode);
                                                    if (getconsul == null)
                                                    {
                                                        validation = false;
                                                        errorMessage += "Emnployee does not exist. Employee Code - " + ecode + "<br>";
                                                    }
                                                    else
                                                    {
                                                        dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                if (cellIndex == 0 && template == "Map4")
                                                {
                                                    var getconsul = ServiceClient.GetHRISMasterDataByCode(ecode);
                                                    if (getconsul != null && getconsul.ID==0)
                                                    {
                                                        validation = false;
                                                        errorMessage += "Emnployee does not exist. Employee Code - " + ecode + "<br>";
                                                    }
                                                    else
                                                    {
                                                        dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                                    }
                                                }
                                                else
                                                    dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                            }
                                        }
                                        //if (colSuccess)
                                    }
                                    else
                                    {
                                        //cell value missing
                                        //Insert history
                                        if (!string.IsNullOrEmpty(ecode))
                                            errorMessage += "Value missing -" + header[cellIndex] + " field <br>";
                                        else
                                            errorMessage += "Employee Code is missing <br>";
                                        validation = false;
                                    }
                                    cellIndex++;
                                }
                                //END Column loop
                                //Check validation of row
                                if (validation)
                                {
                                    ServiceClient.SaveConsultantUploaderHistory("Successfully read", ecode, username.ToString(), bno, template);
                                }
                                else
                                {
                                    ServiceClient.SaveConsultantUploaderHistory(errorMessage, ecode, username.ToString(), bno, template);
                                }
                            }

                        }
                        //}
                        dt.TableName = template == "Cost Center" ? "CostCenterUploader" : (template == "Consultant" ? "ConsultantUploader" : "Map4Uploader");
                        MemoryStream str = new MemoryStream();
                        dt.WriteXml(str, true);
                        str.Seek(0, SeekOrigin.Begin);
                        StreamReader sr = new StreamReader(str);
                        string xmlstr;
                        xmlstr = sr.ReadToEnd();
                        if (dt.Rows.Count > 0)
                        {
                            if (validation)
                            {
                                Res = ServiceClient.UploadConsultantData(xmlstr, template, username, bno);
                            }
                            else
                            {
                                Res = "Validation error occured. File not uploaded.";
                            }
                        }
                        else
                            Res = "The uploaded file is empty";
                    }


                }

            }
            return Res;
        }

        [CustAuthFilter]
        public ActionResult GetConsultantUploaderHISTORY(string template)
        {
            //var getHistory = ServiceClient.GetConsultantUploaderBatchHeader();
            var getHistory = ServiceClient.GetConsultantUploaderByBatchData(template);
            if (getHistory.Count() > 0)
            {
                //CompassEncryption encryption = new CompassEncryption();
                foreach (var item in getHistory)
                {
                    item.UploadedBy = ServiceClient.GetUserNameByEncryptedString(item.UploadedBy, ConfigurationManager.AppSettings["key"]);
                }
            }
            var jsonResult = Json(getHistory, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConsultantUploaderHISTORYByBatch(string batchno, string template)
        {
            //var getHistory = ServiceClient.GetConsultantUploaderBatchHeader();
            var getHistory = ServiceClient.GetConsultantUploaderByBatchDetailsData(template, batchno);
            var jsonResult = Json(getHistory, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetConsultantUploaderData(string template)
        {
            var getCData = ServiceClient.GetConsultantUploaderData(template);
            if (template == "Consultant")
            {
                AesCryptoEngine enc = new AesCryptoEngine();
                foreach (var item in getCData)
                {
                    item.FirstName = enc.DecryptData(item.FirstName, ConfigurationManager.AppSettings["key"]);
                    item.LastName = enc.DecryptData(item.LastName, ConfigurationManager.AppSettings["key"]);
                }
            }
            var jsonResult = Json(getCData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        public ActionResult DownloadExcTemplateDice()
        {
            //var mogList = ServiceClient.GetNOTMappedMOGDataList(GetRequestData());
            DataTable dt = new DataTable();
            string name = "", filename = "";

            //EmployeeCode	EmployeeName	Email	MobileNumber	SiteName	Moderate	CostCenter	ProfitCenter	Region	Org	Entity	BACode	BAName	BAEmail	BAContactNo	
            //L1Code	L1Name	L1Email	L1ContactNo	L2Code	L2Name	L2Email	L2ContactNo	UPINumberICICI	PDCCardNo	L3Code	L3Name	L3Email	L3ContactNo	
            //L4Code	L4Name	L4Email	L4ContactNo	L5Code	L5Name	L5Email	L5ContactNo
            dt.Columns.Add("UserType");
            dt.Columns.Add("EmployeeCode");
            dt.Columns.Add("EmployeeName");
            dt.Columns.Add("Email");
            dt.Columns.Add("MobileNumber");
            //dt.Columns.Add("SiteName");
            dt.Columns.Add("Moderate");
            dt.Columns.Add("CostCenter");
            dt.Columns.Add("ProfitCenter");
            dt.Columns.Add("Region");
            dt.Columns.Add("Org");
            dt.Columns.Add("Entity");
            dt.Columns.Add("BACode");
            dt.Columns.Add("BAName");
            dt.Columns.Add("BAEmail");
            dt.Columns.Add("BAContactNo");
            dt.Columns.Add("L1Code");
            //dt.Columns.Add("L1Name");
            //dt.Columns.Add("L1Email");
            //dt.Columns.Add("L1ContactNo");
            dt.Columns.Add("L2Code");
            //dt.Columns.Add("L2Name");
            //dt.Columns.Add("L2Email");
            //dt.Columns.Add("L2ContactNo");
            dt.Columns.Add("L3Code");
            dt.Columns.Add("L4Code");
            dt.Columns.Add("L5Code");
            dt.Columns.Add("UPINumberICICI");
            //dt.Columns.Add("PDCCardNo");
            dt.Columns.Add("EmployeeSAPID");
            dt.Columns.Add("PreviousCostCenter");
            //dt.Columns.Add("L3Code");
            //dt.Columns.Add("L3Name");
            //dt.Columns.Add("L3Email");
            //dt.Columns.Add("L3ContactNo");
            //dt.Columns.Add("L4Code");
            //dt.Columns.Add("L4Name");
            //dt.Columns.Add("L4Email");
            //dt.Columns.Add("L4ContactNo");
            //dt.Columns.Add("L5Code");
            //dt.Columns.Add("L5Name");
            //dt.Columns.Add("L5Email");
            //dt.Columns.Add("L5ContactNo");            
            dt.Columns.Add("Action");
            name = "DiceUploader";
            filename = "DiceUploader";

            //dt.Columns.Add("Alias");
            //dt.Columns.Add("ArticleNumber");
            //foreach (MOGNotMapped p in mogList)
            //{
            //    dt.Rows.Add(p.MOGCode, p.Name, p.Alias, p.ArticalNumber);
            //}
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName = name + ".xlsx";
                wb.Worksheets.Add(dt);

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename + ".xlsx");
                }
            }
        }
        [CustAuthFilter]
        public ActionResult GetDiceUploaderData()
        {
            var getCData = ServiceClient.GetDiceUploaderData();
            //if (template == "Consultant")
            //{
            //    AesCryptoEngine enc = new AesCryptoEngine();
            //    foreach (var item in getCData)
            //    {
            //        item.FirstName = enc.DecryptData(item.FirstName, ConfigurationManager.AppSettings["key"]);
            //        item.LastName = enc.DecryptData(item.LastName, ConfigurationManager.AppSettings["key"]);
            //    }
            //}
            var jsonResult = Json(getCData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [HttpPost]
        public string UploadDiceExcelsheet()
        {
            string Res = string.Empty;
            DataTable dt = new DataTable();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            int username = userData.UserId;

            if (Request.Files.Count > 0)
            {
                //var template = Request.Params["template"];
                var file = Request.Files[0];
                // List<ProductModel> _lstProductMaster = new List<ProductModel>();
                string filePath = string.Empty;
                if (Request.Files != null)
                {
                    string extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".xlsx" };
                    string contentType = string.Empty;

                    if (!allowedExtensions.Contains(extension))
                    {
                        return "Invalid file type. Only xlsx files are allowed.";
                    }

                    if (extension == ".xlsx")
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    }

                    var allowedMimeTypes = new[] { "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" };

                    if (!allowedMimeTypes.Contains(contentType))
                    {
                        return "Invalid file type. Only JPEG and PNG files are allowed.";
                    }
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/"), Path.GetFileName(file.FileName));
                    //Saving the file
                    file.SaveAs(path);
                    bool validation = true;
                    AesCryptoEngine enc = new AesCryptoEngine();

                    var generator = new Random();
                    string bno = "Batch-" + generator.Next(0, 999999).ToString("D6");
                    using (XLWorkbook workbook = new XLWorkbook(path))
                    {
                        IXLWorksheet worksheet = workbook.Worksheet(1);
                        bool FirstRow = true;

                        //Range for reading the cells based on the last cell used.
                        string readRange = "1:1";
                        List<string> header = new List<string>();

                        foreach (IXLRow row in worksheet.RowsUsed())
                        {

                            if (FirstRow)
                            {

                                readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Columns.Add(cell.Value.ToString());
                                    header.Add(cell.Value.ToString());
                                }
                                FirstRow = false;
                            }
                            else
                            {

                                dt.Rows.Add();
                                int cellIndex = 0;
                                string ecode = string.Empty;
                                string errorMessage = string.Empty;
                                //bool rowSuccess = true;
                                foreach (IXLCell cell in row.Cells(readRange))
                                {

                                    //var colSuccess = true;
                                    var value = cell.Value.ToString();
                                    if (cellIndex == 0)
                                        ecode = value;
                                    //var chktemplate = template == "Cost Center" && (cellIndex == 3 || cellIndex == 4);
                                    //if (!string.IsNullOrEmpty(value))
                                    //{                                       
                                    //if (cellIndex == 22)
                                    //{
                                    //    if (cell.Value.ToString() == "Yes")
                                    //    {
                                    //        var getconsul = ServiceClient.GetHRISMasterDataByCode(ecode);
                                    //        if (getconsul == null)
                                    //        {
                                    //            validation = false;
                                    //            errorMessage += "Emnployee does not exist. Employee Code - " + ecode + "<br>";
                                    //        }
                                    //        else
                                    //        {
                                    //            dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //{
                                    dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                    //}

                                    //if (colSuccess)
                                    //{
                                    //    //Insert history
                                    //    ServiceClient.SaveConsultantUploaderHistory("Successfully read", ecode, username.ToString(), bno, template);
                                    //}
                                    //}
                                    //else
                                    //{
                                    //    //cell value missing
                                    //    //Insert history
                                    //    if (!string.IsNullOrEmpty(ecode))
                                    //        errorMessage += "Value missing -" + header[cellIndex] + " field <br>";
                                    //    //ServiceClient.SaveConsultantUploaderHistory("Value missing -" + header[cellIndex] + " field", ecode, username.ToString(), bno, template);
                                    //    else
                                    //        errorMessage += "Employee Code is missing <br>";
                                    //    //ServiceClient.SaveConsultantUploaderHistory("Employee Code is missing", ecode, username.ToString(), bno, template);
                                    //    validation = false;
                                    //}
                                    cellIndex++;
                                }
                                //END Column loop
                                //Check validation of row
                                //if (validation)
                                //{
                                //    ServiceClient.SaveConsultantUploaderHistory("Successfully read", ecode, username.ToString(), bno, template);
                                //}
                                //else
                                //{
                                //    ServiceClient.SaveConsultantUploaderHistory(errorMessage, ecode, username.ToString(), bno, template);
                                //}
                            }

                        }
                        //}
                        //dt.TableName = template == "Cost Center" ? "CostCenterUploader" : (template == "Consultant" ? "ConsultantUploader" : "Map4Uploader");
                        dt.TableName = "DiceUploader";
                        MemoryStream str = new MemoryStream();
                        dt.WriteXml(str, true);
                        str.Seek(0, SeekOrigin.Begin);
                        StreamReader sr = new StreamReader(str);
                        string xmlstr;
                        xmlstr = sr.ReadToEnd();
                        if (dt.Rows.Count > 0)
                        {
                            //if (validation)
                            //{
                            //string UserName = string.Empty;
                            //var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                            //if (UserInstance.IsAuthenticated)
                            //{
                            //    UserName = UserInstance.FindFirst("preferred_username").Value;
                            //}
                            //var res = ServiceClient.GetUserData(UserName, null);

                            Res = ServiceClient.UploadDiceUploaderData(xmlstr, username);
                            //}
                            //else
                            //{
                            //    Res = "Validation error occured. File not uploaded.";
                            //}
                        }
                        else
                            Res = "The uploaded file is empty";
                    }


                }

            }
            return Res;
        }

        [CustAuthFilter]
        public ActionResult DownloadExcTemplateLens()
        {
            //var mogList = ServiceClient.GetNOTMappedMOGDataList(GetRequestData());
            DataTable dt = new DataTable();
            string name = "", filename = "";

            //EmployeeCode	EmployeeName	Email	MobileNumber	SiteName	Moderate	CostCenter	ProfitCenter	Region	Org	Entity	BACode	BAName	BAEmail	BAContactNo	
            //L1Code	L1Name	L1Email	L1ContactNo	L2Code	L2Name	L2Email	L2ContactNo	UPINumberICICI	PDCCardNo	L3Code	L3Name	L3Email	L3ContactNo	
            //L4Code	L4Name	L4Email	L4ContactNo	L5Code	L5Name	L5Email	L5ContactNo
            dt.Columns.Add("EmployeeCode");
            dt.Columns.Add("EmployeeName");
            
            dt.Columns.Add("LensUserYesNo");
            name = "LensUploader";
            filename = "LensUploader";

            //dt.Columns.Add("Alias");
            //dt.Columns.Add("ArticleNumber");
            //foreach (MOGNotMapped p in mogList)
            //{
            //    dt.Rows.Add(p.MOGCode, p.Name, p.Alias, p.ArticalNumber);
            //}
            using (XLWorkbook wb = new XLWorkbook())
            {
                dt.TableName = name + ".xlsx";
                wb.Worksheets.Add(dt);

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", filename + ".xlsx");
                }
            }
        }
        [CustAuthFilter]
        public ActionResult GetLensUploaderData()
        {
            var getCData = ServiceClient.GetLensUploaderData();
            //if (template == "Consultant")
            //{
            //    AesCryptoEngine enc = new AesCryptoEngine();
            //    foreach (var item in getCData)
            //    {
            //        item.FirstName = enc.DecryptData(item.FirstName, ConfigurationManager.AppSettings["key"]);
            //        item.LastName = enc.DecryptData(item.LastName, ConfigurationManager.AppSettings["key"]);
            //    }
            //}
            var jsonResult = Json(getCData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        [HttpPost]
        public string UploadLensExcelsheet()
        {
            string Res = string.Empty;
            DataTable dt = new DataTable();
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            int username = userData.UserId;

            if (Request.Files.Count > 0)
            {
                //var template = Request.Params["template"];
                var file = Request.Files[0];
                // List<ProductModel> _lstProductMaster = new List<ProductModel>();
                string filePath = string.Empty;
                if (Request.Files != null)
                {
                    string extension = Path.GetExtension(file.FileName);
                    var allowedExtensions = new[] { ".xlsx" };
                    string contentType = string.Empty;

                    if (!allowedExtensions.Contains(extension))
                    {
                        return "Invalid file type. Only xlsx files are allowed.";
                    }

                    if (extension == ".xlsx")
                    {
                        contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    }

                    var allowedMimeTypes = new[] { "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" };

                    if (!allowedMimeTypes.Contains(contentType))
                    {
                        return "Invalid file type. Only JPEG and PNG files are allowed.";
                    }
                    string path = Path.Combine(Server.MapPath("~/Content/Upload/"), Path.GetFileName(file.FileName));
                    //Saving the file
                    file.SaveAs(path);
                    bool validation = true;
                    AesCryptoEngine enc = new AesCryptoEngine();

                    var generator = new Random();
                    string bno = "Batch-" + generator.Next(0, 999999).ToString("D6");
                    using (XLWorkbook workbook = new XLWorkbook(path))
                    {
                        IXLWorksheet worksheet = workbook.Worksheet(1);
                        bool FirstRow = true;

                        //Range for reading the cells based on the last cell used.
                        string readRange = "1:1";
                        List<string> header = new List<string>();

                        foreach (IXLRow row in worksheet.RowsUsed())
                        {

                            if (FirstRow)
                            {

                                readRange = string.Format("{0}:{1}", 1, row.LastCellUsed().Address.ColumnNumber);
                                foreach (IXLCell cell in row.Cells(readRange))
                                {
                                    dt.Columns.Add(cell.Value.ToString());
                                    header.Add(cell.Value.ToString());
                                }
                                FirstRow = false;
                            }
                            else
                            {

                                dt.Rows.Add();
                                int cellIndex = 0;
                                string ecode = string.Empty;
                                string errorMessage = string.Empty;
                                //bool rowSuccess = true;
                                foreach (IXLCell cell in row.Cells(readRange))
                                {

                                    //var colSuccess = true;
                                    var value = cell.Value.ToString();
                                    if (cellIndex == 0)
                                        ecode = value;
                                    //var chktemplate = template == "Cost Center" && (cellIndex == 3 || cellIndex == 4);
                                    //if (!string.IsNullOrEmpty(value))
                                    //{                                       
                                    //if (cellIndex == 22)
                                    //{
                                    //    if (cell.Value.ToString() == "Yes")
                                    //    {
                                    //        var getconsul = ServiceClient.GetHRISMasterDataByCode(ecode);
                                    //        if (getconsul == null)
                                    //        {
                                    //            validation = false;
                                    //            errorMessage += "Emnployee does not exist. Employee Code - " + ecode + "<br>";
                                    //        }
                                    //        else
                                    //        {
                                    //            dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                    //        }
                                    //    }
                                    //}
                                    //else
                                    //{
                                    dt.Rows[dt.Rows.Count - 1][cellIndex] = cell.Value.ToString();
                                    //}

                                    //if (colSuccess)
                                    //{
                                    //    //Insert history
                                    //    ServiceClient.SaveConsultantUploaderHistory("Successfully read", ecode, username.ToString(), bno, template);
                                    //}
                                    //}
                                    //else
                                    //{
                                    //    //cell value missing
                                    //    //Insert history
                                    //    if (!string.IsNullOrEmpty(ecode))
                                    //        errorMessage += "Value missing -" + header[cellIndex] + " field <br>";
                                    //    //ServiceClient.SaveConsultantUploaderHistory("Value missing -" + header[cellIndex] + " field", ecode, username.ToString(), bno, template);
                                    //    else
                                    //        errorMessage += "Employee Code is missing <br>";
                                    //    //ServiceClient.SaveConsultantUploaderHistory("Employee Code is missing", ecode, username.ToString(), bno, template);
                                    //    validation = false;
                                    //}
                                    cellIndex++;
                                }
                                //END Column loop
                                //Check validation of row
                                //if (validation)
                                //{
                                //    ServiceClient.SaveConsultantUploaderHistory("Successfully read", ecode, username.ToString(), bno, template);
                                //}
                                //else
                                //{
                                //    ServiceClient.SaveConsultantUploaderHistory(errorMessage, ecode, username.ToString(), bno, template);
                                //}
                            }

                        }
                        //}
                        //dt.TableName = template == "Cost Center" ? "CostCenterUploader" : (template == "Consultant" ? "ConsultantUploader" : "Map4Uploader");
                        dt.TableName = "LensUploader";
                        MemoryStream str = new MemoryStream();
                        dt.WriteXml(str, true);
                        str.Seek(0, SeekOrigin.Begin);
                        StreamReader sr = new StreamReader(str);
                        string xmlstr;
                        xmlstr = sr.ReadToEnd();
                        if (dt.Rows.Count > 0)
                        {
                            //if (validation)
                            //{
                            //string UserName = string.Empty;
                            //var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                            //if (UserInstance.IsAuthenticated)
                            //{
                            //    UserName = UserInstance.FindFirst("preferred_username").Value;
                            //}
                            //var res = ServiceClient.GetUserData(UserName, null);

                            Res = ServiceClient.UploadLensUploaderData(xmlstr, username);
                            //}
                            //else
                            //{
                            //    Res = "Validation error occured. File not uploaded.";
                            //}
                        }
                        else
                            Res = "The uploaded file is empty";
                    }


                }

            }
            return Res;
        }
        [CustAuthFilter]
        public ActionResult ChangeStatusLens(LensUploaderData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.ModifiedBy = userData.UserId.ToString();
            model.ModifiedDate = DateTime.Now;
            var result = ServiceClient.SaveLensData(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}