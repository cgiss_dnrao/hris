﻿using ClosedXML.Excel;
using HRIS.Aspects.Factory;
using HRIS.Data;
using HRIS.Data.Request;
using HRIS.Data.UserData;
using HRIS.MvcApp.Core;
using HRIS.MvcApp.Filters;
using HRIS.MvcApp.HRISService;
using HRIS.MvcApp.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.DirectoryServices;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
//using System.Web.Script.Serialization;
using HRIS.Data.MasterData;
using System.Reflection;

namespace HRIS.MvcApp.Controllers
{
    public class EmployeeController : BaseController
    {
        private IHRISService ServiceClient = ServiceHelper.GetHRISService();

        [CustAuthFilter]
        public ActionResult Index()
        {
            return View();
        }


        [CustAuthFilter]
        public ActionResult GetEmployeeDataListPageWise(EmployeeRequest employeeRequest)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var employeeData = ServiceClient.GetHRISMasterDataListWisePage(employeeRequest);
           //var employeeList = ServiceClient.GetHRISMasterDataList(false);
           //employeeList = employeeList.Take(10).ToArray();
            var jsonResult = Json(employeeData, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetEmployeeDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var employeeList = ServiceClient.GetHRISMasterDataList(false);
            //employeeList = employeeList.Take(10).ToArray();
            var jsonResult = Json(employeeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }


        [CustAuthFilter]
        public ActionResult GetEXECOMUserDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var employeeList = ServiceClient.GetHRISMasterDataList(true);
            var jsonResult = Json(employeeList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetCostCenterMasterDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var costCenterList = ServiceClient.GetCostCenterMasterDataList(null);
            var jsonResult = Json(costCenterList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetProfitCenterMasterDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var costCenterList = ServiceClient.GetProfitCenterMasterDataList(null);
            var jsonResult = Json(costCenterList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetLeaderShipRoleMasterDataList()
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            var leaderShipRoleList = ServiceClient.GetLeaderShipRoleMasterDataList(null);
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult GetTransferLeaderShipRoleDataList()
        {
            var leaderShipRoleList = ServiceClient.GetTransferLeaderShipDataList();
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }

        [CustAuthFilter]
        public ActionResult SaveEmployeeDataList(HRISMasterData[] model)
        {
            var result = ServiceClient.SaveEmployeeDataList(model,false);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult SaveEmployeeData(HRISMasterData model,bool changeStatus=false)
        {
            List<HRISMasterData> hrisList = new List<HRISMasterData>();
            hrisList.Add(model);
            var result = ServiceClient.SaveEmployeeDataList(hrisList.ToArray(), changeStatus);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ExportDKData()
        {
            var employeeList = ServiceClient.GetHRISMasterDataList(false);
           // employeeList = employeeList.Take(10).ToArray();
            FileContentResult rObj;
            using (XLWorkbook wb = new XLWorkbook())
            {
                IXLWorksheet workSheet = wb.AddWorksheet("Employee Data");
                workSheet.ShowGridLines = false;
                int rowCount = 1;

                DataTable dataTableMenuRequisitionDetails = ToDataTable<HRISMasterData>(employeeList.ToList());
                wb.Worksheet(1).Cell(rowCount, 1).InsertTable(dataTableMenuRequisitionDetails);
                int tableRowCount = dataTableMenuRequisitionDetails.Rows.Count;
                rowCount = rowCount + 2;

                workSheet.Rows().AdjustToContents();
                workSheet.Columns().AdjustToContents();
                workSheet.Columns().Style.Alignment.Vertical = XLAlignmentVerticalValues.Justify;
                foreach (var item in workSheet.Tables)
                {
                    item.ShowAutoFilter = false;
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "EmployeeData.xlsx");
                    rObj = bytesdata;
                }
            }
            var jsonResult = Json(rObj, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
            // return Json(rObj, JsonRequestBehavior.AllowGet);
        }

        [CustAuthFilter]
        public ActionResult ExportITILiteData()
        {
            var employeeList = ServiceClient.GetHRISMasterDataList(false);
            // employeeList = employeeList.Take(10).ToArray();
            FileContentResult rObj;
            using (XLWorkbook wb = new XLWorkbook())
            {
                IXLWorksheet workSheet = wb.AddWorksheet("ITILite Data");
                workSheet.ShowGridLines = false;
                int rowCount = 1;

                DataTable dataTableMenuRequisitionDetails = GetITILiteDetailsTable(employeeList.ToList());
                
                wb.Worksheet(1).Cell(rowCount, 1).InsertTable(dataTableMenuRequisitionDetails);
                int tableRowCount = dataTableMenuRequisitionDetails.Rows.Count;
                rowCount = rowCount + 2;

                workSheet.Rows().AdjustToContents();
                workSheet.Columns().AdjustToContents();
                workSheet.Columns().Style.Alignment.Vertical = XLAlignmentVerticalValues.Justify;
                foreach (var item in workSheet.Tables)
                {
                    item.ShowAutoFilter = false;
                }

                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    var bytesdata = File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "EmployeeData.xlsx");
                    rObj = bytesdata;
                }
            }
            var jsonResult = Json(rObj, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
            // return Json(rObj, JsonRequestBehavior.AllowGet);
        }


        [CustAuthFilter]
        public DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }


        [CustAuthFilter]
        private DataTable GetITILiteDetailsTable(IList<HRISMasterData> employeeList)
        {
            DataTable itiLiteDataHeaderTable = new DataTable("ITILiteDetails");

            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "First Name", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Last Name", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Email ID", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Contact Number", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Employee Level", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Date of Birth", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Gender", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Entity", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Sector / Business Unit", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Department", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Domestic Trip Approver 1", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Domestic Trip Approver 2", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Domestic Trip Approver 3", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Domestic Trip Approver 4", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Last Minute (7 days) Domestic Trip Approver Email 1", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Last Minute (7 days) Domestic Trip Approver Email 2", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Last Minute (7 days) Domestic Trip Approver Email 3", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Last Minute (7 days) Domestic Trip Approver Email 4", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "International Trip Approver 1", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "International Trip Approver 2", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "International Trip Approver 3", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "International Trip Approver 4", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Domestic High Cost 1", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Domestic High Cost 2", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "FYI", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Approver 1", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Approver 2", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Approver 3", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Approver 4", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Approver 5", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Approver 6", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Default Currency", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Employee ID", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Designation", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Location", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Nationality", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Passport Number", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Company Code", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Cost Center", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Profit Center", DataType = typeof(string) }); 
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Personnel No", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Base Location", DataType = typeof(string) });
            itiLiteDataHeaderTable.Columns.Add(new DataColumn { ColumnName = "Action", DataType = typeof(string) });


            foreach (HRISMasterData employee in employeeList)
            {
                DataRow drEmployee = itiLiteDataHeaderTable.NewRow();
                drEmployee["First Name"] = employee?.FirstName;
                drEmployee["Last Name"] = employee?.LastName;
                drEmployee["Email ID"] = employee?.OfficeEmailAddress;
                drEmployee["Contact Number"] = employee?.MobileNumber;
                drEmployee["Employee Level"] = employee?.EmploymentType;
                drEmployee["Date of Birth"] = employee.DateOfBirthAsString;
                drEmployee["Gender"] = employee.Gender;
                drEmployee["Entity"] = employee?.Entity;
                drEmployee["Sector / Business Unit"] = employee?.Derived;
                drEmployee["Department"] = employee?.Department;
                drEmployee["Domestic Trip Approver 1"] = employee?.DomesticTripApprover1;
                drEmployee["Domestic Trip Approver 2"] = employee?.DomesticTripApprover2;
                drEmployee["Domestic Trip Approver 3"] = employee?.DomesticTripApprover3;
                drEmployee["Domestic Trip Approver 4"] = employee?.DomesticTripApprover4;
                drEmployee["Last Minute (7 days) Domestic Trip Approver Email 1"] = employee?.DomesticTripApproverEmail1;
                drEmployee["Last Minute (7 days) Domestic Trip Approver Email 2"] = employee?.DomesticTripApproverEmail2;
                drEmployee["Last Minute (7 days) Domestic Trip Approver Email 3"] = employee?.DomesticTripApproverEmail3;
                drEmployee["Last Minute (7 days) Domestic Trip Approver Email 4"] = employee?.DomesticTripApproverEmail4;
                drEmployee["International Trip Approver 1"] = employee?.InternationalTripApprover1;
                drEmployee["International Trip Approver 2"] = employee?.InternationalTripApprover2;
                drEmployee["International Trip Approver 3"] = employee?.InternationalTripApprover3;
                drEmployee["International Trip Approver 4"] = employee?.InternationalTripApprover4;
                drEmployee["Domestic High Cost 1"] = employee?.DomesticHighCost1;
                drEmployee["Domestic High Cost 2"] = employee?.DomesticHighCost2;
                drEmployee["FYI"] = employee?.FYI;
                drEmployee["Approver 1"] = employee?.Approver1;
                drEmployee["Approver 2"] = employee?.Approver2;
                drEmployee["Approver 3"] = employee?.Approver3;
                drEmployee["Approver 4"] = employee?.Approver4;
                drEmployee["Approver 5"] = employee?.Approver5;
                drEmployee["Approver 6"] = employee?.Approver6;
                drEmployee["Default Currency"] = employee.DefaultCurrency;
                drEmployee["Employee ID"] = employee.EmployeeCode;
                drEmployee["Designation"] = employee?.Designation;
                drEmployee["Location"] = employee?.Location;
                drEmployee["Nationality"] = employee.Nationality;
                drEmployee["Passport Number"] ="";
                drEmployee["Company Code"] =employee?.CompanyCode;
                drEmployee["Cost Center"] =employee?.CostCenter;
                drEmployee["Profit Center"] =employee?.ProfitCenter;
                drEmployee["Personnel No"] = "";
                drEmployee["Base Location"] = "";

                drEmployee["Action"] = employee.Action;

                itiLiteDataHeaderTable.Rows.Add(drEmployee);
            }
            return itiLiteDataHeaderTable;
        }

        [CustAuthFilter]
        public ActionResult SectorDepartmentLeadershipMapping()
        {
            return View();
        }
        [CustAuthFilter]
        public ActionResult GetSectorDepartmentLeadershipMapping()
        {
            var leaderShipRoleList = ServiceClient.GetSectorDepartmentLeadershipMapping();
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult SaveSectorDepartmentLeadershipMapping(SectorDepartmentLeadershipMappingData model)
        {
            UserMasterResponseData userData = (UserMasterResponseData)Session["UserLoginDetails"];
            model.CreatedBy = userData.UserId;
            var result = ServiceClient.SaveSectorDepartmentLeadershipMapping(model);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        [CustAuthFilter]
        public ActionResult GetSectorMasterDataList()
        {
           
            var leaderShipRoleList = ServiceClient.GetSectorMasterList();
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
        [CustAuthFilter]
        public ActionResult GetDepartmentMasterDataList()
        {

            var leaderShipRoleList = ServiceClient.GetDepartmentMasterList();
            var jsonResult = Json(leaderShipRoleList, JsonRequestBehavior.AllowGet);
            jsonResult.MaxJsonLength = Int32.MaxValue;
            return jsonResult;
        }
    }


}