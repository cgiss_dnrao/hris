﻿using HRIS.Data.UserData;
using HRIS.MvcApp.Core;
using HRIS.MvcApp.HRISService;
using System.Configuration;
using System.Web.Mvc;
using HRIS.MvcApp.Filters;
using System.Web;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using Microsoft.Owin.Security.Cookies;
using System.Web.Security;

namespace HRIS.MvcApp.Controllers
{
    public class HomeController : Controller
    {
        IHRISService ServiceClient = ServiceHelper.GetHRISService();
        
        [CustAuthFilter]
        public ActionResult Index()
        {
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                string UserName = UserInstance.FindFirst("preferred_username").Value;

                if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                {
                    UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                }
                var res = ServiceClient.GetUserData(UserName);
                HttpContext.Session["UserLoginDetails"] = res;
            }
            return View();
        }

        public void SignIn()
        {
            if (!Request.IsAuthenticated)
            {
                HttpContext.GetOwinContext().Authentication.Challenge(
                    new AuthenticationProperties { RedirectUri = "/" },
                    OpenIdConnectAuthenticationDefaults.AuthenticationType);
            }
        }

        /// <summary>
        /// Send an OpenID Connect sign-out request.
        /// </summary>
        //public void SignOut()
        //{
        //    HttpContext.GetOwinContext().Authentication.SignOut(
        //            OpenIdConnectAuthenticationDefaults.AuthenticationType,
        //            CookieAuthenticationDefaults.AuthenticationType);
        //}


        [CustAuthFilter]
        public ActionResult MyPage()
        {
            var UserInstance = HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
            if (UserInstance.IsAuthenticated)
            {
                string UserName = UserInstance.FindFirst("preferred_username").Value;

                if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                {
                    UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                }
                var res = ServiceClient.GetUserData(UserName);
                if (res == null)
                {
                    return View("NotAuthenticated");
                }
                HttpContext.Session["UserLoginDetails"] = res;
               
            }
            else
            {
                return View("NotAuthenticated");
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult NotAuthenticated()
        {
            return View();
        }

        public ActionResult SortingToolError()
        {
            return View();
        }

        public ActionResult CheckSessionTimeout() => View();

        [CustAuthFilter]
        public ActionResult ForceSessionRefresh()
        {
            HttpContext.GetOwinContext()
                   .Authentication.Challenge(new AuthenticationProperties { RedirectUri = "Home/CheckSessiontimeout" },
                       OpenIdConnectAuthenticationDefaults.AuthenticationType);

            return null;
        }

        public ActionResult SignOut()
        {
            HttpContext.GetOwinContext().Authentication.SignOut(
                    OpenIdConnectAuthenticationDefaults.AuthenticationType,
                    CookieAuthenticationDefaults.AuthenticationType);
            FormsAuthentication.SignOut();

            // Optionally clear the session data
            Session.Clear();
            Session.Abandon();

            // Redirect to the home page or login page
            return RedirectToAction("Index", "Home");
        }
    }
}