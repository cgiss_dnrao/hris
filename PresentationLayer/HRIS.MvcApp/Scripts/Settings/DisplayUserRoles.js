﻿var selItems = [];
var selRegions = [];
var selStates = [];
var selecSites = [];
var rejStates = [];
var rejSites = [];
var currentUser = "";
var treeDataSource = [];

var dsNorth = [];
var dsSouth = [];
var dsEast = [];
var dsWest = [];

var northId = "";
var southId = "";
var eastId = "";
var westId = "";

function error_handler(e) {
    if (e.errors) {
        var message = "Errors:\n";
        $.each(e.errors, function (key, value) {
            if ('errors' in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        alert(message);
    }
}

$(document).ready(function () {
    HttpClient.MakeRequest(loginDetailsUrl, function (result) {
        
        $("#city").text(result.UserName);
        
    }, {}, false);
    $(".k-grid-toolbar").hide();
    setRowButtonTooltip(".k-grid-edit", "Edit");
    setRowButtonTooltip(".k-grid-delete", "Delete");
    //setRowButtonTooltip(".k-grid-update", "Update");
    //setRowButtonTooltip(".k-grid-cancel", "Cancel");
    dsSites.splice(0, 0, { Name: "All Sites", Value: "0" });
    $("#SiteList").kendoMultiSelect({
        dataTextField: "Name",
        dataValueField: "Value",
        dataSource: dsSites,
        filter: "contains",
        autoClose: false,
        select: function (e) {
            var dataItem = this.dataSource.view()[e.item.index()];
            var values = this.value();

            if (dataItem.Value == "0") {
                this.value("");
            }
            else if (values.indexOf("0") != -1) {
                values = $.grep(values, function (value) {
                    return value != "0";
                });

                this.value(values);
            }
        }
    });

    $('#btnAddUser').on('click', function () {
        $('.k-grid-add').trigger('click');
    });

    $("#btnSearchUser").on("click", function () {
        $('#UsersGrid').data("kendoGrid").dataSource.read();
    });

    $("#btnClearUser").on("click", function () {
        $("#userName").val("");
        $("#SiteList").data("kendoMultiSelect").value("");
        $('#UsersGrid').data("kendoGrid").dataSource.read();
    });
    //for (var i = 0; i < sitesData.length; i++) {
    //    var item = sitesData[i];
    //    treeDataSource.push({ Id: item.Id, DisplayName: item.DisplayName, SiteCode: item.SiteCode, DisplayCheck: item.DisplayCheck, IsRegion: item.IsRegion, IsState: item.IsState, IsSite: item.IsSite, parentId: item.parentId, ParentId: item.ParentId, IsSelected: item.IsSelected });
    //}
    for (var i = 0; i < northData.length; i++) {
        var item = northData[i];

        if (item.IsRegion) {
            northId = item.Id;
        }
        else {
            dsNorth.push({ Id: item.Id, DisplayName: item.DisplayName, SiteCode: item.SiteCode, DisplayCheck: item.DisplayCheck, IsRegion: item.IsRegion, IsState: item.IsState, IsSite: item.IsSite, parentId: item.parentId, ParentId: item.ParentId, IsSelected: item.IsSelected });
        }
    }
    for (var i = 0; i < southData.length; i++) {
        var item = southData[i];
        if (item.IsRegion) {
            southId = item.Id;
        }
        else {
            dsSouth.push({ Id: item.Id, DisplayName: item.DisplayName, SiteCode: item.SiteCode, DisplayCheck: item.DisplayCheck, IsRegion: item.IsRegion, IsState: item.IsState, IsSite: item.IsSite, parentId: item.parentId, ParentId: item.ParentId, IsSelected: item.IsSelected });
        }
    }
    for (var i = 0; i < eastData.length; i++) {
        var item = eastData[i];
        if (item.IsRegion) {
            eastId = item.Id;
        }
        else {
            dsEast.push({ Id: item.Id, DisplayName: item.DisplayName, SiteCode: item.SiteCode, DisplayCheck: item.DisplayCheck, IsRegion: item.IsRegion, IsState: item.IsState, IsSite: item.IsSite, parentId: item.parentId, ParentId: item.ParentId, IsSelected: item.IsSelected });
        }
    }
    for (var i = 0; i < westData.length; i++) {
        var item = westData[i];
        if (item.IsRegion) {
            westId = item.Id;
        }
        else {
            dsWest.push({ Id: item.Id, DisplayName: item.DisplayName, SiteCode: item.SiteCode, DisplayCheck: item.DisplayCheck, IsRegion: item.IsRegion, IsState: item.IsState, IsSite: item.IsSite, parentId: item.parentId, ParentId: item.ParentId, IsSelected: item.IsSelected });
        }
    }

});

function onEdit(e) {
    var dropdownlist = $("#Site").data("kendoDropDownList");
    if ($("#IsSiteUser").is(":checked")) {
        dropdownlist.enable(true);
    }
    else {
        dropdownlist.enable(false);

    }
    if (!e.model.isNew()) {
        $("#UserName").attr("disabled", "disabled");

    }
    $("#IsSiteUser").bind("change", function (e) {
        if ($("#IsSiteUser").is(":checked")) {
            dropdownlist.enable(true);
        }
        else {
            dropdownlist.enable(false);
        }
    });
}

function onSave(e) {
    if ($("#IsSiteUser").is(":checked") && ($("#Site").val() == null || $("#Site").val() == "")) {
        e.preventDefault();
        Utility.Page_Alert("Site is required", "Alert", "OK", function () {
            $("#Site").data("kendoDropDownList").focus();
        });

    }
    if ($("#UserName").val() == "" || $("#UserName").val() == null) {
        e.preventDefault();
        Utility.Page_Alert("UserName is required", "Alert", "OK", function () {
            $("#UserName").focus();
        });

    }
}

function onRequestEnd(e) {
    if (e.type == "create") {
        Utility.Page_Alert("User created successfully", "Alert", "OK", function () {
            $("#UsersGrid").data("kendoGrid").dataSource.read();
        });
    }
    if (e.type == "update") {
        Utility.Page_Alert("User details updated successfully", "Alert", "OK", function () {
            $("#UsersGrid").data("kendoGrid").dataSource.read();
        });
    }
}

function onDataBound(e) {
    //var grid = $("#UsersGrid").data("kendoGrid");
    //var gridRows = this.tbody.find("tr");
    //gridRows.each(function (e) {
    //    var rowItem = grid.dataItem($(this));
    //    if (rowItem.IsSiteUser) {
    //    }
    //});
    //$("#UsersGrid").kendoTooltip({
    //    filter: ".k-grid-toolbar a.k-grid-add",
    //    content: "Add new record"
    //});

}

function setRowButtonTooltip(btn_cl, btn_tooltip) {
    $("#UsersGrid").kendoTooltip({
        filter: btn_cl,
        content: btn_tooltip,
        position: "top"
    });
}

function manageClick(e) {
    e.preventDefault();
    selRegions = [];
    selStates = [];
    selecSites = [];
    rejStates = [];
    rejSites = [];
    selItems = [];
    //var row = e.target.attributes["id"].nodeValue;
    if ($('#UsersGrid').find('.k-grid-edit-row').length > 0) {
        Utility.Page_Alert("Please update the user details first", "Alert", "OK", function () {
            return false;
        });
    } else {
        Utility.Loading();
        var row = e.target.closest("tr");
        var grid = $("#UsersGrid").data("kendoGrid");
        var item = grid.dataItem(row);
        selItems = [];

        //var ajax = $.ajax({
        //    url: manageRolesURL,
        //    //data: { userData: uData },
        //    dataType: "html"
        //});
        $("#roleContent").load(manageRolesURL, function (data) {
            currentUser = item.UserName;
            $("#siteSearch").kendoMultiSelect({
                dataTextField: "Name",
                dataValueField: "Value",
                dataSource: dsSites,
                filter: "contains",
                autoClose: false,
                select: function (e) {
                    var dataItem = this.dataSource.view()[e.item.index()];
                    var values = this.value();

                    if (dataItem.Value == "0") {
                        this.value("");
                    }
                    else if (values.indexOf("0") != -1) {
                        values = $.grep(values, function (value) {
                            return value != "0";
                        });

                        this.value(values);
                    }
                }
            });

            for (var i = 0; i < item.DistinctCodes.length; i++) {
                selItems.push(item.DistinctCodes[i].toString());
            }

            var dataSourceNorth = new kendo.data.TreeListDataSource({
                data: dsNorth,
                schema: {
                    model: {
                        id: "Id",
                        parentId: "parentId",
                        fields: {
                            DisplayName: { field: "DisplayName", type: "string" }
                        },
                        expanded: false
                    }
                }
            });
            var dataSourceSouth = new kendo.data.TreeListDataSource({
                data: dsSouth,
                schema: {
                    model: {
                        id: "Id",
                        parentId: "parentId",
                        fields: {
                            DisplayName: { field: "DisplayName", type: "string" }
                        },
                        expanded: false
                    }
                }
            });
            var dataSourceEast = new kendo.data.TreeListDataSource({
                data: dsEast,
                schema: {
                    model: {
                        id: "Id",
                        parentId: "parentId",
                        fields: {
                            DisplayName: { field: "DisplayName", type: "string" }
                        },
                        expanded: false
                    }
                }
            });
            var dataSourceWest = new kendo.data.TreeListDataSource({
                data: dsWest,
                schema: {
                    model: {
                        id: "Id",
                        parentId: "parentId",
                        fields: {
                            DisplayName: { field: "DisplayName", type: "string" }
                        },
                        expanded: false
                    }
                }
            });

            $("#UserRolesList").data("kendoTreeList").setDataSource(dataSourceNorth);
            $("#UserRolesList1").data("kendoTreeList").setDataSource(dataSourceSouth);
            $("#UserRolesList2").data("kendoTreeList").setDataSource(dataSourceEast);
            $("#UserRolesList3").data("kendoTreeList").setDataSource(dataSourceWest);

            $("#nId").attr("id", northId);
            $("#sId").attr("id", southId);
            $("#eId").attr("id", eastId);
            $("#wId").attr("id", westId);

            $.each(selItems, function (i, item) {
                if ($("#" + item).length) {
                    $("#" + item).prop('checked', true);
                    var type = $("#" + item).attr('value');
                    //
                    if (type == "si") {
                        pushSite(item);
                    }
                    if (type == "st") {
                        pushState(item);
                    }
                    if (type == "r") {
                        pushRegion(item);
                    }
                    ExpandParents(item);
                }
            });

            $("#ManageRoles").data("kendoWindow").center().open();
            Utility.UnLoading();
        });
        //ajax.success(function (data) {
        //    var div = $('#roleContent');
        //    currentUser = item.UserName;
        //    div.html(data);

        //    $("#siteSearch").kendoMultiSelect({
        //        dataTextField: "Name",
        //        dataValueField: "Value",
        //        dataSource: dsSites,
        //        filter: "contains",
        //        autoClose: false,
        //        select: function (e) {
        //            var dataItem = this.dataSource.view()[e.item.index()];
        //            var values = this.value();

        //            if (dataItem.Value == "0") {
        //                this.value("");
        //            }
        //            else if (values.indexOf("0") != -1) {
        //                values = $.grep(values, function (value) {
        //                    return value != "0";
        //                });

        //                this.value(values);
        //            }
        //        }
        //    });
        //    for (var i = 0; i < item.DistinctCodes.length; i++) {
        //        selItems.push(item.DistinctCodes[i].toString());
        //    }
        //    var dataSource = new kendo.data.TreeListDataSource({
        //        data: treeDataSource,
        //        schema: {
        //            model: {
        //                id: "Id",
        //                parentId: "parentId",
        //                fields: {
        //                    DisplayName: { field: "DisplayName", type: "string" }
        //                },
        //                expanded: false
        //            }
        //        }
        //    });
        //    $("#UserRolesList").data("kendoTreeList").setDataSource(dataSource);
        //    $("#ManageRoles").data("kendoWindow").center().open();
        //});
    }
}

function getUserName() {
    return {
        userName: $("#userName").val(),
        sites: $("#SiteList").data("kendoMultiSelect").value().toString()
    };
}

function onWinClose() {
    selItems = [];
    currentUser = "";
}

function TreeChck(me) {
    var id = me.id;
    var name = me.name;
    var type = me.value;
    var clss = $("#" + me.id).attr('class');
    if (type == "si") {
        if (me.checked) {
            pushSite(me.id);
            //SelectChilds(id);
        }
        else {
            removeSite(me.id.toString());
            //ClearChilds(id);
            var data = $("#siteSearch").data("kendoMultiSelect").value().toString();
            if (data != "" && data != null) {
                removeState(me.name.toString());
                if (clss == "treeChkboxN") {
                    removeRegion(northId.toString());
                }
                else if (clss == "treeChkboxS") {
                    removeRegion(southId.toString());
                }
                else if (clss == "treeChkboxE") {
                    removeRegion(eastId.toString());
                }
                else if (clss == "treeChkboxW") {
                    removeRegion(westId.toString());
                }
            }
            else {
                ClearParents(id);
            }
        }
    }
    if (type == "st") {
        if (me.checked) {
            pushState(me.id);
            SelectChilds(id);
        }
        else {
            removeState(me.id.toString());
            ClearChilds(id);
            ClearParents(id);
        }
    }
    //if (!me.checked) {
    //    var index = selItems.indexOf(me.id);
    //    if (index > -1) {
    //        selItems.splice(index, 1);
    //    }
    //    ClearChilds(id);
    //    ClearParents(id);
    //}
    //else {
    //    selItems.push(id);
    //    SelectChilds(id);
    //}
}

function ClearParents(pId) {
    //
    if (pId != null && pId != "null" && pId != "" && pId != "undefined") {
        var name = $("#" + pId).attr('name');
        var type = $("#" + pId).attr('value');
        var clss = $("#" + pId).attr('class');

        $("#" + pId).prop('checked', false);

        //var index = selItems.indexOf(pId.toString());
        //if (index > -1) {
        //    selItems.splice(index, 1);
        //}

        if (type == "si") {
            removeSite(pId.toString());
        }
        if (type == "st") {
            removeState(pId.toString());
        }

        if (name == null || name == "null" || name == "" || name == "undefined") {
            if (clss == "treeChkboxN") {
                $('#' + northId).prop('checked', false);
                //var index = selItems.indexOf(northId.toString());
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                removeRegion(northId.toString());
            }
            else if (clss == "treeChkboxS") {
                $('#' + southId).prop('checked', false);
                //var index = selItems.indexOf(southId.toString());
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                removeRegion(southId.toString());
            }
            else if (clss == "treeChkboxE") {
                $('#' + eastId).prop('checked', false);
                //var index = selItems.indexOf(eastId.toString());
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                removeRegion(eastId.toString());
            }
            else if (clss == "treeChkboxW") {
                $('#' + westId).prop('checked', false);
                //var index = selItems.indexOf(westId.toString());
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                removeRegion(westId.toString());
            }
        }
        ClearParents(name);
    }
}

function ClearChilds(pId) {
    $("[name=" + pId + "]").each(function (index, item) {
        item.checked = false;
        //var index = selItems.indexOf(item.id.toString());
        //if (index > -1) {
        //    selItems.splice(index, 1);
        //}
        removeSite(item.id.toString());
        //ClearChilds(item.id);
    });
}

function SelectChilds(pId) {
    $("[name=" + pId + "]").each(function (index, item) {
        item.checked = true;
        //selItems.push(item.id.toString());
        //SelectChilds(item.id);
        pushSite(item.id);
    });
}

function ExpandParents(pId) {
    if (pId != null && pId != "null" && pId != "") {
        var name = $("#" + pId).attr('name');
        if (name != null && name != "null" && name != "" && $("#" + name).length) {
            var i = 0;
            var suf = "";
            var treeList = null;
            var row = null;
            while (row == null) {
                if (i > 0) {
                    suf = i.toString();
                }
                treeList = $("#UserRolesList" + suf).data("kendoTreeList");
                row = treeList.dataSource.get(name);
                i++;
            }
            treeList.expand(row);
            ExpandParents(name);
        }
        //else if ($("#" + pId).is(":checked")) {
        //
        //    var i = 1;
        //    var suf = "";
        //    var treeList = null;
        //    var row = null;
        //    while (row == null) {
        //        if (i > 0) {
        //            suf = i.toString();
        //        }
        //        treeList = $("#UserRolesList" + suf).data("kendoTreeList");
        //        row = treeList.dataSource.get(pId);
        //        i++;
        //    }
        //    treeList.collapse(row);
        //}
    }
}

function GetFilteredData(data) {
    var dsUniqueNorth = [];
    var dsUniqueSouth = [];
    var dsUniqueEast = [];
    var dsUniqueWest = [];

    if (data != "" && data != null) {
        var s = data.split(',');
        var ds = [];
        var stt = null;
        for (var i = 0; i < dsNorth.length; i++) {
            var item = dsNorth[i];
            if (item.IsSite) {
                var index = s.indexOf(item.SiteCode);
                if (index > -1) {
                    ds.push(item);
                    stt = $.grep(dsNorth, function (loc) {
                        return loc.Id === item.ParentId;
                    })[0];
                    if (stt != null) {
                        stt.DisplayCheck = false;
                        ds.push(stt);
                        //var reg = $.grep(dsNorth, function (loc) {
                        //    return loc.Id === stt.ParentId;
                        //})[0];
                        //reg.DisplayCheck = false;
                        //ds.push(reg);
                    }
                }
            }
        }

        $.each(ds, function (i, el) {
            if ($.inArray(el, dsUniqueNorth) === -1) dsUniqueNorth.push(el);
        })
        ds = [];

        var dataSourceNorth = new kendo.data.TreeListDataSource({
            data: dsUniqueNorth,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        $("#UserRolesList").data("kendoTreeList").setDataSource(dataSourceNorth);
        if (stt != null)
            stt.DisplayCheck = true;

        for (var i = 0; i < dsSouth.length; i++) {
            var item = dsSouth[i];
            if (item.IsSite) {
                var index = s.indexOf(item.SiteCode);
                if (index > -1) {
                    ds.push(item);
                    stt = $.grep(dsSouth, function (loc) {
                        return loc.Id === item.ParentId;
                    })[0];
                    if (stt != null) {
                        stt.DisplayCheck = false;
                        ds.push(stt);
                        //var reg = $.grep(dsSouth, function (loc) {
                        //    return loc.Id === stt.ParentId;
                        //})[0];
                        //reg.DisplayCheck = false;
                        //ds.push(reg);
                    }
                }
            }
        }

        $.each(ds, function (i, el) {
            if ($.inArray(el, dsUniqueSouth) === -1) dsUniqueSouth.push(el);
        })
        ds = [];

        var dataSourceSouth = new kendo.data.TreeListDataSource({
            data: dsUniqueSouth,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        $("#UserRolesList1").data("kendoTreeList").setDataSource(dataSourceSouth);
        if (stt != null)
            stt.DisplayCheck = true;

        for (var i = 0; i < dsEast.length; i++) {
            var item = dsEast[i];
            if (item.IsSite) {
                var index = s.indexOf(item.SiteCode);
                if (index > -1) {
                    ds.push(item);
                    stt = $.grep(dsEast, function (loc) {
                        return loc.Id === item.ParentId;
                    })[0];
                    if (stt != null) {
                        stt.DisplayCheck = false;
                        ds.push(stt);
                        //var reg = $.grep(dsEast, function (loc) {
                        //    return loc.Id === stt.ParentId;
                        //})[0];
                        //reg.DisplayCheck = false;
                        //ds.push(reg);
                    }
                }
            }
        }

        $.each(ds, function (i, el) {
            if ($.inArray(el, dsUniqueEast) === -1) dsUniqueEast.push(el);
        })
        ds = [];

        var dataSourceEast = new kendo.data.TreeListDataSource({
            data: dsUniqueEast,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        $("#UserRolesList2").data("kendoTreeList").setDataSource(dataSourceEast);
        if (stt != null)
            stt.DisplayCheck = true;

        for (var i = 0; i < dsWest.length; i++) {
            var item = dsWest[i];
            if (item.IsSite) {
                var index = s.indexOf(item.SiteCode);
                if (index > -1) {
                    ds.push(item);
                    stt = $.grep(dsWest, function (loc) {
                        return loc.Id === item.ParentId;
                    })[0];
                    if (stt != null) {
                        stt.DisplayCheck = false;
                        ds.push(stt);
                        //var reg = $.grep(dsWest, function (loc) {
                        //    return loc.Id === stt.ParentId;
                        //})[0];
                        //reg.DisplayCheck = false;
                        //ds.push(reg);
                    }
                }
            }
        }

        $.each(ds, function (i, el) {
            if ($.inArray(el, dsUniqueWest) === -1) dsUniqueWest.push(el);
        })
        ds = [];

        var dataSourceWest = new kendo.data.TreeListDataSource({
            data: dsUniqueWest,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        $("#UserRolesList3").data("kendoTreeList").setDataSource(dataSourceWest);
        if (stt != null)
            stt.DisplayCheck = true;

        $(".htemp").hide();
    }
    else {
        var dataSourceNorth = new kendo.data.TreeListDataSource({
            data: dsNorth,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        var dataSourceSouth = new kendo.data.TreeListDataSource({
            data: dsSouth,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        var dataSourceEast = new kendo.data.TreeListDataSource({
            data: dsEast,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });
        var dataSourceWest = new kendo.data.TreeListDataSource({
            data: dsWest,
            schema: {
                model: {
                    id: "Id",
                    parentId: "parentId",
                    fields: {
                        DisplayName: { field: "DisplayName", type: "string" }
                    },
                    expanded: false
                }
            }
        });

        $("#UserRolesList").data("kendoTreeList").setDataSource(dataSourceNorth);
        $("#UserRolesList1").data("kendoTreeList").setDataSource(dataSourceSouth);
        $("#UserRolesList2").data("kendoTreeList").setDataSource(dataSourceEast);
        $("#UserRolesList3").data("kendoTreeList").setDataSource(dataSourceWest);

        $(".htemp").prop('checked', false);
        $(".htemp").show();
    }
    //$.each(selItems, function (i, item) {
    //    if ($("#" + item).length) {
    //        $("#" + item).prop('checked', true);
    //        ExpandParents(item);
    //    }
    //});
    $.each(selecSites, function (i, item) {
        if ($("#" + item).length) {
            $("#" + item).prop('checked', true);
            ExpandParents(item);
        }
    });
    $.each(selStates, function (i, item) {
        if ($("#" + item).length) {
            $("#" + item).prop('checked', true);
            //ExpandParents(item);
        }
    });
    $.each(selRegions, function (i, item) {
        if ($("#" + item).length) {
            $("#" + item).prop('checked', true);
            //ExpandParents(item);
        }
    });
}

function checkAllStates(me) {
    if (me.checked) {
        //selRegions.push(me.id);
        pushRegion(me.id);
    }
    else {
        //var index = selRegions.indexOf(me.id);
        //if (index > -1) {
        //    selRegions.splice(index, 1);
        //}
        removeRegion(me.id.toString());
    }
    if (me.id == northId) {
        $(".treeChkboxN").each(function (i, item) {
            item.checked = me.checked;
            var type = $("#" + item.id).attr('value');
            if (!me.checked) {
                //var index = selItems.indexOf(item.id);
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                if (type == "si") {
                    removeSite(item.id.toString());
                }
                if (type == "st") {
                    removeState(item.id.toString());
                }
            }
            else {
                //selItems.push(item.id);
                if (type == "si") {
                    pushSite(item.id);
                }
                if (type == "st") {
                    pushState(item.id);
                }
            }
        });
    }
    else if (me.id == southId) {
        $(".treeChkboxS").each(function (i, item) {
            item.checked = me.checked;
            var type = $("#" + item.id).attr('value');
            if (!me.checked) {
                //var index = selItems.indexOf(item.id);
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                if (type == "si") {
                    removeSite(item.id.toString());
                }
                if (type == "st") {
                    removeState(item.id.toString());
                }
            }
            else {
                //selItems.push(item.id);
                if (type == "si") {
                    pushSite(item.id);
                }
                if (type == "st") {
                    pushState(item.id);
                }
            }
        });
    }
    else if (me.id == eastId) {
        $(".treeChkboxE").each(function (i, item) {
            item.checked = me.checked;
            var type = $("#" + item.id).attr('value');
            if (!me.checked) {
                //var index = selItems.indexOf(item.id);
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                if (type == "si") {
                    removeSite(item.id.toString());
                }
                if (type == "st") {
                    removeState(item.id.toString());
                }
            }
            else {
                //selItems.push(item.id);
                if (type == "si") {
                    pushSite(item.id);
                }
                if (type == "st") {
                    pushState(item.id);
                }
            }
        });
    }
    else if (me.id == westId) {
        $(".treeChkboxW").each(function (i, item) {
            item.checked = me.checked;
            var type = $("#" + item.id).attr('value');
            if (!me.checked) {
                //var index = selItems.indexOf(item.id);
                //if (index > -1) {
                //    selItems.splice(index, 1);
                //}
                if (type == "si") {
                    removeSite(item.id.toString());
                }
                if (type == "st") {
                    removeState(item.id.toString());
                }
            }
            else {
                //selItems.push(item.id);
                if (type == "si") {
                    pushSite(item.id);
                }
                if (type == "st") {
                    pushState(item.id);
                }
            }
        });
    }

}

function getLocationType(id) {
    var val = $("#" + id).attr('value');
    return val;
}

function pushState(id) {
    var index = selStates.indexOf(id);
    if (index < 0) {
        selStates.push(id);
    }
}

function pushSite(id) {
    var index = selecSites.indexOf(id);
    if (index < 0) {
        selecSites.push(id);
    }
}

function removeState(id) {
    var index = selStates.indexOf(id);
    if (index > -1) {
        selStates.splice(index, 1);
    }
}

function removeSite(id) {
    var index = selecSites.indexOf(id);
    if (index > -1) {
        selecSites.splice(index, 1);
    }
}

function pushRegion(id) {
    var index = selRegions.indexOf(id);
    if (index < 0) {
        selRegions.push(id);
    }
}

function removeRegion(id) {
    var index = selRegions.indexOf(id);
    if (index > -1) {
        selRegions.splice(index, 1);
    }
}