﻿var sShowLoader = false;
var HttpClient = (function () {

    var userId = '';
    //var sShowLoader = true;
    return {
        MakeRequest: function (methodName, callbackResponse, parameterString, showLoader) {

            if (showLoader != null) {
                // sShowLoader = true;
                sShowLoader = showLoader;
            }
            if (parameterString == "") {
                parameterString = "{}";
            }

            pageUrl = window.location.pathname.substring(window.location.pathname.lastIndexOf("/") + 1);

            //if (pageUrl.toLowerCase().search("login") > -1 || pageUrl == "") {
            ExecuteRequest(methodName, callbackResponse, parameterString, showLoader)
            return;
            //}
            //else {
            //    //IsPreviousLoginHttp(methodName, callbackResponse, parameterString, showLoader)
            //}
        },

        MakeRequestToERP: function (methodName, callbackResponse, parameterString, showLoader) {
            try {
                if (showLoader != null) {
                    sShowLoader = true;//showLoader;
                }
                if (parameterString == "") {
                    parameterString = "{}";
                }
                $.support.cors = true;

                var XHR = $.ajax({
                    cache: false,
                    type: "POST",
                    contentType: "text/json; charset=utf-8",
                    url: ERPServiceURL + methodName,
                    data: JSON.stringify(parameterString),
                    dataType: "json",
                    crossDomain: true,
                    beforeSend: startingAjax,
                    complete: ajaxCompleted
                })
                    .success(function (data) {

                        return callbackResponse(data);

                    }).fail(function (jqXHR, textStatus, errorThrown) {

                        alert("Error in connecting with service due to " + errorThrown);

                    });
            } catch (e) {
                alert("Error in Service Request: " + e.message);
            }
        },
        MakeSyncRequest: function (methodName, callbackResponse, parameterString, showLoader, checkSession) {
            try {
                if (showLoader != null) {
                    // sShowLoader = true;
                    sShowLoader = showLoader;
                }
                if (parameterString == "") {
                    parameterString = "{}";
                }
                $.support.cors = true;
                pageUrl = window.location.pathname.substring(window.location.pathname.lastIndexOf("/") + 1);

                var XHR = $.ajax({
                    cache: false,
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: methodName,
                    data: JSON.stringify(parameterString),
                    dataType: "json",
                    crossDomain: true,
                    async: false,
                    //beforeSend: startingAjax,
                    beforeSend: function (xhr, settings) {
                        loading();
                    },
                    // complete: ajaxCompleted,
                    success: function (result) {
                        return callbackResponse(result);
                    },
                    onerror: function (e) {
                        alert(e);
                    },
                });


            } catch (e) {
                alert("Error in Service Request: " + e.message);
            }
        }
    };
})();



//function IsPreviousLoginHttp(methodName, callbackResponse, parameterString, showLoader) {
//    var dataObj = {};
//    dataObj.userID = dashboard.userID;
//    $.ajax({
//        url: $("#ContentArea").attr("data-ispreviouslogin"),
//        type: "POST",
//        contentType: "application/json",
//        data: JSON.stringify(dataObj),
//        success: function (result) {
//            if (result.Data == "false" || result.Data == false) {
//                HideProgressPopup();
//                Utility.Page_Alert(dashboard.msgAlreadyLogin, dashboard.warningType, dashboard.buttonOk, function () {
//                    window.location = $("#ContentArea").attr("data-loginurl");
//                });

//            }
//            else {
//                SessionCheckHttp(methodName, callbackResponse, parameterString, showLoader);
//            }
//        },
//    });
//};

function SessionCheckHttp(methodName, callbackResponse, parameterString, showLoader) {
    $.ajax({
        url: $("#ContentArea").attr("data-sessioncheck"),
        type: "GET",
        success: function (result) {
            if (result.Data == "false" || result.Data == false) {
                HideProgressPopup();
                window.location = $("#ContentArea").attr("data-logoffurl");
            }
            else {
                ExecuteRequest(methodName, callbackResponse, parameterString, showLoader);

            }
        },
    });
};

function ExecuteRequest(methodName, callbackResponse, parameterString, showLoader) {

    try {
        $.support.cors = true;
        var XHR = $.ajax({
            cache: false,
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: methodName,
            data: JSON.stringify(parameterString),
            dataType: "json",
            crossDomain: true,
            async: true,
            beforeSend: startingAjax,
            complete: ajaxCompleted
        })
            .success(function (data) {
                return callbackResponse(data);

            }).fail(function (jqXHR, textStatus, errorThrown) {
                alert("Error in connecting with service due to " + errorThrown);

            });
    } catch (e) {
        alert("Error in Service Request: " + e.message);
    }
}

function startingAjax() {
    try {
        if (sShowLoader) {
            ShowProgressPopup();
        }
    }
    catch (err) { }
}

function ajaxCompleted() {
    try {
        if (sShowLoader) {
            HideProgressPopup();
        }
    }
    catch (err) { }
}


function loading() {
    if ($('#mainloader').css('display') == 'none') {
        $('#mainloader').css('display', 'block');
    }
}
// hide loading and overlay
function unloading() {
    if ($('#mainloader').css('display') == 'block') {
        $('#mainloader').fadeOut(2000);
    }
}

function ShowProgressPopup() {
    loading();
}

function HideProgressPopup() {
    unloading();
}








