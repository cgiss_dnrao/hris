﻿
var user;
var status = "";
var varname = "";
var datamodel;

var BATCHNUMBER = "";


$(document).ready(function () {
    $("#UploadExcelFile").kendoWindow({
        modal: true,
        width: "575px",
        height: "105px",
        title: "Patient Master EXCEL",
        actions: ["Close"],
        visible: false,
        animation: false
    });
    //$("#GETALLPmViewALLHistoryBATCHWISE").kendoWindow({
    //    modal: true,
    //    width: "1005px",
    //    height: "115px",
    //    title: "Patient Master EXCEL",
    //    actions: ["Close"],
    //    visible: false,
    //    animation: false

    //});
    $('#myInput').on('input', function (e) {
        //var template = $("#ddTemplate").data('kendoDropDownList').value();
        var grid = $('#gridLensUploader').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };

        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "EmployeeName" || x.field == "EmployeeCode" || x.field == "OfficeEmailAddress" || x.field == "MobileNumber" || x.field == "IsLensUser" || x.field == "IsActiveYesNo") {
                    if (grid.dataSource.options.schema.model.fields[x.field] != undefined) {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        if (type == 'string') {
                            var targetValue = e.target.value;

                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {
                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                    else {
                        console.log(x.field)
                    }
                }
            }
        });

        grid.dataSource.filter(filter);
    });
    $('#BATCHWISEmyInput').on('input', function (e) {
        //var template = $("#ddTemplate").data('kendoDropDownList').value();
        var grid = $('#GETALLPmViewALLHistoryBATCHWISE').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };

        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "EmployeeName" || x.field == "EmployeeCode" || x.field == "OfficeEmailAddress" || x.field == "ConsultantOrGroup" || x.field == "IsTEPortalUser" || x.field == "BatchStatus" || x.field == "Notes") {
                    if (grid.dataSource.options.schema.model.fields[x.field] != undefined) {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        if (type == 'string') {
                            var targetValue = e.target.value;

                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        else if (type == 'number') {
                            if (isNumeric(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: e.target.value
                                });
                            }
                        } else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                    else {
                        console.log(x.field)
                    }
                }
            }
        });

        grid.dataSource.filter(filter);
    });

    $('#BatchmyInput').on('input', function (e) {

        var grid = $('#gridConUpViewCustomHistory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };

        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "BatchNumber" || x.field == "BatchStatus" || x.field == "TotalRecords" || x.field == "FailedRecords" || x.field == "UploadedBy") {
                    if (grid.dataSource.options.schema.model.fields[x.field] != undefined) {
                        var type = grid.dataSource.options.schema.model.fields[x.field].type;
                        if (type == 'string') {
                            var targetValue = e.target.value;

                            filter.filters.push({
                                field: x.field,
                                operator: 'contains',
                                value: targetValue
                            })
                        }
                        //else if (type == 'number' || type == 'int') {
                        //    //if (isNumeric(e.target.value)) {
                        //        filter.filters.push({
                        //            field: x.field,
                        //            operator: 'eq',
                        //            value: e.target.value
                        //        });
                        //    //}
                        //}
                        else if (type == 'date') {
                            var data = grid.dataSource.data();
                            for (var i = 0; i < data.length; i++) {
                                var dateStr = kendo.format(x.format, data[i][x.field]);
                                if (dateStr.startsWith(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: data[i][x.field]
                                    })
                                }
                            }
                        } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                            var bool = getBoolean(e.target.value);
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: bool
                            });
                        }
                    }
                    else {
                        console.log(x.field)
                    }
                }
            }
        });

        grid.dataSource.filter(filter);
    });

    $(".k-window").hide();
    $(".k-overlay").hide();
    //Utility.Loading();
    //setTimeout(function () {
    onLoad();
    //Utility.UnLoading();
    //}, 500);

    $("input:checkbox").click(function () { return false; });

    //populatePatientMasterGrid();
});
//function populateTemplateDD() {
//    //var item = { Name: "Select", Value: "Select" };
//    var data = [];
//    data.push({ Name: "Select a Template", Value: "Select" });
//    data.push({ Name: "Consultant", Value: "Consultant" });
//    data.push({ Name: "Cost Center", Value: "Cost Center" });
//    data.push({ Name: "Map 4", Value: "Map4" });
//    //data.splice(0, 0, item);
//    //departmentdata = data;
//    $("#ddTemplate").kendoDropDownList({
//        filter: "contains",
//        dataTextField: "Name",
//        dataValueField: "Value",
//        dataSource: data,
//        index: 0,
//        change: dropdownlist_selectGridChange
//    });
//    //$("#ddTemplate1").kendoDropDownList({
//    //    filter: "contains",
//    //    dataTextField: "Name",
//    //    dataValueField: "Value",
//    //    dataSource: data,
//    //    index: 0,
//    //    change: dropdownlist_select
//    //});

//    $("#ddTemplate").data('kendoDropDownList').value("Consultant");
//    //$("#ddTemplate1").data('kendoDropDownList').value("Select");
//}
//function dropdownlist_select(e) {
//    var getTemp = $("#ddTemplate1").data('kendoDropDownList').value();
//    if (getTemp == "Select") {
//        $("#uploaderText").html("Please select a template to upload");
//    }
//    else {
//        $("#uploaderText").html("Please select " + getTemp + " template to upload");
//    }
//}
function dropdownlist_selectGridChange() {
    //populateConsultantUploaderGrid($("#ddTemplate").data('kendoDropDownList').value());
}
function onLoad() {

    //populateTemplateDD();
    populateConsultantUploaderGrid();
    $("#btnExport").click(function (e) {
        var grid = $("#gridPatientMaster").data("kendoGrid");
        grid.saveAsExcel();
    });
    $("#BtnExportTemp").click(function (e) {
        //$.ajax({
        //    url:  '/MOG/DownloadExcTemplate',
        //    type: 'GET',       
        //    contentType: 'application/json; charset=utf-8',
        //    success: function (data) {
        //        alert("success");
        //    },
        //    error: function (data) {
        //        alert(data);
        //    }
        //});    
        var baseUrl = HRISMasters.baseurl;
        //var getTemp = $("#ddTemplate").data('kendoDropDownList').value();
        //if (getTemp === "Select") {
        //    toastr.error("Please Select a Template First.");
        //    $("#ddTemplate").focus();

        //}
        //else
        window.location.href = baseUrl + '/HRISMaster/DownloadExcTemplateLens';
    });
    $("#UploadPmExcel").click(function (e) {
        //var getTemp = $("#ddTemplate").data('kendoDropDownList').value();
        //if (getTemp === "Select") {
        //    toastr.error("Please Select a Template First.");
        //    //$("#ddTemplate1").focus();
        //    return false;
        //}
        //else {
        if ($("#file").val() != "") {
            //var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
            var regex = /^([a-zA-Z0-9\s_()\\.\-:])+(.xlsx)$/;
            /*Checks whether the file is a valid excel file*/
            if (!regex.test($("#file").val().toLowerCase())) {
                toastr.error("Please upload a valid xlsx file!");
                return false;
            }
            else {
                Utility.Loading();
                setTimeout(
                    UploadSelectedExcelsheet(), 500);
            }
        }
        else {
            toastr.error("Please upload a valid file!");
            return false;
        }
        //}
    });
    $("#Pmbtnback").click(function () {
        //alert("call");
        $("#PMMaster").show();
        $("#pmviewhistorymain").hide();
        $("#batchsearch").hide();
    });
    $("#Btnviewuploadhistory").click(function (e) {
        // window.location.href = 'MOG/GetMOGAPLHISTORY';

        // var getTemp = $("#ddTemplate").data('kendoDropDownList').value();
        //if (getTemp != "Select") {
        $("#inputdishname1").html("Lens Uploader Batch status.");
        $("#historytext").show();
        populateCafeGrid();
        //populateCafeGridDetails();
        //}
        //else {
        //    $("#historytext").hide();
        //    $("#inputdishname1").html("Please select a template first to view history.");
        //}
        $("#PMMaster").hide();
        $("#pmviewhistorymain").show();
        $("#gridPMViewALLHistory").hide();



        $("#gridConUpViewCustomHistory").show();
        //$("#gridPMViewCustomHistoryDetails").show();
        $("#ALLBATCHVIEW").hide();
        $("#Pmbtnback").show();
        $("#batchsearch").show();
        $("#GETALLPmViewALLHistoryBATCHWISE").hide();
        $("#BATCHWISEearch").hide();
    });
    $("#ALLBATCHVIEW").click(function (e) {
        //ViewALLHistoryBatch();
        //$("#gridPMViewALLHistory").show();
        $("#gridConUpViewCustomHistory").show();
        //$("#gridPMViewCustomHistoryDetails").hide();

        $("#ALLBATCHVIEW").hide();
        $("#batchsearch").show();
        $("#Pmbtnback").show();
        $("#GETALLPmViewALLHistoryBATCHWISE").hide();
        $("#BATCHWISEearch").hide();
    });
    $('#BatchmyInput').on('input', function (e) {
        var grid = $('#gridPMViewALLHistory').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "BATCHNUMBER" || x.field == "FLAG" || x.field == "CreationTime" || x.field == "TOTALRECORDS"
                    || x.field == "FAILED" || x.field == "CreationTime" || x.field == "UPLOADBY") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#ExcelUpload").on("click", function () {

        if ($("#file").val() != "") {
            $("#file").val('');
        }
        var dialogs = $("#UploadExcelFile").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialogs.open().element.closest(".k-window").css({
            top: 167,
        });
        dialogs.center();

        //var getTemp = $("#ddTemplate").data('kendoDropDownList').value();
        //if (getTemp == "Select") {
        //    $("#uploaderText").html("Please select a template to upload");
        //}
        //else {
        //    $("#uploaderText").html("Please select " + getTemp + " template to upload");
        //}

        dialogs.title("Upload File");
    })

    function populateCafeGrid() {

        //var getTemp = $("#ddTemplate").data('kendoDropDownList').value();
        Utility.Loading();
        var gridVariable = $("#gridConUpViewCustomHistory");
        //.height(250);
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "ConsultantUploaderHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            scrollable: true,
            height: "280px",
            columns: [
                {
                    field: "BatchNumber", title: "Batch Number", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "EmployeeCode", title: "Employee Code", width: "80px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    }
                //},
                {
                    field: "BatchStatus", title: "Error Message", width: "80px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "TotalRecords", title: "Total Records", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FailedRecords", title: "Failed Records", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                //{
                //    field: "UploadedOn", title: "Uploaded Date and Time", width: "80px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    }
                //},
                {
                    field: "UploadedOn", title: "Uploaded Date and Time", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                    template: '# if (UploadedOn == null) {#<span>-</span>#} else{#<span>#: kendo.toString(UploadedOn, "dd-MMM-yyyy")#</span>#}#',
                    headerAttributes: {
                        //style: "text-align: center;"
                    }
                },
                {
                    field: "UploadedBy", title: "Uploaded By", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },

                {
                    field: "View", title: "View Details", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'View',
                            click: function (e) {
                                Utility.Loading();
                                setTimeout(function () {

                                    var gridObj = $("#gridConUpViewCustomHistory").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    populateGetallBatchwishDetails(tr.BatchNumber);
                                    $("#gridConUpViewCustomHistory").hide();
                                    $("#ALLBATCHVIEW").show();
                                    $("#BATCHWISEearch").show();
                                    $("#Pmbtnback").hide();
                                    $("#batchsearch").hide();
                                    $("#GETALLPmViewALLHistoryBATCHWISE").show();

                                    Utility.UnLoading();
                                    return true;
                                }, 1000);
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {


                        HttpClient.MakeSyncRequest(HRISMasters.GetConsultantUploaderHISTORY, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                template: "Lens"
                            }
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BatchNumber: { type: "string" },
                            BatchStatus: { type: "string" },
                            TotalRecords: { type: "int" },
                            FailedRecords: { type: "int" },
                            UploadedOn: { type: "date" },
                            UploadedBy: { type: "string" }
                            //BATCHSTATUS: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.BatchStatus == "Success") {

                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");

                    }
                });
                //items.each(function (e) {
                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //        $(this).find('.chkbox').removeAttr('disabled');

                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //        $(this).find('.chkbox').attr('disabled', 'disabled');
                //    }
                //});


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    $(document).ready(function () {
        var content = "";
        $("#gridPMViewALLHistory").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }
                var dataItem = $("#gridPMViewALLHistory").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
        $("#gridPMViewCustomHistoryDetails").kendoTooltip({
            filter: "td:nth-child(2), th:nth-child(2)",
            position: "center",


            content: function (e) {

                if (e.target.is("th")) {

                    return e.target.text();
                }

                var dataItem = $("#gridPMViewCustomHistoryDetails").data("kendoGrid").dataItem(e.target.closest("tr"));
                console.log(dataItem);
                if (dataItem.FLAG == "1") {
                    content = "Success";
                }
                else if (dataItem.FLAG == "2") {
                    content = "Partially Success";
                }
                else {
                    content = "Failed";
                }
                return content;
            }
        }).data("kendoTooltip");
    });

    function populateCafeGridDetails() {

        Utility.Loading();
        var gridVariable = $("#gridPMViewCustomHistoryDetails");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "PatientMasterHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "80px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    }, template: '<div class="colortag"></div>'
                },
                {
                    field: "TOTALRECORDS", title: "Records", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FAILED", title: "Failed", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                },


                //{
                //    field: "View", title: "View Details", width: "50px",

                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    command: [
                //        {
                //            name: 'View',
                //            click: function (e) {

                //                var gridObj = $("#gridPMViewCustomHistoryDetails").data("kendoGrid");
                //                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //                datamodel = tr;
                //                BATCHNUMBER = tr.BATCHNUMBER;

                //                alert(BATCHNUMBER);
                //                return true;
                //            }
                //        }
                //    ],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetPMHISTORYDetails, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" }

                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.FLAG == "1") {
                        console.log($(this));
                        console.log($(this).find(".colortag"));
                        $(this).find(".colortag").css("background-color", "green");

                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }

                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}


                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateGetallBatchwishDetails(BATCHNUMBER) {

        Utility.Loading();
        var gridVariable = $("#GETALLPmViewALLHistoryBATCHWISE");
        //.height(180);
        gridVariable.html("");
        //if (template == "Consultant") {
        //    gridVariable.kendoGrid({
        //        excel: {
        //            fileName: "ConsultantUploaderLHistory.xlsx",
        //            filterable: true,
        //            allPages: true
        //        },
        //        sortable: true,
        //        filterable: {
        //            extra: true,
        //            operators: {
        //                string: {
        //                    contains: "Contains",
        //                    startswith: "Starts with",
        //                    eq: "Is equal to",
        //                    neq: "Is not equal to",
        //                    doesnotcontain: "Does not contain",
        //                    endswith: "Ends with"
        //                }
        //            }
        //        },
        //        //pageable: true,
        //        groupable: false,
        //        //reorderable: true,
        //        scrollable: true,
        //        //height: "280px",
        //        columns: [
        //            {
        //                field: "EmployeeCode", title: "Employee Code", width: "80px", attributes: {
        //                    style: "text-align: left; font-weight:normal"
        //                }
        //            },
        //            {
        //                field: "EmployeeName", title: "Employee Name", width: "80px", attributes: {
        //                    style: "text-align: left; font-weight:normal"
        //                }
        //            },
        //            {
        //                field: "ConsultantOrGroup", title: "Consultant/Group", width: "100px", attributes: {

        //                    style: "text-align: left; font-weight:normal"
        //                }
        //            },
        //            {
        //                field: "IsTEPortalUser", title: "TEPortal User", width: "80px", attributes: {

        //                    style: "text-align: left; font-weight:normal"
        //                }
        //            },
        //            {
        //                field: "BatchStatus", title: "Record Status", width: "100px", attributes: {
        //                    "class": "FLAGCELL",
        //                    style: "text-align: left; font-weight:normal"
        //                }
        //            },
        //            {
        //                field: "Notes", title: "Message", width: "100px", attributes: {

        //                    style: "text-align: left; font-weight:normal"
        //                }
        //            }



        //        ],
        //        dataSource: {
        //            transport: {
        //                read: function (options) {

        //                    var varCodes = "";

        //                    HttpClient.MakeSyncRequest(HRISMasters.GetConsultantUploaderHISTORYByBatch, function (result) {
        //                        Utility.UnLoading();

        //                        if (result != null) {
        //                            options.success(result);
        //                        }
        //                        else {
        //                            options.success("");
        //                        }
        //                    }, {
        //                        batchno: BATCHNUMBER,
        //                        template: template
        //                    }
        //                        //{
        //                        //filter: mdl
        //                        //}
        //                        , false);
        //                }
        //            },
        //            schema: {
        //                model: {
        //                    id: "ID",
        //                    fields: {
        //                        EmployeeCode: { type: "string" },
        //                        EmployeeName: { type: "string" },
        //                        ConsultantOrGroup: { type: "string" },
        //                        IsTEPortalUser: { type: "string" },
        //                        BatchStatus: { type: "string" },
        //                        Notes: { type: "string" }
        //                    }
        //                }
        //            },
        //            //pageSize: 15,
        //        },
        //        columnResize: function (e) {
        //            var grid = gridVariable.data("kendoGrid");
        //            e.preventDefault();
        //        },
        //        noRecords: {
        //            template: "No Records Available"
        //        },
        //        dataBound: function (e) {
        //            var items = e.sender.items();
        //            var grid = this;
        //            grid.tbody.find("tr[role='row']").each(function () {
        //                var model = grid.dataItem(this);

        //                if (model.BatchStatus == "Success") {
        //                    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
        //                    //alert("call");
        //                }
        //                //else if (model.FLAG == "Uploaded" || model.FLAG == "Uploaded") {
        //                //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
        //                //    //alert("call");
        //                //}
        //                else {
        //                    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
        //                }
        //            });
        //            //items.each(function (e) {
        //            //    if (user.UserRoleId == 1) {
        //            //        $(this).find('.k-grid-Edit').text("Edit");
        //            //        $(this).find('.chkbox').removeAttr('disabled');

        //            //    } else {
        //            //        $(this).find('.k-grid-Edit').text("View");
        //            //        $(this).find('.chkbox').attr('disabled', 'disabled');
        //            //    }
        //            //});


        //        },
        //        change: function (e) {
        //        },
        //        excelExport: function onExcelExport(e) {
        //            var sheet = e.workbook.sheets[0];
        //            var data = e.data;
        //            var cols = Object.keys(data[0])
        //            var columns = cols.filter(function (col) {
        //                if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
        //                else
        //                    return col;
        //            });
        //            var columns1 = columns.map(function (col) {
        //                return {
        //                    value: col,
        //                    autoWidth: true,
        //                    background: "#7a7a7a",
        //                    color: "#fff"
        //                };
        //            });
        //            console.log(columns1);
        //            var rows = [{ cells: columns1, type: "header" }];

        //            for (var i = 0; i < data.length; i++) {
        //                var rowCells = [];
        //                for (var j = 0; j < columns.length; j++) {
        //                    var cellValue = data[i][columns[j]];
        //                    rowCells.push({ value: cellValue });
        //                }
        //                rows.push({ cells: rowCells, type: "data" });
        //            }
        //            sheet.rows = rows;
        //        }

        //    })
        //}
        //else {
        gridVariable.kendoGrid({
            excel: {
                fileName: "LensUploaderHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            scrollable: true,
            // height: "20px",
            columns: [
                {
                    field: "EmployeeCode", title: "Employee Code", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "EmployeeName", title: "Employee Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "BatchStatus", title: "Record Status", width: "100px", attributes: {
                        "class": "FLAGCELL",
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Notes", title: "Message", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    }
                }



            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(HRISMasters.GetConsultantUploaderHISTORYByBatch, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, {
                            batchno: BATCHNUMBER,
                            template: "Lens"
                        }
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "EmployeeCode",
                        fields: {
                            EmployeeCode: { type: "string" },
                            EmployeeName: { type: "string" },
                            BatchStatus: { type: "string" },
                            Notes: { type: "string" }
                        }
                    }
                },
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.BatchStatus == "Success") {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                        //alert("call");
                    }
                    //else if (model.FLAG == "Uploaded" || model.FLAG == "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //    //alert("call");
                    //}
                    else {
                        $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    }
                });
                //items.each(function (e) {
                //    if (user.UserRoleId == 1) {
                //        $(this).find('.k-grid-Edit').text("Edit");
                //        $(this).find('.chkbox').removeAttr('disabled');

                //    } else {
                //        $(this).find('.k-grid-Edit').text("View");
                //        $(this).find('.chkbox').attr('disabled', 'disabled');
                //    }
                //});


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //}
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }



    function ViewALLHistoryBatch() {

        Utility.Loading();
        var gridVariable = $("#gridPMViewALLHistory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "PatientMasterHistory.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            //pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "BATCHNUMBER", title: "Batch Number", width: "120px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FLAG", title: "Batch Status", width: "60px", attributes: {
                        style: "text-align: center; font-weight:normal",

                    }, template: '<div class="colortag"></div>',
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "TOTALRECORDS", title: "Records", width: "60px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "CreationTime", title: "Batch Upload Time", width: "80px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "FAILED", title: "Failed", width: "30px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },
                {
                    field: "UPLOADBY", title: "Upload By", width: "80px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    }
                },


                {
                    field: "View", title: "View Details", width: "50px",

                    attributes: {
                        style: "text-align: center; font-weight:normal;color:red!important",
                        "class": "ViewBatchcell"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    command: [
                        {
                            name: 'View',
                            click: function (e) {
                                //debugger;
                                var MDgridObj = $("#gridPMViewALLHistory").data("kendoGrid");
                                var tr = MDgridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                BATCHNUMBER = tr.BATCHNUMBER;
                                console.log(e.currentTarget);

                                var trEdit = $(this).closest("tr");
                                $(trEdit).find(".k-grid-View").addClass("k-state-disabled");


                                var dialogs = $("#GETALLPmViewALLHistoryBATCHWISE").data("kendoWindow");
                                populateGetallBatchwishDetails(BATCHNUMBER);
                                $(".k-overlay").css("display", "block");
                                $(".k-overlay").css("opacity", "0.5");
                                dialogs.open().element.closest(".k-window").css({
                                    left: 305,
                                    top: 215
                                });
                                // dialogs.center();


                                dialogs.title("Batch Details");

                                return false;

                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeSyncRequest(CookBookMasters.GetALLHISTORYDetailsPatientMaster, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            BATCHNUMBER: { type: "string" },
                            FLAG: { type: "string" },
                            TOTALRECORDS: { type: "string" },
                            FAILED: { type: "string" },
                            CreationTime: { type: "string" },
                            UPLOADBY: { type: "string" }

                        }
                    }
                },
                sort: {
                    field: "CreationTime",
                    dir: "desc"
                }
                //pageSize: 15,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);
                    //$(this).find(".k-grid-View").addClass("ViewBatch");
                    $(this).find(".k-grid-View").addClass("k-grid-Edit");

                    //if (model.FLAG != "Uploaded") {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolorred");
                    //    //alert("call");
                    //}
                    //else {
                    //    $(this).find(".FLAGCELL").addClass("batchstatuscolor");
                    //}
                    if (model.FLAG == "1") {

                        $(this).find(".colortag").css("background-color", "green");

                    }
                    else if (model.FLAG == "2") {
                        $(this).find(".colortag").css("background-color", "orange");

                    }
                    else {

                        $(this).find(".colortag").css("background-color", "red");
                    }
                });
                items.each(function (e) {
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').text("Edit");
                        $(this).find('.chkbox').removeAttr('disabled');

                    } else {
                        $(this).find('.k-grid-Edit').text("View");
                        $(this).find('.chkbox').attr('disabled', 'disabled');
                    }
                });


            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function UploadSelectedExcelsheet(getTemp) {

        var data = new FormData();
        var i = 0;
        var fl = $("#file").get(0).files[0];

        if (fl != undefined) {

            data.append("file", fl);

        }
        //data.append("template", getTemp);
        //HttpClient.MakeSyncRequest(CookBookMasters.UploadExcelsheet, function () { },
        //    {
        //        data: data
        //    } ,
        // null, true);
        Utility.Loading();
        $.ajax({
            type: "POST",
            url: HRISMasters.UploadExcelsheet,
            contentType: false,
            processData: false,
            async: false,
            data: data,
            success: function (result) {
                //if (result == 'PatientID is null') {
                //    toastr.error("Can not be PatientID is Null");
                //    $(".k-window").hide();
                //    $(".k-overlay").hide();


                //    //return true;
                //    Utility.UnLoading();
                //}

                if (result == "The uploaded file is empty") {
                    toastr.error(result);
                    $(".k-window").hide();
                    $(".k-overlay").hide();
                    Utility.UnLoading();
                }
                //else if (result == 'Artical Number is Dupliacte') {
                //    toastr.error("Artical Number is Dupliacte");
                //    $(".k-window").hide();
                //    $(".k-overlay").hide();

                //    //return true;
                //    Utility.UnLoading();
                //}

                else if (result == "Successfully read.") {
                    toastr.success("Data upload successfully");
                    $(".k-window").hide();
                    $(".k-overlay").hide();
                    //debugger;
                    //$("#gridPatientMaster").data("kendoGrid").dataSource.data([]);
                    //$("#gridPatientMaster").data("kendoGrid").dataSource.read();
                    // return true;
                    //Refresh grid
                    populateConsultantUploaderGrid();

                    //Utility.UnLoading();
                }
                else {
                    toastr.warning("Partialy uploaded.");
                    $(".k-window").hide();
                    $(".k-overlay").hide();
                    Utility.UnLoading();
                    populateConsultantUploaderGrid();
                }
                result = "";
                return false;
            },
            error: function (xhr, status, p3, p4) {
                var err = "Error " + " " + status + " " + p3 + " " + p4;
                if (xhr.responseText && xhr.responseText[0] == "{")
                    err = JSON.parse(xhr.responseText).Message;
                alert(err);
                Utility.UnLoading();
                return false;
            }
        });
    }


    //aplMasterdataSource.push({ "ArticleID": 0, "ArticleDescription": "Select APL" })
    //populateAPLMasterDropdown();

    //HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
    //    user = result;
    //    //if (user.UserRoleId === 1) {
    //    //    $("#InitiateBulkChanges").css("display", "inline");
    //    //    $("#AddNew").css("display", "inline");
    //    //}
    //    //if (user.UserRoleId === 2) {
    //    //    $("#InitiateBulkChanges").css("display", "none");
    //    //    $("#AddNew").css("display", "none");
    //    //}
    //    populatePatientMasterGrid();
    //    //HttpClient.MakeSyncRequest(CookBookMasters.GetAPLMasterDataList, function (data) {
    //    //    aplMasterdataSource = data.filter(m => m.ArticleType == 'ZFOD' || m.ArticleType == 'ZPRP');
    //    //    aplMasterdataSource.unshift({ "ArticleID": 0, "ArticleDescription": "Select APL" })



    //    //}, null, true);
    //}, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetSectorDataList, function (data) {
    //    var dataSource = data;
    //    sectorMasterdataSource = [];
    //    for (var i = 0; i < dataSource.length; i++) {
    //        sectorMasterdataSource.push({ "value": dataSource[i].ID, "text": dataSource[i].SectorName, "SectorNumber": dataSource[i].SectorNumber });
    //    }
    //    populateSectorMasterDropdown();

    //}, null, true);

    //HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (data) {

    //    var dataSource = data;
    //    uomdata = [];
    //    uomdata.push({ "value": "Select", "text": "Select" });
    //    for (var i = 0; i < dataSource.length; i++) {
    //        uomdata.push({ "value": dataSource[i].UOMCode, "text": dataSource[i].Name });
    //    }
    //    console.log(uomdata)
    //    populateUOMDropdown();
    //    populateUOMDropdown_Bulk();
    //}, null, true);

    //$("#gridBulkChange").css("display", "none");
    //populateBulkChangeControls();
    //var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
    //changeControls.css("background-color", "#fff");
    //changeControls.css("border", "none");
    //changeControls.eq(3).text("");
    //changeControls.eq(3).append("<div id='uom' style='width:100%; font-size:10.5px!important'></div>");

    //HttpClient.MakeRequest(CookBookMasters.GetAPLMasterDataList, function (data) {
    //    aplMasterdataSource = data.filter(m => m.ArticleType == 'ZFOD' && m.ArticleType == 'ZPRP');
    //    aplMasterdataSource.unshift({ "ArticleID": 0, "ArticleDescription": "Select APL" })

    //    //populateRegionMasterDropdown();
    //    //$("#btnGo").css("display", "inline-block");

    //}, null , true);
}



function populateConsultantUploaderGrid() {

    var gridVariable = $("#gridLensUploader");
    gridVariable.html("");

    Utility.Loading();
    gridVariable.kendoGrid({
        excel: {
            fileName: "LensUploader.xlsx",
            filterable: true,
            allPages: true
        },
        sortable: true,
        filterable: {
            extra: true,
            operators: {
                string: {
                    contains: "Contains",
                    startswith: "Starts with",
                    eq: "Is equal to",
                    neq: "Is not equal to",
                    doesnotcontain: "Does not contain",
                    endswith: "Ends with"
                }
            }
        },
        //pageable: true,
        pageable: {
            messages: {
                display: "{0}-{1} of {2} records"
            }
        },

        groupable: false,
        height: 485,
        width: 1022,
        //reorderable: true,
        //scrollable: true,
        columns: [
            //{
            //    field: "SimulationCode", title: "Simulation Code", width: "40px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //},
            {
                field: "EmployeeCode",
                title: "Employee Code",
                //width: "85px",
                attributes: {
                    style: "text-align: left; font-weight:normal;text-transform: uppercase;"
                },
                headerAttributes: {
                    style: "text-align: left;"
                }
            },
            {
                field: "EmployeeName", title: "Employee Name",
                //width: "150px",
                attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            }
            ,
            //{
            //    field: "FirstName", title: "First Name", width: "150px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //}
            //,
            //{
            //    field: "LastName", title: "Last Name", width: "150px", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //}
            // ,
            {
                field: "OfficeEmailAddress", title: "Email ID", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            }
            ,

            {
                field: "MobileNumber", title: "Mobile Number", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            {
                field: "IsActiveYesNo", title: "Active", attributes: {
                    style: "text-align: left; font-weight:normal"
                }
            },
            // {
            //    field: "IsActive",
            //     title: "Active", 
            //    attributes: {
            //        style: "text-align: center; font-weight:normal"
            //    },
            //    headerAttributes: {
            //        style: "text-align: center;"
            //    },
            //    template: '<label class= "switch"><input disabled type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            //},
            {
                field: "IsLensUser",
                title: "Lens User",
                attributes: {
                    style: "text-align: center; font-weight:normal"
                },
                headerAttributes: {
                    style: "text-align: center;"
                },
                template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsLensUser ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
            }
            //{
            //    field: "IsLensUser", title: "Lens User", attributes: {
            //        style: "text-align: left; font-weight:normal"
            //    }
            //}
        ],
        dataSource: {
            transport: {
                read: function (options) {

                    var varCodes = "";

                    HttpClient.MakeRequest(HRISMasters.GetLensUploaderData, function (result) {
                        Utility.UnLoading();
                        if (result != null) {

                            options.success(result);
                        }
                        else {
                            options.success("");
                        }
                    }, null
                        , false);
                }
            },
            schema: {
                model: {
                    id: "EmployeeCode",
                    fields: {
                        EmployeeCode: { type: "string" },
                        EmployeeName: { type: "string" },
                        OfficeEmailAddress: { type: "string" },
                        MobileNumber: { type: "string" },
                        IsLensUser: { type: "string" },
                        IsActive: { type: "string" },
                        IsActiveYesNo: { type: "string" },
                    }
                }
            },
            pageSize: 100,
        },
        dataBound: function (e) {
            $(".chkbox").on("change", function () {
                // 
                var gridObj = $("#gridLensUploader").data("kendoGrid");
                var tr = gridObj.dataItem($(this).closest("tr"));
                var trEdit = $(this).closest("tr");
                var th = this;
                datamodel = tr;
                datamodel.IsLensUser = $(this)[0].checked;
                var active = "";
                if (datamodel.IsLensUser) {
                    active = "Active";
                } else {
                    active = "Inactive";
                }

                Utility.Page_Alert_Save("Are you sure to mark lens user<b>" + datamodel.EmployeeName + "</b> " + active + "?", "Lens User Update Confirmation", "Yes", "No", function () {
                    $("#error").css("display", "none");
                    HttpClient.MakeRequest(HRISMasters.ChangeStatusLens, function (result) {
                        if (result == false) {
                            toastr.error("Some error occured, please try again");
                        }
                        else {
                            toastr.success("Lens User updated successfully");

                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        }
                    }, {
                        model: datamodel
                    }, false);
                }, function () {
                        $(th)[0].checked = !datamodel.IsLensUser;
                    if ($(th)[0].checked) {
                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                    } else {
                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                    }
                });
                return true;
            });
        },
        columnResize: function (e) {
            var grid = gridVariable.data("kendoGrid");
            e.preventDefault();
        },
        noRecords: {
            template: "No Records Available"
        },
        excelExport: function onExcelExport(e) {
            //var sheet = e.workbook.sheets[0];
            //var data = e.data;
            //var cols = Object.keys(data[0])
            //var columns = cols.filter(function (col) {
            //    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
            //    else
            //        return col;
            //});
            //var columns1 = columns.map(function (col) {
            //    return {
            //        value: col,
            //        autoWidth: true,
            //        background: "#7a7a7a",
            //        color: "#fff"
            //    };
            //});
            //console.log(columns1);
            //var rows = [{ cells: columns1, type: "header" }];

            //for (var i = 0; i < data.length; i++) {
            //    var rowCells = [];
            //    for (var j = 0; j < columns.length; j++) {
            //        var cellValue = data[i][columns[j]];
            //        rowCells.push({ value: cellValue });
            //    }
            //    rows.push({ cells: rowCells, type: "data" });
            //}
            //sheet.rows = rows;
        }
        //,
        //search: {
        //    fields: ["SimulationCode", "Name", "Status", "Version", "CreateOnFormatted"]
        //},        
        //toolbar: ["search"],
    })


    //$(".k-label")[0].innerHTML.replace("items", "records");
    //getDishCategoriesWithDishes();
    //Utility.UnLoading();

    //$("#topHeading").text("My Simulations");
}