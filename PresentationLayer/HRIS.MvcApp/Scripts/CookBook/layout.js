﻿var action = "@Url.Content('Index','Site')";
$(document).ready(function () {

    var user;
    
    HttpClient.MakeSyncRequest(CookBookLayout.GetLoginUserDetailsUrl, function (result) {
        user = result;

        if (user.UserRoleId === 1) {
            $("#InitiateBulkChanges").css("display", "inline");
            $("#AddNew").css("display", "inline");
        }
        if (user.UserRoleId === 2) {
            $("#InitiateBulkChanges").css("display", "none");
            $("#AddNew").css("display", "none");
        }


    }, null, false);

    HttpClient.MakeRequest(CookBookLayout.GetModulesByUserID, function (data) {
        var dataSource = data;
        moduledata = [];
        for (var i = 0; i < dataSource.length; i++) {
            if (dataSource[i].MenuDisplay == true) {
                moduledata.push({ "ModuleCode": dataSource[i].ModuleCode, "MenuDisplayName": dataSource[i].MenuDisplayName, "PageUrl": dataSource[i].PageUrl, "MenuHeading": dataSource[i].MenuHeading, "MenuParent": dataSource[i].MenuParent });
            }
        }
        populateSideNavBar();
    }, { "userID": user.UserId }, false);
});

function populateSideNavBar() {
    var navmmt2 = $("nav.mt-2 .sectormenu");
    var lhtml = "";
    var header = "";
    var parent = "";
    for (module of moduledata) {
        if (header != module.MenuHeading) {
            if (parent != "") {
                if (parent != "blank") {
                    lhtml = lhtml + " </ul></li>";
                }
            }
            header = module.MenuHeading;
            lhtml = lhtml + "<li class='nav-header'>" + header + "</li>";
        }
        if (parent != module.MenuParent && header == module.MenuHeading) {
            if (parent != "") {
                if (parent != "blank") {
                    lhtml = lhtml + " </ul></li>";
                }
            }
            if (module.MenuParent == "null") {
                parent = "blank";
            } else {
                parent = module.MenuParent;
                lhtml = lhtml + "<li class='nav-item has-treeview'>" +
                    "    <a href='#' class='nav-link'>" +
                    "       <i class='nav-icon fas fa-copy'></i>" +
                    "       <p>" + module.MenuParent + " <i class='fas fa-angle-right right'></i></p>" +
                    "    </a>" +
                    "    <ul class='nav nav-treeview'>";
            }
        }
        var link = module.PageUrl;
        if (module.MenuParent == "null") {
            parent = "blank";
            lhtml = lhtml + "\n" +
                "       <li class='nav-item'>" +
                "           <a href= " + action + " class='nav-link'>" +
                "                <i class='nav-icon fas fa-copy'></i>" +
                "                <p>" + module.MenuDisplayName + "</p>" +
                "           </a>" +
                "       </li>";
        } else {
            parent = module.MenuParent
            lhtml = lhtml + "\n" +
                "       <li class='nav-item'>" +
                "           <a href=" + action + " class='nav-link'>" +
                "                <i class='far nav-icon'></i>" +
                "                <p>" + module.MenuDisplayName + "</p>" +
                "           </a>" +
                "       </li>";
        }

    }
    navmmt2.append(lhtml);

}