﻿
$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var leadershiprolename = "";
    var sitedata = [];
    var dkdata = [];

    $('#myInput').on('input', function (e) {
        var grid = $('#gridLeaderShipRole').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "LeaderShipRole" || x.field == "Remark" || x.field == "LeadershipRoleCode") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    

    $('#myInput').focus(function () {
        $('.leadershiproledetailsearch .fa-search').css('opacity', 0);
        $(this).css('padding-left', '10px');
        $(this).attr('placeholder', 'Start typing to filter');
    }).blur(function () {
        $('.leadershiproledetailsearch .fa-search').css('opacity', 1);
        $(this).css('padding-left', '25px');
        $(this).attr('placeholder', 'Search');
    })

    $("#windowEdit").kendoWindow({
        modal: true,
        width: "340px",
        height: "140px",
        title: "LeaderShip Role Details - " + leadershiprolename,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {
        $("#btnExport").click(function (e) {
            var grid = $("#gridLeaderShipRole").data("kendoGrid");
            grid.saveAsExcel();
        });
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            //if (user.UserRoleId === 1) {
            //    $("#InitiateBulkChanges").css("display", "inline");
            //    $("#AddNew").css("display", "inline");
            //}
            //if (user.UserRoleId === 2) {
            //    $("#InitiateBulkChanges").css("display", "none");
            //    $("#AddNew").css("display", "none");
            //}
            populateLeaderShipRoleGrid();
        }, null, false);
    });


    function populateLeaderShipRoleGrid() {

        Utility.Loading();
        var gridVariable = $("#gridLeaderShipRole");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "LeaderShipRole.xlsx",
                filterable: true,
                allPages: true
            },
            // sortable: true,
            editable: "inline",
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
           // toolbar: ["create"],
            columns: [
                {
                    field: "LeaderShipRole", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    command: ["edit"],
                    click: function (e) {

                                $("#success").css("display", "none");
                                $("#error").css("display", "none");
                                var gridObj = $("#gridLeaderShipRole").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodel = tr;
                                $("#leaderShipRoleid").val(tr.ID);
                                $("#leadershiprolename ").val(tr.LeaderShipRole);
                                $("#leadershiprolecode ").val(tr.LeadershipRoleCode);
                                $("#remark").val(tr.Remark);
                                leadershiprolename = tr.LeaderShipRole;
                                return true;
                            },
                    title: "Action", width: "250px"
                }
               
                //{
                //    field: "Edit", title: "Action", width: "10px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    command: [
                //        {
                //            name: 'Edit',
                //            click: function (e) {

                //                $("#success").css("display", "none");
                //                $("#error").css("display", "none");
                //                var gridObj = $("#gridLeaderShipRole").data("kendoGrid");
                //                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                //                datamodel = tr;
                //                ColorName = tr.Name + " (" + tr.ColorCode + ")";
                //                var dialog = $("#windowEdit").data("kendoWindow");

                //                $(".k-overlay").css("display", "block");
                //                $(".k-overlay").css("opacity", "0.5");
                //                dialog.open();
                //                dialog.center();
                //                // 
                //                $("#leaderShipRoleid").val(tr.ID);
                //                $("#leadershiprolename ").val(tr.LeaderShipRole);
                //                $("#leadershiprolecode ").val(tr.LeadershipRoleCode);
                //                $("#remark").val(tr.Remark);
                //                leadershiprolename = tr.LeaderShipRole;
                //                dialog.title("Leadership RoleDetails - " + leadershiprolename);
                //                return true;
                //            }
                //        }
                //    ],
                //}
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetLeaderShipRoleDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null, false);
                    },
                    update: function (options) {
                        updateLeaderShipRole(options.data);
                    },
                    destroy: {
                       // url: crudServiceBaseUrl + "/Products/Destroy",
                        dataType: "jsonp"
                    },
                    create: {
                      //  url: crudServiceBaseUrl + "/Products/Create",
                        dataType: "jsonp"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                    }

                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            LeaderShipRole: { editable: true, type: "string" },
                            //LeadershipRoleCode: { editable: false, type: "string" },
                            //Remark: { type: "string", editable: false, },
                            //IsActive: { editable: false }
                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                $(".chkbox").on("change", function () {
                    // 
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    var gridObj = $("#gridLeaderShipRole").data("kendoGrid");
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.ColorName + "</b> Color " + active + "?", "Color Update Confirmation", "Yes", "No", function () {
                        $("#error").css("display", "none");
                        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                            if (result == false) {
                                $("#error").css("display", "flex");
                                $("#error").find("p").text("Some error occured, please try again");
                            }
                            else {
                                $("#error").css("display", "flex");
                                $("#success").css("display", "flex");
                                $("#success").find("p").text("Color updated successfully");
                            }
                        }, {
                            model: datamodel
                        }, false);
                    }, function () {
                        $(this)[0].checked = !datamodel.IsActive;
                        //$("#gridColor").data("kendoGrid").dataSource.data([]);
                        //$("#gridColor").data("kendoGrid").dataSource.read();
                    });
                    return true;
                });
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $("#gridLeaderShipRole").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    $("#AddNew").on("click", function () {

        $("#success").css("display", "none");
        $("#error").css("display", "none");
       
        var model; 
        var dialog = $("#windowEdit").data("kendoWindow");

        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        datamodel = model;
        $("#leaderShipRoleid").val("");
        $("#leadershiprolename ").val("");
        $("#leadershiprolecode ").val("");
        $("#remark").val("");

        dialog.title("New Leadership Role Creation");
    })

    $("#btnCancel").on("click", function () {
        //Utility.Page_Alert_Save("All Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
        //    function () {
        //        $(".k-overlay").hide();
        //        var orderWindow = $("#windowEdit").data("kendoWindow");
        //        orderWindow.close();
        //    },
        //    function () {
        //    }
        //);
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit").data("kendoWindow");
        orderWindow.close();
    });

    function checkIsLeaderShipRoleExists() {
        var isExists = false;
        var LeaderShipRoleList = $("#gridLeaderShipRole").data("kendoGrid").dataSource._data;
        if (LeaderShipRoleList != null && LeaderShipRoleList.length > 0) {
            var filterData = LeaderShipRoleList.filter(m => m.LeaderShipRole === $("#leadershiprolename").val());
            if (filterData != null && filterData.length > 0)
            {
                isExists = true;
            }
        }
        return isExists;
    };

    function saveLeaderShipRole() {
        if ($("#leadershiproleid").val() == "" && checkIsLeaderShipRoleExists()) {
            toastr.error("Leadership Role already exists.");
            return false;
        }
        if ($("#leadershiprolename").val() === "") {
            toastr.error("Please provide Leadership Role Name");
            $("#leadershiprolename").focus();
            return;
        }
        else {
            $("#error").css("display", "none");

            var model;
            if (datamodel != null) {
                model = datamodel;
                model.LeaderShipRole = $("#leadershiprolename").val();
                model.Remark = $("#remark").val();
                model.LeadershipRoleCode = $("#leadershiprolecode").val();
                model.CreatedOn = kendo.parseDate(model.CreatedOn);
            }
            else {
                model = {
                    "ID": $("#LeaderShipRoleid").val(),
                    "LeaderShipRole": $("#leadershiprolename").val(),
                    "Remark": $("#remark").val(),
                    "LeadershipRoleCode": $("#remark").val(),
                    "CreatedOn": Utility.CurrentDate(),
                    "IsActive": true,
                }
            }

            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookMasters.SaveLeaderShipRoleData, function (result) {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#error").css("display", "flex");
                    toastr.error("Some error occured, please try again");
                    // $("#error").find("p").text("Some error occured, please try again");
                    $("#sitealias").focus();
                }
                else {
                    $(".k-overlay").hide();
                    //$("#error").css("display", "flex");
                    var orderWindow = $("#windowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    //$("#success").css("display", "flex");
                    if (model.ID > 0) {
                        toastr.success("Leadership Role updated successfully");
                    }
                    else {
                        toastr.success("Leadership Role added successfully");
                    }

                    // $("#success").find("p").text("Color updated successfully");
                    $("#gridLeaderShipRole").data("kendoGrid").dataSource.data([]);
                    $("#gridLeaderShipRole").data("kendoGrid").dataSource.read();
                    //$("#gridLeaderShipRole").data("kendoGrid").dataSource.sort({ field: "LeaderShipRole", dir: "asc" });
                }
            }, {
                model: model

            }, true);
        }
      
    }

    function updateLeaderShipRole(model) {
        if ($("#leadershiproleid").val() == "" && checkIsLeaderShipRoleExists()) {
            toastr.error("Leadership Role already exists.");
            return;
        }
        else {
            if (model != null) {
                model.CreatedOn = kendo.parseDate(model.CreatedOn);
                model.ModifiedOn = kendo.parseDate(model.ModifiedOn);
            }

            HttpClient.MakeSyncRequest(CookBookMasters.SaveLeaderShipRoleData, function (result) {
                if (result == false) {
                    toastr.error("Some error occured, please try again");
                }
                else {
                    if (model.ID > 0) {
                        toastr.success("Leadership Role updated successfully");
                    }
                    $("#gridLeaderShipRole").data("kendoGrid").dataSource.data([]);
                    $("#gridLeaderShipRole").data("kendoGrid").dataSource.read();
                }
            }, {
                model: model

            }, true);
        }
        
    }

    $("#btnSubmit").click(function () {
        saveLeaderShipRole();
    });
});