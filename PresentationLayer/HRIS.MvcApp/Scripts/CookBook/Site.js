﻿var sitetypedata = [];
var xresult;
var SCode;
var siteSubSiteMappingData = [];
var siteDayPartResult = [];
var daypartdataInactive = [];
function populateDayPartMultiSelect() {
    $("#OtherSiteinputdaypart").kendoMultiSelect({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "code",
        dataSource: daypartdata,
        index: 0,
        autoClose: false
    });
}
function GetDayPartMapping() {
    HttpClient.MakeSyncRequest(HRISMasters.GetSiteDayPartMappingDataList, function (data) {

        siteDayPartResult = data;

    },
        { siteCode: SCode }, true);
    var multiarray = siteDayPartResult.map(item => item.DayPartCode);
    var multiselect = $("#OtherSiteinputdaypart").data("kendoMultiSelect");
    multiselect.value(multiarray);
    Utility.UnLoading();
}
function SaveDayPartMapping() {

    HttpClient.MakeSyncRequest(HRISMasters.SaveSiteDayPartMappingDataList, function (result) {
        if (result == false) {
            toastr.error("Some error occured, please try again.");
            dayPartArray = [];
            Utility.UnLoading();
        }
        else {

            if (user.SectorNumber != "20")
                toastr.success("Food Program Mealtime Mapping saved");
            else
                toastr.success("Food Program Service Type Mapping saved");

            Utility.UnLoading();
        }
    }, {
        model: dayPartArray
    }, true);
}

$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var SiteName = "";
    var cpudata = [];
    var dkdata = [];
    var maptypedata = [];
    var billingendtypedata = [];
   
    var retaildata = [];
    var isSubSectorApplicable = false;
   
    $("#OtherEditwindowEdit").kendoWindow({
        modal: true,
        width: "680px",
       // height: "400px",
        title: "Location Spliiter Details - " + SiteName,
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridSite').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SiteCode" || x.field == "SiteAlias" || x.field == "PeopleStrongSiteAlias" || x.field == "SubSiteName"
                   ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    $("#btnExport").click(function (e) {
        var grid = $("#gridSite").data("kendoGrid");
        grid.saveAsExcel();
    });
    var user;
    $(document).ready(function () {
        $(".k-window").hide();
        $(".k-overlay").hide();
       
       //// HttpClient.MakeRequest(HRISMasters.GetLoginUserDetailsUrl, function (result) {
       //     user = result;
       //     //if (user.UserRoleId === 1) {
       //     //    $("#InitiateBulkChanges").css("display", "inline");
       //     //}
       //     //if (user.UserRoleId === 2) {
       //     //    $("#InitiateBulkChanges").css("display", "none");
       //     //}
       //     if (user.SectorNumber == 20) {
       //         $(".manufacturing").css("display", "block");
       //         $("#btnExport").click(function (e) {
       //             var grid = $("#gridSite").data("kendoGrid");
       //             grid.saveAsExcel();
       //         });
       //     }
       //     else {
       //         $(".manufacturing").css("display", "none");
       //     }

       //     populateSiteGrid();
       // }, null, false);
        populateSiteGrid();
    });

    function populateSiteGrid() {
            Utility.Loading();
            var gridVariable = $("#gridSite");
            gridVariable.html("");
            gridVariable.kendoGrid({
                excel: {
                    fileName: "Site.xlsx",
                    filterable: true,
                    allPages: true
                },
                sortable: true,
                filterable: true,
                groupable: false,

                columns: [
                    {
                        field: "SiteCode", title: "Code", width: "40px", attributes: {
                            style: "text-align: left; font-weight:normal"
                        }
                    },
                    {
                        field: "SiteName", title: "Site Name", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    {
                        field: "PeopleStrongSiteAlias", title: "Location", width: "100px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    {
                        field: "SubSiteName", title: "Child Locations", width: "150px", attributes: {

                            style: "text-align: left; font-weight:normal"
                        },
                    },
                    //{field: "IsActive",
                    //title: "Active", width: "40px",
                    //attributes: {
                    //    style: "text-align: center; font-weight:normal"
                    //},
                    //headerAttributes: {
                    //    style: "text-align: center;"
                    //},
                    //template: '# if (IsActive == "1") {#<span>Yes</span>#} else  {#<span>No</span>#}  #',
                    // },
                    {
                        field: "Edit", title: "Action", width: "50px",
                        attributes: {
                            style: "text-align: center; font-weight:normal"
                        },
                        headerAttributes: {
                            style: "text-align: center;"
                        },
                        command: [
                            {
                                name: 'Edit',
                                click: function (e) {

                                    $("#success").css("display", "none");
                                    $("#error").css("display", "none");
                                    var gridObj = $("#gridSite").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    if (!tr.IsActive) {
                                        return;
                                    }
                                    datamodel = tr;
                                    sitename = tr.SiteName + " (" + tr.SiteCode + ")";
                                    var dialog = $("#OtherEditwindowEdit").data("kendoWindow");
                                    dialog.open().element.closest(".k-window").css({
                                        top: 167,

                                    });
                                    //dialog.open();
                                    dialog.center();
                                    $("#siteid").text(tr.SiteID);
                                    $("#sitecode").text(tr.SiteCode);
                                    $("#sitename").text(tr.SiteName);
                                    //$("#sitealias").val(tr.SiteAlias).focus();
                                    SCode = tr.SiteCode;
                                    siteSubSiteMappingData = [];
                                    if (tr.SubSiteName !== "" && tr.SubSiteName !== null) {
                                        var subSites = tr.SubSiteName.split(',');
                                        if (subSites !== null) {
                                            subSites.forEach(function (item, index) {
                                                var siteSubSiteModel = {
                                                    "ID": 0,
                                                    "SiteCode": SCode,
                                                    "SubSiteName": item,
                                                    "IsActive": 1
                                                }
                                                siteSubSiteMappingData.push(siteSubSiteModel);
                                            });
                                        }
                                    }
                                    dialog.title(" " + sitename + "");

                                    populateSiteSubSiteMappingGird();
                                    return true;
                                }
                            }
                        ],
                    }
                ],
                dataSource: {
                    transport: {
                        read: function (options) {

                            var varCodes = "";

                            HttpClient.MakeSyncRequest(HRISMasters.GetSiteDataList, function (result) {
                                Utility.UnLoading();

                                if (result != null) {

                                    result.forEach(function (item) {
                                        if (item.SiteType != null) {
                                            for (x of sitetypedata)
                                                if (x.value == item.SiteType)
                                                    item.SiteNameType = x.text;
                                        }
                                        else {
                                            item.SiteNameType = null;
                                        }

                                        return item;
                                    });

                                    options.success(result);
                                }
                                else {
                                    options.success("");
                                }
                            }, null
                                //{
                                //filter: mdl
                                //}
                                , false);
                        }
                    },
                    schema: {
                        model: {
                            id: "ID",
                            fields: {
                                SiteCode: { type: "string" },
                                SiteAlias: { type: "string" },
                                PeopleStrongSiteAlias: { type: "string" },
                                SubSiteName: { type: "string" },
                                RegionDesc: { type: "string" },
                                CPUName: { type: "string" },
                                MapType: { type: "string" },
                                SiteType: { type: "string" },
                                Retail: { type: "string" },
                                IsActive: { editable: false },
                                SiteName: { type: "string" }
                            }
                        }
                    },
                    //pageSize: 15,
                },
                columnResize: function (e) {
                    var grid = gridVariable.data("kendoGrid");
                    e.preventDefault();
                },
                noRecords: {
                    template: "No Records Available"
                },

                dataBound: function (e) {
                    var items = e.sender.items();
                    // 
                    //items.each(function (e) {
                    //    if (user.UserRoleId == 1) {
                    //        $(this).find('.k-grid-Edit').text("Edit");
                    //        $(this).find('.chkbox').removeAttr('disabled');

                    //    } else {
                    //        $(this).find('.k-grid-Edit').text("View");
                    //        $(this).find('.chkbox').attr('disabled', 'disabled');
                    //    }
                    //});

                    $(".chkbox").on("change", function () {
                        // 
                        var gridObj = $("#gridSite").data("kendoGrid");
                        var tr = gridObj.dataItem($(this).closest("tr"));
                        var trEdit = $(this).closest("tr");
                        var th = this;
                        datamodel = tr;
                        datamodel.IsActive = $(this)[0].checked;
                        var active = "";
                        if (datamodel.IsActive) {
                            active = "Active";
                        } else {
                            active = "Inactive";
                        }

                        Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.SiteName + "</b> Site " + active + "?", "Site Update Confirmation", "Yes", "No", function () {
                            $("#error").css("display", "none");
                            HttpClient.MakeRequest(HRISMasters.ChangeStatus, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again");
                                }
                                else {
                                    toastr.success("Site updated successfully");

                                    if ($(th)[0].checked) {
                                        $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                                    } else {
                                        $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                                    }
                                }
                            }, {
                                model: datamodel
                            }, false);
                        }, function () {
                            $(th)[0].checked = !datamodel.IsActive;
                            if ($(th)[0].checked) {
                                $(trEdit).find(".k-grid-Edit").removeClass("k-state-disabled");
                            } else {
                                $(trEdit).find(".k-grid-Edit").addClass("k-state-disabled");
                            }
                        });
                        return true;
                    });
                },
                change: function (e) {
                },
                excelExport: function onExcelExport(e) {
                    var sheet = e.workbook.sheets[0];
                    var data = e.data;
                    var cols = Object.keys(data[0])
                    var columns = cols.filter(function (col) {
                        if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                        else
                            return col;
                    });
                    var columns1 = columns.map(function (col) {
                        return {
                            value: col,
                            autoWidth: true,
                            background: "#7a7a7a",
                            color: "#fff"
                        };
                    });
                    console.log(columns1);
                    var rows = [{ cells: columns1, type: "header" }];

                    for (var i = 0; i < data.length; i++) {
                        var rowCells = [];
                        for (var j = 0; j < columns.length; j++) {
                            var cellValue = data[i][columns[j]];
                            rowCells.push({ value: cellValue });
                        }
                        rows.push({ cells: rowCells, type: "data" });
                    }
                    sheet.rows = rows;
                }

            })
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function populateSiteSubSiteMappingGird() {

        // Utility.Loading();
        var gridVariable = $("#gridSubSiteMapping");
        gridVariable.html("");
        gridVariable.kendoGrid({
            toolbar: [{ name: "cancel", text: "Reset" }, { name: "create", text: "Add" }],
            groupable: false,
            editable: true,

            columns: [
                {
                    field: "SubSiteName", title: "Name", width: "50px",
                    headerAttributes: {
                        style: "text-align:left;"
                    },
                    //template: '<input type="text" name="SubSiteName"  class="inputsubsitename"  oninput="calculateItemTotalMOG(this,null)"/>',
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    title: "Delete",
                    width: "60px",
                    headerTemplate: '<label style="margin-bottom: 0px;font-weight: 600;">Action</label>',
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    attributes: {
                        style: "text-align: center; font-weight:normal;"
                    },
                    command: [
                        {
                            name: 'Delete',
                            text: "Delete",

                            click: function (e) {
                                var gridObj = $("#gridSubSiteMapping").data("kendoGrid");
                                if ($(e.currentTarget).hasClass("k-state-disabled") || gridObj._data.length == 1) {
                                    return false;
                                }
                                var selectedRow = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                gridObj.dataSource._data.remove(selectedRow);
                                return true;
                            }
                        },
                    ],
                }
            ],
            dataSource: {
                data: siteSubSiteMappingData,
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            SubSiteName: { editable: true },
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function (item, index) {
                    var model = grid.dataItem(this);
                    if (index == 1) {
                        $(this).find(".k-grid-Delete").addClass("k-state-disabled");
                    }
                });
            },
                change: function (e) {
                },
            });
        var toolbar = $("#gridSubSiteMapping").find(".k-grid-toolbar");
        toolbar.find(".k-add").remove();
        toolbar.find(".k-cancel").remove();

        toolbar.addClass("gridToolbarShift");
        toolbar.find(".k-grid-cancel-changes").addClass("gridButton")
        toolbar.find(".k-grid-add").addClass("gridButton")
        $("#gridSubSiteMapping .k-grid-content").addClass("gridInside");
    }

    $("#OthSMbtnCancel").on("click", function () {
        Utility.Page_Alert_Save("All Unsaved Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
            function () {
                $(".k-overlay").hide();
                var orderWindow = $("#OtherEditwindowEdit").data("kendoWindow");
                orderWindow.close();
            },
        );
    });

    function checkSiteExists() {
        var selectedSiteAlias = $("#sitealias").val();
        var siteCode =  $("#sitecode").text();
        var searchNameFound = $("#gridSite").data("kendoGrid").dataSource.data().some(
            function (dataItem) {
                return dataItem.SiteAlias == selectedSiteAlias && dataItem.SiteCode != siteCode;
            });
        return searchNameFound;
    }
    
    $("#OthSMbtnSubmit").click(function () {
        if (1 === 2) {
            toastr.error("Please provide valid input(s)");
            $("#sitealias").focus();
        }
        else {
            var siteSubSiteMapping = [];
            var siteSubSiteGridData = $("#gridSubSiteMapping").data('kendoGrid').dataSource._data;
            if (siteSubSiteGridData !== null) {
                siteSubSiteGridData.forEach(function (item, index) {
                    if (item.SubSiteName !== "") {
                        var siteSubSiteModel = {
                            "ID": 0,
                            "SiteCode": SCode,
                            "SubSiteName": item.SubSiteName,
                            "IsActive": 1
                        }
                        siteSubSiteMapping.push(siteSubSiteModel);
                    }
                });

            }

           
            $("#OthSMbtnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(HRISMasters.SaveSiteSubSiteMapping, function (result) {
                if (result == false) {
                    $('#OthSMbtnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again.");
                    $("#sitealias").focus();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#OtherEditwindowEdit").data("kendoWindow");
                    orderWindow.close();
                    $('#OthSMbtnSubmit').removeAttr("disabled");
                    toastr.success("Site updated successfully");
                    $("#gridSite").data("kendoGrid").dataSource.data([]);
                    $("#gridSite").data("kendoGrid").dataSource.read();
                }
            }, {
                    model: siteSubSiteMapping

            }, false);
         }
    });

    $("#SubSiteGo").click(function () {
        var subSiteHTML = "";
        $("#SubSiteDiv").html(subSiteHTML);
        var numberOfSubSite = $("#NumberOfSubSite").val();
        if (numberOfSubSite != undefined && numberOfSubSite != '') {
            for (var i = 0; i < numberOfSubSite; i++) {
                if (i % 2 == 0) {
                    subSiteHTML += '<div class="col-6">';
                }
                else {
                    subSiteHTML += '<div class="col-5">';
                }
                subSiteHTML += '<label class="form-control-label" >Sub Site Name ' + (i+1) + '</label>';
                subSiteHTML += '<input type="text" class="subSiteName form-control" />';
                subSiteHTML += '</div>';
            };
        }
        $("#SubSiteDiv").append(subSiteHTML);
    });
});