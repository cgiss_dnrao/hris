﻿$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var leadershiprolename = "";
    var sitedata = [];
    var exeCOMUserData = [];
    var saveEXECOMUserData = [];

    $("#windowEditTransferLeaderShipRole").kendoWindow({
        modal: true,
        width: "800px",
        height: "300px",
        title: "Transfer Leadership Role",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {

        $("#transferleadershiprole").click(function () {

            loadDropDownData();

            var tdialog = $("#windowEditTransferLeaderShipRole").data("kendoWindow");
            $(".k-overlay").css("display", "block");
            $(".k-overlay").css("opacity", "0.5");
            tdialog.open();
            tdialog.center();

            populateTransferLeaderShipRoleGrid();
        });

        //$("#btnExport").click(function (e) {
        //    var grid = $("#gridLeaderShipRole").data("kendoGrid");
        //    grid.saveAsExcel();
        //});
       
    });


    function loadDropDownData() {
        HttpClient.MakeSyncRequest(CookBookLayout.GetEXECOMUserDataList, function (result) {
            var data = result;
            var item = { OfficeEmailAddress: "Select" };
            data.splice(0, 0, item);
            exeCOMUserData = data;
        }, null, false);

        HttpClient.MakeSyncRequest(CookBookLayout.GetLoginUserDetailsUrl, function (result) {
            user = result;
        }, null, false);
    }

    function populateTransferLeaderShipRoleGrid() {

        Utility.Loading();
        var gridVariable = $("#gridTransferLeaderShipRole");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "LeaderShipRole.xlsx",
                filterable: true,
                allPages: true
            },
            // sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: false,
            groupable: false,
            editable: true,
            height:250,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "LeaderShipRole", title: "Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "OfficeEmailAddress", title: "From", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "OfficeEmailAddressTo", title: "To", width: "120px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        if (dataItem.OfficeEmailAddressTo == null) {
                            dataItem.OfficeEmailAddressTo = 'Select';
                        }
                        var html = '<span class="brspan">' + kendo.htmlEncode(dataItem.OfficeEmailAddressTo) + '</span>'
                        return html;
                    },
                    editor: function (container, options) {
                        $('<input tabindex="0" id="ddlEXECOMUser" text="' + options.model.OfficeEmailAddressTo + '" class="brTemplate" data-text-field="OfficeEmailAddress" data-value-field="OfficeEmailAddress" data-bind="value:' + options.field + '" />')
                            .appendTo(container)
                            .kendoDropDownList({
                                index: 0,
                                filter: "contains",
                                dataTextField: "OfficeEmailAddress",
                                dataValueField: "OfficeEmailAddress",
                                autoBind: false,
                                dataSource: exeCOMUserData,
                                value: options.field,
                                change: onLeadershipChange,
                               
                            });
                    }
                },
                
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookLayout.GetTransferLeaderShipRoleDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            LeaderShipRole: { editable: false, type: "string" },
                            EmployeeName: { type: "string" },
                            EmployeeCode: { type: "string" },
                            OfficeEmailAddress: { type: "string", editable: false},
                            OfficeEmailAddressTo: { type: "string" },

                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            //dataBound: function (e) {
            //    var grid = e.sender;
            //    var items = e.sender.items();
            //    items.each(function (e) {
            //        var dataItem = grid.dataItem(this);
            //        var ddt = $(this).find('.brTemplate');
            //        $(ddt).kendoDropDownList({
            //            index: 0,
            //            value: dataItem.value,
            //            dataSource: exeCOMUserData,
            //            dataTextField: "OfficeEmailAddress",
            //            dataValueField: "OfficeEmailAddress",
            //        });
            //        ddt.find(".k-input").val(dataItem.OfficeEmailAddress);
            //        $(this).next().focus();
            //    });
            //},
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $("#gridTransferLeaderShipRole").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    function onLeadershipChange(e) {

        //if (checkItemAlreadyExist(e.sender.value(), 'gridTransferLeaderShipRole')) {
        //    toastr.error("Email ID already selected.");
        //    var dropdownlist = $("#gridTransferLeaderShipRole").data("kendoDropDownList");
        //    dropdownlist.select(0);
        //    dropdownlist.trigger("change");
        //    return false;
        //}

        var element = e.sender.element;
        var row = element.closest("tr");
        var grid = $("#gridTransferLeaderShipRole").data("kendoGrid");
        var dataItem = grid.dataItem(row);
        var data = row.find(".k-input")[0].innerText;
        row.find(".brspan").val(data);

        var Obj = exeCOMUserData.filter(function (exeObj) {
            return exeObj.OfficeEmailAddress == e.sender.value();
        });

        dataItem.set("OfficeEmailAddressTo", e.sender.value());
        dataItem.set("OfficeEmailAddressTo", data);
        dataItem.set("EmployeeCodeTo", Obj[0].EmployeeCode);
        var officeemailid = Obj[0].OfficeEmailAddress;
        dataItem.OfficeEmailAddressTo = officeemailid;
        dataItem.EmployeeCodeTo = Obj[0].EmployeeCode;


    };

    function checkItemAlreadyExist(selectedId, gridID) {
        var dataExists = false;
        var gridObj = $("#" + gridID).data("kendoGrid");
        var data = gridObj.dataSource.data();
        var existingData = [];
        existingData = data.filter(m => m.OfficeEmailAddress == selectedId);
        if (existingData.length == 2) {
            dataExists = true;
        }
        return dataExists;
    }

    $("#btnCancelTransferLeadershipRole").on("click", function () {
        //Utility.Page_Alert_Save("All Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
        //    function () {
               
        //    },
        //    function () {
        //    }
        //);

        $(".k-overlay").hide();
        var orderWindow = $("#windowEditTransferLeaderShipRole").data("kendoWindow");
        orderWindow.close();
    });

    function checkIsLeaderShipRoleExists() {
        var isExists = false;
        var LeaderShipRoleList = $("#gridLeaderShipRole").data("kendoGrid").dataSource._data;
        if (LeaderShipRoleList != null && LeaderShipRoleList.length > 0) {
            var filterData = LeaderShipRoleList.filter(m => m.LeaderShipRole === $("#leadershiprolename").val());
            if (filterData != null && filterData.length > 0) {
                isExists = true;
            }
        }
        return isExists;
    };

    $("#btnSubmitTransferLeadershipRole").click(function () {
        var isValid = true;
        if (!isValid) {
            toastr.error("Leadership Role already exists.");
            return;
        }
        else {
            var dataFiltered = $("#gridTransferLeaderShipRole").data("kendoGrid").dataSource.dataFiltered();
            var model = dataFiltered;
            if (model != null && model.length > 0) {
                $.each(exeCOMUserData, function (index, item) {
                    var selectedUser = model.filter(m => m.EmployeeCodeTo == item.EmployeeCode);
                    if (selectedUser != null && selectedUser.length > 0 && item.LeadershipRoleCode != undefined && item.LeadershipRoleCode != null && selectedUser[0].EmployeeCode != null) {
                        item.LeadershipRoleCode = selectedUser[0].LeaderShipRoleCode;
                        saveEXECOMUserData.push(item);
                    }
                });
            }

            $.each(saveEXECOMUserData, function () {
                this.CreatedOn = kendo.parseDate(this.CreatedOn);
                this.ModifiedBy = user.UserId;
                this.ModifiedOn = Utility.CurrentDate();
                this.DateOfBirth = kendo.toString(this.DateOfBirth, "MM/dd/yyyy hh:mm:s tt");
            });
            console.log(saveEXECOMUserData);

            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeSyncRequest(CookBookLayout.SaveEmployeeDataList, function (result) {
                if (result == false) {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again");
                    Utility.UnLoading();
                }
                else {
                    $(".k-overlay").hide();
                    var orderWindow = $("#windowEditTransferLeaderShipRole").data("kendoWindow");
                    orderWindow.close();
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.success("Leadership Role changed successfully");
                    Utility.UnLoading();
                }
            }, {
                model: saveEXECOMUserData

            }, true);
        }
    });
});