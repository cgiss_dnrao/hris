﻿
$(function () {
    var user;
    var employeeName = "";
    var datamodel;
    var costcenterdata = [];
    var profitcenterdata = [];
    var employeedata = [];
    var dropdownlist = ["DomesticTripApprover1", "DomesticTripApprover3", "DomesticHighCost1","DomesticHighCost2",
        "InternationalTripApprover2", "InternationalTripApprover3", "InternationalTripApprover4", "Approver3", "Approver4", "Approver5", "Approver6"];

    var dropdownlistbulk = ["DomesticTripApprover1Bulk", "DomesticTripApprover3Bulk", "DomesticHighCost1Bulk", "DomesticHighCost2Bulk",
        "InternationalTripApprover2Bulk", "InternationalTripApprover3Bulk", "InternationalTripApprover4Bulk", "Approver3Bulk", "Approver4Bulk", "Approver5Bulk", "Approver6Bulk"];

    $("#windowEditITILite").kendoWindow({
        modal: true,
        width: "1200px",
        height: "486px",
        title: "Employee Details",
        actions: ["Close"],
        animation: false,
        visible: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    var user;
    $(document).ready(function () {
        Utility.Loading();

        setTimeout(function () {
            HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
                user = result;
                if (user.UserRoleId === 1) {
                    $("#InitiateBulkChanges").css("display", "inline");
                    $("#btnExport").css("display", "inline");
                }
                if (user.UserRoleId === 2) {
                    $("#InitiateBulkChanges").css("display", "none");
                    $("#btnExport").css("display", "none");
                }
            }, null, false);

            $('#myInput').on('input', function (e) {
                var grid = $('#gridITILite').data('kendoGrid');
                var columns = grid.columns;

                var filter = { logic: 'or', filters: [] };
                columns.forEach(function (x) {
                    if (x.field) {
                        if (x.field == "EmployeeCode" || x.field == "LastName" || x.field == "FirstName" || x.field == "Grade"  || x.field == "Department"
                            || x.field == "DomesticTripApprover1" || x.field == "DomesticTripApprover2" || x.field == "DomesticTripApprover3" || x.field == "InternationalTripApprover1"
                            || x.field == "InternationalTripApprover2" || x.field == "InternationalTripApprover3" || x.field == "InternationalTripApprover4" || x.field == "DomesticHighCost1"
                            || x.field == "DomesticHighCost2" || x.field == "Approver1" || x.field == "Approver2" || x.field == "Approver3" || x.field == "Approver4" || x.field == "Approver5"
                            || x.field == "Approver6" || x.field == "DefaultCurrency" || x.field == "Nationality" || x.field == "Derived" || x.field == "DomesticTripApprover4"
                            || x.field == "DomesticTripApproverEmail1" || x.field == "DomesticTripApproverEmail2" || x.field == "DomesticTripApproverEmail3" || x.field == "DomesticTripApproverEmail4"
                            || x.field == "CompanyCode" || x.field == "FYI" 
                        ) {
                            var type = grid.dataSource.options.schema.model.fields[x.field].type;
                            if (type == 'string') {
                                var targetValue = e.target.value;


                                filter.filters.push({
                                    field: x.field,
                                    operator: 'contains',
                                    value: targetValue
                                })
                            }
                            else if (type == 'number') {
                                if (isNumeric(e.target.value)) {
                                    filter.filters.push({
                                        field: x.field,
                                        operator: 'eq',
                                        value: e.target.value
                                    });
                                }
                            } else if (type == 'date') {
                                var data = grid.dataSource.data();
                                for (var i = 0; i < data.length; i++) {
                                    var dateStr = kendo.format(x.format, data[i][x.field]);
                                    if (dateStr.startsWith(e.target.value)) {
                                        filter.filters.push({
                                            field: x.field,
                                            operator: 'eq',
                                            value: data[i][x.field]
                                        })
                                    }
                                }
                            } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                                var bool = getBoolean(e.target.value);
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: bool
                                });
                            }
                        }
                    }
                });
                grid.dataSource.filter(filter);
            });

            $('#myInput').focus(function () {
                $('.dashboarddetailsearch .fa-search').css('opacity', 0);
                $(this).css('padding-left', '10px');
                $(this).attr('placeholder', 'Start typing to filter');
            }).blur(function () {
                $('.dashboarddetailsearch .fa-search').css('opacity', 1);
                $(this).css('padding-left', '25px');
                $(this).attr('placeholder', 'Search');
            })

            $("#btnExport").click(function (e) {
                HttpClient.MakeRequest(CookBookMasters.ExportITILiteData, function (result) {
                    if (result != null) {
                        var bytes = new Uint8Array(result.FileContents);
                        var blob = new Blob([bytes], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = "EmployeeData.xlsx";
                        link.click();
                    }
                    else {
                    }
                }, null
                    //{
                    //filter: mdl
                    //}
                    , true);
            });

            populateEmployeeGrid();

            $("#gridBulkChange").css("display", "none");
            populateBulkChangeControls();
            var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
            changeControls.css("background-color", "#fff");
            changeControls.css("border", "none");
            changeControls.eq(3).text("");
            changeControls.eq(3).append("<div id='DomesticTripApprover1Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(5).text("");
            changeControls.eq(5).append("<div id='DomesticTripApprover3Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(7).text("");
            changeControls.eq(7).append("<div id='InternationalTripApprover2Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(8).text("");
            changeControls.eq(8).append("<div id='InternationalTripApprover3Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(9).text("");
            changeControls.eq(9).append("<div id='InternationalTripApprover4Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(10).text("");
            changeControls.eq(10).append("<div id='DomesticHighCost1Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(11).text("");
            changeControls.eq(11).append("<div id='DomesticHighCost2Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(14).text("");
            changeControls.eq(14).append("<div id='Approver3Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(15).text("");
            changeControls.eq(15).append("<div id='Approver4Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(16).text("");
            changeControls.eq(16).append("<div id='Approver5Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(17).text("");
            changeControls.eq(17).append("<div id='Approver6Bulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(18).text("");
            changeControls.eq(18).append("<input type='text' placeholder='Default Currency' id='defaultCurrencyBulk' class='form-control' />");
            changeControls.eq(19).text("");
            changeControls.eq(19).append("<input type='text' placeholder='Nationality' id='nationalitybulk' class='form-control' />");

            populateDropDownDataBulk();
          

            $("#btnCancel").on("click", function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEditITILite").data("kendoWindow");
                orderWindow.close();
            });

            $("#btnClose").on("click", function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEditITILite").data("kendoWindow");
                orderWindow.close();
            });

            $("#btnSubmit").click(function () {
             
                var Nationality = $("#nationality").val();
                var DefaultCurrency = $("#defaultCurrency").val();
                if (!validateDropDownValueCommon() || Nationality == "" || DefaultCurrency == "") {
                    toastr.error("Please fill all mandantory fields first.");
                    return false;
                }

                var DomesticTripApprover1 = $("#DomesticTripApprover1").data('kendoDropDownList').value();
                var DomesticTripApprover3 = $("#DomesticTripApprover3").data('kendoDropDownList').value();
                var InternationalTripApprover2 = $("#InternationalTripApprover2").data('kendoDropDownList').value();
                var InternationalTripApprover3 = $("#InternationalTripApprover3").data('kendoDropDownList').value();
                var InternationalTripApprover4 = $("#InternationalTripApprover4").data('kendoDropDownList').value();
                var Approver3 = $("#Approver3").data('kendoDropDownList').value();
                var Approver4 = $("#Approver4").data('kendoDropDownList').value();
                var Approver5 = $("#Approver5").data('kendoDropDownList').value();
                var Approver6 = $("#Approver6").data('kendoDropDownList').value();
                var DomesticHighCost1 = $("#DomesticHighCost1").data('kendoDropDownList').value();
                var DomesticHighCost2 = $("#DomesticHighCost2").data('kendoDropDownList').value();
                
               
                var model;
                if (datamodel != null) {
                    model = datamodel;
                    model.DomesticTripApprover1 = DomesticTripApprover1 == "Select" ? "" : DomesticTripApprover1;
                    model.DomesticTripApprover3 = DomesticTripApprover3 == "Select" ? "" : DomesticTripApprover3;
                    model.InternationalTripApprover2 = InternationalTripApprover2 == "Select" ? "" : InternationalTripApprover2;
                    model.InternationalTripApprover3 = InternationalTripApprover2 == "Select" ? "" : InternationalTripApprover3;
                    model.InternationalTripApprover4 = InternationalTripApprover2 == "Select" ? "" : InternationalTripApprover4;
                    model.Approver3 = Approver3 == "Select" ? "" : Approver3;
                    model.Approver4 = Approver4 == "Select" ? "" : Approver4;
                    model.Approver5 = Approver5 == "Select" ? "" : Approver5;
                    model.Approver6 = Approver6 == "Select" ? "" : Approver6;
                    model.Nationality = $("#nationality").val();
                    model.DefaultCurrency = $("#defaultCurrency").val();
                    model.DomesticHighCost1 = DomesticHighCost1 == "Select" ? "" : DomesticHighCost1;
                    model.DomesticHighCost2 = DomesticHighCost2 == "Select" ? "" : DomesticHighCost2;
                    model.ModifiedBy = user.UserId;
                    model.ModifiedOn = Utility.CurrentDate();
                    model.CreatedOn = kendo.parseDate(model.CreatedOn);
                    model.Action = "Update";
                    $("#btnSubmit").attr('disabled', 'disabled');
                    Utility.Loading();
                    HttpClient.MakeRequest(CookBookMasters.SaveEmployeeData, function (result) {
                        if (result == false) {
                            $('#btnSubmit').removeAttr("disabled");
                            toastr.error("Some error occured, please try again");
                            $("#DomesticTripApprover1").focus();
                            Utility.UnLoading();
                        }
                        else {
                            $('#btnSubmit').removeAttr("disabled");
                            toastr.success("Employee updated successfully");
                            Utility.Loading();
                            $("#gridITILite").data("kendoGrid").dataSource.data([]);
                            $("#gridITILite").data("kendoGrid").dataSource.read();
                            setTimeout(function () {
                                $(".k-overlay").hide();
                                var orderWindow = $("#windowEditITILite").data("kendoWindow");
                                orderWindow.close();
                                Utility.UnLoading();
                            }, 1000)

                        }
                    }, {
                        model: model

                    }, false);
                }
            });

            $("#InitiateBulkChanges").on("click", function () {
                $("#gridBulkChange").css("display", "block");
                $("#gridBulkChange").children(".k-grid-header").css("border", "none");
                $("#InitiateBulkChanges").css("display", "none");
                $("#CancelBulkChanges").css("display", "inline");
                $("#SaveBulkChanges").css("display", "inline");
                resetDropDownValueCommonBulk();
            });


            $("#CancelBulkChanges").on("click", function () {
                Utility.Loading();
                setTimeout(function () {
                    $("#InitiateBulkChanges").css("display", "inline");
                    $("#CancelBulkChanges").css("display", "none");
                    $("#SaveBulkChanges").css("display", "none");
                    $("#gridBulkChange").css("display", "none");
                    populateEmployeeGrid();
                    Utility.UnLoading();
                },2000);
               
            });

            $("#SaveBulkChanges").on("click", function () {

                var dataFiltered = $("#gridITILite").data("kendoGrid").dataSource.dataFiltered();

                var model = dataFiltered;
                var validChange = true;
                var Nationality = $("#nationalitybulk").val();
                var DefaultCurrency = $("#defaultCurrencybulk").val();
                var DomesticTripApprover1 = $("#DomesticTripApprover1Bulk").data('kendoDropDownList').value();
                var DomesticTripApprover3 = $("#DomesticTripApprover3Bulk").data('kendoDropDownList').value();
                var InternationalTripApprover2 = $("#InternationalTripApprover2Bulk").data('kendoDropDownList').value();
                var InternationalTripApprover3 = $("#InternationalTripApprover3Bulk").data('kendoDropDownList').value();
                var InternationalTripApprover4 = $("#InternationalTripApprover4Bulk").data('kendoDropDownList').value();
                var Approver3 = $("#Approver3Bulk").data('kendoDropDownList').value();
                var Approver4 = $("#Approver4Bulk").data('kendoDropDownList').value();
                var Approver5 = $("#Approver5Bulk").data('kendoDropDownList').value();
                var Approver6 = $("#Approver6Bulk").data('kendoDropDownList').value();
                var DomesticHighCost1 = $("#DomesticHighCost1Bulk").data('kendoDropDownList').value();
                var DomesticHighCost2 = $("#DomesticHighCost2Bulk").data('kendoDropDownList').value();


                if (Nationality == "" && DefaultCurrency == "" && DomesticTripApprover1 == "" && DomesticTripApprover3 == ""
                    && InternationalTripApprover2 == "" && InternationalTripApprover3 == "" && InternationalTripApprover3 == ""
                    && Approver3 == "" && Approver4 == "" && Approver5 == "" && Approver6 == "" && DomesticHighCost1 == "" && DomesticHighCost2 == ""
                    && DomesticTripApprover1 == "Select" && DomesticTripApprover3 == "Select"
                    && InternationalTripApprover2 == "Select" && InternationalTripApprover3 == "Select" && InternationalTripApprover3 == "Select"
                    && Approver3 == "Select" && Approver4 == "Select" && Approver5 == "Select" && Approver6 == "Select" && DomesticHighCost1 == "Select" && DomesticHighCost2 == "Select"
                ) {
                    validChange = false;
                }

                if (validChange == false) {
                    toastr.error("Please change at least one attribute for Bulk Update");
                } else {
                    Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                        function () {
                            $.each(model, function () {
                                this.CreatedOn = kendo.parseDate(this.CreatedOn);
                                this.ModifiedOn = Utility.CurrentDate();
                                this.ModifiedBy = user.UserId;
                                this.DomesticTripApprover1 = DomesticTripApprover1 == "Select" ? this.DomesticTripApprover1 : DomesticTripApprover1;
                                this.DomesticTripApprover3 = DomesticTripApprover3 == "Select" ? this.DomesticTripApprover3 : DomesticTripApprover3;
                                this.InternationalTripApprover2 = InternationalTripApprover2 == "Select" ? this.InternationalTripApprover2 : InternationalTripApprover2;
                                this.InternationalTripApprover3 = InternationalTripApprover2 == "Select" ? this.InternationalTripApprover3 : InternationalTripApprover3;
                                this.InternationalTripApprover4 = InternationalTripApprover2 == "Select" ? this.InternationalTripApprover4 : InternationalTripApprover4;
                                this.Approver3 = Approver3 == "Select" ? this.Approver3 : Approver3;
                                this.Approver4 = Approver4 == "Select" ? this.Approver4 : Approver4;
                                this.Approver5 = Approver5 == "Select" ? this.Approver5 : Approver5;
                                this.Approver6 = Approver6 == "Select" ? this.Approver6 : Approver6;
                                this.Nationality = $("#nationalitybulk").val() == "" ? this.Nationality : $("#nationalitybulk").val();
                                this.DefaultCurrency = $("#defaultCurrencyBulk").val() == "" ? this.DefaultCurrency : $("#defaultCurrencyBulk").val() ;
                                this.DomesticHighCost1 = DomesticHighCost1 == "Select" ? this.DomesticHighCost1 : DomesticHighCost1;
                                this.DomesticHighCost2 = DomesticHighCost2 == "Select" ? this.DomesticHighCost2 : DomesticHighCost2;
                            });

                            HttpClient.MakeRequest(CookBookMasters.SaveEmployeeDataList, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again.");
                                }
                                else {
                                    $(".k-overlay").hide();
                                    toastr.success("Records have been updated successfully");
                                    $("#gridITILite").data("kendoGrid").dataSource.data([]);
                                    $("#gridITILite").data("kendoGrid").dataSource.read();
                                    $("#InitiateBulkChanges").css("display", "inline");
                                    $("#CancelBulkChanges").css("display", "none");
                                    $("#SaveBulkChanges").css("display", "none");
                                    $("#gridBulkChange").css("display", "none");
                                }
                                Utility.UnLoading();
                            }, {
                                model: model

                            }, true);
                        },
                        function () {
                            maptype.focus();
                            Utility.UnLoading();
                        }
                    );
                }
            });

            $('#gridITILite .k-grid-content').scroll(function () {
                //$("#gridBulkChange .k-grid-content, #gridITILite .k-grid-content").scrollTop($(this).scrollTop());

                var kendoGrid = $("#gridBulkChange").data("kendoGrid");
                kendoGrid.element.find(".k-grid-content").animate({
                    scrollLeft: $(this).scrollLeft()
                });
            });

            Utility.UnLoading();
        }, 2000);
    });

    function populateDropDownData() {
       
        dropdownlist.forEach(function (item,index) {
            populateDropDownDataCommon(item);
        });

        resetDropDownValueCommon();
    };

    function populateDropDownDataBulk() {

        dropdownlistbulk.forEach(function (item, index) {
            populateDropDownDataCommon(item);
        });

        resetDropDownValueCommonBulk();
    };

    function populateDropDownDataCommon(dropdownid) {
        $("#" + dropdownid + "").kendoDropDownList({
            filter: "contains",
            dataTextField: "OfficeEmailAddress",
            dataValueField: "OfficeEmailAddress",
            dataSource: employeedata,
            index: 0,
        });
    };

    function setAllDropDownValue(tr) {
        setDropDownValueCommon("DomesticTripApprover1", tr.DomesticTripApprover1);
        //setDropDownValueCommon("DomesticTripApprover2", tr.DomesticTripApprover2);
        setDropDownValueCommon("DomesticTripApprover3", tr.DomesticTripApprover3);
        //setDropDownValueCommon("InternationalTripApprover1", tr.InternationalTripApprover1);
        setDropDownValueCommon("InternationalTripApprover2", tr.InternationalTripApprover2);
        setDropDownValueCommon("InternationalTripApprover3", tr.InternationalTripApprover3);
        setDropDownValueCommon("InternationalTripApprover4", tr.InternationalTripApprover4);
        setDropDownValueCommon("DomesticHighCost1", tr.DomesticHighCost1);
        setDropDownValueCommon("DomesticHighCost2", tr.DomesticHighCost2);
        //setDropDownValueCommon("Approver1", tr.Approver1);
        //setDropDownValueCommon("Approver2", tr.Approver2);
        setDropDownValueCommon("Approver3", tr.Approver3);
        setDropDownValueCommon("Approver4", tr.Approver4);
        setDropDownValueCommon("Approver5", tr.Approver5);
        setDropDownValueCommon("Approver6", tr.Approver6);
    };


    function setDropDownValueCommon(dropdownid, dropdownvalue) {
        if (dropdownvalue != null) {
            //  $("#" + dropdownid + "").data('kendoDropDownList').value(dropdownvalue);
            $("#" + dropdownid + "").text(dropdownvalue);
        }
    };

    function resetDropDownValueCommon() {
        dropdownlist.forEach(function (item, index) {
            console.log(item);

              $("#" + item + "").data('kendoDropDownList').value("Select");
        });
        
    };

    function resetDropDownValueCommonBulk() {
        dropdownlistbulk.forEach(function (item, index) {
            console.log(item);

            $("#" + item + "").data('kendoDropDownList').value("Select");
        });

    };

    function disableControl() {
        dropdownlist.forEach(function (item, index) {
            var dropdownlist = $("#" + item + "").data("kendoDropDownList");
            dropdownlist.enable(false);
        });
        $("#defaultCurrency").attr('disabled', 'disabled');
        $("#nationality").attr('disabled', 'disabled');
        $("#btnSubmit").css("display", "none");
        $("#btnCancel").css("display", "none");
        $("#btnClose").css("display", "block");
    };

    function enableControl() {
        dropdownlist.forEach(function (item, index) {
            var dropdownlist = $("#" + item + "").data("kendoDropDownList");
            dropdownlist.enable(true);
        });
        $("#defaultCurrency").removeAttr('disabled', 'disabled');
        $("#nationality").removeAttr('disabled', 'disabled');
        $("#btnSubmit").css("display", "block");
        $("#btnCancel").css("display", "block");
        $("#btnClose").css("display", "none");
    };

    function validateDropDownValueCommon() {
        var isValid = true;
        dropdownlist.forEach(function (item, index) {
            var dropdownValue = $("#" + item + "").data('kendoDropDownList').value();
            if (dropdownValue == undefined || dropdownValue == null || dropdownValue == "") {
                isValid = false;
            }

        });
        return isValid;
    };



    function populateEmployeeGrid() {

       // Utility.Loading();
        var gridVariable = $("#gridITILite");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "Employee.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: true,
            pageable: true,
            groupable: false,
            height: 616,
            scrollable: true,
            columns: [
                //{
                //    field: "EmployeeCode", title: "Employee Code", width: "110px", attributes: {
                //        style: "text-align: left; font-weight:normal"
                //    }
                //},
                {
                    field: "FirstName", title: "First Name", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "LastName", title: "Last Name", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "OfficeEmailAddress", title: "Email Id", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "MobileNumber", title: "Contact Number", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Grade", title: "Employee Level", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "DateOfBirth", title: "Date of Birth", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                
                {
                    field: "Gender", title: "Gender", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Entity", title: "Entity", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Derived", title: "Sector/Business unit", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Department", title: "Department", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApprover1", title: "Domestic Trip Approver 1", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "DomesticTripApprover2", title: "Domestic Trip Approver 2", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApprover3", title: "Domestic Trip Approver 3", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApprover4", title: "Domestic Trip Approver 4", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApproverEmail1", title: "Last Minute (7 days)  Domestic Trip Approver Email 1", width: "300px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "DomesticTripApproverEmail2", title: "Last Minute (7 days)  Domestic Trip Approver Email 2", width: "300px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApproverEmail3", title: "Last Minute (7 days)  Domestic Trip Approver Email 3", width: "300px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApproverEmail4", title: "Last Minute (7 days)  Domestic Trip Approver Email 4", width: "300px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover1", title: "International Trip Approver1", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover2", title: "International Trip Approver2", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover3", title: "International Trip Approver3", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover4", title: "International Trip Approver4", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticHighCost1", title: "Domestic High Cost1", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticHighCost2", title: "Domestic High Cost2", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "FYI", title: "FYI", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver1", title: "Approver 1", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver2", title: "Approver 2", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver3", title: "Approver 3", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver4", title: "Approver 4", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver5", title: "Approver 5", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver6", title: "Approver 6", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DefaultCurrency", title: "Default Currency", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "EmployeeCode", title: "Employee Code", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Designation", title: "Designation", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Location", title: "Location", width: "350px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Nationality", title: "Nationality", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "Passport Number", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CompanyCode", title: "Company Code", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "CostCenter", title: "Cost Center", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "ProfitCenter", title: "Profit Center", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "Personnel No", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "Base Location", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Action", title: "ITILite Action", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "Edit", title: "Action", width: "60px",
                    class: "editcolumn",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        {
                            name: 'View',
                            click: function (e) {
                                Utility.Loading();
                                setTimeout(function () {
                                   // populateDropDownData();

                                    var gridObj = $("#gridITILite").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;

                                    var dialog = $("#windowEditITILite").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();

                                    $("#employeeid").val(tr.ID);
                                    $("#employeecode").text(tr.EmployeeCode);
                                    $("#employeefirstname").text(tr.FirstName);
                                    $("#employeelastname").text(tr.LastName);
                                    $("#emailid").text(tr.OfficeEmailAddress);
                                    $("#contactnumber").text(tr.MobileNumber);
                                    $("#employeelevel").text(tr.Grade);
                                    $("#department").text(tr.Department);
                                    $("#gender").text(tr.Gender);
                                    $("#entity").text(tr.Entity);
                                    $("#derived").text(tr.Derived);
                                    $("#location").text(tr.Location);
                                    $("#designation").text(tr.Designation);
                                    $("#compute").text(tr.Action);
                                    $("#DomesticTripApprover2").text(tr.DomesticTripApprover2);
                                    $("#InternationalTripApprover1").text(tr.InternationalTripApprover1);
                                    $("#Approver1").text(tr.Approver1);
                                    $("#Approver2").text(tr.Approver2);
                                    $("#nationality").text(tr.Nationality);
                                    $("#defaultCurrency").text(tr.DefaultCurrency);
                                    //$("#DomesticHighCost1").val(tr.DomesticHighCost1);
                                    //$("#DomesticHighCost2").val(tr.DomesticHighCost2);

                                    //var dob = kendo.toString(tr.DateOfBirth, "dd-MMM-yyyy");
                                    $("#dob").text(tr.DateOfBirthAsString);
                                    $("#department").text(tr.Department);
                                    setAllDropDownValue(tr);
                                    //disableControl();
                                    //if (!tr.IsActive) {
                                    //    disableControl();
                                    //}
                                    //else {
                                    //    enableControl();
                                    //}
                                    dialog.title("ITILite Details");
                                    Utility.UnLoading();
                                    return true;
                                }, 1000);
                            }
                        }
                    ],
                }

            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        HttpClient.MakeSyncRequest(CookBookMasters.GetEmployeeDataList, function (result) {
                            if (result != null) {
                                var empData = result;
                                employeedata = JSON.parse(JSON.stringify(empData));

                                employeedata = employeedata.filter(m => m.OfficeEmailAddress != "" && m.OfficeEmailAddress != null);
                                var item = { OfficeEmailAddress: "Select", OfficeEmailAddress:"Select" };
                                employeedata.splice(0, 0, item);


                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { type: "string", editable: false },
                            EmployeeCode: { type: "string" },
                            FirstName: { type: "string" },
                            LastName: { type: "string" },
                            Grade: { type: "string" },
                            Department: { type: "string" },
                            DomesticTripApprover1: { type: "string" },
                            DomesticTripApprover2: { type: "string" },
                            DomesticTripApprover3: { type: "string" },
                            DomesticTripApprover4: { type: "string" },
                            DomesticTripApproverEmail1: { type: "string" },
                            DomesticTripApproverEmail2: { type: "string" },
                            DomesticTripApproverEmail3: { type: "string" },
                            DomesticTripApproverEmail4: { type: "string" },
                            InternationalTripApprover1: { type: "string" },
                            InternationalTripApprover2: { type: "string" },
                            InternationalTripApprover3: { type: "string" },
                            InternationalTripApprover4: { type: "string" },
                            DomesticHighCost1: { type: "string" },
                            DomesticHighCost2: { type: "string" },
                            Approver1: { type: "string" },
                            Approver2: { type: "string" },
                            Approver3: { type: "string" },
                            Approver4: { type: "string" },
                            Approver5: { type: "string" },
                            Approver6: { type: "string" },
                            DefaultCurrency: { type: "string" },
                            CompanyCode: { type: "string" },
                            Nationality: { type: "string" },
                            Derived: { type: "string" }, 
                            DomesticTripApprover4: { type: "string" }, 
                            DomesticTripApproverEmail1: { type: "string" }, 
                            DomesticTripApproverEmail2: { type: "string" }, 
                            DomesticTripApproverEmail3: { type: "string" }, 
                            DomesticTripApproverEmail4: { type: "string" }, 
                            CompanyCode: { type: "string" }, 
                            FYI: { type: "string" }, 
                            IsActive: { editable: false },

                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var grid = this;
                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    if (model.IsActive) {
                        $(this).find('.k-grid-Edit').text("Edit");
                    }
                    else {
                        $(this).find('.k-grid-Edit').text("View");
                    }
                });
            },

            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });
               
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
               // var columns = e.workbook.sheets[0].columns;
                //columns1.forEach(function (column) {
                //    delete column.width;
                //    column.autoWidth = true;
                //});
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue;
                        if (data[i][columns[j]] != null && columns[j] == "DateOfBirth" || columns[j] == "DateofJoining" || columns[j] == "CreatedOn"
                            || columns[j] == "ModifiedOn") {
                            var dateValue = kendo.parseDate(data[i][columns[j]]); 
                            cellValue = kendo.toString(dateValue, "dd/MM/yyyy");
                        }
                        else {
                            cellValue = data[i][columns[j]];
                        }
                        rowCells.autoWidth = true;
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
                for (var i = 0; i < sheet.columns.length; i++) {
                    delete sheet.columns[i].width;
                    sheet.columns[i].autoWidth = true;
                }
            }

        })

    }

    function populateBulkChangeControls() {

        Utility.Loading();
        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            selectable: "cell",
            sortable: false,
            scrollable: true,
            filterable: false,
            columns: [
                {
                    field: "EmployeeCode", title: "", width: "110px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "FirstName", title: "", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "LastName", title: "", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApprover1", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "DomesticTripApprover2", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticTripApprover3", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover1", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover2", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover3", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "InternationalTripApprover4", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticHighCost1", title: "", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DomesticHighCost2", title: "", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver1", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver2", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver3", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver4", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver5", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Approver6", title: "", width: "250px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "DefaultCurrency", title: "", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Nationality", title: "", width: "100px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Derived", title: "", width: "150px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "Action", title: "ITILite Action", width: "120px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },

                {
                    field: "Edit", title: "Action", width: "60px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                   
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

});