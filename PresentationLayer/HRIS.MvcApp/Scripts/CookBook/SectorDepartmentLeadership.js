﻿$(function () {
    var user;
    var status = "";
    var varname = "";
    var datamodel;
    var leadershiprolename = "";
    var sdlmappingdata = [];
    var exeCOMUserData = [];
    var saveEXECOMUserData = [];
    var leadershiproledata = [];
    var sectordata = [];
    var departmentdata = [];

    $("#windowEditSDLMapping").kendoWindow({
        modal: true,
        width: "450px",
        height: "220px",
        title: "Sector Department Leadership Role Mapping",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridSDLMapping').data('kendoGrid');
        var columns = grid.columns;
        //console.log(employeedata);
        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "SectorName" || x.field == "DepartmentName" || x.field == "LeaderShipRoleName" || x.field == "SectorCode" || x.field == "DepartmentCode" || x.field == "LeadershipRoleCode" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                       
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        });
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });
    //$('#myInput').focus(function () {
    //    $('.dashboarddetailsearch .fa-search').css('opacity', 0);
    //    $(this).css('padding-left', '10px');
    //    $(this).attr('placeholder', 'Start typing to filter');
    //}).blur(function () {
    //    $('.dashboarddetailsearch .fa-search').css('opacity', 1);
    //    $(this).css('padding-left', '25px');
    //    $(this).attr('placeholder', 'Search');
    //});
    //var user;
    $(document).ready(function () {

        $("#btnAdd").click(function () {
            datamodel = null;
            $("#hdnSDLMId").val("");   
            populateDropDownData();

            var tdialog = $("#windowEditSDLMapping").data("kendoWindow");
            $(".k-overlay").css("display", "block");
            $(".k-overlay").css("opacity", "0.5");
            tdialog.open();
            tdialog.center();

            
        });

        populateSectorDepartmentLeadershipGrid();
        $("#btnExport").click(function (e) {
            var grid = $("#gridSDLMapping").data("kendoGrid");
            grid.saveAsExcel();
        });

    });

    function populateDropDownData() {
        HttpClient.MakeSyncRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetSectorMasterDataList, function (data) {
            var item = { Name: "Select", Code: "Select" };
            data.splice(0, 0, item);
            sectordata = data;
            $("#inputSector").kendoDropDownList({
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "Code",
                dataSource: data,
                index: 0,
            });
            
            $("#inputSector").data('kendoDropDownList').value("Select")

        }, null, false);  

        HttpClient.MakeSyncRequest(CookBookMasters.GetDepartmentMasterDataList, function (data) {
            var item = { Name: "Select", Code:"Select" };
            data.splice(0, 0, item);
            departmentdata = data;
            $("#inputDepartment").kendoDropDownList({
                filter: "contains",
                dataTextField: "Name",
                dataValueField: "Code",
                dataSource: data,
                index: 0,
            });
           
            $("#inputDepartment").data('kendoDropDownList').value("Select")

        }, null, false);  

        HttpClient.MakeSyncRequest(CookBookMasters.GetLeadershipRoleMasterDataList, function (data) {
            var item = { LeaderShipRole: "Select", LeadershipRoleCode: "Select" };
            data.splice(0, 0, item);
            leadershiproledata = data;
            $("#inputLeadership").kendoDropDownList({
                filter: "contains",
                dataTextField: "LeaderShipRole",
                dataValueField: "LeadershipRoleCode",
                dataSource: data,
                index: 0,
            });
            
            $("#inputLeadership").data('kendoDropDownList').value("Select")
            Utility.UnLoading();
        }, null, false);
    };
        
    function populateSectorDepartmentLeadershipGrid() {

        Utility.Loading();
        var gridVariable = $("#gridSDLMapping");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "SectorDepartmentLeadership.xlsx",
                filterable: true,
                allPages: true
            },
             sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: true,
            //pageSize:50,
            groupable: false,
            editable: true,
            height: 525,
            //reorderable: true,
            //scrollable: true,
            columns: [
                {
                    field: "SectorName", title: "Sector Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "DepartmentName", title: "Department Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "LeaderShipRoleName", title: "LeaderShipRole Name", width: "40px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Edit", title: "Action", width: "30px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                Utility.Loading();
                                setTimeout(function () {
                                    if ($("#inputSector").data('kendoDropDownList') == undefined)
                                        populateDropDownData();

                                    var inputSector = $("#inputSector").data('kendoDropDownList');
                                    var inputDepartment = $("#inputDepartment").data('kendoDropDownList');
                                    var inputLeadership = $("#inputLeadership").data('kendoDropDownList');
                                    if (inputSector!=undefined)
                                        inputSector.value("Select");
                                    if (inputDepartment != undefined)
                                        inputDepartment.value("Select");
                                    if (inputLeadership != undefined)
                                    inputLeadership.value("Select");

                                    var gridObj = $("#gridSDLMapping").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    var dialog = $("#windowEditSDLMapping").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();

                                    $("#hdnSDLMId").val(tr.ID);                                    

                                    if ($("#inputSector").data('kendoDropDownList')!=undefined)
                                        $("#inputSector").data('kendoDropDownList').value(tr.SectorCode);       
                                    if ($("#inputDepartment").data('kendoDropDownList') != undefined)
                                        $("#inputDepartment").data('kendoDropDownList').value(tr.DepartmentCode);  
                                    if ($("#inputLeadership").data('kendoDropDownList') != undefined)
                                    $("#inputLeadership").data('kendoDropDownList').value(tr.LeadershipRoleCode);
                                   
                                    dialog.title("Sector Department Leadership Role Mapping");
                                    Utility.UnLoading();
                                    return true;
                                }, 1000);
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                                                
                        HttpClient.MakeRequest(CookBookMasters.GetSectorDepartmentLeadershipMapping, function (result) {
                            Utility.UnLoading();
                            //console.log(result)
                            if (result != null) {
                                sdlmappingdata = result;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , false);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            SectorName: { editable: false, type: "string" },
                            DepartmentName: { type: "string", editable: false },
                            LeaderShipRoleName: { type: "string", editable: false},
                            SectorCode: { type: "string", editable: false },
                            DepartmentCode: { type: "string", editable: false },
                            LeadershipRoleCode: { type: "string", editable: false },
                            IsActive: { editable: false }                     
                        }
                    }
                },
                pageSize: 50,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            //dataBound: function (e) {
            //    var grid = e.sender;
            //    var items = e.sender.items();
            //    items.each(function (e) {
            //        var dataItem = grid.dataItem(this);
            //        var ddt = $(this).find('.brTemplate');
            //        $(ddt).kendoDropDownList({
            //            index: 0,
            //            value: dataItem.value,
            //            dataSource: exeCOMUserData,
            //            dataTextField: "OfficeEmailAddress",
            //            dataValueField: "OfficeEmailAddress",
            //        });
            //        ddt.find(".k-input").val(dataItem.OfficeEmailAddress);
            //        $(this).next().focus();
            //    });
            //},
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid" || col == "DepartmentCode" || col == "SectorCode" || col == "LeadershipRoleCode" || col == "IsActive" || col == "CreatedBy" || col=="ID") { }
                    else
                        return col;
                });
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                console.log(columns1);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        $("#gridSDLMapping").data("kendoGrid").dataSource.sort({ field: "ID", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

    //function onLeadershipChange(e) {

    //    //if (checkItemAlreadyExist(e.sender.value(), 'gridTransferLeaderShipRole')) {
    //    //    toastr.error("Email ID already selected.");
    //    //    var dropdownlist = $("#gridTransferLeaderShipRole").data("kendoDropDownList");
    //    //    dropdownlist.select(0);
    //    //    dropdownlist.trigger("change");
    //    //    return false;
    //    //}

    //    var element = e.sender.element;
    //    var row = element.closest("tr");
    //    var grid = $("#gridTransferLeaderShipRole").data("kendoGrid");
    //    var dataItem = grid.dataItem(row);
    //    var data = row.find(".k-input")[0].innerText;
    //    row.find(".brspan").val(data);

    //    var Obj = exeCOMUserData.filter(function (exeObj) {
    //        return exeObj.OfficeEmailAddress == e.sender.value();
    //    });

    //    dataItem.set("OfficeEmailAddressTo", e.sender.value());
    //    dataItem.set("OfficeEmailAddressTo", data);
    //    dataItem.set("EmployeeCodeTo", Obj[0].EmployeeCode);
    //    var officeemailid = Obj[0].OfficeEmailAddress;
    //    dataItem.OfficeEmailAddressTo = officeemailid;
    //    dataItem.EmployeeCodeTo = Obj[0].EmployeeCode;


    //};

    //function checkItemAlreadyExist(selectedId, gridID) {
    //    var dataExists = false;
    //    var gridObj = $("#" + gridID).data("kendoGrid");
    //    var data = gridObj.dataSource.data();
    //    var existingData = [];
    //    existingData = data.filter(m => m.OfficeEmailAddress == selectedId);
    //    if (existingData.length == 2) {
    //        dataExists = true;
    //    }
    //    return dataExists;
    //}

    $("#btnCancel").on("click", function () {
        //Utility.Page_Alert_Save("All Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
        //    function () {

        //    },
        //    function () {
        //    }
        //);

        $(".k-overlay").hide();
        var orderWindow = $("#windowEditSDLMapping").data("kendoWindow");
        orderWindow.close();
    });

    //function checkIsLeaderShipRoleExists() {
    //    var isExists = false;
    //    var LeaderShipRoleList = $("#gridLeaderShipRole").data("kendoGrid").dataSource._data;
    //    if (LeaderShipRoleList != null && LeaderShipRoleList.length > 0) {
    //        var filterData = LeaderShipRoleList.filter(m => m.LeaderShipRole === $("#leadershiprolename").val());
    //        if (filterData != null && filterData.length > 0) {
    //            isExists = true;
    //        }
    //    }
    //    return isExists;
    //};

    $("#btnSubmit").click(function () {
        Utility.Loading();
        var isValid = true;
        var inputSector = $("#inputSector").data('kendoDropDownList').value();
        var inputDepartment = $("#inputDepartment").data('kendoDropDownList').value();
        var inputLeadership = $("#inputLeadership").data('kendoDropDownList').value();        
        
        if (isValid && inputSector === "" || inputSector === "Select") {
            toastr.error("Please Select Sector First.");
            $("#inputSector").focus();
            isValid = false;
        }
        if (isValid && inputDepartment === "" || inputDepartment === "Select") {
            toastr.error("Please Select Department First.");
            $("#inputDepartment").focus();
            isValid = false;
        }
        if (isValid && inputLeadership === "" || inputLeadership === "Select") {
            toastr.error("Please Select Leadership Role First.");
            $("#inputLeadership").focus();
            isValid = false;
        }
        var chkEdit = datamodel != null && (datamodel.SectorCode == inputSector && datamodel.DepartmentCode == inputDepartment && datamodel.LeadershipRoleCode == inputLeadership) ? true : false;
        var chkExist = sdlmappingdata.filter(a => a.SectorCode == inputSector && a.DepartmentCode == inputDepartment && a.LeadershipRoleCode == inputLeadership)[0];
        if (chkExist != undefined && !chkEdit) {
            toastr.error("Duplicate entry not allowed.");
            isValid = false;
        }

        if (isValid) {
            var model;
            if (datamodel != null) {
                model = datamodel;
                model.SectorCode = inputSector == "Select" ? "" : inputSector;
                model.DepartmentCode = inputDepartment == "Select" ? "" : inputDepartment;
                model.LeadershipRoleCode = inputLeadership == "Select" ? "" : inputLeadership;
                model.ModifiedBy = user.UserId;
                model.ModifiedOn = Utility.CurrentDate();
                //model.CreatedOn = kendo.parseDate(model.CreatedOn);               
                //model.ID = "Update";  
                model.IsActive = true;
            }
            else {                
                model = {
                    "SectorCode": inputSector == "Select" ? "" : inputSector,
                    "DepartmentCode": inputDepartment == "Select" ? "" : inputDepartment,
                    "LeadershipRoleCode": inputLeadership == "Select" ? "" : inputLeadership,
                    "IsActive": true                    
                }
                console.log(model)
            }
            $("#btnSubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSectorDepartmentLeadershipMapping, function (result) {
                if (result != "true") {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.error("Some error occured, please try again. "+result);
                    $("#inputSector").focus();
                }
                else {
                    $('#btnSubmit').removeAttr("disabled");
                    toastr.success("Updated successfully");
                    Utility.Loading();
                    setTimeout(function () {
                        $("#gridSDLMapping").data("kendoGrid").dataSource.data([]);
                        $("#gridSDLMapping").data("kendoGrid").dataSource.read();
                        $(".k-overlay").hide();
                        var orderWindow = $("#windowEditSDLMapping").data("kendoWindow");
                        orderWindow.close();
                        Utility.UnLoading();
                    }, 1000)

                }
            }, {
                model: model

            }, false);
        }
        Utility.UnLoading();
    });
});