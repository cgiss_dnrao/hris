﻿
$(function () {
    var user;
    var employeeName = "";
    var datamodel;
    var costcenterdata = [];
    var profitcenterdata = [];
    var leadershiproledata = [];
    var siteData = [];
    var subSiteData = [];
    var employeedata = [];


    $("#windowEditEmployee").kendoWindow({
        modal: true,
        width: "900px",
        height: "485px",
        title: "Employee Details",
        actions: ["Close"],
        animation: false,
        visible: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }

    $('#myInput').on('input', function (e) {
        var grid = $('#gridEmployee').data('kendoGrid');
        var columns = grid.columns;
        //console.log(employeedata);
        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "EmployeeCode" || x.field == "EmployeeName" || x.field == "EmploymentType" || x.field == "LastModifiedOnFormatted" || x.field == "IsActiveFormatted" || x.field == "Designation" || x.field == "Department"
                    || x.field == "Location" || x.field == "MonthlyLeaveEncashment" || x.field == "LeaderShipRole" ) {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;
                    if (type == 'string') {
                        var targetValue = e.target.value;
                        
                        if (x.field == "Status") {
                            var pendingString = "pending";
                            var savedString = "saved";
                            var mappedString = "mapped";
                            if (pendingString.includes(e.target.value.toLowerCase())) {
                                targetValue = "1";
                            }
                            else if (savedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "2";
                            }
                            else if (mappedString.includes(e.target.value.toLowerCase())) {
                                targetValue = "3";
                            }
                        }
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        });
                    }
                    else if (type == 'number') {
                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $('#myInput').focus(function () {
        $('.dashboarddetailsearch .fa-search').css('opacity', 0);
        $(this).css('padding-left', '10px');
        $(this).attr('placeholder', 'Start typing to filter');
    }).blur(function () {
        $('.dashboarddetailsearch .fa-search').css('opacity', 1);
        $(this).css('padding-left', '25px');
        $(this).attr('placeholder', 'Search');
    });

    var user;
    $(document).ready(function () {
        Utility.Loading();

        setTimeout(function () {
            HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
                user = result;
                //if (user.UserRoleId === 1) {
                //    $("#InitiateBulkChanges").css("display", "inline");
                //    $("#btnExport").css("display", "inline");
                //}
                //if (user.UserRoleId === 2) {
                    $("#InitiateBulkChanges").css("display", "none");
                //    $("#btnExport").css("display", "none");
                //}
            }, null, false);

            

            $("#btnExport").click(function (e) {
                HttpClient.MakeRequest(CookBookMasters.ExportDKData, function (result) {
                    if (result != null) {
                        var bytes = new Uint8Array(result.FileContents);
                        var blob = new Blob([bytes], { type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" });
                        var link = document.createElement('a');
                        link.href = window.URL.createObjectURL(blob);
                        link.download = "EmployeeData.xlsx";
                        link.click();
                    }
                    else {
                    }
                }, null
                    //{
                    //filter: mdl
                    //}
                    , true);
                //Utility.Loading();
                //setTimeout(function () {

                //    Utility.UnLoading();
                //},2000);

            });

            populateEmployeeGrid();

            $("#gridBulkChange").css("display", "none");
            populateBulkChangeControls();
            var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
            changeControls.css("background-color", "#fff");
            changeControls.css("border", "none");
            changeControls.eq(4).text("");
            changeControls.eq(4).append("<div id='inputcostcenterbulk' style='width:100%; font-size:10.5px!important'></div>");
            changeControls.eq(5).text("");
            changeControls.eq(5).append("<div id='inputprofitcenterbulk' style='width:100%; font-size:10.5px!important'></div>");

            populateDropDownData();

            $("#btnCancel").on("click", function () {
                $(".k-overlay").hide();
                var orderWindow = $("#windowEditEmployee").data("kendoWindow");
                orderWindow.close();
            });

            $("#btnSubmit").click(function () {
                Utility.Loading();
                var isValid = true;
                var profitcenter = $("#inputprofitcenter").data('kendoDropDownList').value();
                var costcenter = $("#inputcostcenter").data('kendoDropDownList').value();
                var leadershiprole = $("#inputleadershiprole").data('kendoDropDownList').value();
                var siteCode = $("#inputSite").data('kendoDropDownList').value();
                var subSite = $("#inputsubsite").data('kendoDropDownList').text();
                if (costcenter === "" || costcenter === "Select") {
                    toastr.error("Please select Cost Center First.");
                    $("#inputcostcenter").focus();
                    isValid = false;
                }
                if (isValid && profitcenter === "" || profitcenter === "Select") {
                    toastr.error("Please select Profit Center First.");
                    $("#inputprofitcenter").focus();
                    isValid = false;
                }
                if (isValid && siteCode === "" || siteCode === "Select Site") {
                    toastr.error("Please select Site First.");
                    $("#inputSite").focus();
                    isValid = false;
                }
                if (isValid && datamodel.Division != null && datamodel.Division != "" && datamodel.Division == "MAP4") {
                    if (profitcenter !== costcenter) {
                        toastr.error("Cost center and Profit center should be same for Division MAP4.");
                        isValid = false;
                    }
                }
                if (isValid && datamodel.Division != null && datamodel.Division != "" && datamodel.Division == "MAP5") {
                    if (profitcenter === costcenter) {
                        toastr.error("Cost center and Profit center should be different for Division MAP5.");
                        isValid = false;
                    }
                }
                if (leadershiprole != "EC-014" && checkLeaderShipRoleCodeExists()) {
                    toastr.error("Selected Leadershp role aready assigned.");
                    isValid = false;
                }
                if (isValid) {
                    var model;
                    if (datamodel != null) {
                        model = datamodel;
                        model.CostCenter = costcenter == "Select" ? "" : costcenter;
                        model.ProfitCenter = profitcenter == "Select" ? "" : profitcenter;
                        model.LeadershipRoleCode = leadershiprole == "Select" ? "" : leadershiprole;
                        model.ModifiedBy = user.UserId;
                        model.ModifiedOn = Utility.CurrentDate();
                        model.CreatedOn = kendo.parseDate(model.CreatedOn);
                        model.DateOfBirth = kendo.toString(model.DateOfBirth, "MM/dd/yyyy hh:mm:s tt");
                        model.Action = "Update";
                        model.SiteCode = siteCode;
                        model.SubSiteName = subSite;
                        $("#btnSubmit").attr('disabled', 'disabled');
                        HttpClient.MakeRequest(CookBookMasters.SaveEmployeeData, function (result) {
                            if (result == false) {
                                $('#btnSubmit').removeAttr("disabled");
                                toastr.error("Some error occured, please try again");
                                $("#inputcostcenter").focus();
                            }
                            else {
                                $('#btnSubmit').removeAttr("disabled");
                                toastr.success("Employee updated successfully");
                                Utility.Loading();
                                setTimeout(function () {
                                    $("#gridEmployee").data("kendoGrid").dataSource.data([]);
                                    $("#gridEmployee").data("kendoGrid").dataSource.read();
                                    $(".k-overlay").hide();
                                    var orderWindow = $("#windowEditEmployee").data("kendoWindow");
                                    orderWindow.close();
                                    Utility.UnLoading();
                                }, 1000)

                            }
                        }, {
                            model: model

                        }, false);
                    }
                }
                Utility.UnLoading();
            });

            function checkLeaderShipRoleCodeExists() {
                var isExists = false;
                var leadershiprole = $("#inputleadershiprole").data('kendoDropDownList').value();
                if (leadershiprole != null && leadershiprole != "Select" && leadershiprole != "" && employeedata != null && employeedata.length > 0) {
                    var data = employeedata.filter(m => m.LeadershipRoleCode == leadershiprole && m.EmployeeCode != datamodel.EmployeeCode);
                    if (data != null && data.length > 0 && data[0].EmployeeCode != null) {
                        isExists = true;
                    }
                };
                return isExists;
            };

            $("#InitiateBulkChanges").on("click", function () {
                $("#gridBulkChange").css("display", "block");
                $("#gridBulkChange").children(".k-grid-header").css("border", "none");
                $("#InitiateBulkChanges").css("display", "none");
                $("#CancelBulkChanges").css("display", "inline");
                $("#SaveBulkChanges").css("display", "inline");
            });


            $("#CancelBulkChanges").on("click", function () {
                $("#InitiateBulkChanges").css("display", "inline");
                $("#CancelBulkChanges").css("display", "none");
                $("#SaveBulkChanges").css("display", "none");
                $("#gridBulkChange").css("display", "none");
                populateEmployeeGrid();
            });

            $("#SaveBulkChanges").on("click", function () {

                var neVal = $(this).val();
                var dataFiltered = $("#gridEmployee").data("kendoGrid").dataSource.dataFiltered();


                var model = dataFiltered;
                var changeControls = $("#gridBulkChange").children(".k-grid-header").children(".k-grid-header-wrap").children("table").children("thead").children("tr").children("th");
                var validChange = false;
                var costcenter = $("#inputcostcenterbulk").val();
                var profitcenter = $("#inputprofitcenterbulk").val();

                if (costcenter === "Select" && profitcenter === "Select") {
                    validChange = false
                }
                else {
                    validChange = true;
                }
                if (validChange == false) {
                    toastr.error("Please change at least one attribute for Bulk Update");
                } else {
                    Utility.Page_Alert_Save("Proceeding will perform bulk changes on filtered records. Are you sure to proceed?", "Bulk Change Confirmation", "Yes", "No",
                        function () {
                            $.each(model, function () {
                                this.CreatedOn = kendo.parseDate(this.CreatedOn);
                                this.ModifiedOn = Utility.CurrentDate();
                                this.ModifiedBy = user.UserId;
                                if (costcenter != "")
                                    this.CostCenter = costcenter == "Select" ? "" : costcenter;
                                if (profitcenter != "")
                                    this.ProfitCenter = profitcenter == "Select" ? "" : profitcenter;
                            });

                            HttpClient.MakeRequest(CookBookMasters.SaveEmployeeDataList, function (result) {
                                if (result == false) {
                                    toastr.error("Some error occured, please try again.");
                                }
                                else {
                                    $(".k-overlay").hide();
                                    toastr.success("Records have been updated successfully");
                                    $("#gridEmployee").data("kendoGrid").dataSource.data([]);
                                    $("#gridEmployee").data("kendoGrid").dataSource.read();
                                    $("#InitiateBulkChanges").css("display", "inline");
                                    $("#CancelBulkChanges").css("display", "none");
                                    $("#SaveBulkChanges").css("display", "none");
                                    $("#gridBulkChange").css("display", "none");
                                }
                            }, {
                                model: model

                            }, true);
                        },
                        function () {
                            maptype.focus();
                        }
                    );
                }
            });

            Utility.UnLoading();
        }, 2000);
    });

    function populateDropDownData() {
        HttpClient.MakeSyncRequest(CookBookMasters.GetCostCenterMasterDataList, function (data) {
            var item = { CostCenter: "Select" };
            data.splice(0, 0, item);
            costcenterdata = data;
            $("#inputcostcenter").kendoDropDownList({
                filter: "contains",
                dataTextField: "CostCenter",
                dataValueField: "CostCenter",
                dataSource: data,
                change: function (e) {
                    var value = this.value();
                    if (datamodel.Division != null && datamodel.Division != "" && datamodel.Division == "MAP4") {
                        var inputprofitcenter = $("#inputprofitcenter").data("kendoDropDownList");
                        inputprofitcenter.value(value);
                    };
                },
                index: 0,
            });

            $("#inputcostcenterbulk").kendoDropDownList({
                filter: "contains",
                dataTextField: "CostCenter",
                dataValueField: "CostCenter",
                dataSource: data,
                index: 0,
            });

        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetProfitCenterMasterDataList, function (data) {
            var item = { ProfitCenter: "Select" };
            data.splice(0, 0, item);
            profitcenterdata = data;
            $("#inputprofitcenter").kendoDropDownList({
                filter: "contains",
                dataTextField: "ProfitCenter",
                dataValueField: "ProfitCenter",
                dataSource: data,
                index: 0,
            });

            $("#inputprofitcenterbulk").kendoDropDownList({
                filter: "contains",
                dataTextField: "ProfitCenter",
                dataValueField: "ProfitCenter",
                dataSource: data,
                index: 0,
            });

        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetLeadershipRoleMasterDataList, function (data) {
            var item = { LeaderShipRole: "Select" };
            data.splice(0, 0, item);
            leadershiproledata = data;
            $("#inputleadershiprole").kendoDropDownList({
                filter: "contains",
                dataTextField: "LeaderShipRole",
                dataValueField: "LeadershipRoleCode",
                dataSource: data,
                index: 0,
            });

        }, null, false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetSiteDataList, function (result) {
            if (result != null) {
                // options.success(result);
                result = result.filter(m => m.IsActive == 1);
                var item = { SiteName: "Select Site" };
                result.splice(0, 0, item);
                siteData = result;
                $("#inputSite").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "SiteName",
                    dataValueField: "SiteCode",
                    dataSource: siteData,
                    change: function (e) {
                        var value = this.value();
                        var subSites = siteData.filter(m => m.SiteCode == value);
                        if (subSites != null) {
                            subSiteData = [];
                            if (subSites !== null && subSites.length > 0 && subSites[0].SubSiteName != null) {
                                var subSiteList = subSites[0].SubSiteName.split(',');
                                subSiteList.forEach(function (item, index) {
                                    var siteSubSiteModel = {
                                        "ID": 0,
                                        "SiteCode": subSites[0].SiteCode,
                                        "SubSiteName": item,
                                        "IsActive": 1
                                    }
                                    subSiteData.push(siteSubSiteModel);
                                });
                            }
                            populateSubSiteDropdown();

                        }
                    },
                    index: 0,
                });
                populateSubSiteDropdown();




            }
            else {
                options.success("");
            }
        }, null
            //{
            //filter: mdl
            //}
            , false);

        HttpClient.MakeSyncRequest(CookBookMasters.GetLeadershipRoleMasterDataList, function (data) {
            var item = { LeaderShipRole: "Select" };
            data.splice(0, 0, item);
            leadershiproledata = data;
            $("#inputleadershiprole").kendoDropDownList({
                filter: "contains",
                dataTextField: "LeaderShipRole",
                dataValueField: "LeadershipRoleCode",
                dataSource: data,
                index: 0,
            });

        }, null, false);
    };

    function populateSubSiteDropdownOnEdit(sitecode) {
        var subSites = siteData.filter(m => m.SiteCode == sitecode);
        if (subSites != null) {
            subSiteData = [];
            if (subSites !== null && subSites.length > 0 && subSites[0].SubSiteName != null) {
                var subSiteList = subSites[0].SubSiteName.split(',');
                subSiteList.forEach(function (item, index) {
                    var siteSubSiteModel = {
                        "ID": 0,
                        "SiteCode": subSites[0].SiteCode,
                        "SubSiteName": item,
                        "IsActive": 1
                    }
                    subSiteData.push(siteSubSiteModel);
                });
            }
            var item = { SubSiteName: "Select Sub Site" };
            subSiteData.splice(0, 0, item);
            populateSubSiteDropdown();
        }
    }

    function populateSubSiteDropdown() {
        $("#inputsubsite").kendoDropDownList({
            filter: "contains",
            dataTextField: "SubSiteName",
            dataValueField: "SubSiteName",
            dataSource: subSiteData,
            index: 0,
        });
    }

    function populateEmployeeGrid() {
        //alert('');
        Utility.Loading();
        var gridVariable = $("#gridEmployee");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "Employee.xlsx",
                filterable: true,
                allPages: true
            },
            sortable: true,
            filterable: true,
            pageable: true,
            groupable: false,
            height: 616,
            columns: [
                {
                    field: "EmployeeCode", title: "Employee Code", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "EmployeeName", title: "Name", width: "125px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "EmploymentType", title: "Employment Type", width: "60px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //},
                //{
                //    field: "DateOfBirth", title: "Date Of Birth", width: "50px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //    template: '# if (DateOfBirth == null) {#<span>-</span>#} else{#<span>#: kendo.toString(DateOfBirth, "dd-MMM-yyyy")#</span>#}#',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    }
                //},
                {
                    field: "Department", title: "Department", width: "70px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                //{
                //    field: "CostCenter", title: "Cost Center", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //},
                //{
                //    field: "ProfitCenter", title: "Profit Center", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //},
                {
                    field: "Designation", title: "Designation", width: "60px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    },
                },
                {
                    field: "Location", title: "Location", width: "150px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: left;"
                    },
                },
                {
                    field: "LastModifiedOnFormatted", title: "Last Modified On", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "IsActiveFormatted", title: "Status", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "MonthlyLeaveEncashment", title: "Leave Encashment", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },


                //{
                //    field: "LastModifiedOn", title: "Last Modified On", width: "50px", attributes: {

                //        style: "text-align: center; font-weight:normal"
                //    },
                //    template: '# if (LastModifiedOn == null) {#<span>-</span>#} else{#<span>#: kendo.toString(LastModifiedOn, "dd-MMM-yyyy")#</span>#}#',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    }
                //},
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    //template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //    template: '<span>#= IsActive ? "Active" : "Inactive" #</span>',
                //},
                {
                    field: "Edit", title: "Action", width: "30px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                Utility.Loading();
                                setTimeout(function () {

                                    var costcenter = $("#inputcostcenter").data('kendoDropDownList');
                                    var leadershiprole = $("#inputleadershiprole").data('kendoDropDownList');
                                    var profitcenter = $("#inputprofitcenter").data('kendoDropDownList');
                                    costcenter.value("Select");
                                    profitcenter.value("Select");
                                    leadershiprole.value("Select");

                                    var gridObj = $("#gridEmployee").data("kendoGrid");
                                    var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                    datamodel = tr;
                                    var dialog = $("#windowEditEmployee").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();

                                    $("#employeeid").val(tr.ID);
                                    $("#employeecode").text(tr.EmployeeCode);
                                    $("#employeename").text(tr.EmployeeName);
                                    $("#employmenttype").text(tr.EmploymentType);
                                    $("#employmentstatus").text(tr.EmploymentStatus);
                                    $("#fathername").text(tr.FatherName);
                                    $("#department").text(tr.Department);
                                    $("#gender").text(tr.Gender);
                                    $("#company").text(tr.Company);
                                    $("#functionname").text(tr.FunctionName);
                                    $("#entity").text(tr.Entity);
                                    $("#ltogroup").text(tr.LTOGroup);
                                    $("#companygroup").text(tr.CompanyGroup);
                                    $("#role").text(tr.Role);
                                    $("#state").text(tr.State);
                                    $("#branch").text(tr.Branch);
                                    $("#division").text(tr.Division);
                                    $("#location").text(tr.Location);
                                    $("#doj").text(tr.DateofJoiningFormatted);
                                    $("#grade").text(tr.Grade);
                                    $("#designation").text(tr.Designation);
                                    $("#confirmationstatus").text(tr.ConfirmationStatus);
                                    $("#l1managercode").text(tr.L1ManagerCode);
                                    $("#l1managername").text(tr.L1Manager);
                                    $("#l2managercode").text(tr.L2ManagerCode);
                                    $("#l2managername").text(tr.L2Manager);
                                    $("#hrmanagercode").text(tr.HRManagerCode);
                                    $("#hrmanagername").text(tr.HRManager);
                                    $("#IsHealthCare").text(tr.IsHealthCare);

                                    console.log("health b" + tr.IsHealthCare);

                                    //var dob = kendo.toString(tr.DateOfBirth, "dd-MMM-yyyy");
                                    //$("#dob").text(dob);


                                    $("#department").text(tr.Department);
                                    if (tr.ProfitCenter != null) {
                                        $("#inputprofitcenter").data('kendoDropDownList').value(tr.ProfitCenter);
                                    }
                                    if (tr.CostCenter != null) {
                                        $("#inputcostcenter").data('kendoDropDownList').value(tr.CostCenter);
                                    }
                                    if (tr.LeadershipRoleCode != null) {
                                        $("#inputleadershiprole").data('kendoDropDownList').value(tr.LeadershipRoleCode);
                                    }
                                    if (tr.SiteCode != null) {
                                        $("#inputSite").data('kendoDropDownList').value(tr.SiteCode);
                                        populateSubSiteDropdownOnEdit(tr.SiteCode);
                                    }
                                    if (tr.SubSiteName != null) {
                                        $("#inputsubsite").data('kendoDropDownList').value(tr.SubSiteName);
                                    }
                                    dialog.title("Employee Details");
                                    Utility.UnLoading();
                                    return true;
                                }, 1000);
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        var empRequest = {
                            "PageNumber": options.data.page,
                            "PageSize": options.data.pageSize,
                            "SearchKeyword": $("#myInput").val(),
                            "IsItiliteSearch": false
                        };
                        //alert('2');
                        HttpClient.MakeRequest(CookBookMasters.GetEmployeeDataList, function (result) {
                            console.log(result)
                            if (result != null) {
                                employeedata = result.HRISMasterDataList;
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        },
                            {
                                employeeRequest: empRequest
                            }
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            //Name: { type: "string", editable: false },
                            EmployeeCode: { type: "string" },
                            EmployeeName: { type: "string" },
                            EmploymentType: { type: "string" },
                            //DateOfBirth: { type: "date" },
                            Department: { type: "string" },
                            //CostCenter: { type: "string" },
                           // ProfitCenter: { type: "string" },
                            Designation: { type: "string" },
                            Location: { type: "string" },
                            //LeaderShipRole: { type: "string" },
                            //LeadershipRoleCode: { type: "string" },
                            LastModifiedOn: {type:"date"},
                            IsActive: { editable: false },
                            LastModifiedOnFormatted: { type: "string" },
                            IsActiveFormatted: { type: "string" },
                            MonthlyLeaveEncashment: { type: "string" },
                        },
                    },
                    //parse: function (response) {
                    //    for (var i = 0; i < response.length; i++) {
                    //        console.log(response[i])
                    //        response[i].LastModifiedOnFormatted = kendo.toString(kendo.parseDate(response[i].LastModifiedOn), "dd-MMM-yyyy");
                    //        if (response[i].IsActive == true)
                    //            response[i].IsActiveFormatted = "Active";
                    //        else
                    //            response[i].IsActiveFormatted = "Inctive";
                    //        //kendo.format("{0:dd MMM yyyy hh:mm tt}", response[i].CreateOn);
                    //        //console.log(response[i].CreateOnFormatted)
                    //    }

                    //    return response;
                    //},
                    data: function (response) {
                        return response.HRISMasterDataList;
                        //var data = response.HRISMasterDataList;
                        //if (data != undefined) {
                        //    for (var i = 0; i < data.length; i++) {
                        //        console.log(data[i])
                        //        data[i].LastModifiedOnFormatted = kendo.toString(kendo.parseDate(data[i].LastModifiedOn), "dd-MMM-yyyy");
                        //        if (data[i].IsActive == true)
                        //            data[i].IsActiveFormatted = "Active";
                        //        else
                        //            data[i].IsActiveFormatted = "Inactive";
                        //        //kendo.format("{0:dd MMM yyyy hh:mm tt}", response[i].CreateOn);
                        //        //console.log(response[i].CreateOnFormatted)
                        //    }
                        //}
                        //return data;
                    },
                    
                    total: function (response) {
                        console.log(response);
                        if (response!=null)
                        return response.TotalCount;
                    }
                },
                sort: { field: "IsActive", dir: "desc" },
                pageSize: 150,
                // pageSize: 20,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: false
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },            
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                var items = e.sender.items();
                var gridObj = $("#gridEmployee").data("kendoGrid");
                
                gridObj.tbody.find("tr[role='row']").each(function () {
                    var model = gridObj.dataItem(this);                    
                    if (!model.IsActive) 
                        $(this).find(".k-grid-Edit").hide();
                    //}
                });

                $(".chkbox").on("change", function () {
                    // 
                    var tr = gridObj.dataItem($(this).closest("tr"));
                    datamodel = tr;
                    var chkEle = $(this)[0];
                    $("#success").css("display", "none");
                    $("#error").css("display", "none");
                    
                    
                    datamodel.IsActive = $(this)[0].checked;
                    var active = "";
                    if (datamodel.IsActive) {
                        active = "Active";
                    } else {
                        active = "Inactive";
                    }

                    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.EmployeeName + "</b> Employee " + active + "?", "Employee Update Confirmation", "Yes", "No", function () {
                        $("#error").css("display", "none");
                        HttpClient.MakeRequest(CookBookMasters.SaveEmployeeData, function (result) {
                            if (result == false) {
                                $("#error").css("display", "flex");
                                $("#error").find("p").text("Some error occured, please try again");
                            }
                            else {
                                $("#error").css("display", "flex");
                                $("#success").css("display", "flex");
                                $("#success").find("p").text("Color updated successfully");
                            }
                        }, {
                                model: datamodel,
                                changeStatus:true
                        }, false);
                    }, function () {
                            
                        chkEle.checked = !datamodel.IsActive;
                        //$("#gridColor").data("kendoGrid").dataSource.data([]);
                        //$("#gridColor").data("kendoGrid").dataSource.read();
                    });
                    return true;
                });
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                console.log(data);
                var data = e.data;
                var cols = Object.keys(data[0])
                var columns = cols.filter(function (col) {
                    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid") { }
                    else
                        return col;
                });

                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                // var columns = e.workbook.sheets[0].columns;
                //columns1.forEach(function (column) {
                //    delete column.width;
                //    column.autoWidth = true;
                //});
                console.log(columns1);
                
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue;
                        if (data[i][columns[j]] != null && columns[j] == "DateOfBirth" || columns[j] == "DateofJoining" || columns[j] == "CreatedOn"
                            || columns[j] == "ModifiedOn") {
                            var dateValue = kendo.parseDate(data[i][columns[j]]);
                            cellValue = kendo.toString(dateValue, "dd/MM/yyyy");
                        }
                        else {
                            cellValue = data[i][columns[j]];
                        }
                        rowCells.autoWidth = true;
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
                for (var i = 0; i < sheet.columns.length; i++) {
                    delete sheet.columns[i].width;
                    sheet.columns[i].autoWidth = true;
                }
                
            }

        })

    }

    function populateBulkChangeControls() {

        Utility.Loading();
        var gridVariable = $("#gridBulkChange");
        gridVariable.html("");
        gridVariable.kendoGrid({
            selectable: "cell",
            sortable: false,
            filterable: false,
            columns: [
                {
                    field: "", title: "", width: "60px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "", title: "", width: "125px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                {
                    field: "", title: "", width: "80px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "DateOfBirth", title: "Date Of Birth", width: "50px", attributes: {

                //        style: "text-align: left; font-weight:normal"
                //    },
                //    template: '# if (DateOfBirth == null) {#<span>-</span>#} else{#<span>#: kendo.toString(DateOfBirth, "dd-MMM-yyyy")#</span>#}#',
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    }
                //},
                {
                    field: "", title: "", width: "90px", attributes: {

                        style: "text-align: left; font-weight:normal"
                    },
                },
                //{
                //    field: "IsActive",
                //    title: "Status", width: "50px",
                //    attributes: {
                //        style: "text-align: center; font-weight:normal"
                //    },
                //    headerAttributes: {
                //        style: "text-align: center;"
                //    },
                //    template: '<label class= "switch"><input type="checkbox" class="chkbox" #= IsActive ? \'checked="checked"\' : "" #><span class="slider round"></span></label>',
                //},
                {
                    field: "", title: "Cost Center", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "", title: "Profit Center", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "", title: "Leadership Role", width: "50px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                },
                {
                    field: "", title: "", width: "60px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center"
                    },
                }
            ],
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
            },
            change: function (e) {
            },
        });
    }

});