﻿var locdesgdata = [];
var locationname = "";
var locationdesignationdata = [];
var locationdata = [];
var designationdata = [];
var datamodelforedit = [];

$(function () {

    $('#myInput1').on('input', function (e) {
        var grid = $('#gridLocationDesignation').data('kendoGrid');
        var columns = grid.columns;

        var filter = { logic: 'or', filters: [] };
        columns.forEach(function (x) {
            if (x.field) {
                if (x.field == "Location" || x.field == "Designation") {
                    var type = grid.dataSource.options.schema.model.fields[x.field].type;

                    if (type == 'string') {
                        var targetValue = e.target.value;
                        filter.filters.push({
                            field: x.field,
                            operator: 'contains',
                            value: targetValue
                        })
                    }
                    else if (type == 'number') {

                        if (isNumeric(e.target.value)) {
                            filter.filters.push({
                                field: x.field,
                                operator: 'eq',
                                value: e.target.value
                            });
                        }
                    } else if (type == 'date') {
                        var data = grid.dataSource.data();
                        for (var i = 0; i < data.length; i++) {
                            var dateStr = kendo.format(x.format, data[i][x.field]);
                            if (dateStr.startsWith(e.target.value)) {
                                filter.filters.push({
                                    field: x.field,
                                    operator: 'eq',
                                    value: data[i][x.field]
                                })
                            }
                        }
                    } else if (type == 'boolean' && getBoolean(e.target.value) !== null) {
                        var bool = getBoolean(e.target.value);
                        filter.filters.push({
                            field: x.field,
                            operator: 'eq',
                            value: bool
                        });
                    }
                }
            }
        });
        grid.dataSource.filter(filter);
    });

    $("#btnExport").click(function (e) {
        var grid = $("#gridLocationDesignation").data("kendoGrid");
        grid.saveAsExcel();
    });

    $('#myInput1').focus(function () {
        $('.locationdesignationdetailsearch .fa-search').css('opacity', 0);
        //$(this).css('padding-left', '0px');
        $(this).attr('placeholder', 'Start typing to filter');
    }).blur(function () {
        $('.locationdesignationdetailsearch .fa-search').css('opacity', 1);
        //$(this).css('padding-left', '0px');
        $(this).attr('placeholder', 'Search');
    })

    $("#windowEdit").kendoWindow({
        modal: true,
        width: "450px",
        //height: "250px",
        title: "Leave Encashment",
        actions: ["Close"],
        visible: false,
        animation: false
    });

    kendo.data.DataSource.prototype.dataFiltered = function () {
        // Gets the filter from the dataSource
        var filters = this.filter();

        // Gets the full set of data from the data source
        var allData = this.data();

        // Applies the filter to the data
        var query = new kendo.data.Query(allData);

        // Returns the filtered data
        return query.filter(filters).data;
    }


    HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
        user = result;
        //if (user.UserRoleId === 1) {
        //    $("#InitiateBulkChanges").css("display", "inline");
        //    $("#AddNew").css("display", "inline");
        //}
        //if (user.UserRoleId === 2) {
        //    $("#InitiateBulkChanges").css("display", "none");
        //    $("#AddNew").css("display", "none");
        //}
        populateLocationDesignationGrid();
    }, null, false);

    HttpClient.MakeRequest(CookBookMasters.GetUniqueLocationAndDesignation, function (result) {
        locationdesignationdata = [];
        locationdata = [];
        locationdata.push({ text: "Please Select", value: "" });
        console.log(result);

        result.forEach(function (item, index) {
            var chkExisting = locationdesignationdata.filter(a => a.Location == item.Location)[0];
            if (chkExisting == undefined) {
                var locFilter = result.filter(a => a.Location == item.Location).map(a => a.Designation);
                //console.log(locFilter)
                locationdesignationdata.push({ Designation: locFilter, Location: item.Location });
                locationdata.push({ text: item.Location, value: item.Location });
            }
        });
        console.log(locationdesignationdata);
        populateLocationSelect();
    }, null, false);

    function populateLocationSelect() {
        $("#inputLocationdd").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: locationdata,
            index: 0,
            change: onLocationChange,
        });
    }
    function onLocationChange(e) {
        //var dropdownlist = $("#inputLocationdd").data("kendoDropDownList");
        //var getLocation = dropdownlist.value();
        //console.log(getLocation)
        populateDesignationMultiFromLocation();
    }
    function populateDesignationMultiFromLocation() {
        var dropdownlist = $("#inputLocationdd").data("kendoDropDownList");
        var getLocation = dropdownlist.value();
        console.log(getLocation)
        var getDesignation = locationdesignationdata.filter(a => a.Location == getLocation).map(a => a.Designation)[0];

        designationdata = [];
        for (var i = 0; i < getDesignation.length; i++) {
            designationdata.push({ text: getDesignation[i], value: getDesignation[i] });
        }
        console.log(designationdata)

        var multiselect = $("#inputDesignationmulti").data("kendoMultiSelect");
        if (multiselect == undefined) {
            $("#inputDesignationmulti").kendoMultiSelect({
                filter: "contains",
                dataTextField: "text",
                dataValueField: "value",
                dataSource: designationdata,
                placeholder: " ",
                index: 0,
                autoBind: true,
                autoClose: false,
            });
        }
        else {
            //$("#inputDesignationmulti").data("kendoMultiSelect").dataSource.read();
            multiselect.setDataSource(designationdata);
        }
        //var multiselect = $("#inputDesignationmulti").data("kendoMultiSelect");
    }

    $("#AddNew").on("click", function () {
        $("#success").css("display", "none");
        $("#error").css("display", "none");

        var model;
        var dialog = $("#windowEdit").data("kendoWindow");
        locationname = "";
        $(".k-overlay").css("display", "block");
        $(".k-overlay").css("opacity", "0.5");
        dialog.open();
        dialog.center();

        reformatLocationDesignation();
        datamodelforedit = model;

        var multiselect = $("#inputDesignationmulti").data("kendoMultiSelect");
        designationdata = [];
        if (multiselect != undefined)
            multiselect.setDataSource(designationdata);
        //multiselect.dataSource.filter({});
        //multiselect.value([""]);

        //var select = $("#inputLocationdd").data("kendoDropDownList");
        $("#inputLocationdd").data('kendoDropDownList').value("");
        //dialog.title("New Leadership Role Creation");
    });

    function reformatLocationDesignation() {
        if (locdesgdata.length > 0) {
            //locationdata
            console.log(locdesgdata)
            locdesgdata.forEach(function (item, index) {
                locationdata = locationdata.filter(a => a.text != item.Location);
                //locationdesignationdata = locationdesignationdata.filter(a => a.Location != item.Location);

            });

            var inputselect = $("#inputLocationdd").data("kendoDropDownList");
            if (inputselect != undefined) {
                inputselect.dataSource.data([]);
                inputselect.setDataSource(locationdata);
            }
        }
    }

    function checkIsLocationExists(val) {
        var isExists = false;
        var LocationDesignationList = $("#gridLocationDesignation").data("kendoGrid").dataSource._data;
        if (LocationDesignationList != null && LocationDesignationList.length > 0) {
            var filterData = LocationDesignationList.filter(m => m.Location === val);
            if (filterData != null && filterData.length > 0) {
                isExists = true;
            }
        }
        return isExists;
    };

    $("#btnSubmit").on("click", function () {
        //console.log(datamodelforedit)
        var multiselect = $("#inputDesignationmulti").data("kendoMultiSelect");
        var select = $("#inputLocationdd").data("kendoDropDownList");
        if (select.value().length == 0) {
            toastr.error("Please select Location");
            //$("#leadershiprolename").focus();
            return false;
        }
        if ((datamodelforedit == undefined || datamodelforedit.length == 0) && checkIsLocationExists(select.value())) {
            toastr.error("Location already exists.");
            return false;
        }
        //if (multiselect.value().length == 0) {
        //    toastr.error("Please select Designation");
        //    //$("#leadershiprolename").focus();
        //    return false;
        //}

        //console.log(multiselect.value().toString());

        //var model;
        if (datamodelforedit != undefined && datamodelforedit.length > 0) {
            //model = datamodelforedit;
            // model.ID = datamodelforedit.ID;
            model = {
                "ID": datamodelforedit.ID,
                "Location": select.value(),
                //"Remark": $("#remark").val(),
                "Designation": multiselect.value().toString(),
                //"CreatedOn": Utility.CurrentDate(),
                //"IsActive": true,
            }

        }
        else {
            model = {
                "ID": 0,
                "Location": select.value(),
                //"Remark": $("#remark").val(),
                "Designation": multiselect.value().toString(),
                //"CreatedOn": Utility.CurrentDate(),
                //"IsActive": true,
            }
        }

        $("#btnSubmit").attr('disabled', 'disabled');
        HttpClient.MakeSyncRequest(CookBookMasters.SaveLocationDesignationData, function (result) {
            if (result == false) {
                $('#btnSubmit').removeAttr("disabled");
                //$("#error").css("display", "flex");
                toastr.error("Some error occured, please try again");
                // $("#error").find("p").text("Some error occured, please try again");
                //$("#sitealias").focus();
            }
            else {
                $(".k-overlay").hide();
                //$("#error").css("display", "flex");
                var orderWindow = $("#windowEdit").data("kendoWindow");
                orderWindow.close();
                $('#btnSubmit').removeAttr("disabled");
                //$("#success").css("display", "flex");
                if (model.ID > 0) {
                    toastr.success("Configuration saved");
                }
                else {
                    toastr.success("Configuration saved");
                }

                // $("#success").find("p").text("Color updated successfully");
                $("#gridLocationDesignation").data("kendoGrid").dataSource.data([]);
                $("#gridLocationDesignation").data("kendoGrid").dataSource.read();
                //$("#gridLeaderShipRole").data("kendoGrid").dataSource.sort({ field: "LeaderShipRole", dir: "asc" });
            }
        }, {
            location: select.value(),
            designation: multiselect.value().toString()
        }, true);

    });

    $("#btnCancel").on("click", function () {
        //Utility.Page_Alert_Save("All Changes will be Lost. Do you want to Continue?", "Confirmation", "Yes", "No",
        //    function () {
        //        $(".k-overlay").hide();
        //        var orderWindow = $("#windowEdit").data("kendoWindow");
        //        orderWindow.close();
        //    },
        //    function () {
        //    }
        //);
        $(".k-overlay").hide();
        var orderWindow = $("#windowEdit").data("kendoWindow");
        orderWindow.close();
    });
    //$("#inputNutrient").kendoMultiSelect({
    //    filter: "contains",
    //    dataTextField: "Name",
    //    dataValueField: "NutritionElementCode",
    //    dataSource: nutrientFilterDataSource,
    //    placeholder: " ",
    //    index: 0,
    //    autoBind: true,
    //    autoClose: false,
    //});
    //var multiselect = $("#inputNutrient").data("kendoMultiSelect");

    function populateLocationDesignationGrid() {
        Utility.Loading();
        var gridVariable = $("#gridLocationDesignation");
        gridVariable.html("");
        gridVariable.kendoGrid({
            excel: {
                fileName: "LocationDesignation.xlsx",
                filterable: true,
                allPages: true
            },
            // sortable: true,
            editable: "inline",
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            pageable: true,
            groupable: false,
            //reorderable: true,
            //scrollable: true,
            // toolbar: ["create"],
            columns: [
                {
                    field: "Location", title: "Location", width: "300px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    },

                },
                {
                    field: "Designation", title: "Designation",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    template: function (dataItem) {
                        var html = '<span class="txtdesig" style="margin-left:8px"></span>'
                        return html;
                    },
                },
                {
                    field: "Edit", title: "Action", width: "60px",
                    attributes: {
                        style: "text-align: center; font-weight:normal"
                    },

                    headerAttributes: {
                        style: "text-align: center"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                $("#success").css("display", "none");
                                $("#error").css("display", "none");
                                var gridObj = $("#gridLocationDesignation").data("kendoGrid");
                                var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                datamodelforedit = tr;
                                //$("#leaderShipRoleid").val(tr.ID);
                                //$("#leadershiprolename ").val(tr.LeaderShipRole);
                                //$("#leadershiprolecode ").val(tr.LeadershipRoleCode);
                                //$("#remark").val(tr.Remark);
                                locationname = tr.Location;
                                if (locationdata.length > 0) {

                                    locdesgdata.forEach(function (item, index) {
                                        locationdata = locationdata.filter(a => a.text != item.Location);
                                        //locationdesignationdata = locationdesignationdata.filter(a => a.Location != item.Location);

                                    });

                                    var chkExist = locationdata.filter(a => a.text == locationname)[0];
                                    if (chkExist == undefined) {
                                        locationdata.push({ text: locationname, value: locationname });
                                        var inputselect = $("#inputLocationdd").data("kendoDropDownList");
                                        if (inputselect != undefined) {
                                            inputselect.dataSource.data([]);
                                            inputselect.setDataSource(locationdata);
                                        }
                                    }
                                }
                                $("#inputLocationdd").data('kendoDropDownList').value(tr.Location);

                                onLocationChange();

                                setTimeout(function () {
                                    var multiselect = $("#inputDesignationmulti").data("kendoMultiSelect");
                                    //designationdata = [];
                                    //if (multiselect != undefined)
                                    //    multiselect.setDataSource(designationdata);
                                    //multiselect.dataSource.filter({});
                                    var desig = tr.Designation.split(',');
                                    if (multiselect != undefined)
                                        multiselect.value(desig);

                                    //var select = $("#inputLocationdd").data("kendoDropDownList");                                

                                    var dialog = $("#windowEdit").data("kendoWindow");

                                    $(".k-overlay").css("display", "block");
                                    $(".k-overlay").css("opacity", "0.5");
                                    dialog.open();
                                    dialog.center();

                                    return true;

                                }, 50);
                            }
                        },
                    ]
                },

            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        //var varCodes = "";
                        HttpClient.MakeRequest(CookBookMasters.GetLocationDesignationMappingList, function (result) {
                            Utility.UnLoading();
                            locdesgdata = [];
                            result.forEach(function (item, index) {
                                var chkExisting = locdesgdata.filter(a => a.Location == item.Location)[0];
                                if (chkExisting == undefined) {
                                    var locFilter = result.filter(a => a.Location == item.Location).map(a => a.Designation);
                                    //console.log(locFilter)
                                    locdesgdata.push({ Designation: locFilter.toString(), Location: item.Location });
                                }
                            });
                            if (result != null) {
                                options.success(locdesgdata);
                            }
                            else {
                                options.success("");
                            }

                        }, null, false);
                        //HttpClient.MakeRequest(CookBookMasters.GetLeaderShipRoleDataList, function (result) {
                        //    Utility.UnLoading();

                        //    if (result != null) {
                        //        options.success(result);
                        //    }
                        //    else {
                        //        options.success("");
                        //    }
                        //}, null, false);
                    },
                    //update: function (options) {
                    //    updateLeaderShipRole(options.data);
                    //},
                    destroy: {
                        // url: crudServiceBaseUrl + "/Products/Destroy",
                        dataType: "jsonp"
                    },
                    create: {
                        //  url: crudServiceBaseUrl + "/Products/Create",
                        dataType: "jsonp"
                    },
                    parameterMap: function (options, operation) {
                        if (operation !== "read" && options.models) {
                            return { models: kendo.stringify(options.models) };
                        }
                    }

                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Location: { editable: false, type: "string" },
                            Designation: { editable: false, type: "string" }
                            //LeadershipRoleCode: { editable: false, type: "string" },
                            //Remark: { type: "string", editable: false, },
                            //IsActive: { editable: false }
                        }
                    }
                },
                pageSize: 150,
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            noRecords: {
                template: "No Records Available"
            },
            dataBound: function (e) {
                //var items = e.sender.items();
                var grid = $("#gridLocationDesignation").data("kendoGrid");
                //    var tr = gridObj.dataItem($(this).closest("tr"));
                //    datamodel = tr;

                grid.tbody.find("tr[role='row']").each(function () {
                    var model = grid.dataItem(this);

                    //if (!model.IsActive) {
                    $(this).find(".txtdesig").html(model.Designation.toString());
                    //}
                });

                //$(".chkbox").on("change", function () {
                //    // 
                //    $("#success").css("display", "none");
                //    $("#error").css("display", "none");
                //    var gridObj = $("#gridLocationDesignation").data("kendoGrid");
                //    var tr = gridObj.dataItem($(this).closest("tr"));
                //    datamodel = tr;
                //    datamodel.IsActive = $(this)[0].checked;
                //    var active = "";
                //    if (datamodel.IsActive) {
                //        active = "Active";
                //    } else {
                //        active = "Inactive";
                //    }

                //    Utility.Page_Alert_Save("Are you sure to mark <b>" + datamodel.ColorName + "</b> Color " + active + "?", "Color Update Confirmation", "Yes", "No", function () {
                //        $("#error").css("display", "none");
                //        HttpClient.MakeRequest(CookBookMasters.ChangeStatus, function (result) {
                //            if (result == false) {
                //                $("#error").css("display", "flex");
                //                $("#error").find("p").text("Some error occured, please try again");
                //            }
                //            else {
                //                $("#error").css("display", "flex");
                //                $("#success").css("display", "flex");
                //                $("#success").find("p").text("Color updated successfully");
                //            }
                //        }, {
                //            model: datamodel
                //        }, false);
                //    }, function () {
                //        $(this)[0].checked = !datamodel.IsActive;
                //        //$("#gridColor").data("kendoGrid").dataSource.data([]);
                //        //$("#gridColor").data("kendoGrid").dataSource.read();
                //    });
                //    return true;
                //});
            },
            change: function (e) {
            },
            excelExport: function onExcelExport(e) {
                var sheet = e.workbook.sheets[0];
                var data = e.data;
                var cols = Object.keys(data[0]);
                //var columns = cols.filter(function (col) {
                //    if (col == "id" || col == "_events" || col == "uid" || col == "dirty" || col == "_handlers" || col == "parent" || col == "_handlers" || col == "uid" || col=="Action") { }
                //    else
                //        return col;
                //});
                columns = ["Location", "Designation"];
                var columns1 = columns.map(function (col) {
                    return {
                        value: col,
                        autoWidth: true,
                        background: "#7a7a7a",
                        color: "#fff"
                    };
                });
                //console.log(cols);
                var rows = [{ cells: columns1, type: "header" }];

                for (var i = 0; i < data.length; i++) {
                    var rowCells = [];
                    for (var j = 0; j < columns.length; j++) {
                        var cellValue = data[i][columns[j]];
                        rowCells.push({ value: cellValue });
                    }
                    rows.push({ cells: rowCells, type: "data" });
                }
                sheet.rows = rows;
            }

        })
        //$("#gridLocationDesignation").data("kendoGrid").dataSource.sort({ field: "ColorCode", dir: "asc" });
        //$(".k-label")[0].innerHTML.replace("items", "records");
    }

});