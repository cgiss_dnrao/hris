﻿var FoodProgramCodeInitial = null;
var tileImage = null;
var VisualCategoryCodeInitial = null;
var DishCategoryCodeIntial = null;
var DishSubCategoryCodeInitial = null;
var DietCategoryCodeInitial = null;
var CT1CodeInitial = null;
var CT2CodeInitial = null;
var CT3CodeInitial = null;
var CT4CodeInitial = null;
var CT5CodeInitial = null;
var ServingTempCodeInitial = null;
var UOMCodeInitial = null;
var ServewareCodeInitial = null;
var SiteProfile1CodeInitial = null;
var SiteProfile2CodeInitial = null;
var SiteProfile3CodeInitial = null;
var ItemType1CodeInitial = null;
var ItemType2CodeInitial = null;
var AllergenCodeInitial = null;
var DayPartInitial = null;
var conceptype2data = [];
var item = null;

$(function () {
    var user;
    var status = "";
    var varname = "";
  
    $(document).ready(function () {
        HttpClient.MakeRequest(CookBookMasters.GetLoginUserDetailsUrl, function (result) {
            user = result;
            if (user.UserRoleId === 1) {
                $("#InitiateBulkChanges").css("display", "inline");
                $("#fpaddnew").css("display", "inline");
                $("#fnaddnew").css("display", "inline");
                $("#aladdnew").css("display", "inline");
                $("#vcaddnew").css("display", "inline");
                $("#dpaddnew").css("display", "inline");
                $("#dcaddnew").css("display", "inline");
                $("#dtaddnew").css("display", "inline");
                $("#scaddnew").css("display", "inline");
                $("#ct1addnew").css("display", "inline");
                $("#ct2addnew").css("display", "inline");
                $("#ct3addnew").css("display", "inline");
                $("#ct4addnew").css("display", "inline");
                $("#ct5addnew").css("display", "inline");
                $("#it1addnew").css("display", "inline");
                $("#it2addnew").css("display", "inline");
                $("#it3addnew").css("display", "inline");
                $("#sp1addnew").css("display", "inline");
                $("#sp2addnew").css("display", "inline");
                $("#sp3addnew").css("display", "inline");
                $("#swaddnew").css("display", "inline");
                $("#umaddnew").css("display", "inline");
                $("#staddnew").css("display", "inline");
            }
            if (user.UserRoleId === 2) {
                $("#InitiateBulkChanges").css("display", "none");
                $("#fpaddnew").css("display", "none");
                $("#fnaddnew").css("display", "none");
                $("#aladdnew").css("display", "none");
                $("#vcaddnew").css("display", "none");
                $("#dpaddnew").css("display", "none");
                $("#dcaddnew").css("display", "none");
                $("#dtaddnew").css("display", "none");
                $("#scaddnew").css("display", "none");
                $("#ct1addnew").css("display", "none");
                $("#ct2addnew").css("display", "none");
                $("#ct3addnew").css("display", "none");
                $("#ct4addnew").css("display", "none");
                $("#ct5addnew").css("display", "none");
                $("#it1addnew").css("display", "none");
                $("#it2addnew").css("display", "none");
                $("#it3addnew").css("display", "none");
                $("#sp1addnew").css("display", "none");
                $("#sp2addnew").css("display", "none");
                $("#sp3addnew").css("display", "none");
                $("#swaddnew").css("display", "none");
                $("#umaddnew").css("display", "none");
                $("#staddnew").css("display", "none");
            }
            populateFoodProgramGrid();
            populateFoodPanGrid();
            populateAllergenGrid();
            populateVisualCategoryGrid();
            populateDayPartGrid();
            populateDietCategoryGrid();
            populateDishCategoryGrid();
            populateDishSubCategoryGrid();
            populateConceptType1Grid();
            populateConceptType2Grid();
            populateConceptType3Grid();
            populateConceptType4Grid();
            populateConceptType5Grid();
            populateItemType1Grid();
            populateItemType2Grid();
            populateServewareGrid();
            populateSiteProfile1Grid();
            populateSiteProfile2Grid();
            populateSiteProfile3Grid();
            populateUOMGrid();
            populateServingTemperatureGrid();
        }, null, true);
        HttpClient.MakeRequest(CookBookMasters.GetConceptType2DataList, function (data) {

            var dataSource = data;
            conceptype2data = [];
            conceptype2data.push({ "value": "Select", "text": "Select" });
            for (var i = 0; i < dataSource.length; i++) {
                conceptype2data.push({ "value": dataSource[i].ConceptType2Code, "text": dataSource[i].Name });
            }
            populateConceptType2Dropdown();
        }, null, true);
    });

    function populateConceptType2Dropdown() {
        $("#fptype").kendoDropDownList({
            filter: "contains",
            dataTextField: "text",
            dataValueField: "value",
            dataSource: conceptype2data,
            index: 0,
        });
    }

    //Allergen Section Started
    //Food Program Section Start

    function populateAllergenGrid() {

        Utility.Loading();
        var gridVariable = $("#gridAllergen");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "110px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "70px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive

                                };
                                AllergenCodeInitial = item.AllergenCode;
                                $("#alsuccess").css("display", "none");
                                $("#alerror").css("display", "none");
                                $('#alsubmit').removeAttr("disabled");
                                $("#gridAllergen").css("display", "none");
                                $("#newAllergen").css("display", "inline");
                                $("#aladdnew").css("display", "none");
                                $("#alid").val(model.ID);
                                $("#alname").val(model.Name);
                                $("#alactive").prop('checked', model.IsActive);
                                $("#alname").focus();
                                $("#alname").removeClass("is-invalid");
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetAllergenDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {

                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#alcancel").on("click", function () {
        $("#gridAllergen").css("display", "inline-block");
        $("#newAllergen").css("display", "none");
        $("#aladdnew").css("display", "inline");
    });

    $("#aladdnew").on("click", function () {
        $("#alsuccess").css("display", "none");
        $("#alerror").css("display", "none");
        $('#alsubmit').removeAttr("disabled");
        $("#gridAllergen").css("display", "none");
        $("#newAllergen").css("display", "inline");
        $("#aladdnew").css("display", "none");
        $("#alid").val("");
        $("#alname").val("");
        $("#alname").removeClass("is-invalid");
        $("#alactive").prop('checked', true);
        $("#alname").focus();
        AllergenCodeInitial = null;
    });

    function alvalidate() {
        var valid = true;

        if ($("#alname").val() === "") {
            $("#alerror").css("display", "flex");
            $("#alerror").find("p").text("Please provide input");
            $("#alname").addClass("is-invalid");
            valid = false;
            $("#alname").focus();
        }
        if (($.trim($("#alname").val())).length > 30) {
            $("#alerror").css("display", "flex");
            $("#alerror").find("p").text("Food Name accepts upto 30 charactres");
            $("#alname").addClass("is-invalid");
            valid = false;
            $("#alname").focus();
        }
        return valid;
    }

    $("#alsubmit").click(function () {

        if (alvalidate() === true) {
            $("#alname").removeClass("is-invalid");
            $("#alerror").css("display", "none");
            //form.submit();
            var model = {
                "ID": $("#alid").val(),
                "Name": $("#alname").val(),
                "IsActive": $("#alactive")[0].checked,
                "AllergenCode": AllergenCodeInitial
            }
            $("#alsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveAllergen, function (result) {
                if (result == false) {
                    $('#alsubmit').removeAttr("disabled");
                    $("#alerror").css("display", "flex");
                    $("#alerror").find("p").text("Some error occured, please try again");
                    $("#alname").focus();
                }
                else {

                    if (result == "Duplicate") {
                        $('#alsubmit').removeAttr("disabled");
                        $("#alerror").css("display", "flex");
                        $("#alerror").find("p").text("Duplicate Record, use different name");
                        $("#alname").focus();
                    } else {
                        $("#alerror").css("display", "inline");
                        $(".k-overlay").hide();
                        $('#alsubmit').removeAttr("disabled");
                        $("#alsuccess").css("display", "flex");
                        if (model.ID > 0)
                            $("#alsuccess").find("p").text("Record updated successfully");
                        else
                            $("#alsuccess").find("p").text("Record created successfully");
                        //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                        $("#gridAllergen").data("kendoGrid").dataSource.data([]);
                        $("#gridAllergen").data("kendoGrid").dataSource.read();
                        $("#gridAllergen").css("display", "inline-block");
                        $("#newAllergen").css("display", "none");
                        $("#aladdnew").css("display", "inline");

                        setTimeout(fade_out, 10000);

                        function fade_out() {
                            $("#alsuccess").fadeOut();
                        }
                    }
                    //});
                }
            }, {
                model: model
            }, true);

        }
    });

    //Allergen End


    //Food Program Section Start

    function populateFoodProgramGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridFoodProgram");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "", title: "Image", width: "35px", attributes: {
                        style: "text-align: center; font-weight:normal"
                    },
                    template: "<div class='item-photo'" +
                        "style='background-image: url(FoodProgramImages/#:data.TileImage#);'></div>", width: "35px"
                },
                {
                    field: "Name", title: "Name", width: "80px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "ConceptType2Name", title: "Type", width: "50px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "Description", title: "Brand Story", width: "150px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "70px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                item = this.dataItem(tr);          // get the date of this row
                                var model = item;
                                FoodProgramCodeInitial = item.FoodProgramCode;
                                tileImage = item.TileImage;
                                $("#fpsuccess").css("display", "none");
                                $("#fperror").css("display", "none");
                                $('#fpsubmit').removeAttr("disabled");
                                $("#gridFoodProgram").css("display", "none");
                                $("#newFoodProgram").css("display", "inline");
                                $("#fpaddnew").css("display", "none");
                                $("#fpid").val(model.ID);
                                $("#fpname").val(model.Name);
                                $("#fptype").data('kendoDropDownList').value(model.ConceptType2Code);
                                $("#fpdesc").val(model.Description);
                                $("#fpactive").prop('checked', model.IsActive);
                                $("#file").val("");
                                $("#image").attr('src', 'FoodProgramImages/' + model.TileImage);
                                $("#fpname").focus();
                                $("#fpname").removeClass("is-invalid");
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetFoodProgramDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#fpcancel").on("click", function () {
        $("#gridFoodProgram").css("display", "inline-block");
        $("#newFoodProgram").css("display", "none");
        $("#fpaddnew").css("display", "inline");
    });

    $("#fpaddnew").on("click", function () {
        $("#fpsuccess").css("display", "none");
        $("#fperror").css("display", "none");
        $('#fpsubmit').removeAttr("disabled");
        $("#gridFoodProgram").css("display", "none");
        $("#newFoodProgram").css("display", "inline");
        $("#fpaddnew").css("display", "none");
        $("#fpid").val("");
        $("#fpname").val("");
        $("#file").val("");
        $("#image").attr('src', 'FoodProgramImages/default.png');
        $("#fpname").removeClass("is-invalid");
        $("#fpactive").prop('checked', true);
        $("#fpname").focus();
        item = null;
        FoodProgramCodeInitial = null;
    });

    function fpvalidate() {
        var valid = true;

        if ($("#fpname").val() === "") {
            $("#fperror").css("display", "flex");
            $("#fperror").find("p").text("Please provide input");
            $("#fpname").addClass("is-invalid");
            valid = false;
            $("#fpname").focus();
        }
        if (($.trim($("#fpname").val())).length > 30) {
            $("#fperror").css("display", "flex");
            $("#fperror").find("p").text("Food Name accepts upto 30 charactres");
            $("#fpname").addClass("is-invalid");
            valid = false;
            $("#fpname").focus();
        }
        return valid;
    }

    $('#btnUpload').click(function () {

        $('#txtFilePath').removeClass('validationError');
        $('#file').val('').trigger('click');
    });

    function validateImportFile(filename) {

        if (filename != '') {
            var fileExtension = ['png', 'jpg'];
            if ($.inArray(filename.split('.').pop().toLowerCase(), fileExtension) == -1) {
                Utility.Page_Alert(ExtensionAllowedMessage);
                return false;
            }
        }
        return true;
    };


    $('#file').change(function () {

        var that = this;
        if (validateImportFile(that.value)) {
            //$('#txtFilePath').val(that.value);
            $('#txtFilePath').val(that.files[0].name);
            if (that.files[0]) {
                var uploadimg = new FileReader();
                uploadimg.onload = function (displayimg) {
                    $("#image").attr('src', displayimg.target.result);
                }
                uploadimg.readAsDataURL(that.files[0]);
            }
        }
        else {
            //ImportReport.refreshPage();
        }
    });

    $("#fpsubmit").click(function () {
        
        var fpcode = "";    
        if (fpvalidate() === true) {
            $("#fpname").removeClass("is-invalid");
            $("#fperror").css("display", "none");
            //form.submit();
            var model = item;
            if (model == null) {
                model = {
                    "ID": $("#fpid").val(),
                    "Name": $("#fpname").val(),
                    "IsActive": $("#fpactive")[0].checked,
                    "FoodProgramCode": FoodProgramCodeInitial,
                    "ConceptType2Code": $("#fptype").val(),
                    "Description": $("#fpdesc").val(),
                    "TileImage": tileImage,
                }
            } else {
                model.Name = $("#fpname").val();
                model.IsActive = $("#fpactive")[0].checked;
                model.FoodProgramCode = FoodProgramCodeInitial;
                model.ConceptType2Code = $("#fptype").val();
                model.Description = $("#fpdesc").val();
                model.TileImage = tileImage;
            }
            if (document.getElementById("file").files.count > 0) {
                model.TileImage = model.FoodProgramCode + '.png'
            }
            fpcode = model.FoodProgramCode;
            $("#fpsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveFoodProgram, function (result) {
                if (result == false) {
                    $('#fpsubmit').removeAttr("disabled");
                    $("#fperror").css("display", "flex");
                    $("#fperror").find("p").text("Some error occured, please try again");
                    $("#fpname").focus();
                }
                else {
                    
                    if (result == "Duplicate") {
                        $('#fpsubmit').removeAttr("disabled");
                        $("#fperror").css("display", "flex");
                        $("#fperror").find("p").text("Duplicate Record, use different name");
                        $("#fpname").focus();
                    } else {
                        $("#fperror").css("display", "inline");
                        $(".k-overlay").hide();
                        $('#fpsubmit').removeAttr("disabled");
                        $("#fpsuccess").css("display", "flex");
                        if (model.ID > 0)
                            $("#fpsuccess").find("p").text("Record updated successfully");
                        else
                            $("#fpsuccess").find("p").text("Record created successfully");
                        //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                        $("#gridFoodProgram").data("kendoGrid").dataSource.data([]);
                        $("#gridFoodProgram").data("kendoGrid").dataSource.read();
                        $("#gridFoodProgram").css("display", "inline-block");
                        $("#newFoodProgram").css("display", "none");
                        $("#fpaddnew").css("display", "inline");

                        setTimeout(fade_out, 10000);

                        function fade_out() {
                            $("#fpsuccess").fadeOut();
                        }
                    }
                    //});
                }
            }, {
                model: model
            }, true);

            var formData = new FormData();
            var file = document.getElementById("file").files[0];
            formData.append('FoodProgramCode', fpcode);
            formData.append('folderName', "FoodProgramImages");
            formData.append("file", file);
            $.ajax({
                type: "POST",
                url: 'Item/UploadFile',
                data: formData,
                contentType: false,
                processData: false,
                success: function (response) {

                },
                error: function (error) {
                    Utility.UnLoading();
                    Utility.Page_Alert("Problem while uploading the Food Program Image on Server. Please contact Administrator", "Success", "OK", function () {
                    });
                }
            });   
        }
    });
   
    //Food Program Section End


    //Food Pan Section Start

    function populateFoodPanGrid() {

        Utility.Loading();
        var gridVariable = $("#gridFoodPan");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "110px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "70px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {

                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                item = this.dataItem(tr);          // get the date of this row
                                var model = item;
                                FoodPanCodeInitial = item.FoodPanCode;
                                $("#fnsuccess").css("display", "none");
                                $("#fnerror").css("display", "none");
                                $('#fnsubmit').removeAttr("disabled");
                                $("#gridFoodPan").css("display", "none");
                                $("#newFoodPan").css("display", "inline");
                                $("#fnaddnew").css("display", "none");
                                $("#fnid").val(model.ID);
                                $("#fnname").val(model.Name);
                                $("#fnsize").val(model.Size);
                                $("#fndepth").val(model.Depth);
                                $("#fnactive").prop('checked', model.IsActive);
                                $("#fnname").focus();
                                $("#fnname").removeClass("is-invalid");
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {

                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetFoodPanDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {

                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#fncancel").on("click", function () {
        $("#gridFoodPan").css("display", "inline-block");
        $("#newFoodPan").css("display", "none");
        $("#fnaddnew").css("display", "inline");
    });

    $("#fnaddnew").on("click", function () {
        $("#fnsuccess").css("display", "none");
        $("#fnerror").css("display", "none");
        $('#fnsubmit').removeAttr("disabled");
        $("#gridFoodPan").css("display", "none");
        $("#newFoodPan").css("display", "inline");
        $("#fnaddnew").css("display", "none");
        $("#fnname").css("display", "block");
        $("#fnid").val("");
        $("#fnname").val("");
        $("#fnsize").val("");
        $("#fndepth").val("");
        $("#fnname").removeClass("is-invalid");
        $("#fnactive").prop('checked', true);
        $("#fnname").focus();
        item = null;
        FoodPanCodeInitial = null;
    });

    function fnvalidate() {
        var valid = true;

        if ($("#fnname").val() === "") {
            $("#fnerror").css("display", "flex");
            $("#fnerror").find("p").text("Please provide input");
            $("#fnname").addClass("is-invalid");
            valid = false;
            $("#fnname").focus();
        }
        if (($.trim($("#fnname").val())).length > 30) {
            $("#fnerror").css("display", "flex");
            $("#fnerror").find("p").text("Food Pan Name accepts upto 30 charactres");
            $("#fnname").addClass("is-invalid");
            valid = false;
            $("#fnname").focus();
        }
        return valid;
    }

    $("#fnsubmit").click(function () {

        var fncode = "";
        if (fnvalidate() === true) {
            $("#fnname").removeClass("is-invalid");
            $("#fnerror").css("display", "none");
            //form.submit();
            var model = item;
            if (model == null) {
                model = {
                    "ID": $("#fnid").val(),
                    "Name": $("#fnname").val(),
                    "IsActive": $("#fnactive")[0].checked,
                    "FoodPanCode": FoodPanCodeInitial,
                    "Size": $("#fnsize").val(),
                    "Depth": $("#fndepth").val(),
                }
            } else {
                model.Name = $("#fnname").val();
                model.IsActive = $("#fnactive")[0].checked;
                model.FoodPanCode = FoodPanCodeInitial;
                model.Size = $("#fnsize").val();
                model.Depth = $("#fndepth").val();
            }
            
            fncode = model.FoodPanCode;
            $("#fnsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveFoodPan, function (result) {
                if (result == false) {
                    $('#fnsubmit').removeAttr("disabled");
                    $("#fnerror").css("display", "flex");
                    $("#fnerror").find("p").text("Some error occured, please try again");
                    $("#fnname").focus();
                }
                else {

                    if (result == "Duplicate") {
                        $('#fnsubmit').removeAttr("disabled");
                        $("#fnerror").css("display", "flex");
                        $("#fnerror").find("p").text("Duplicate Record, use different name");
                        $("#fnname").focus();
                    } else {
                        $("#fnerror").css("display", "inline");
                        $(".k-overlay").hide();
                        $('#fnsubmit').removeAttr("disabled");
                        $("#fnsuccess").css("display", "flex");
                        if (model.ID > 0)
                            $("#fnsuccess").find("p").text("Record updated successfully");
                        else
                            $("#fnsuccess").find("p").text("Record created successfully");
                        //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                        $("#gridFoodPan").data("kendoGrid").dataSource.data([]);
                        $("#gridFoodPan").data("kendoGrid").dataSource.read();
                        $("#gridFoodPan").css("display", "inline-block");
                        $("#newFoodPan").css("display", "none");
                        $("#fnaddnew").css("display", "inline");

                        setTimeout(fade_out, 10000);

                        function fade_out() {
                            $("#fnsuccess").fadeOut();
                        }
                    }
                    //});
                }
            }, {
                model: model
            }, true);
        }
    });

    //Food Pan Section End


    function populateVisualCategoryGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridVisualCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive,
                                    "VisualCategoryCode": item.VisualCategoryCode
                                };
                                VisualCategoryCodeInitial = model.VisualCategoryCode;
                                $("#vcname").removeClass("is-invalid");
                                $("#vcsuccess").css("display", "none");
                                $("#vcerror").css("display", "none");
                                $('#vcsubmit').removeAttr("disabled");
                                $("#gridVisualCategory").css("display", "none");
                                $("#newVisualCategory").css("display", "inline");
                                $("#vcaddnew").css("display", "none");
                                $("#vcid").val(model.ID);
                                $("#vcname").val(model.Name);
                                $("#vcactive").prop('checked', model.IsActive);
                                $("#vcname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetVisualCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#vccancel").on("click", function () {
        $("#gridVisualCategory").css("display", "inline-block");
        $("#newVisualCategory").css("display", "none");
        $("#vcaddnew").css("display", "inline");
    });

    $("#vcaddnew").on("click", function () {
        $("#vcname").removeClass("is-invalid");
        $("#vcsuccess").css("display", "none");
        $("#vcerror").css("display", "none");
        $('#vcsubmit').removeAttr("disabled");
        $("#gridVisualCategory").css("display", "none");
        $("#newVisualCategory").css("display", "inline");
        $("#vcaddnew").css("display", "none");
        $("#vcid").val("");
        $("#vcname").val("");
        $("#vcactive").prop('checked', true);
        $("#vcname").focus();
        VisualCategoryCodeInitial = null;
    });

    function vcvalidate() {
        var valid = true;

        if ($("#vcname").val() === "") {
            $("#vcerror").css("display", "flex");
            $("#vcerror").find("p").text("Please provide input");
            $("#vcname").addClass("is-invalid");
            valid = false;
            $("#vcname").focus();
        }
        if (($.trim($("#vcname").val())).length > 30) {
            $("#vcerror").css("display", "flex");
            $("#vcerror").find("p").text("Visual Category accepts upto 30 charactres");
            $("#vcname").addClass("is-invalid");
            valid = false;
            $("#vcname").focus();
        }
        return valid;
    }

    $("#vcsubmit").click(function () {
         
        if(vcvalidate() === true)  {
            $("#vcname").removeClass("is-invalid");
            $("#vcerror").css("display", "none");
            var model = {
                "ID": $("#vcid").val(),
                "Name": $("#vcname").val(),
                "IsActive": $("#vcactive")[0].checked,
                "VisualCategoryCode": VisualCategoryCodeInitial
            }

            $("#vcsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveVisualCategory, function (result) {
                if (result == false) {
                    $('#vcsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#vcname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#vcsubmit').removeAttr("disabled");
                    $("#vcsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#vcsuccess").find("p").text("Record updated successfully");
                    else
                        $("#vcsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridVisualCategory").data("kendoGrid").dataSource.data([]);
                    $("#gridVisualCategory").data("kendoGrid").dataSource.read();
                    $("#gridVisualCategory").css("display", "inline-block");
                    $("#newVisualCategory").css("display", "none");
                    $("#vcaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#vcsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateDayPartGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridDayPart");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                DayPartInitial = item.DayPartCode;
                                $("#dpname").removeClass("is-invalid");
                                $("#dpsuccess").css("display", "none");
                                $("#dperror").css("display", "none");
                                $('#dpsubmit').removeAttr("disabled");
                                $("#gridDayPart").css("display", "none");
                                $("#newDayPart").css("display", "inline");
                                $("#dpaddnew").css("display", "none");
                                $("#dpid").val(model.ID);
                                $("#dpname").val(model.Name);
                                $("#dpactive").prop('checked', model.IsActive);
                                $("#dpname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetDayPartDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#dpcancel").on("click", function () {
        $("#gridDayPart").css("display", "inline-block");
        $("#newDayPart").css("display", "none");
        $("#dpaddnew").css("display", "inline");
    });

    $("#dpaddnew").on("click", function () {
        $("#dpname").removeClass("is-invalid");
        $("#dpsuccess").css("display", "none");
        $("#dperror").css("display", "none");
        $('#dpsubmit').removeAttr("disabled");
        $("#gridDayPart").css("display", "none");
        $("#newDayPart").css("display", "inline");
        $("#dpaddnew").css("display", "none");
        $("#dpid").val("");
        $("#dpname").val("");
        $("#dpactive").prop('checked', true);
        $("#dpname").focus();
        DayPartInitial = null;
    });

    function dpvalidate() {
        var valid = true;

        if ($("#dpname").val() === "") {
            $("#dperror").css("display", "flex");
            $("#dperror").find("p").text("Please provide input");
            $("#dpname").addClass("is-invalid");
            valid = false;
            $("#dpname").focus();
        }
        if (($.trim($("#dpname").val())).length > 15) {
            $("#dperror").css("display", "flex");
            $("#dperror").find("p").text("Day Part accepts upto 15 charactres");
            $("#dpname").addClass("is-invalid");
            valid = false;
            $("#dpname").focus();
        }
        return valid;
    }

    $("#dpsubmit").click(function () {
        

        if (dpvalidate() === true) {
            $("#dpname").removeClass("is-invalid");
            $("#dperror").css("display", "none");
            var model = {
                "ID": $("#dpid").val(),
                "Name": $("#dpname").val(),
                "IsActive": $("#dpactive")[0].checked,
                "DayPartCode": DayPartInitial 
            }

            $("#dpsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDayPart, function (result) {
                if (result == false) {
                    $('#dpsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#dpname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#dpsubmit').removeAttr("disabled");
                    $("#dpsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#dpsuccess").find("p").text("Record updated successfully");
                    else
                        $("#dpsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridDayPart").data("kendoGrid").dataSource.data([]);
                    $("#gridDayPart").data("kendoGrid").dataSource.read();
                    $("#gridDayPart").css("display", "inline-block");
                    $("#newDayPart").css("display", "none");
                    $("#dpaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#dpsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateDishCategoryGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridDishCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: center;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                DishCategoryCodeIntial = item.DishCategoryCode;
                                $("#dcname").removeClass("is-invalid");
                                $("#dcsuccess").css("display", "none");
                                $("#dcerror").css("display", "none");
                                $('#dcsubmit').removeAttr("disabled");
                                $("#gridDishCategory").css("display", "none");
                                $("#newDishCategory").css("display", "inline");
                                $("#dcaddnew").css("display", "none");
                                $("#dcid").val(model.ID);
                                $("#dcname").val(model.Name);
                                $("#dcactive").prop('checked', model.IsActive);
                                $("#dcname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetDishCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#dccancel").on("click", function () {
        $("#gridDishCategory").css("display", "inline-block");
        $("#newDishCategory").css("display", "none");
        $("#dcaddnew").css("display", "inline");
    });

    $("#dcaddnew").on("click", function () {
        $("#dcname").removeClass("is-invalid");
        $("#dcsuccess").css("display", "none");
        $("#dcerror").css("display", "none");
        $('#dcsubmit').removeAttr("disabled");
        $("#gridDishCategory").css("display", "none");
        $("#newDishCategory").css("display", "inline");
        $("#dcaddnew").css("display", "none");
        $("#dcid").val("");
        $("#dcname").val("");
        $("#dcactive").prop('checked', true);
        $("#dcname").focus();
        DishCategoryCodeIntial = null;
    });

    function dcvalidate() {
        var valid = true;

        if ($("#dcname").val() === "") {
            $("#dcerror").css("display", "flex");
            $("#dcerror").find("p").text("Please provide input");
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        if (($.trim($("#dcname").val())).length > 30) {
            $("#dcerror").css("display", "flex");
            $("#dcerror").find("p").text("Dish Category accepts upto 30 charactres");
            $("#dcname").addClass("is-invalid");
            valid = false;
            $("#dcname").focus();
        }
        return valid;
    }

    $("#dcsubmit").click(function () {
        

        if (dcvalidate() === true) {
            $("#dcname").removeClass("is-invalid");
            $("#dcerror").css("display", "none");
            var model = {
                "ID": $("#dcid").val(),
                "Name": $("#dcname").val(),
                "IsActive": $("#dcactive")[0].checked,
                "DishCategoryCode": DishCategoryCodeIntial
            }

            $("#dcsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishCategory, function (result) {
                if (result == false) {
                    $('#dcsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#dcname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#dcsubmit').removeAttr("disabled");
                    $("#dcsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#dcsuccess").find("p").text("Record updated successfully");
                    else
                        $("#dcsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridDishCategory").data("kendoGrid").dataSource.data([]);
                    $("#gridDishCategory").data("kendoGrid").dataSource.read();
                    $("#gridDishCategory").css("display", "inline-block");
                    $("#newDishCategory").css("display", "none");
                    $("#dcaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#dcsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateDietCategoryGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridDietCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                DietCategoryCodeInitial = item.DietCategoryCode;
                                
                                $("#dtname").removeClass("is-invalid");
                                $("#dtsuccess").css("display", "none");
                                $("#dterror").css("display", "none");
                                $('#dtsubmit').removeAttr("disabled");
                                $("#gridDietCategory").css("display", "none");
                                $("#newDietCategory").css("display", "inline");
                                $("#dtaddnew").css("display", "none");
                                $("#dtid").val(model.ID);
                                $("#dtname").val(model.Name);
                                $("#dtactive").prop('checked', model.IsActive);
                                $("#dtname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetDietCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#dtcancel").on("click", function () {
        $("#gridDietCategory").css("display", "inline-block");
        $("#newDietCategory").css("display", "none");
        $("#dtaddnew").css("display", "inline");
    });

    $("#dtaddnew").on("click", function () {
        $("#dtname").removeClass("is-invalid");
        $("#dtsuccess").css("display", "none");
        $("#dterror").css("display", "none");
        $('#dtsubmit').removeAttr("disabled");
        $("#gridDietCategory").css("display", "none");
        $("#newDietCategory").css("display", "inline");
        $("#dtaddnew").css("display", "none");
        $("#dtid").val("");
        $("#dtname").val("");
        $("#dtactive").prop('checked', true);
        $("#dtname").focus();
        DietCategoryCodeInitial = null;
    });

    function dtvalidate() {
        var valid = true;

        if ($("#dtname").val() === "") {
            $("#dterror").css("display", "flex");
            $("#dterror").find("p").text("Please provide input");
            $("#dtname").addClass("is-invalid");
            valid = false;
            $("#dtname").focus();
        }
        if (($.trim($("#dtname").val())).length > 8) {
            $("#dterror").css("display", "flex");
            $("#dterror").find("p").text("Diet Category accepts upto 8 charactres");
            $("#dtname").addClass("is-invalid");
            valid = false;
            $("#dtname").focus();
        }
        return valid;
    }

    $("#dtsubmit").click(function () {
        

        if (dtvalidate() !== true) {
            $("#dtname").removeClass("is-invalid");
            $("#dterror").css("display", "flex");
            $("#dterror").find("p").text("Please provide input");
            $("#dtname").focus();
        }
        else {
            $("#dterror").css("display", "none");
            var model = {
                "ID": $("#dtid").val(),
                "Name": $("#dtname").val(),
                "IsActive": $("#dtactive")[0].checked,
                "DietCategoryCode":DietCategoryCodeInitial
            }

            $("#dtsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDietCategory, function (result) {
                if (result == false) {
                    $('#dtsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#dtname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#dtsubmit').removeAttr("disabled");
                    $("#dtsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#dtsuccess").find("p").text("Record updated successfully");
                    else
                        $("#dtsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridDietCategory").data("kendoGrid").dataSource.data([]);
                    $("#gridDietCategory").data("kendoGrid").dataSource.read();
                    $("#gridDietCategory").css("display", "inline-block");
                    $("#newDietCategory").css("display", "none");
                    $("#dtaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#dtsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateDishSubCategoryGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridDishSubCategory");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                DishSubCategoryCodeInitial = item.DishSubCategoryCode
                                $("#scname").removeClass("is-invalid");
                                $("#scsuccess").css("display", "none");
                                $("#scerror").css("display", "none");
                                $('#scsubmit').removeAttr("disabled");
                                $("#gridDishSubCategory").css("display", "none");
                                $("#newDishSubCategory").css("display", "inline");
                                $("#scaddnew").css("display", "none");
                                $("#scid").val(model.ID);
                                $("#scname").val(model.Name);
                                $("#scactive").prop('checked', model.IsActive);
                                $("#scname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetDishSubCategoryDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#sccancel").on("click", function () {
        $("#gridDishSubCategory").css("display", "inline-block");
        $("#newDishSubCategory").css("display", "none");
        $("#scaddnew").css("display", "inline");
    });

    $("#scaddnew").on("click", function () {
        $("#scname").removeClass("is-invalid");
        $("#scsuccess").css("display", "none");
        $("#scerror").css("display", "none");
        $('#scsubmit').removeAttr("disabled");
        $("#gridDishSubCategory").css("display", "none");
        $("#newDishSubCategory").css("display", "inline");
        $("#scaddnew").css("display", "none");
        $("#scid").val("");
        $("#scname").val("");
        $("#scactive").prop('checked', true);
        $("#scname").focus();
        DishSubCategoryCodeInitial = null;
    });

    function scvalidate() {
        var valid = true;

        if ($("#scname").val() === "") {
            $("#scerror").css("display", "flex");
            $("#scerror").find("p").text("Please provide input");
            $("#scname").addClass("is-invalid");
            valid = false;
            $("#scname").focus();
        }
        if (($.trim($("#scname").val())).length > 30) {
            $("#scerror").css("display", "flex");
            $("#scerror").find("p").text("Dish Sub Category accepts upto 30 charactres");
            $("#scname").addClass("is-invalid");
            valid = false;
            $("#scname").focus();
        }
        return valid;
    }


    $("#scsubmit").click(function () {
        

        if (scvalidate() === true) {
            $("#scname").removeClass("is-invalid");
            $("#scerror").css("display", "none");
            var model = {
                "ID": $("#scid").val(),
                "Name": $("#scname").val(),
                "IsActive": $("#scactive")[0].checked,
                "DishSubCategoryCode": DishSubCategoryCodeInitial
            }

            $("#scsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveDishSubCategory, function (result) {
                if (result == false) {
                    $('#scsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#scname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#scsubmit').removeAttr("disabled");
                    $("#scsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#scsuccess").find("p").text("Record updated successfully");
                    else
                        $("#scsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridDishSubCategory").data("kendoGrid").dataSource.data([]);
                    $("#gridDishSubCategory").data("kendoGrid").dataSource.read();
                    $("#gridDishSubCategory").css("display", "inline-block");
                    $("#newDishSubCategory").css("display", "none");
                    $("#scaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#scsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateConceptType1Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridConceptType1");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                CT1CodeInitial = item.ConceptType1Code;
                                
                                $("#ct1name").removeClass("is-invalid");
                                $("#ct1success").css("display", "none");
                                $("#ct1error").css("display", "none");
                                $('#ct1submit').removeAttr("disabled");
                                $("#gridConceptType1").css("display", "none");
                                $("#newConceptType1").css("display", "inline");
                                $("#ct1addnew").css("display", "none");
                                $("#ct1id").val(model.ID);
                                $("#ct1name").val(model.Name);
                                $("#ct1active").prop('checked', model.IsActive);
                                $("#ct1name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetConceptType1DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#ct1cancel").on("click", function () {
        $("#gridConceptType1").css("display", "inline-block");
        $("#newConceptType1").css("display", "none");
        $("#ct1addnew").css("display", "inline");
    });

    $("#ct1addnew").on("click", function () {
        $("#ct1name").removeClass("is-invalid");
        $("#ct1success").css("display", "none");
        $("#ct1error").css("display", "none");
        $('#ct1submit').removeAttr("disabled");
        $("#gridConceptType1").css("display", "none");
        $("#newConceptType1").css("display", "inline");
        $("#ct1addnew").css("display", "none");
        $("#ct1id").val("");
        $("#ct1name").val("");
        $("#ct1active").prop('checked', true);
        $("#ct1name").focus();
        CT1CodeInitial = null;
    });

    function ct1validate() {
        var valid = true;

        if ($("#ct1name").val() === "") {
            $("#ct1error").css("display", "flex");
            $("#ct1error").find("p").text("Please provide input");
            $("#ct1name").addClass("is-invalid");
            valid = false;
            $("#ct1name").focus();
        }
        if (($.trim($("#ct1name").val())).length > 20) {
            $("#ct1error").css("display", "flex");
            $("#ct1error").find("p").text("Concept Type 1 accepts upto 20 charactres");
            $("#ct1name").addClass("is-invalid");
            valid = false;
            $("#ct1name").focus();
        }
        return valid;
    }

    $("#ct1submit").click(function () {
        
        if (ct1validate() === true) {
            $("#ct1name").removeClass("is-invalid");
            $("#ct1error").css("display", "none");
            var model = {
                "ID": $("#ct1id").val(),
                "Name": $("#ct1name").val(),
                "IsActive": $("#ct1active")[0].checked,
                "ConceptType1Code": CT1CodeInitial
            }

            $("#ct1submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveConceptType1, function (result) {
                if (result == false) {
                    $('#ct1submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#ct1name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#ct1submit').removeAttr("disabled");
                    $("#ct1success").css("display", "flex");
                    if (model.ID > 0)
                        $("#ct1success").find("p").text("Record updated successfully");
                    else
                        $("#ct1success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridConceptType1").data("kendoGrid").dataSource.data([]);
                    $("#gridConceptType1").data("kendoGrid").dataSource.read();
                    $("#gridConceptType1").css("display", "inline-block");
                    $("#newConceptType1").css("display", "none");
                    $("#ct1addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#ct1success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateConceptType2Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridConceptType2");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                CT2CodeInitial = item.ConceptType2Code;
                                $("#ct2name").removeClass("is-invalid");
                                $("#ct2success").css("display", "none");
                                $("#ct2error").css("display", "none");
                                $('#ct2submit').removeAttr("disabled");
                                $("#gridConceptType2").css("display", "none");
                                $("#newConceptType2").css("display", "inline");
                                $("#ct2addnew").css("display", "none");
                                $("#ct2id").val(model.ID);
                                $("#ct2name").val(model.Name);
                                $("#ct2active").prop('checked', model.IsActive);
                                $("#ct2name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetConceptType2DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#ct2cancel").on("click", function () {
        $("#gridConceptType2").css("display", "inline-block");
        $("#newConceptType2").css("display", "none");
        $("#ct2addnew").css("display", "inline");
    });

    $("#ct2addnew").on("click", function () {
        $("#ct2name").removeClass("is-invalid");
        $("#ct2success").css("display", "none");
        $("#ct2error").css("display", "none");
        $('#ct2submit').removeAttr("disabled");
        $("#gridConceptType2").css("display", "none");
        $("#newConceptType2").css("display", "inline");
        $("#ct2addnew").css("display", "none");
        $("#ct2id").val("");
        $("#ct2name").val("");
        $("#ct2active").prop('checked', true);
        $("#ct2name").focus();
        CT2CodeInitial = null;
    });

    function ct2validate() {
        var valid = true;

        if ($("#ct2name").val() === "") {
            $("#ct2error").css("display", "flex");
            $("#ct2error").find("p").text("Please provide input");
            $("#ct2name").addClass("is-invalid");
            valid = false;
            $("#ct2name").focus();
        }
        if (($.trim($("#ct2name").val())).length > 20) {
            $("#ct2error").css("display", "flex");
            $("#ct2error").find("p").text("Concept Typ 2 accepts upto 20 charactres");
            $("#ct2name").addClass("is-invalid");
            valid = false;
            $("#ct2name").focus();
        }
        return valid;
    }

    $("#ct2submit").click(function () {
        

        if (ct2validate() === true) {
            $("#ct2name").removeClass("is-invalid");
            $("#ct2error").css("display", "none");
            var model = {
                "ID": $("#ct2id").val(),
                "Name": $("#ct2name").val(),
                "IsActive": $("#ct2active")[0].checked,
                "ConceptType2Code": CT2CodeInitial
            }

            $("#ct2submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveConceptType2, function (result) {
                if (result == false) {
                    $('#ct2submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#ct2name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#ct2submit').removeAttr("disabled");
                    $("#ct2success").css("display", "flex");
                    if (model.ID > 0)
                        $("#ct2success").find("p").text("Record updated successfully");
                    else
                        $("#ct2success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridConceptType2").data("kendoGrid").dataSource.data([]);
                    $("#gridConceptType2").data("kendoGrid").dataSource.read();
                    $("#gridConceptType2").css("display", "inline-block");
                    $("#newConceptType2").css("display", "none");
                    $("#ct2addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#ct2success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateConceptType3Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridConceptType3");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                CT3CodeInitial = item.ConceptType3Code;
                                
                                $("#ct3name").removeClass("is-invalid");
                                $("#ct3success").css("display", "none");
                                $("#ct3error").css("display", "none");
                                $('#ct3submit').removeAttr("disabled");
                                $("#gridConceptType3").css("display", "none");
                                $("#newConceptType3").css("display", "inline");
                                $("#ct3addnew").css("display", "none");
                                $("#ct3id").val(model.ID);
                                $("#ct3name").val(model.Name);
                                $("#ct3active").prop('checked', model.IsActive);
                                $("#ct3name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetConceptType3DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#ct3cancel").on("click", function () {
        $("#gridConceptType3").css("display", "inline-block");
        $("#newConceptType3").css("display", "none");
        $("#ct3addnew").css("display", "inline");
    });

    $("#ct3addnew").on("click", function () {
        $("#ct3name").removeClass("is-invalid");
        $("#ct3success").css("display", "none");
        $("#ct3error").css("display", "none");
        $('#ct3submit').removeAttr("disabled");
        $("#gridConceptType3").css("display", "none");
        $("#newConceptType3").css("display", "inline");
        $("#ct3addnew").css("display", "none");
        $("#ct3id").val("");
        $("#ct3name").val("");
        $("#ct3active").prop('checked', true);
        $("#ct3name").focus();
        CT3CodeInitial = null;
    });

    function ct3validate() {
        var valid = true;

        if ($("#ct3name").val() === "") {
            $("#ct3error").css("display", "flex");
            $("#ct3error").find("p").text("Please provide input");
            $("#ct3name").addClass("is-invalid");
            valid = false;
            $("#ct3name").focus();
        }
        if (($.trim($("#ct3name").val())).length > 20) {
            $("#ct3error").css("display", "flex");
            $("#ct3error").find("p").text("Concept Type 3 accepts upto 20 charactres");
            $("#ct3name").addClass("is-invalid");
            valid = false;
            $("#ct3name").focus();
        }
        return valid;
    }

    $("#ct3submit").click(function () {
        

        if (ct3validate() === true) {
            $("ct3pname").removeClass("is-invalid");
            $("#ct3error").css("display", "none");
            var model = {
                "ID": $("#ct3id").val(),
                "Name": $("#ct3name").val(),
                "IsActive": $("#ct3active")[0].checked,
                "ConceptType3Code": CT3CodeInitial
            }

            $("#ct3submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveConceptType3, function (result) {
                if (result == false) {
                    $('#ct3submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#ct3name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#ct3submit').removeAttr("disabled");
                    $("#ct3success").css("display", "flex");
                    if (model.ID > 0)
                        $("#ct3success").find("p").text("Record updated successfully");
                    else
                        $("#ct3success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridConceptType3").data("kendoGrid").dataSource.data([]);
                    $("#gridConceptType3").data("kendoGrid").dataSource.read();
                    $("#gridConceptType3").css("display", "inline-block");
                    $("#newConceptType3").css("display", "none");
                    $("#ct3addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#ct3success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateConceptType4Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridConceptType4");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                CT4CodeInitial = item.ConceptType4Code;
                                $("#ct4name").removeClass("is-invalid");
                                $("#ct4success").css("display", "none");
                                $("#ct4error").css("display", "none");
                                $('#ct4submit').removeAttr("disabled");
                                $("#gridConceptType4").css("display", "none");
                                $("#newConceptType4").css("display", "inline");
                                $("#ct4addnew").css("display", "none");
                                $("#ct4id").val(model.ID);
                                $("#ct4name").val(model.Name);
                                $("#ct4active").prop('checked', model.IsActive);
                                $("#ct4name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetConceptType4DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#ct4cancel").on("click", function () {
        $("#gridConceptType4").css("display", "inline-block");
        $("#newConceptType4").css("display", "none");
        $("#ct4addnew").css("display", "inline");
    });

    $("#ct4addnew").on("click", function () {
        $("#ct4name").removeClass("is-invalid");
        $("#ct4success").css("display", "none");
        $("#ct4error").css("display", "none");
        $('#ct4submit').removeAttr("disabled");
        $("#gridConceptType4").css("display", "none");
        $("#newConceptType4").css("display", "inline");
        $("#ct4addnew").css("display", "none");
        $("#ct4id").val("");
        $("#ct4name").val("");
        $("#ct4active").prop('checked', true);
        $("#ct4name").focus();
        CT4CodeInitial = null;
    });

    function ct4validate() {
        var valid = true;

        if ($("#ct4name").val() === "") {
            $("#ct4error").css("display", "flex");
            $("#ct4error").find("p").text("Please provide input");
            $("#ct4name").addClass("is-invalid");
            valid = false;
            $("#ct4name").focus();
        }
        if (($.trim($("#ct4name").val())).length > 20) {
            $("#ct4error").css("display", "flex");
            $("#ct4error").find("p").text("Concept Type 4 accepts upto 20 charactres");
            $("#ct4name").addClass("is-invalid");
            valid = false;
            $("#ct4name").focus();
        }
        return valid;
    }

    $("#ct4submit").click(function () {
        

        if (ct4validate() === true) {
            $("#ct4name").removeClass("is-invalid");
            $("#ct4error").css("display", "none");
            var model = {
                "ID": $("#ct4id").val(),
                "Name": $("#ct4name").val(),
                "IsActive": $("#ct4active")[0].checked,
                "ConceptType4Code": CT4CodeInitial
            }

            $("#ct4submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveConceptType4, function (result) {
                if (result == false) {
                    $('#ct4submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#ct4name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#ct4submit').removeAttr("disabled");
                    $("#ct4success").css("display", "flex");
                    if (model.ID > 0)
                        $("#ct4success").find("p").text("Record updated successfully");
                    else
                        $("#ct4success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridConceptType4").data("kendoGrid").dataSource.data([]);
                    $("#gridConceptType4").data("kendoGrid").dataSource.read();
                    $("#gridConceptType4").css("display", "inline-block");
                    $("#newConceptType4").css("display", "none");
                    $("#ct4addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#ct4success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateConceptType5Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridConceptType5");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                CT5CodeInitial = item.ConceptType5Code;
                                $("#ct5name").removeClass("is-invalid");
                                $("#ct5success").css("display", "none");
                                $("#ct5error").css("display", "none");
                                $('#ct5submit').removeAttr("disabled");
                                $("#gridConceptType5").css("display", "none");
                                $("#newConceptType5").css("display", "inline");
                                $("#ct5addnew").css("display", "none");
                                $("#ct5id").val(model.ID);
                                $("#ct5name").val(model.Name);
                                $("#ct5active").prop('checked', model.IsActive);
                                $("#ct5name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetConceptType5DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#ct5cancel").on("click", function () {
        $("#gridConceptType5").css("display", "inline-block");
        $("#newConceptType5").css("display", "none");
        $("#ct5addnew").css("display", "inline");
    });

    $("#ct5addnew").on("click", function () {
        $("#ct5name").removeClass("is-invalid");
        $("#ct5success").css("display", "none");
        $("#ct5error").css("display", "none");
        $('#ct5submit').removeAttr("disabled");
        $("#gridConceptType5").css("display", "none");
        $("#newConceptType5").css("display", "inline");
        $("#ct5addnew").css("display", "none");
        $("#ct5id").val("");
        $("#ct5name").val("");
        $("#ct5active").prop('checked', true);
        $("#ct5name").focus();
        CT5CodeInitial = null;
    });

    function ct5validate() {
        var valid = true;

        if ($("#ct5name").val() === "") {
            $("#ct5error").css("display", "flex");
            $("#ct5error").find("p").text("Please provide input");
            $("#ct5name").addClass("is-invalid");
            valid = false;
            $("#ct5name").focus();
        }
        if (($.trim($("#ct5name").val())).length > 20) {
            $("#ct5error").css("display", "flex");
            $("#ct5error").find("p").text("Concept Type 5 accepts upto 20 charactres");
            $("#ct5name").addClass("is-invalid");
            valid = false;
            $("#ct5name").focus();
        }
        return valid;
    }

    $("#ct5submit").click(function () {
        

        if (ct5validate() === true) {
            $("#ct5name").removeClass("is-invalid");
            $("#ct5error").css("display", "none");
            var model = {
                "ID": $("#ct5id").val(),
                "Name": $("#ct5name").val(),
                "IsActive": $("#ct5active")[0].checked,
                "ConceptType5Code": CT5CodeInitial
            }

            $("#ct5submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveConceptType5, function (result) {
                if (result == false) {
                    $('#ct5submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#ct5name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#ct5submit').removeAttr("disabled");
                    $("#ct5success").css("display", "flex");
                    if (model.ID > 0)
                        $("#ct5success").find("p").text("Record updated successfully");
                    else
                        $("#ct5success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridConceptType5").data("kendoGrid").dataSource.data([]);
                    $("#gridConceptType5").data("kendoGrid").dataSource.read();
                    $("#gridConceptType5").css("display", "inline-block");
                    $("#newConceptType5").css("display", "none");
                    $("#ct5addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#ct5success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateItemType1Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridItemType1");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                ItemType1CodeInitial = item.ItemType1Code;
                                $("#it1name").removeClass("is-invalid");
                                $("#it1success").css("display", "none");
                                $("#it1error").css("display", "none");
                                $('#it1submit').removeAttr("disabled");
                                $("#gridItemType1").css("display", "none");
                                $("#newItemType1").css("display", "inline");
                                $("#it1addnew").css("display", "none");
                                $("#it1id").val(model.ID);
                                $("#it1name").val(model.Name);
                                $("#it1active").prop('checked', model.IsActive);
                                $("#it1name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetItemType1DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#it1cancel").on("click", function () {
        $("#gridItemType1").css("display", "inline-block");
        $("#newItemType1").css("display", "none");
        $("#it1addnew").css("display", "inline");
    });

    $("#it1addnew").on("click", function () {
        $("#it1name").removeClass("is-invalid");
        $("#it1success").css("display", "none");
        $("#it1error").css("display", "none");
        $('#it1submit').removeAttr("disabled");
        $("#gridItemType1").css("display", "none");
        $("#newItemType1").css("display", "inline");
        $("#it1addnew").css("display", "none");
        $("#it1id").val("");
        $("#it1name").val("");
        $("#it1active").prop('checked', true);
        $("#it1name").focus();
        ItemType1CodeInitial = null;
    });

    function it1validate() {
        var valid = true;

        if ($("#it1name").val() === "") {
            $("#it1error").css("display", "flex");
            $("#it1error").find("p").text("Please provide input");
            $("#it1name").addClass("is-invalid");
            valid = false;
            $("#it1name").focus();
        }
        if (($.trim($("#it1name").val())).length > 20) {
            $("#it1error").css("display", "flex");
            $("#it1error").find("p").text("Item Type 1 accepts upto 20 charactres");
            $("#it1name").addClass("is-invalid");
            valid = false;
            $("#it1name").focus();
        }
        return valid;
    }

    $("#it1submit").click(function () {
        

        if (it1validate() === true) {
            $("#it1name").removeClass("is-invalid");
            $("#it1error").css("display", "none");
            var model = {
                "ID": $("#it1id").val(),
                "Name": $("#it1name").val(),
                "IsActive": $("#it1active")[0].checked,
                "ItemType1Code": ItemType1CodeInitial
            }

            $("#it1submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveItemType1, function (result) {
                if (result == false) {
                    $('#it1submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#it1name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#it1submit').removeAttr("disabled");
                    $("#it1success").css("display", "flex");
                    if (model.ID > 0)
                        $("#it1success").find("p").text("Record updated successfully");
                    else
                        $("#it1success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridItemType1").data("kendoGrid").dataSource.data([]);
                    $("#gridItemType1").data("kendoGrid").dataSource.read();
                    $("#gridItemType1").css("display", "inline-block");
                    $("#newItemType1").css("display", "none");
                    $("#it1addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#it1success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateItemType2Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridItemType2");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                ItemType2CodeInitial = item.ItemType2Code;
                                $("#it2name").removeClass("is-invalid");
                                $("#it2success").css("display", "none");
                                $("#it2error").css("display", "none");
                                $('#it2submit').removeAttr("disabled");
                                $("#gridItemType2").css("display", "none");
                                $("#newItemType2").css("display", "inline");
                                $("#it2addnew").css("display", "none");
                                $("#it2id").val(model.ID);
                                $("#it2name").val(model.Name);
                                $("#it2active").prop('checked', model.IsActive);
                                $("#it2name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetItemType2DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#it2cancel").on("click", function () {
        $("#gridItemType2").css("display", "inline-block");
        $("#newItemType2").css("display", "none");
        $("#it2addnew").css("display", "inline");
    });

    $("#it2addnew").on("click", function () {
        $("#it2name").removeClass("is-invalid");
        $("#it2success").css("display", "none");
        $("#it2error").css("display", "none");
        $('#it2submit').removeAttr("disabled");
        $("#gridItemType2").css("display", "none");
        $("#newItemType2").css("display", "inline");
        $("#it2addnew").css("display", "none");
        $("#it2id").val("");
        $("#it2name").val("");
        $("#it2active").prop('checked', true);
        $("#it2name").focus();
        ItemType2CodeInitial = null;
    });

    function it2validate() {
        var valid = true;

        if ($("#it2name").val() === "") {
            $("#it2error").css("display", "flex");
            $("#it2error").find("p").text("Please provide input");
            $("#it2name").addClass("is-invalid");
            valid = false;
            $("#it2name").focus();
        }
        if (($.trim($("#it2name").val())).length > 20) {
            $("#it2error").css("display", "flex");
            $("#it2error").find("p").text("Item Type 2 accepts upto 20 charactres");
            $("#it2name").addClass("is-invalid");
            valid = false;
            $("#it2name").focus();
        }
        return valid;
    }

    $("#it2submit").click(function () {
        

        if (it2validate() === true) {
            $("#it2name").removeClass("is-invalid");
            $("#it2error").css("display", "none");
            var model = {
                "ID": $("#it2id").val(),
                "Name": $("#it2name").val(),
                "IsActive": $("#it2active")[0].checked,
                "ItemType2Code": ItemType2CodeInitial
            }

            $("#it2submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveItemType2, function (result) {
                if (result == false) {
                    $('#it2submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#it2name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#it2submit').removeAttr("disabled");
                    $("#it2success").css("display", "flex");
                    if (model.ID > 0)
                        $("#it2success").find("p").text("Record updated successfully");
                    else
                        $("#it2success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridItemType2").data("kendoGrid").dataSource.data([]);
                    $("#gridItemType2").data("kendoGrid").dataSource.read();
                    $("#gridItemType2").css("display", "inline-block");
                    $("#newItemType2").css("display", "none");
                    $("#it2addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#it2success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateSiteProfile1Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridSiteProfile1");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                SiteProfile1CodeInitial = item.SiteProfile1Code;
                                $("#sp1name").removeClass("is-invalid");
                                $("#sp1success").css("display", "none");
                                $("#sp1error").css("display", "none");
                                $('#sp1submit').removeAttr("disabled");
                                $("#gridSiteProfile1").css("display", "none");
                                $("#newSiteProfile1").css("display", "inline");
                                $("#sp1addnew").css("display", "none");
                                $("#sp1id").val(model.ID);
                                $("#sp1name").val(model.Name);
                                $("#sp1active").prop('checked', model.IsActive);
                                $("#sp1name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetSiteProfile1DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#sp1cancel").on("click", function () {
        $("#gridSiteProfile1").css("display", "inline-block");
        $("#newSiteProfile1").css("display", "none");
        $("#sp1addnew").css("display", "inline");
    });

    $("#sp1addnew").on("click", function () {
        $("#sp1name").removeClass("is-invalid");
        $("#sp1success").css("display", "none");
        $("#sp1error").css("display", "none");
        $('#sp1submit').removeAttr("disabled");
        $("#gridSiteProfile1").css("display", "none");
        $("#newSiteProfile1").css("display", "inline");
        $("#sp1addnew").css("display", "none");
        $("#sp1id").val("");
        $("#sp1name").val("");
        $("#sp1active").prop('checked', true);
        $("#sp1name").focus();
        SiteProfile1CodeInitial = null;
    });

    function sp1validate() {
        var valid = true;

        if ($("#sp1name").val() === "") {
            $("#sp1error").css("display", "flex");
            $("#sp1error").find("p").text("Please provide input");
            $("#sp1name").addClass("is-invalid");
            valid = false;
            $("#sp1name").focus();
        }
        if (($.trim($("#sp1name").val())).length > 20) {
            $("#sp1error").css("display", "flex");
            $("#sp1error").find("p").text("Site Profile 1 accepts upto 20 charactres");
            $("#sp1name").addClass("is-invalid");
            valid = false;
            $("#sp1name").focus();
        }
        return valid;
    }

    $("#sp1submit").click(function () {
        

        if (sp1validate() === true) {
            $("#sp1name").removeClass("is-invalid");
            $("#sp1error").css("display", "none");
            var model = {
                "ID": $("#sp1id").val(),
                "Name": $("#sp1name").val(),
                "IsActive": $("#sp1active")[0].checked,
                "SiteProfile1Code": SiteProfile1CodeInitial
            }

            $("#sp1submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteProfile1, function (result) {
                if (result == false) {
                    $('#sp1submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#sp1name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#sp1submit').removeAttr("disabled");
                    $("#sp1success").css("display", "flex");
                    if (model.ID > 0)
                        $("#sp1success").find("p").text("Record updated successfully");
                    else
                        $("#sp1success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridSiteProfile1").data("kendoGrid").dataSource.data([]);
                    $("#gridSiteProfile1").data("kendoGrid").dataSource.read();
                    $("#gridSiteProfile1").css("display", "inline-block");
                    $("#newSiteProfile1").css("display", "none");
                    $("#sp1addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#sp1success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateSiteProfile2Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridSiteProfile2");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                SiteProfile1CodeInitial = SiteProfile1Code;
                                $("#sp2name").removeClass("is-invalid");
                                $("#sp2success").css("display", "none");
                                $("#sp2error").css("display", "none");
                                $('#sp2submit').removeAttr("disabled");
                                $("#gridSiteProfile2").css("display", "none");
                                $("#newSiteProfile2").css("display", "inline");
                                $("#sp2addnew").css("display", "none");
                                $("#sp2id").val(model.ID);
                                $("#sp2name").val(model.Name);
                                $("#sp2active").prop('checked', model.IsActive);
                                $("#sp2name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetSiteProfile2DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#sp2cancel").on("click", function () {
        $("#gridSiteProfile2").css("display", "inline-block");
        $("#newSiteProfile2").css("display", "none");
        $("#sp2addnew").css("display", "inline");
    });

    $("#sp2addnew").on("click", function () {
        $("#sp2name").removeClass("is-invalid");
        $("#sp2success").css("display", "none");
        $("#sp2error").css("display", "none");
        $('#sp2submit').removeAttr("disabled");
        $("#gridSiteProfile2").css("display", "none");
        $("#newSiteProfile2").css("display", "inline");
        $("#sp2addnew").css("display", "none");
        $("#sp2id").val("");
        $("#sp2name").val("");
        $("#sp2active").prop('checked', true);
        $("#sp2name").focus();
        SiteProfile2CodeInitial = null;
    });

    function sp2validate() {
        var valid = true;

        if ($("#sp2name").val() === "") {
            $("#sp2error").css("display", "flex");
            $("#sp2error").find("p").text("Please provide input");
            $("#sp2name").addClass("is-invalid");
            valid = false;
            $("#sp2name").focus();
        }
        if (($.trim($("#sp2name").val())).length > 20) {
            $("#sp2error").css("display", "flex");
            $("#sp2error").find("p").text("Site Profile 2 accepts upto 20 charactres");
            $("#sp2name").addClass("is-invalid");
            valid = false;
            $("#sp2name").focus();
        }
        return valid;
    }

    $("#sp2submit").click(function () {
        

        if (sp2validate() === true) {
            $("#sp2name").removeClass("is-invalid");
            $("#sp2error").css("display", "none");
            var model = {
                "ID": $("#sp2id").val(),
                "Name": $("#sp2name").val(),
                "IsActive": $("#sp2active")[0].checked,
                "SiteProfile2Code": SiteProfile2CodeInitial
            }

            $("#sp2submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteProfile2, function (result) {
                if (result == false) {
                    $('#sp2submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#sp2name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#sp2submit').removeAttr("disabled");
                    $("#sp2success").css("display", "flex");
                    if (model.ID > 0)
                        $("#sp2success").find("p").text("Record updated successfully");
                    else
                        $("#sp2success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridSiteProfile2").data("kendoGrid").dataSource.data([]);
                    $("#gridSiteProfile2").data("kendoGrid").dataSource.read();
                    $("#gridSiteProfile2").css("display", "inline-block");
                    $("#newSiteProfile2").css("display", "none");
                    $("#sp2addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#sp2success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateSiteProfile3Grid() {
        
        Utility.Loading();
        var gridVariable = $("#gridSiteProfile3");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                SiteProfile3CodeInitial = item.SiteProfile3Code;
                                
                                $("#sp3name").removeClass("is-invalid");
                                $("#sp3success").css("display", "none");
                                $("#sp3error").css("display", "none");
                                $('#sp3submit').removeAttr("disabled");
                                $("#gridSiteProfile3").css("display", "none");
                                $("#newSiteProfile3").css("display", "inline");
                                $("#sp3addnew").css("display", "none");
                                $("#sp3id").val(model.ID);
                                $("#sp3name").val(model.Name);
                                $("#sp3active").prop('checked', model.IsActive);
                                $("#sp3name").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetSiteProfile3DataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#sp3cancel").on("click", function () {
        $("#gridSiteProfile3").css("display", "inline-block");
        $("#newSiteProfile3").css("display", "none");
        $("#sp3addnew").css("display", "inline");
    });

    $("#sp3addnew").on("click", function () {
        $("#sp3name").removeClass("is-invalid");
        $("#sp3success").css("display", "none");
        $("#sp3error").css("display", "none");
        $('#sp3submit').removeAttr("disabled");
        $("#gridSiteProfile3").css("display", "none");
        $("#newSiteProfile3").css("display", "inline");
        $("#sp3addnew").css("display", "none");
        $("#sp3id").val("");
        $("#sp3name").val("");
        $("#sp3active").prop('checked', true);
        $("#sp3name").focus();
        SiteProfile3CodeInitial = null;
    });

    function sp3validate() {
        var valid = true;

        if ($("#sp3name").val() === "") {
            $("#sp3error").css("display", "flex");
            $("#sp3error").find("p").text("Please provide input");
            $("#sp3name").addClass("is-invalid");
            valid = false;
            $("#sp3name").focus();
        }
        if (($.trim($("#sp3name").val())).length > 20) {
            $("#sp3error").css("display", "flex");
            $("#sp3error").find("p").text("Site Profile 3 accepts upto 20 charactres");
            $("#sp3name").addClass("is-invalid");
            valid = false;
            $("#sp3name").focus();
        }
        return valid;
    }

    $("#sp3submit").click(function () {
        

        if (sp3validate() === true) {
            $("#sp3name").removeClass("is-invalid");
            $("#sp3error").css("display", "none");
            var model = {
                "ID": $("#sp3id").val(),
                "Name": $("#sp3name").val(),
                "IsActive": $("#sp3active")[0].checked,
                "SiteProfile3Code": SiteProfile3CodeInitial
            }

            $("#sp3submit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveSiteProfile3, function (result) {
                if (result == false) {
                    $('#sp3submit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#sp3name").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#sp3submit').removeAttr("disabled");
                    $("#sp3success").css("display", "flex");
                    if (model.ID > 0)
                        $("#sp3success").find("p").text("Record updated successfully");
                    else
                        $("#sp3success").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridSiteProfile3").data("kendoGrid").dataSource.data([]);
                    $("#gridSiteProfile3").data("kendoGrid").dataSource.read();
                    $("#gridSiteProfile3").css("display", "inline-block");
                    $("#newSiteProfile3").css("display", "none");
                    $("#sp3addnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#sp3success").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateServewareGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridServeware");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                ServewareCodeInitial = item.ServewareCode;
                                $("#swname").removeClass("is-invalid");
                                $("#swsuccess").css("display", "none");
                                $("#swerror").css("display", "none");
                                $('#swsubmit').removeAttr("disabled");
                                $("#gridServeware").css("display", "none");
                                $("#newServeware").css("display", "inline");
                                $("#swaddnew").css("display", "none");
                                $("#swid").val(model.ID);
                                $("#swname").val(model.Name);
                                $("#swactive").prop('checked', model.IsActive);
                                $("#swname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetServewareDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#swcancel").on("click", function () {
        $("#gridServeware").css("display", "inline-block");
        $("#newServeware").css("display", "none");
        $("#swaddnew").css("display", "inline");
    });

    $("#swaddnew").on("click", function () {
        $("#swname").removeClass("is-invalid");
        $("#swsuccess").css("display", "none");
        $("#swerror").css("display", "none");
        $('#swsubmit').removeAttr("disabled");
        $("#gridServeware").css("display", "none");
        $("#newServeware").css("display", "inline");
        $("#swaddnew").css("display", "none");
        $("#swid").val("");
        $("#swname").val("");
        $("#swactive").prop('checked', true);
        $("#swname").focus();
        ServewareCodeInitial = null;
    });

    function swvalidate() {
        var valid = true;

        if ($("#swname").val() === "") {
            $("#swerror").css("display", "flex");
            $("#swerror").find("p").text("Please provide input");
            $("#swname").addClass("is-invalid");
            valid = false;
            $("#swname").focus();
        }
        if (($.trim($("#swname").val())).length > 15) {
            $("#swerror").css("display", "flex");
            $("#swerror").find("p").text("Serveware accepts upto 15 charactres");
            $("#swname").addClass("is-invalid");
            valid = false;
            $("#swname").focus();
        }
        return valid;
    }

    $("#swsubmit").click(function () {
        

        if (swvalidate() === true) {
            $("#swname").removeClass("is-invalid");
            $("#swerror").css("display", "none");
            var model = {
                "ID": $("#swid").val(),
                "Name": $("#swname").val(),
                "IsActive": $("#swactive")[0].checked,
                "ServewareCode": ServewareCodeInitial
            }

            $("#swsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveServeware, function (result) {
                if (result == false) {
                    $('#swsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#swname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#swsubmit').removeAttr("disabled");
                    $("#swsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#swsuccess").find("p").text("Record updated successfully");
                    else
                        $("#swsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridServeware").data("kendoGrid").dataSource.data([]);
                    $("#gridServeware").data("kendoGrid").dataSource.read();
                    $("#gridServeware").css("display", "inline-block");
                    $("#newServeware").css("display", "none");
                    $("#swaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#swsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateUOMGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridUOM");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                //var tr = gridObj.dataItem($(e.currentTarget).closest("tr"));
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                UOMCodeInitial = item.UOMCode;
                                $("#umname").removeClass("is-invalid");
                                $("#umsuccess").css("display", "none");
                                $("#umerror").css("display", "none");
                                $('#umsubmit').removeAttr("disabled");
                                $("#gridUOM").css("display", "none");
                                $("#newUOM").css("display", "inline");
                                $("#umaddnew").css("display", "none");
                                $("#umid").val(model.ID);
                                $("#umname").val(model.Name);
                                $("#umactive").prop('checked', model.IsActive);
                                $("#umname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetUOMDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#umcancel").on("click", function () {
        $("#gridUOM").css("display", "inline-block");
        $("#newUOM").css("display", "none");
        $("#umaddnew").css("display", "inline");
    });

    $("#umaddnew").on("click", function () {
        $("#umname").removeClass("is-invalid");
        $("#umsuccess").css("display", "none");
        $("#umerror").css("display", "none");
        $('#umsubmit').removeAttr("disabled");
        $("#gridUOM").css("display", "none");
        $("#newUOM").css("display", "inline");
        $("#umaddnew").css("display", "none");
        $("#umid").val("");
        $("#umname").val("");
        $("#umactive").prop('checked', true);
        $("#umname").focus();
        UOMCodeInitial = 0;
    });

    function umvalidate() {
        var valid = true;

        if ($("#umname").val() === "") {
            $("#umerror").css("display", "flex");
            $("#umerror").find("p").text("Please provide input");
            $("#umname").addClass("is-invalid");
            valid = false;
            $("#umname").focus();
        }
        if (($.trim($("#umname").val())).length > 8) {
            $("#umerror").css("display", "flex");
            $("#umerror").find("p").text("UOM accepts upto 8 charactres");
            $("#umname").addClass("is-invalid");
            valid = false;
            $("#umname").focus();
        }
        return valid;
    }

    $("#umsubmit").click(function () {
        

        if (umvalidate() === true) {
            $("#umname").removeClass("is-invalid");
            $("#umerror").css("display", "none");
            var model = {
                "ID": $("#umid").val(),
                "Name": $("#umname").val(),
                "IsActive": $("#umactive")[0].checked,
                "UOMCode": UOMCodeInitial
            }
          
            $("#umsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveUOM, function (result) {
                if (result == false) {
                    $('#umsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#umname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#umsubmit').removeAttr("disabled");
                    $("#umsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#umsuccess").find("p").text("Record updated successfully");
                    else
                        $("#umsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridUOM").data("kendoGrid").dataSource.data([]);
                    $("#gridUOM").data("kendoGrid").dataSource.read();
                    $("#gridUOM").css("display", "inline-block");
                    $("#newUOM").css("display", "none");
                    $("#umaddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#umsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });

    function populateServingTemperatureGrid() {
        
        Utility.Loading();
        var gridVariable = $("#gridServingTemp");
        gridVariable.html("");
        gridVariable.kendoGrid({
            sortable: true,
            filterable: {
                extra: true,
                operators: {
                    string: {
                        contains: "Contains",
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to",
                        doesnotcontain: "Does not contain",
                        endswith: "Ends with"
                    }
                }
            },
            groupable: false,
            columns: [
                {
                    field: "Name", title: "Name", width: "100px", attributes: {
                        style: "text-align: left; font-weight:normal"
                    }
                },
                {
                    field: "IsActive", title: "Active", width: "75px", attributes: {

                        style: "text-align: center; font-weight:normal"
                    },
                    headerAttributes: {
                        style: "text-align: right;"
                    },
                    template: '# if (IsActive == true) {#<div class="status green-status"></div>#} else {#<div class="status red-status"></div>#}#',
                },
                {
                    field: "Edit", title: "Action", width: "50px",
                    attributes: {
                        style: "text-align: left; font-weight:normal"
                    },
                    command: [
                        {
                            name: 'Edit',
                            click: function (e) {
                                var gridObj = gridVariable.data("kendoGrid");
                                var tr = $(e.target).closest("tr");    // get the current table row (tr)
                                var item = this.dataItem(tr);          // get the date of this row
                                var model = {
                                    "ID": item.ID,
                                    "Name": item.Name,
                                    "IsActive": item.IsActive
                                };
                                ServingTempCodeInitial = item.ServingTemperatureCode;
                                $("#stname").removeClass("is-invalid");
                                $("#stsuccess").css("display", "none");
                                $("#sterror").css("display", "none");
                                $('#stsubmit').removeAttr("disabled");
                                $("#gridServingTemp").css("display", "none");
                                $("#newServingTemp").css("display", "inline");
                                $("#staddnew").css("display", "none");
                                $("#stid").val(model.ID);
                                $("#stname").val(model.Name);
                                $("#stactive").prop('checked', model.IsActive);
                                $("#stname").focus();
                                return true;
                            }
                        }
                    ],
                }
            ],
            dataSource: {
                transport: {
                    read: function (options) {
                        
                        var varCodes = "";

                        HttpClient.MakeRequest(CookBookMasters.GetServingTemperatureDataList, function (result) {
                            Utility.UnLoading();

                            if (result != null) {
                                options.success(result);
                            }
                            else {
                                options.success("");
                            }
                        }, null
                            //{
                            //filter: mdl
                            //}
                            , true);
                    }
                },
                schema: {
                    model: {
                        id: "ID",
                        fields: {
                            Name: { editable: false },
                            IsActive: { editable: false }
                        }
                    }
                }
            },
            columnResize: function (e) {
                var grid = gridVariable.data("kendoGrid");
                e.preventDefault();
            },
            dataBound: function (e) {
                var items = e.sender.items();

                items.each(function (e) {
                    
                    if (user.UserRoleId == 1) {
                        $(this).find('.k-grid-Edit').css('display', 'inline');
                    } else {
                        $(this).find('.k-grid-Edit').css('display', 'none');
                    }
                });
            },
            change: function (e) {
            },
        })
    }

    $("#stcancel").on("click", function () {
        $("#gridServingTemp").css("display", "inline-block");
        $("#newServingTemp").css("display", "none");
        $("#staddnew").css("display", "inline");
    });

    $("#staddnew").on("click", function () {
        $("#stname").removeClass("is-invalid");
        $("#stsuccess").css("display", "none");
        $("#sterror").css("display", "none");
        $('#stsubmit').removeAttr("disabled");
        $("#gridServingTemp").css("display", "none");
        $("#newServingTemp").css("display", "inline");
        $("#staddnew").css("display", "none");
        $("#stid").val("");
        $("#stname").val("");
        $("#stactive").prop('checked', true);
        $("#stname").focus();
        ServingTempCodeInitial = null;
    });

    function stvalidate() {
        var valid = true;

        if ($("#stname").val() === "") {
            $("#sterror").css("display", "flex");
            $("#sterror").find("p").text("Please provide input");
            $("#stname").addClass("is-invalid");
            valid = false;
            $("#stname").focus();
        }
        if (($.trim($("#stname").val())).length > 10) {
            $("#sterror").css("display", "flex");
            $("#sterror").find("p").text("Serving Temp. accepts upto 10 charactres");
            $("#stname").addClass("is-invalid");
            valid = false;
            $("#stname").focus();
        }
        return valid;
    }

    $("#stsubmit").click(function () {
        

        if (stvalidate() === true) {
            $("#stname").removeClass("is-invalid");
            $("#sterror").css("display", "none");
            var model = {
                "ID": $("#stid").val(),
                "Name": $("#stname").val(),
                "IsActive": $("#stactive")[0].checked,
                "ServingTemperatureCode": ServingTempCodeInitial
            }

            $("#stsubmit").attr('disabled', 'disabled');
            HttpClient.MakeRequest(CookBookMasters.SaveServingTemperature, function (result) {
                if (result == false) {
                    $('#stsubmit').removeAttr("disabled");
                    Utility.Page_Alert_Warning("There was some error, the record cannot be saved", "Alert", "OK", function () {
                        $("#stname").focus();
                    });
                }
                else {
                    $(".k-overlay").hide();
                    $('#stsubmit').removeAttr("disabled");
                    $("#stsuccess").css("display", "flex");
                    if (model.ID > 0)
                        $("#stsuccess").find("p").text("Record updated successfully");
                    else
                        $("#stsuccess").find("p").text("Record created successfully");
                    //Utility.Page_Alert("Record has been saved successfully", "Alert", "OK", function () {
                    $("#gridServingTemp").data("kendoGrid").dataSource.data([]);
                    $("#gridServingTemp").data("kendoGrid").dataSource.read();
                    $("#gridServingTemp").css("display", "inline-block");
                    $("#newServingTemp").css("display", "none");
                    $("#staddnew").css("display", "inline");

                    setTimeout(fade_out, 10000);

                    function fade_out() {
                        $("#stsuccess").fadeOut();
                    }
                    //});
                }
            }, {
                model: model

            }, true);
        }
    });
});