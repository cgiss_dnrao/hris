﻿var Utility = (function () {
    return {
        Currency: "₹",
        Cost_Format: "{0:₹0.00}",
        CurrentDate: function () {
            return (new Date()).toLocaleString("en-US");
        },
        Page_Alert: function (data, warningType, buttonText, callback) {
            data = data || '';
            //jQuery.noConflict();
            $('#confirmbox').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmbox').addClass("show");

            $('#confirmHeader').html(warningType);

            $('#confirmMessage').html(data);
            $('#confirmOk').html(buttonText);
            $('#confirmOk').click(function () {

                $('#confirmbox').modal('hide');
                $('#confirmbox').removeClass("show");
                if (callback) {
                    callback();
                }
                return false;
            });

        },

        Page_Alert_Warning: function (data, warningType, buttonText, callback) {
            data = data || '';
            //jQuery.noConflict();
            $('#confirmbox').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmbox').addClass("show");

            $('#confirmHeader').html(warningType);

            $('#confirmMessage').html(data);

            $('#confirmOk').click(function () {

                $('#confirmbox').modal('hide');
                $('#confirmbox').removeClass("show");
                if (callback) {
                    callback();
                }
                return false;
            });

        },

        GetDate: function (dt) {
            var dt1 = new Date(Date.UTC(dt.getFullYear(), dt.getMonth(), dt.getDate(), dt.getHours(), dt.getMinutes(), dt.getSeconds(), dt.getMilliseconds()));
            var date = '/Date(' + dt1.getTime() + ')/';
            return date;
        },

        AspNetDate: function (stringDateValue) {
            return new Date(parseInt(stringDateValue.replace("/Date(", "").replace(")/", ""), 10));
        },
        ClearFormErrors: function (formid) {

            $('#' + formid).find(':input').each(function (i, item) {
                $(this).removeClass('error');
            });

            $('#' + formid).find('label.error').remove();

        },
        Page_Alert_Optional: function (data, warningType, buttonText, callback) {

            data = data || '';
            //jQuery.noConflict();
            $('#confirmbox_optional').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmbox_optional').addClass("show");

            $('#confirmHeader_Optional').html(warningType);
            $('#confirmMessage_Optional').html(data);

            // $("#confirm_Ok").html(buttonText);
            $('#confirm_Ok').click(function () {
                $('#confirmbox_optional').modal('hide');
                $('#confirmbox_optional').removeClass("show");
                if (callback) {
                    callback();
                }
                return false;
            });
            $('#confirm_No').click(function () {
                $('#confirmbox_optional').modal('hide');
                $('#confirmbox_optional').removeClass("show");
                callback = false;
            });
        },

        Page_Alert_Warning: function (data, warningType, buttonText, callback) {

            data = data || '';
            //jQuery.noConflict();
            $('#confirmbox_warning').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmbox_warning').addClass("show");

            $('#confirmHeader_Warning').html(warningType);
            $('#confirmMessage_Warning').html(data);

            $("#confirm_Ok").html(buttonText);
            $('#confirm_Ok').click(function () {
                $('#confirmbox_warning').modal('hide');
                $('#confirmbox_warning').removeClass("show");
                if (callback) {
                    callback();
                }
                return false;
            });
            $('#confirm_No').click(function () {
                $('#confirmbox_warning').modal('hide');
                $('#confirmbox_warning').removeClass("show");
                callback = false;
            });
        },

        Page_Alert_Save: function (data, warningType, yesButtonText, noButtonText, yesCallback, noCallback) {

            data = data || '';
            //jQuery.noConflict();
            $('#confirmSave_Box').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmSave_Box').addClass("show");

            $("#warningIcon").removeClass("fa-info-circle").addClass("fa-exclamation-triangle");

            $('#confirmSave_Header').html(warningType);

            $('#confirmSave_Message').html(data);
            $("#confirmSave_Ok").html(yesButtonText);
            $("#confirmSave_No").html(noButtonText);
            $('#confirmSave_Ok').off("click").on("click", function () {
                $('#confirmSave_Box').modal('hide');
                $('#confirmSave_Box').removeClass("show");
                if (yesCallback) {
                    yesCallback();
                }
                return false;
            });
            $('#confirmSave_No').off("click").on("click", function () {
                $('#confirmSave_Box').modal('hide');
                $('#confirmSave_Box').removeClass("show");
                if (noCallback) {
                    noCallback();
                }
                return false;
            });

        },

        Page_Alert_Save_Info: function (data, warningType, yesButtonText, noButtonText, yesCallback, noCallback) {

            data = data || '';
            //jQuery.noConflict();
            $('#confirmSave_Box').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmSave_Box').addClass("show");

            $("#warningIcon").removeClass("fa-exclamation-triangle").addClass("fa-info-circle");

            $('#confirmSave_Header').html(warningType);

            $('#confirmSave_Message').html(data);
            $("#confirmSave_Ok").html(yesButtonText);
            $("#confirmSave_No").html(noButtonText);
            $('#confirmSave_Ok').off("click").on("click", function () {
                $('#confirmSave_Box').modal('hide');
                $('#confirmSave_Box').removeClass("show");
                if (yesCallback) {
                    yesCallback();
                }
                return false;
            });
            $('#confirmSave_No').off("click").on("click", function () {
                $('#confirmSave_Box').modal('hide');
                $('#confirmSave_Box').removeClass("show");
                if (noCallback) {
                    noCallback();
                }
                return false;
            });

        },

        Page_Alert_Save_Confirm: function (data, warningType, yesButtonText, noButtonText, continueButtonText, yesCallback, noCallback, continueCallback) {

            data = data || '';
            //jQuery.noConflict();
            $('#confirmSave').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });
            $('#confirmSave').addClass("show");
            $('#save_Header').html(warningType);

            $('#save_Message').html(data);
            $("#save_Ok").html(yesButtonText);
            $("#save_No").html(noButtonText);
            $("#save_Continue").html(continueButtonText);
            $('#save_Ok').off("click").on("click", function () {
                $('#confirmSave').modal('hide');
                $('#confirmSave').removeClass("show");
                if (yesCallback) {
                    yesCallback();
                }
                return false;
            });
            $('#save_No').off("click").on("click", function () {
                $('#confirmSave').modal('hide');
                $('#confirmSave').removeClass("show");
                if (noCallback) {
                    noCallback();
                }
                return false;
            });
            $('#save_Continue').off("click").on("click", function () {
                $('#confirmSave').modal('hide');
                $('#confirmSave').removeClass("show");
                if (continueCallback) {
                    continueCallback();
                }
                return false;
            });

        },

        Page_Alert_Success: function (data, warningType, yesButtonText, noButtonText, yesCallback, noCallback) {

            data = data || '';
            //jQuery.noConflict();
            $('#confirmSave_Box1').modal({
                show: true,
                backdrop: false,
                keyboard: false
            });

            $('#confirmSave_Box1').addClass("show");


            $('#confirmSave_Header1').html(warningType);

            $('#confirmSave_Message').html(data);
            $("#confirmSave_Ok").html(yesButtonText);
            $("#confirmSave_No").html(noButtonText);
            $('#confirmSave_Ok').click(function () {
                $('#confirmSave_Box1').modal('hide');
                $('#confirmSave_Box1').removeClass("show");
                if (yesCallback) {
                    yesCallback();
                }
                return false;
            });
            $('#confirmSave_No').click(function () {
                $('#confirmSave_Box1').modal('hide');
                $('#confirmSave_Box1').removeClass("show");
                if (noCallback) {
                    noCallback();
                }
                return false;
            });

        },

        isEmpty: function (value) {
            return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
        },
        Loading: function loading() {
            if ($('#mainloader').css('display') == 'none') {
                $('#mainloader').css('display', 'block');
            }
        },
        UnLoading: function loading() {
            if ($('#mainloader').css('display') == 'block') {
                $('#mainloader').fadeOut(2000);

            }

        },
        MathRound: function (value, exp) {
            var retVal = 0;

            if (value == 0) {
                return retVal.toFixed(exp);
            }
            if (typeof value != 'number') {
                value = parseFloat(value)
            }
            if (!isNaN(value) && value != null && (typeof exp === 'number' && exp % 1 === 0)) {
                retVal = value.toFixed(exp);
            }
            if (retVal == 0) {
                return retVal.toFixed(exp);
            }

            return retVal;
        },
        AddCurrencySymbol: function (value, exp) {

            return this.Currency + this.MathRound(value, exp);
        }
    };
})();