﻿//using System;
//using System.Threading.Tasks;
//using Microsoft.AspNet.Identity;
//using Microsoft.AspNet.Identity.EntityFramework;
//using Microsoft.AspNet.Identity.Owin;
//using Microsoft.Owin;
//using CookBook.MvcApp.Models;

//namespace CookBook.MvcApp
//{
//    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.

//    public class ApplicationUserManager : UserManager<ApplicationUser>
//    {
//        public ApplicationUserManager(IUserStore<ApplicationUser> store)
//            : base(store)
//        {
//        }

//        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
//        {
//            ApplicationDbContext appContext = null;
//            ApplicationUserManager manager = null;
//            UserStore<ApplicationUser> store = null;
//            try
//            {
//                appContext = context.Get<ApplicationDbContext>();
//                store = new UserStore<ApplicationUser>(appContext);
//                manager = new ApplicationUserManager(store);

//                // Configure validation logic for usernames
//                manager.UserValidator = new UserValidator<ApplicationUser>(manager)
//                {
//                    AllowOnlyAlphanumericUserNames = false,
//                    RequireUniqueEmail = true
//                };
//                // Configure validation logic for passwords
//                manager.PasswordValidator = new PasswordValidator
//                {
//                    RequiredLength = 6,
//                    RequireNonLetterOrDigit = true,
//                    RequireDigit = true,
//                    RequireLowercase = true,
//                    RequireUppercase = true,
//                };
//                // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
//                // You can write your own provider and plug in here.
//                manager.RegisterTwoFactorProvider("PhoneCode", new PhoneNumberTokenProvider<ApplicationUser>
//                {
//                    MessageFormat = "Your security code is: {0}"
//                });
//                manager.RegisterTwoFactorProvider("EmailCode", new EmailTokenProvider<ApplicationUser>
//                {
//                    Subject = "Security Code",
//                    BodyFormat = "Your security code is: {0}"
//                });
//                manager.EmailService = new EmailService();
//                manager.SmsService = new SmsService();
//                var dataProtectionProvider = options.DataProtectionProvider;
//                if (dataProtectionProvider != null)
//                {
//                    manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
//                }
//            }
//            catch (Exception ex)
//            {
//                if(manager != null)
//                {
//                    manager.Dispose();
//                }
//                throw ex;
//            }
//            finally
//            {
//                if (appContext != null)
//                {
//                    appContext.Dispose();
//                }
//                if (store != null)
//                {
//                    store.Dispose();
//                }
//            }
//            return manager;
//        }
//    }

//    public class EmailService : IIdentityMessageService
//    {
//        public Task SendAsync(IdentityMessage message)
//        {
//            // Plug in your email service here to send an email.
//            return Task.FromResult(0);
//        }
//    }

//    public class SmsService : IIdentityMessageService
//    {
//        public Task SendAsync(IdentityMessage message)
//        {
//            // Plug in your sms service here to send a text message.
//            return Task.FromResult(0);
//        }
//    }
//}
