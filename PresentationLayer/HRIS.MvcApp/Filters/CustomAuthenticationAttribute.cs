﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace HRIS.MvcApp.Filters
{
    public class CustomAuthenticationAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            //For demo purpose only. In real life our custom principal might be retrieved via different source. i.e context/request etc.
            filterContext.Principal = new CustomPrincipal(filterContext.HttpContext.User.Identity, new[] { "Admin" }, "6382");
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            //var color = ((CustomPrincipal)filterContext.HttpContext.User).VehicleNo;
            var user = filterContext.HttpContext.User;

            if (!user.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
        }
    }
}