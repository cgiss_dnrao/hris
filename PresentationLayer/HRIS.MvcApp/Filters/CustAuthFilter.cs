﻿using HRIS.MvcApp.Core;
using HRIS.MvcApp.HRISService;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OpenIdConnect;
using ClosedXML.Excel;
using HRIS.Data.UserData;

namespace HRIS.MvcApp.Filters
{
    public class CustAuthFilter : AuthorizeAttribute
    {
        IHRISService ServiceClient = ServiceHelper.GetHRISService();
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.RequestContext.HttpContext.Session["UserLoginDetails"] == null || (filterContext.RequestContext.HttpContext.Session["UserLoginDetails"] != null && ((UserMasterResponseData)filterContext.RequestContext.HttpContext.Session["UserLoginDetails"]).UserId <= 0))
            {
                if (!filterContext.RequestContext.HttpContext.Request.IsAuthenticated)
                {
                    filterContext.RequestContext.HttpContext.GetOwinContext().Authentication.Challenge(
                        new AuthenticationProperties { RedirectUri = ConfigurationManager.AppSettings["redirectUri"].ToString() },
                        OpenIdConnectAuthenticationDefaults.AuthenticationType);
                }
                else
                {
                    //string UserName = User.Identity.Name;
                    var UserInstance = filterContext.RequestContext.HttpContext.User.Identity as System.Security.Claims.ClaimsIdentity;
                    string UserName = UserInstance.FindFirst("preferred_username").Value;

                    if (string.IsNullOrEmpty(UserName) && !string.IsNullOrEmpty(ConfigurationManager.AppSettings[Constants.TEST_USER]))
                    {
                        UserName = ConfigurationManager.AppSettings[Constants.TEST_USER];
                    }
                    var res = ServiceClient.GetUserData(UserName);
                    filterContext.RequestContext.HttpContext.Session["UserLoginDetails"] = res;
                    if (res == null && (res != null && res.UserId <= 0))
                        filterContext.Result = new RedirectResult("~/Home/NotAuthenticated");
                }
            }
        }
    }
}