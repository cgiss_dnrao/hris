﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using HRIS.Ioc;
using HRIS.Aspects.Behaviors;

namespace HRIS.MvcApp
{
    public class MvcIocManager : IocManager
    {
        public MvcIocManager()
            : base(DiscoveryStrategy.SearchBaseDirectory)
        {

        }

        protected override IUnityContainer RegisterContract(Type type, Type contractType, string contractName, Microsoft.Practices.Unity.LifetimeManager manager)
        {
            //return base.Container.RegisterType(contractType, type, contractName, manager)
            return Container.RegisterType(contractType, type, contractName, manager, new InjectionMember[] { new Interceptor<InterfaceInterceptor>(), new InterceptionBehavior<ExceptionBehavior>() });
        }
    }
}