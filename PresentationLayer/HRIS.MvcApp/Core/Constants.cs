﻿namespace HRIS.MvcApp.Core
{
    public class Constants
    {
        public const string HRISSERVICE_ENDPOINTKEY = "HRISServiceEndPoint";
        public const string ApplicationUrl = "ApplicationUrl";
        public const string HRISSERVICE_ENDPOINT = "Web";
        public const string TEST_USER = "TestUser";
        public const string ProductionPlanFolder = "ProductionPlanFolder";
        public const string ProductionPlanExcelFile = "ProductionPlanExcelFile";
        public const string ProductionPlanSheetName = "ProductionPlanSheetName";
    }
}