﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using ClosedXML.Excel;

namespace HRIS.MvcApp.Core.ExportExcel
{
    public class OpenXLSExporter
    {
        /// <summary>
        /// Export the given dataset to an Excel Document 2007/2010/2013 format
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="fileName"></param>
        /// <param name="sheetNames"></param>
        /// <param name="templateCount"></param>
        /// <returns></returns>
        public static byte[] Export2Excel(DataSet ds, string fileName, List<string> sheetNames, int templateCount = 0)//, bool isLanguageChinese = false
        {
            string file = fileName.Split('.')[0];
            if (ds != null && ds.Tables.Count == sheetNames.Count)
            {
                //creating a new Workbook
                using (XLWorkbook wb = new XLWorkbook())
                {
                    //Prepare separate worksheets for every datatable in dataset
                    for (int i = 0; i < ds.Tables.Count; ++i)
                    {
                        if (ds.Tables[i].Rows.Count > 0)
                        {
                            PrepareWorksheet(ds.Tables[i], wb.Worksheets.Add(sheetNames[i]), file + i);//, isLanguageChinese
                        }
                    }
                    if (templateCount == 1)
                    {
                        //Code to save the file
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);

                        // Flush the workbook to the Response.OutputStream
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            wb.SaveAs(memoryStream);
                            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);

                            HttpContext.Current.Response.Flush();
                            HttpContext.Current.Response.End();
                            return new byte[0];
                        }
                    }
                    else
                    {
                        // Flush the workbook to the Response.OutputStream
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            wb.SaveAs(memoryStream);
                            return memoryStream.ToArray();
                        }
                    }
                }
            }
            else
            {
                return new byte[0];
            }

        }

        /// <summary>
        /// Export the given dataset to an Excel Document 2007/2010/2013 format
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="fileName"></param>
        /// <param name="sheetNames"></param>
        /// <param name="templateCount"></param>
        /// <returns></returns>
        public static byte[] Export2Excel_Rebate(DataSet ds, string fileName, List<string> sheetNames, int templateCount = 0)
        {
            int startCharCols = 1;
            int startIndexCols = 1;
            if (ds != null && ds.Tables.Count == 3)
            {
                //creating a new Workbook
                using (XLWorkbook wb = new XLWorkbook())
                {
                    //Creating worksheets
                    wb.Worksheets.Add(sheetNames[0]);
                    wb.Worksheets.Add(sheetNames[1]);
                    List<string> lstCols = new List<string>();
                    List<string> lstCols1 = new List<string>();
                    List<string> lstCols2 = new List<string>();
                    for (int i = 0; i < ds.Tables[0].Columns.Count; i++)
                    {
                        lstCols.Add(ds.Tables[0].Columns[i].ToString().Replace('_', ' '));
                    }
                    for (int i = 0; i < ds.Tables[1].Columns.Count; i++)
                    {
                        lstCols1.Add(ds.Tables[1].Columns[i].ToString().Replace('_', ' '));
                    }
                    for (int i = 0; i < ds.Tables[2].Columns.Count; i++)
                    {
                        lstCols2.Add(ds.Tables[2].Columns[i].ToString().Replace('_', ' '));
                    }

                    for (int i = 0; i < lstCols.Count; ++i)
                    {
                        string cols = lstCols[i];
                        wb.Worksheet(1).Cell(startIndexCols, startCharCols).Value = cols;
                        wb.Worksheet(1).Cell(startIndexCols, startCharCols).WorksheetColumn().Width = cols.Length;
                        wb.Worksheet(1).Cell(startIndexCols, startCharCols).Style.Fill.BackgroundColor = XLColor.PeachOrange;
                        wb.Worksheet(1).Cell(startIndexCols, startCharCols).Style.Font.FontColor = XLColor.Black;
                        startCharCols++;
                    }

                    //Write first data table from 1 column in refer sheet
                    startCharCols = 1;
                    startIndexCols = 1;
                    for (int i = 0; i < lstCols1.Count; ++i)
                    {
                        string cols = lstCols1[i];
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Value = cols;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).WorksheetColumn().Width = cols.Length;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Style.Font.Bold = false;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Style.Fill.BackgroundColor = XLColor.PeachOrange;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Style.Font.FontColor = XLColor.Black;
                        startCharCols++;
                    }

                    //Write second data table from 6 column in refer sheet
                    startCharCols = 6;
                    startIndexCols = 1;
                    for (int i = 0; i < lstCols2.Count; ++i)
                    {
                        string cols = lstCols2[i];
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Value = cols;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).WorksheetColumn().Width = cols.Length;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Style.Font.Bold = false;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Style.Fill.BackgroundColor = XLColor.PeachOrange;
                        wb.Worksheet(2).Cell(startIndexCols, startCharCols).Style.Font.FontColor = XLColor.Black;
                        startCharCols++;
                    }
                    wb.Worksheet(1).Cell(2, 1).InsertData(ds.Tables[0].Rows);
                    wb.Worksheet(2).Cell(2, 1).InsertData(ds.Tables[1].Rows);
                    wb.Worksheet(2).Cell(2, 6).InsertData(ds.Tables[2].Rows);

                    //Merge columns
                    wb.Worksheet(2).Cell("F2").Value = "Z002";
                    wb.Worksheet(2).Cell("F2").Style.Alignment.WrapText = true;
                    wb.Worksheet(2).Range("F2:G8").Column(1).Merge();
                    wb.Worksheet(2).Cell("F2").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    wb.Worksheet(2).Cell("A2").Value = "Z001";
                    wb.Worksheet(2).Cell("A2").Style.Alignment.WrapText = true;
                    wb.Worksheet(2).Range("A2:B15").Column(1).Merge();
                    wb.Worksheet(2).Cell("A2").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    wb.Worksheet(2).Cell("A16").Value = "Z002";
                    wb.Worksheet(2).Cell("A16").Style.Alignment.WrapText = true;
                    wb.Worksheet(2).Range("A16:B22").Column(1).Merge();
                    wb.Worksheet(2).Cell("A16").Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;

                    //Formatting of sheet
                    wb.Worksheet(1).Columns().Width = 19;
                    wb.Worksheet(2).Cell("B1").Value = " ";
                    wb.Worksheet(2).Columns().Width = 19;
                    wb.Worksheet(2).Column(4).Width = 35;
                    wb.Worksheet(2).Column(5).Width = 5;
                    wb.Worksheet(2).Column(2).Width = 10;
                    if (templateCount == 1)
                    {
                        //Code to save the file
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);

                        // Flush the workbook to the Response.OutputStream
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            wb.SaveAs(memoryStream);
                            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        }
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                        return new byte[0];
                    }
                    else
                    {
                        // Flush the workbook to the Response.OutputStream
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            wb.SaveAs(memoryStream);
                            return memoryStream.ToArray();
                        }
                    }
                }
            }
            else
            {
                return new byte[0];
            }
        }

        /// <summary>
        /// Export the given dataset to an Excel Document 2007/2010/2013 format
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="fileName"></param>
        /// <param name="sheetNames"></param>
        /// <param name="templatePath"></param>
        /// <param name="templateCount"></param>
        /// <returns></returns>
        public static byte[] Export2Excel(DataSet ds, string fileName, List<string> sheetNames, string templatePath, int templateCount = 0)
        {
            if (ds != null && ds.Tables.Count == sheetNames.Count)
            {
                //creating a new Workbook
                using (XLWorkbook wb = new XLWorkbook(templatePath))
                {
                    wb.Worksheet(1).Cell(7, 1).InsertData(ds.Tables[0].Rows);
                    if (templateCount == 1)
                    {
                        //Code to save the file
                        HttpContext.Current.Response.Clear();
                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);

                        // Flush the workbook to the Response.OutputStream
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            wb.SaveAs(memoryStream);
                            memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                        }
                        HttpContext.Current.Response.Flush();
                        HttpContext.Current.Response.End();
                        return new byte[0];
                    }
                    else
                    {
                        // Flush the workbook to the Response.OutputStream
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            wb.SaveAs(memoryStream);
                            return memoryStream.ToArray();
                        }
                    }
                }
            }
            else
            {
                return new byte[0];
            }
        }

        /// <summary>
        /// Prepare work sheets for a report
        /// </summary>
        /// <param name="table"></param>
        /// <param name="ws"></param>
        /// <param name="file"></param>
        private static void PrepareWorksheet(DataTable table, IXLWorksheet ws, string file)
        {
            XLColor columnBackgroundColor, columnForeColor;
            XLColor errorColor = XLColor.Red;
            XLColor headerColor2 = XLColor.PeachOrange;
            XLColor headerColor1 = XLColor.PeachOrange;
            string errorText = "Fail";

            //MAKE LIST OF COLUMN NAMES
            List<string> lstCols = new List<string>();
            for (int i = 0; i < table.Columns.Count; i++)
            {
                lstCols.Add(table.Columns[i].ToString().Replace('_', ' '));
            }
            string column = lstCols.FirstOrDefault(col => col.Contains('#'));

            int noOfHeaderRows = 0;
            if (!string.IsNullOrWhiteSpace(column))
                noOfHeaderRows = column.CharCount('#');

            int startCharCols = 1;
            int startIndexCols = 1;

            #region CreatingFirstThreeColumnHeaders
            columnForeColor = XLColor.Black;
            columnBackgroundColor = headerColor1;
            #endregion

            //SETUP THE FOURTH ROW
            #region CreatingFourthRow
            columnBackgroundColor = columnBackgroundColor == headerColor1 ? headerColor2 : headerColor1;
            startCharCols = 1;
            for (int i = 0; i < lstCols.Count; ++i)
            {
                string cols = lstCols[i];
                ws.Cell(startIndexCols, startCharCols).Value = cols;
                ws.Cell(startIndexCols, startCharCols).WorksheetColumn().Width = cols.Length;
                ws.Cell(startIndexCols, startCharCols).Style.Font.Bold = false;
                ws.Cell(startIndexCols, startCharCols).Style.Fill.BackgroundColor = columnBackgroundColor;
                ws.Cell(startIndexCols, startCharCols).Style.Font.FontColor = columnForeColor;
                startCharCols++;
            }
            #endregion

            #region FillDataInExcel
            int startCharData = 1;
            int startIndexData = startIndexCols + 1;
            //FILL DATATABLE VALUES IN EXCEL SHEET
            for (int i = 0; i < table.Rows.Count; i++)
            {
                for (int j = 0; j < table.Columns.Count; j++)
                {
                    var tRows = table.Rows[i][j].ToString();
                    tRows = tRows.Replace("&nbsp;", " ");
                    tRows = tRows.Replace("&amp;", "&");

                    //Special data update in case of Admin Report
                    //Note: Any other report that contains a contribution column must not call this method
                    string colName = table.Columns[j].ColumnName;
                    //check if value is of integer type
                    int val = 0;
                    DateTime dt = DateTime.Now;
                    if (int.TryParse(tRows, out val))
                    {
                        if (!(colName.Contains("#") && val == 0))
                        {
                            ws.Cell(startIndexData, startCharData).Value = val;
                            ws.Cell(startIndexData, startCharData).Style.NumberFormat.NumberFormatId = 1;
                        }
                    }
                    //check if datetime type
                    else if (!string.IsNullOrWhiteSpace(tRows) && !tRows.Contains('.') && DateTime.TryParse(tRows, out dt))
                    {
                        ws.Cell(startIndexData, startCharData).Value = dt.ToShortDateString();
                    }
                    else
                    {
                        ws.Cell(startIndexData, startCharData).SetValue(tRows);
                        if (tRows == errorText)
                        {
                            ws.Cell(startIndexData, startCharData).Style.Fill.BackgroundColor = errorColor;
                            ws.Cell(startIndexData, startCharData).Style.Font.FontColor = XLColor.White;
                        }
                    }
                    ws.Cell(startIndexData, startCharData).WorksheetColumn().Width = ws.Cell(startIndexData, startCharData).WorksheetColumn().Width > ws.Cell(startIndexData, startCharData).Value.ToString().Length ?
                                                                ws.Cell(startIndexData, startCharData).WorksheetColumn().Width : (ws.Cell(startIndexData, startCharData).Value.ToString().Length + 1);
                    startCharData++;
                }
                startCharData = 1;
                startIndexData++;
            }
            #endregion

            int totalRows = table.Rows.Count + noOfHeaderRows + 1;
            int totalCols = table.Columns.Count;
            ws.Range(1, 1, totalRows, totalCols).Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            ws.Range(1, 1, totalRows, totalCols).Style.Border.RightBorder = XLBorderStyleValues.Thin;
            ws.Range(1, 1, totalRows, totalCols).Style.Border.TopBorder = XLBorderStyleValues.Thin;
            ws.Range(1, 1, totalRows, totalCols).Style.Border.BottomBorder = XLBorderStyleValues.Thin;
        }
    }
}