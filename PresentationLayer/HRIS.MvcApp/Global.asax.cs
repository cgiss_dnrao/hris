﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using HRIS.Aspects.Factory;
using HRIS.DryIoc;
using HRIS.Ioc.Mvc;
using HRIS.MvcApp.Controllers;

namespace HRIS.MvcApp
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            LogTraceFactory.InitializeLoggingService();
            ExceptionFactory.InitializeExceptionAopFramework();

            MvcBootstrapper.Initialize(new MvcIocManager().Container);
            //IDryIocManager locator = new DryIocManager(DiscoveryStrategy.SearchBaseDirectory, HostType.Mvc);
        }

        protected void Application_Error()
        {
            Exception ex = Server.GetLastError();
            LogTraceFactory.WriteLogWithCategory(ex.ToString(), Aspects.Constants.LogTraceCategoryNames.Important);
            Response.Redirect("~/Home/Error");
        }
        protected void Session_Start(object sender, EventArgs e)
        {
            Session.Timeout = 100000;
        }
    }
}
