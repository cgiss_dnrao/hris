﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.MvcApp.Models
{
    public class ConsultantGroupUploaderModel
    {
        public string EmployeeCode { get; set; }
        public string EmployeeName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string DateOfBirth { get; set; }
        public string Department { get; set; }
        public string HRISSectorCode { get; set; }
        public string Grade { get; set; }
        public string Designation { get; set; }
        public string Company { get; set; }
        public string ProfitCenter { get; set; }
        public string CostCenter { get; set; }
        public string OfficeEmailAddress { get; set; }
        public string Location { get; set; }
        public string State { get; set; }
        public string MobileNumber { get; set; }
        public string L1ManagerCode { get; set; }
        public string L1Manager { get; set; }
        public string L2ManagerCode { get; set; }
        public string L2Manager { get; set; }        
        public string ExeComEmpCode { get; set; }
        public string ExeComEmpName { get; set; }
        public string CFOEmpCode { get; set; }
        public string CFOEmpName { get; set; }
        public string GroupEmployee { get; set; }
        public string TEPortalUser { get; set; }
        
    }
}