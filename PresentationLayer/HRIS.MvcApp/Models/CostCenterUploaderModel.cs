﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.MvcApp.Models
{
    public class CostCenterUploaderModel
    {
        public string EmployeeCode { get; set; }
        public string CostCenterCode { get; set; }
        public string ProfitCenterCode { get; set; }
        public string CostCenterRemarks { get; set; }
        public string ProfitCenterRemarks { get; set; }
    }
}