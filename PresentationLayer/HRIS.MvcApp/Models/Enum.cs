﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HRIS.MvcApp.Models
{
    enum StatusEnum
    {
        Pending = 1,
        Saved = 2,
        Mapped = 3
    }
}